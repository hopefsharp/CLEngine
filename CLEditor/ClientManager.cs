﻿#if Server
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Threading;
using Lidgren.Network;

namespace CLEngine.Editor
{
    public static class ClientManager
    {
        public static NetClient Client;
        public static NetIncomingMessage msg;

        public static NetConnection Connection;

        static ClientManager()
        {
            var config = new NetPeerConfiguration(ServerConfig.CommonServer);

            Client = new NetClient(config);
            Client.Start();

            var backgroundThread = new BackgroundWorker();
            backgroundThread.DoWork += (sender, args) =>
            {
                while (true)
                {
                    while ((msg = Client.ReadMessage()) != null)
                    {
                        if (msg.MessageType == NetIncomingMessageType.StatusChanged)
                            if (msg.SenderConnection.Status == NetConnectionStatus.Disconnected)
                            {
                                Connection = null;

                                ShowMessage("连接失败");
                            }

                        Client.Recycle(msg);
                    }
                }
            };

            backgroundThread.RunWorkerAsync();
        }

        private static void ShowMessage(string message)
        {
            Dispatcher.CurrentDispatcher.Invoke(() =>
            {
                MessageBox.Show(message);
            });
        }

        public static NetConnection Connect(Server_LR serverLR)
        {
            var message = Client.CreateMessage(serverLR.ToStringFormatter());
            return Client.Connect(ServerConfig.CommonServerHost, ServerConfig.CommonServerPort, message);
        }
    }
}
#endif