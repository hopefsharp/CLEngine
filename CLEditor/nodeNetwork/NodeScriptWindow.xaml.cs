﻿using System;
using System.Reactive.Linq;
using System.Windows;
using DynamicData;
using NodeNetwork.Toolkit.NodeList;
using NodeNetwork.Toolkit.ValueNode;
using NodeNetwork.ViewModels;
using ReactiveUI;

namespace CLEngine.Editor.nodeNetwork
{
    /// <summary>
    /// NodeScriptWindow.xaml 的交互逻辑
    /// </summary>
    public partial class NodeScriptWindow : Window
    {
        public NodeListViewModel ListViewModel { get; set; }

        public NodeScriptWindow()
        {
            InitializeComponent();

            initNodeView();
        }

        private void initNodeView()
        {
            var network = new NetworkViewModel();

            var node1 = new NodeViewModel();
            node1.Name = "节点 1";
            network.Nodes.Add(node1);
            ListViewModel.AddNodeType(() => node1);

            var node1Input = new ValueNodeInputViewModel<string>();
            node1Input.Name = "节点 1 输入";
            node1.Inputs.Add(node1Input);

            node1Input.ValueChanged.Subscribe(newValue =>
            {
                Console.WriteLine(newValue);
            });

            var node2 = new NodeViewModel();
            node2.Name = "节点 2";
            network.Nodes.Add(node2);
            ListViewModel.AddNodeType(() => node2);

            var node2Output = new ValueNodeOutputViewModel<string>();
            node2Output.Value = Observable.Return("测试字符串");
            node2Output.Name = "节点 2 输出";
            node2.Outputs.Add(node2Output);

            networkView.ViewModel = network;
        }
    }
}
