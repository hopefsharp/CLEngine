﻿namespace CLEngine.Editor.core
{
    public class OutputMessage
    {
        public string Time { get; set; }
        public string Message { get; set; }
    }
}