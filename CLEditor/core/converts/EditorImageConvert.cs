﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using CLEngine.Core;

namespace CLEngine.Editor.core.converts
{
	/// <summary>
	/// 编辑器内路径转图片
	/// </summary>
	public class EditorImageConvert : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is string path)
			{
				// 先转绝对路径
				var relativePath = Path.Combine(SceneManager.GameProject.ProjectPath, path);

                return InitImage(relativePath);
            }

			return null;
		}

        /// <summary>
        /// 解决不同进程读取同一张图片的问题
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private BitmapImage InitImage(string filePath)
        {
            BitmapImage bitmapImage;
            using (var reader = new BinaryReader(File.Open(filePath, FileMode.Open)))
            {
                var fi = new FileInfo(filePath);
                byte[] bytes = reader.ReadBytes((int)fi.Length);
                reader.Close();

                bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = new MemoryStream(bytes);
                bitmapImage.EndInit();
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                reader.Dispose();
            }
            return bitmapImage;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return null;
		}
	}
}