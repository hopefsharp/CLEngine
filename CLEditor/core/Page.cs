﻿namespace CLEngine.Editor.core
{
    public class Page
    {
        #region Properties

        public string Subtitle { get; private set; }
        public string Description { get; private set; }
        public string PicturePath { get; private set; }

        #endregion

        #region Constructors

        public Page()
        {
            Subtitle = string.Empty;
            Description = string.Empty;
        }
        public Page(string subtitle, string description, string picturePath)
        {
            Subtitle = subtitle;
            Description = description;
            PicturePath = picturePath;
        }

        #endregion

        #region Methods

        #endregion
    }
}