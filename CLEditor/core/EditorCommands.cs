﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Windows.Forms;
using CLEngine.Editor.model;
using CLEngine.Editor.windows;
using CLEngine.Core;
using CLEngine.Core.framework;
using EnvDTE;
using Clipboard = System.Windows.Clipboard;
using MessageBox = System.Windows.Forms.MessageBox;
using Process = System.Diagnostics.Process;
using Thread = System.Threading.Thread;

namespace CLEngine.Editor.core
{
    public class EditorCommands
    {
        internal static void DeleteDirectoryRecursively(string path)
        {
            try
            {
                Directory.Delete(path, true);
            }
            catch (Exception)
            {
                Thread.Sleep(1);
                DeleteDirectoryRecursively(path);
            }
        }

        static readonly object lockPaste = new object();
        internal static void CopySelectedObjects()
        {
            lock (lockPaste)
            {
                Clipboard.Clear();

                foreach (var obj in EditorHandler.SelectedGameObjects)
                    obj.SaveComponentValues();

                if (EditorHandler.SelectedGameObjects != null && EditorHandler.SelectedGameObjects.Count > 0)
                    Clipboard.SetData("GameObjects", new List<GameObject>(EditorHandler.SelectedGameObjects));
            }
        }

        internal static void UpdatePropertyGrid()
        {
	        if (EditorHandler.SelectedObjectPG != null)
	        {
		        EditorHandler.SelectedObjectPG.Dispatcher.BeginInvoke((Action)(() =>
		        {
			        EditorHandler.SelectedObjectPG.Update();
		        }));
	        }
        }


		internal static void PasteSelectedObjects()
        {
            lock (lockPaste)
            {
                List<GameObject> list = (List<GameObject>)Clipboard.GetData("GameObjects");
                //Clipboard.Clear();

                if (list == null || list.Count == 0) return;

                EditorHandler.SelectedGameObjects.Clear();
                
                foreach (var obj in list)
                {
                    var parent = obj.Transform.Parent;
                    while (parent != null && !list.Contains(parent.GameObject))
                        parent = parent.Parent;

                    if (parent != null && list.Contains(parent.GameObject))
                    {
                        parent.GameObject.Children.Add(obj);
                    }
                    else
                    {
	                    EditorHandler.SceneTreeView.AddGameObject(obj, string.Empty, false, true);
                    }

                    EditorHandler.SelectedGameObjects.Add(obj);
                }

                EditorHandler.ChangeSelectedObjects();
            }
        }

        internal static void CreatePropertyGridView()
        {
            EditorHandler.PropertyGridContainer.Dispatcher.BeginInvoke(new Action(() =>
            {
                EditorHandler.PropertyGridContainer.Children.Clear();

                // TODO: handle multiple selection
                if (EditorHandler.SelectedGameObjects.Count == 1)
                {
	                var properties = new PropertyBox {SelectedObject = EditorHandler.SelectedGameObjects[0]};

	                EditorHandler.SelectedObjectPG = properties.PropertyGrid;
                    properties.ToggleExpand();

                    EditorHandler.PropertyGridContainer.Children.Add(properties);

                    if (EditorHandler.SelectedGameObjects[0] != null)
                    {
                        foreach (var component in EditorHandler.SelectedGameObjects[0].GetComponents())
                        {
	                        properties = new PropertyBox {SelectedObject = component};
	                        properties.Title.Content += " (Component)";

                            if (component.EditorExpanded)
                                properties.ToggleExpand();

                            EditorHandler.PropertyGridContainer.Children.Add(properties);
                        }
                    }
                }
            }));
        }

        internal static void CheckPropertyGridConsistency()
        {
            for (int i = EditorHandler.PropertyGridContainer.Children.Count - 1; i >= 0; i--)
            {
                if ((EditorHandler.PropertyGridContainer.Children[i] as PropertyBox)?.SelectedObject is ObjectComponent)
                {
	                if (((PropertyBox) EditorHandler.PropertyGridContainer.Children[i]).SelectedObject is ObjectComponent component && component.Transform.GameObject.GetComponents().Find(o => o == component) == null)
                        EditorHandler.PropertyGridContainer.Children.RemoveAt(i);
                }
            }
        }

        /// <summary>
        /// Loads a saved project
        /// </summary>
        [SuppressMessage("ReSharper", "LocalizableElement")]
        [SuppressMessage("ReSharper", "StringLiteralTypo")]
        internal static bool LoadProject()
        {
	        OpenFileDialog ofd = new OpenFileDialog {Title = "打开工程", Filter = @"(*.clengine)|*.clengine"};

	        if (ofd.ShowDialog() == DialogResult.OK)
            {
                LoadProject(ofd.FileName);
                AddToProjectHistory(ofd.FileName);
            }

            ShowOutputMessage("工程加载成功");

            return false;
        }

        [SuppressMessage("ReSharper", "LocalizableElement")]
        [SuppressMessage("ReSharper", "StringLiteralTypo")]
        internal static bool LoadProject(string filename)
        {
            try
            {
                if (File.Exists(filename))
                {
                    SceneManager.GameProject = CProject.Load(filename);

                    File.Copy("CLEngine.Core.dll", SceneManager.GameProject.ProjectPath + "\\CLEngine.Core.dll", true);
                    File.Copy("Project Templates\\CLEngine.Core.xml", SceneManager.GameProject.ProjectPath + "\\CLEngine.Core.xml", true);
                    File.Copy("Project Templates\\CLEngine.Windows.exe", SceneManager.GameProject.ProjectPath + "\\CLEngine.Windows.exe", true);
                    CHelper.CopyDirectory("Project Templates\\libs", SceneManager.GameProject.ProjectPath + "", true);
                    File.Copy("Xceed.Wpf.Toolkit.dll", SceneManager.GameProject.ProjectPath + "\\Xceed.Wpf.Toolkit.dll", true);

                    if (!File.Exists(SceneManager.GameProject.ProjectPath + "\\_userPrefs.pgb"))
                    {
                        UserPreferences.Instance = new UserPreferences();
                        CHelper.SerializeObject(SceneManager.GameProject.ProjectPath + "\\_userPrefs.pgb", UserPreferences.Instance);
                    }
                    else
                    {
                        UserPreferences.Instance = CHelper.DeserializeObject(SceneManager.GameProject.ProjectPath + "\\_userPrefs.pgb") as UserPreferences;
                    }

                    SceneManager.ActiveScene = null;
                    EditorHandler.SelectedGameObjects.Clear();
                    EditorHandler.ChangeSelectedObjects();
                    EditorHandler.SceneTreeView.CreateView();
                    EditorHandler.ProjectTreeView.CreateView();

					// 加载游戏框架数据库
					FrameworkManager.LoadAllData();

					CompilerWindow cf = new CompilerWindow();
                    cf.ShowDialog();
                    bool success = cf.Result;

                    Reload();

                    if (success)
                    {
                        LoadLastScene();

                        ShowOutputMessage("工程加载成功");
                    }
                    else
                    {
                        ShowOutputMessage("项目加载脚本错误");
                    }

                    EditorHandler.Settings = new IniFile(SceneManager.GameProject.ProjectPath + "\\settings.ini");

                    return true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("无效的文件\n\n" + ex.Message, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        internal static void Reload()
        {
            SceneManager.ActiveScene = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        internal static bool LoadLastScene()
        {
            if (SceneManager.GameProject == null) return false;

            string path = SceneManager.GameProject.ProjectPath + "\\" + SceneManager.GameProject.EditorSettings.LastOpenScenePath;

            if (!path.Trim().Equals(string.Empty) && File.Exists(path))
            {
                SceneManager.LoadScene(path, true);
                EditorHandler.SceneTreeView.CreateView();

                return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        internal static void ShowOutputMessage(string message)
        {
            EditorHandler.OutputMessages.Add(new OutputMessage() { Time = DateTime.Now.ToString("HH:mm:ss"), Message = message });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        internal static void AddToProjectHistory(string path)
        {
            if (Properties.Settings.Default.LastLoadedProjects.Contains(path))
            {
                Properties.Settings.Default.LastLoadedProjects = Properties.Settings.Default.LastLoadedProjects.Replace(path + "|", string.Empty);
            }

            Properties.Settings.Default.LastLoadedProjects = path + "|" + Properties.Settings.Default.LastLoadedProjects;
            Properties.Settings.Default.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        [SuppressMessage("ReSharper", "LocalizableElement")]
        internal static void SaveScene(bool saveAs)
        {
            if (SceneManager.ActiveScene == null) return;

            if (!File.Exists(SceneManager.ActiveScenePath))
            {
	            SaveFileDialog sfd = new SaveFileDialog
	            {
		            InitialDirectory = SceneManager.GameProject.ProjectPath, Filter = "(*.scene)|*.scene"
	            };
	            DialogResult dr = sfd.ShowDialog();

                if (dr == DialogResult.Yes || dr == DialogResult.OK)
                {
                    SceneManager.ActiveScenePath = sfd.FileName;
                }
                else
                {
                    return;
                }
            }

            if (saveAs)
            {
                // TODO: implement "Save As" funcionality

            }
            else
            {
                //SceneManager.ActiveScene.SaveComponentValues();
                SceneManager.SaveActiveScene();
            }
        }

        /// <summary>
        /// 使用不同的线程保存当前项目
        /// </summary>
        [SuppressMessage("ReSharper", "StringLiteralTypo")]
        internal static void SaveProject()
        {
            if (SceneManager.GameProject != null)
            {
                // 将项目保存在其他线程中
                Thread saveThread = new Thread(() =>
                {
                    SceneManager.GameProject.Save();
                    if (UserPreferences.Instance != null)
                        CHelper.SerializeObject(SceneManager.GameProject.ProjectPath + "\\_userPrefs.pgb", UserPreferences.Instance);
                });

                ShowOutputMessage("Project Saved");
                saveThread.Start();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [SuppressMessage("ReSharper", "LocalizableElement")]
        internal static void DebugGame()
        {
            if (SceneManager.ActiveScene == null)
            {
                MessageBox.Show("Ups!\n\n没有加载场景", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            SceneManager.GameProject.SceneStartPath = CHelper.MakeExclusiveRelativePath(SceneManager.GameProject.ProjectPath, SceneManager.ActiveScenePath);

            SaveProject();
            SaveScene(false);

            if (File.Exists(SceneManager.GameProject.ProjectPath + "\\CLEngine.Windows.exe"))
            {
                if (File.Exists(SceneManager.GameProject.ProjectPath + "\\libs\\Scripts.dll"))
                    File.Delete(SceneManager.GameProject.ProjectPath + "\\libs\\Scripts.dll");

                if (!Directory.Exists(SceneManager.GameProject.ProjectPath + "\\libs"))
                    Directory.CreateDirectory(SceneManager.GameProject.ProjectPath + "\\libs");

                // Compile scripts:
                // old (depracated): CompileScripts(false);
                //if (SceneManager.ScriptsAssembly != null)
                //{
                string dllpath = SceneManager.GameProject.ProjectPath + "\\bin\\" + (SceneManager.GameProject.Debug ? "Debug" : "Release") + "\\" + SceneManager.ScriptsAssembly.GetName().Name + ".dll";

                // The scripts .dll exists?
                if (!File.Exists(dllpath))
                {
                    // Compile scripts
                    CompilerWindow cf = new CompilerWindow();
                    cf.ShowDialog();

                    if (cf.DialogResult != null && cf.DialogResult.Value) return;

                    // Update path
                    dllpath = SceneManager.GameProject.ProjectPath + "\\bin\\" + (SceneManager.GameProject.Debug ? "Debug" : "Release") + "\\" + SceneManager.ScriptsAssembly.GetName().Name + ".dll";
                }

                File.Copy(dllpath, SceneManager.GameProject.ProjectPath + "\\libs\\Scripts.dll", true);

                try
                {
	                Process debug = new Process
	                {
		                StartInfo =
		                {
			                WorkingDirectory = SceneManager.GameProject.ProjectPath,
			                FileName = SceneManager.GameProject.ProjectPath + "\\CLEngine.Windows.exe",
			                Arguments = "",
			                CreateNoWindow = true
		                }
	                };
	                debug.Start();

                    if (Properties.Settings.Default.AttachVisualStudio)
                    {
	                    // Try to restore the current project visual studio solution instance, returning it if successful
                        if (TryToRestoreSolution(out var vsInstance))
                        {
                            // Tries to attach the retrieved instance to the game debug process
                            debug.Attach(vsInstance);
                        }

                        /*
                        if (VisualStudioInstancePID != 0 && Extensions.TryToRetrieveVSInstance(VisualStudioInstancePID, out vsInstance))
                        {
                            // restore window, in case the process is only running on background
                            vsInstance.MainWindow.Visible = true;

                            debug.Attach(vsInstance);
                        }
                        else // if PID attempt failed, try using the project solution name
                        {
                            // reset PID
                            VisualStudioInstancePID = 0;

                            // try to retrieve instance based on solution name
                            vsInstance = Extensions.GetInstance(UserPreferences.Instance.ProjectSlnFilePath);
                            if (vsInstance != null)
                            {
                                // restore window, in case the process is only running on background
                                vsInstance.MainWindow.Visible = true;

                                debug.Attach(vsInstance); // Attach visual studio dte
                            }
                        }*/


                    }

                    debug.WaitForExit();
                    debug.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERROR: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Ups!\n\nIt seems there is no engine set up for your game!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Restores the a solution by making it visible
        /// </summary>
        /// <param name="instance">the visual studio solution instance</param>
        /// <returns>Whether the restore happened or not</returns>
        internal static bool RestoreSolution(DTE instance)
        {
            // if the instance is valid
            if (instance != null)
            {
                // restore window, in case the process is only running on background
                instance.MainWindow.Visible = true;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Tries to restore a solution, using its PID and then its file path.
        /// </summary>
        /// <param name="instance">The visual studio solution instance</param>
        /// <returns>Whether the restore was successful or not</returns>
        internal static bool TryToRestoreSolution(out DTE instance)
        {
            // the visual studio instance 
            DTE vsInstance;

            // if visual studio instance process Id is valid
            if (VisualStudioInstancePID != 0)
            {
                // if an instance has been found with the provided PID
                if (Extensions.TryToRetrieveVSInstance(VisualStudioInstancePID, out vsInstance))
                {
                    // try to restore the solution
                    if (RestoreSolution(vsInstance))
                    {
                        // assign out instance
                        instance = vsInstance;
                        // all done
                        return true;
                    }
                }
            }

            // if PID attempt failed, try to use solution file path. But first, reset the PID
            VisualStudioInstancePID = 0;
            // try to retrieve instance based on solution name
            vsInstance = Extensions.GetInstance(UserPreferences.Instance.ProjectSlnFilePath);

            // try to restore the solution
            if (RestoreSolution(vsInstance))
            {
                // assign out instance
                instance = vsInstance;
                // all done
                return true;
            }

            // in case both PID and solution file path attempts failed, set out instance to null
            instance = null;
            return false;
        }

        // Visual Studio Solution Instance Process Id
        internal static int VisualStudioInstancePID { get; set; }

        /// <summary>
        /// Apply Blur Effect on the window
        /// </summary>
        /// <param name="win"></param>
        internal static void ApplyBlurEffect(System.Windows.Window win)
        {
	        System.Windows.Media.Effects.BlurEffect objBlur = new System.Windows.Media.Effects.BlurEffect {Radius = 4};
	        win.Effect = objBlur;
        }

        /// <summary>
        /// Remove Blur Effects
        /// </summary>
        /// <param name="win"></param>
        internal static void ClearEffect(System.Windows.Window win)
        {
            win.Effect = null;
        }
    }

    public static class Extensions
    {
        internal static void Attach(this Process process, DTE dte)
        {
            int tryCount = 5;
            while (tryCount-- > 0)
            {
                try
                {
                    var processes = dte.Debugger.LocalProcesses;

                    foreach (EnvDTE.Process proc in processes.Cast<EnvDTE.Process>().Where(
                        proc => proc.Name.IndexOf(process.ProcessName, StringComparison.Ordinal) != -1))
                    {
                        proc.Attach();
                        Debug.WriteLine($"Attached to process {process.ProcessName} successfully.");
                        break;
                    }
                    break;
                }
                catch (System.Runtime.InteropServices.COMException)
                {
                    Thread.Sleep(1000);
                }
            }
        }

        internal static DTE GetInstance(string displayName)
        {
            //List<string> names = new List<string>();
            //names.AddRange(from i in GetVisualStudioInstances() select i.Solution.FullName);
            IEnumerable<DTE> instances = GetVisualStudioInstances();

            var enumerable = instances as DTE[] ?? instances.ToArray();
            bool exists = enumerable.Any(x => x.Solution.FullName.Equals(displayName));
            if (exists)
                return enumerable.First(x => x.Solution.FullName.Equals(displayName));

            return null;
        }

        /// <summary>
        /// Retrieve every visual studio instance
        /// </summary>
        /// <returns>List of visual studio instances</returns>
        internal static IEnumerable<DTE> GetVisualStudioInstances()
        {
	        int retVal = GetRunningObjectTable(0, out var rot);

            if (retVal == 0)
            {
	            rot.EnumRunning(out var enumMoniker);

                IntPtr fetched = IntPtr.Zero;
                var moniker = new IMoniker[1];
                while (enumMoniker.Next(1, moniker, fetched) == 0)
                {
	                CreateBindCtx(0, out var bindCtx);
	                moniker[0].GetDisplayName(bindCtx, null, out var displayName);
                    //Console.Info("Display Name: {0}", displayName);
                    bool isVisualStudio = displayName.StartsWith("!VisualStudio");
                    if (isVisualStudio)
                    {
	                    rot.GetObject(moniker[0], out var obj);
                        var dte = obj as DTE;
                        yield return dte;
                    }
                }
            }
        }

        /// <summary>
        /// 尝试根据其PID检索特定的Visual Studio实例
        /// </summary>
        /// <param name="processId">Visual Studio instance PID</param>
        /// <param name="instance">Visual Studio instance if able to found. Null otherwise.</param>
        /// <returns>Whether the visual studio instance was found or not.</returns>
        internal static bool TryToRetrieveVSInstance(int processId, out DTE instance)
        {
            IntPtr numFetched = IntPtr.Zero;
            var monikers = new IMoniker[1];

            GetRunningObjectTable(0, out var runningObjectTable);
            runningObjectTable.EnumRunning(out var monikerEnumerator);
            monikerEnumerator.Reset();

            while (monikerEnumerator.Next(1, monikers, numFetched) == 0)
            {
	            CreateBindCtx(0, out var ctx);

	            monikers[0].GetDisplayName(ctx, null, out var runningObjectName);

	            runningObjectTable.GetObject(monikers[0], out var runningObjectVal);

                if (runningObjectVal is DTE dte && runningObjectName.StartsWith("!VisualStudio"))
                {
                    // retrieve process id - "process_name:pid"
                    int currentProcessId = int.Parse(runningObjectName.Split(':')[1]);

                    // 如果它是匹配的
                    if (currentProcessId == processId)
                    {
                        instance = dte;
                        return true;
                    }
                }
            }

            instance = null;
            return false;
        }

        [System.Runtime.InteropServices.DllImport("ole32.dll")]
        private static extern void CreateBindCtx(int reserved, out IBindCtx ppbc);

        [System.Runtime.InteropServices.DllImport("ole32.dll")]
        private static extern int GetRunningObjectTable(int reserved, out IRunningObjectTable prot);
    }
}