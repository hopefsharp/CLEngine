﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using CLEngine.Editor.controls;
using Microsoft.Win32;

namespace CLEngine.Editor.core
{
    public static class EditorUtils
    {
	    public static BitmapImage ConvertBitmapToImage96DPI(BitmapImage bitmapImage)
        {
            double dpi = 96;
            int width = bitmapImage.PixelWidth;
            int height = bitmapImage.PixelHeight;

            int stride = width * 4; // 4 bytes per pixel
            byte[] pixelData = new byte[stride * height];
            bitmapImage.CopyPixels(pixelData, stride, 0);

            BitmapSource bs = BitmapSource.Create(width, height, dpi, dpi, PixelFormats.Bgra32, null, pixelData, stride);

            PngBitmapEncoder encoder = new PngBitmapEncoder();
            MemoryStream memoryStream = new MemoryStream();
            BitmapImage bImg = new BitmapImage();

            encoder.Frames.Add(BitmapFrame.Create(bs));
            encoder.Save(memoryStream);

            bImg.BeginInit();
            bImg.StreamSource = new MemoryStream(memoryStream.ToArray());
            bImg.EndInit();

            memoryStream.Close();

            return bImg;
        }

	    internal static StackPanel CreateHeader(string text, ImageSource imageSource)
	    {
		    StackPanel stackPanel = new StackPanel();
		    stackPanel.Orientation = Orientation.Horizontal;
		    if (imageSource != null)
			    stackPanel.Children.Add(new Image() { Source = imageSource, Margin = new Thickness(0, 0, 4, 0) });
		    stackPanel.Children.Add(new TextBlock() { Text = text });

		    return stackPanel;
	    }

		public static BitmapSource ConvertBitmapToSource96DPI(BitmapImage bitmapImage)
	    {
		    double dpi = 96;
		    int width = bitmapImage.PixelWidth;
		    int height = bitmapImage.PixelHeight;

		    int stride = width * 4; // 4 bytes per pixel
		    byte[] pixelData = new byte[stride * height];
		    bitmapImage.CopyPixels(pixelData, stride, 0);

		    return BitmapSource.Create(width, height, dpi, dpi, PixelFormats.Bgra32, null, pixelData, stride);
	    }

		internal static CMenuItem CreateMenuItem(string text, ImageSource imageSource = null)
        {
	        CMenuItem menuItem = new CMenuItem {Header = text};


	        if (imageSource != null)
                menuItem.Icon = new Image() { Source = imageSource, HorizontalAlignment = HorizontalAlignment.Center };

            return menuItem;
        }

        internal static DependencyObject GetParent(DependencyObject obj, int levels = 1)
        {
	        DependencyObject result = obj;

	        for (int i = 0; i < levels; i++)
            {
	            DependencyObject t;
	            if ((t = VisualTreeHelper.GetParent(result)) == null)
                    break;

                result = t;
            }

            return result;
        }

        internal static TObject FindVisualParent<TObject>(UIElement child) where TObject : UIElement
        {
            if (child == null)
            {
                return null;
            }

            UIElement parent = VisualTreeHelper.GetParent(child) as UIElement;

            while (parent != null)
            {
	            if (parent is TObject found)
                {
                    return found;
                }
                else
                {
                    parent = VisualTreeHelper.GetParent(parent) as UIElement;
                }
            }

            return null;
        }

        internal static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child is T dependencyObject)
                    {
                        yield return dependencyObject;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }


        internal static Dictionary<string, string> InstalledApps { get; } = new Dictionary<string, string>();

        internal static void StoreInstalledApplications()
        {
	        // store: CurrentUser
            var keyName = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
            StoreSubKey(Registry.CurrentUser, keyName);

            // store: LocalMachine_32
            keyName = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
            StoreSubKey(Registry.LocalMachine, keyName);

            // store: LocalMachine_64
            keyName = @"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall";
            StoreSubKey(Registry.LocalMachine, keyName);
        }

        [SuppressMessage("ReSharper", "ConvertToUsingDeclaration")]
        private static void StoreSubKey(RegistryKey root, string subKeyName)
        {
	        using (RegistryKey key = root.OpenSubKey(subKeyName))
            {
                if (key != null)
                {
                    foreach (string kn in key.GetSubKeyNames())
                    {
	                    RegistryKey subkey;
	                    using (subkey = key.OpenSubKey(kn))
                        {
                            var displayName = (subkey?.GetValue("DisplayName") as string);
                            var pathName = (subkey?.GetValue("InstallLocation") as string);

                            if (!string.IsNullOrEmpty(displayName) && !InstalledApps.ContainsKey(displayName))
                                InstalledApps.Add(displayName, pathName);

                            //if (displayName != null && displayName.Replace(" ", "").ToLower().Contains(p_name.ToLower()))
                            //return true;
                        }
                    }
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="version">VisualStudio2015, VisualStudio2013, VisualStudio2012, VisualStudio2010</param>
        /// <returns></returns>
        internal static bool CheckVisualStudioExistance(string version)
        {
            string src;
            switch (version)
            {
                case "VisualStudio2015":
                    src = @"VisualStudio\14.0";
                    break;
                case "VisualStudio2013":
                    src = @"VisualStudio\12.0";
                    break;
                case "VisualStudio2012":
                    src = @"VisualStudio\11.0";
                    break;
                case "VisualStudio2010":
                    src = @"VisualStudio\10.0";
                    break;
                default:
                    return true;
                    //case "CSExpress2010":
                    //    src = @"VCSExpress\10.0";
                    //    break;
            }

            var Key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\" + src);
            using (Key)
                if (Key != null)
                {
                    return true;
                }

            return false;
        }

        internal static void RenderPicture(ref Image picture, string path, int maxWidth, int maxHeight)
        {
            if (!File.Exists(path))
            {
                EditorCommands.ShowOutputMessage("Error loading Images");
                return;
            }

            BitmapImage image = new BitmapImage();
            image.BeginInit(); // AppDomain.CurrentDomain.BaseDirectory + @"\Tutorials\FirstSteps\Pictures\"
            image.UriSource = new Uri(path, UriKind.Absolute);
            image.CacheOption = BitmapCacheOption.OnLoad;
            image.EndInit();

            if (image.Width < maxWidth && image.Height < maxHeight)
            {
                picture.Width = image.Width;
                picture.Height = image.Height;
            }
            else
            {
                double ratioX = maxWidth / image.Width;
                double ratioY = maxHeight / image.Height;
                // use whichever multiplier is smaller
                double ratio = ratioX < ratioY ? ratioX : ratioY;

                // now we can get the new height and width            
                int newWidth = Convert.ToInt32(image.Width * ratio);
                int newHeight = Convert.ToInt32(image.Height * ratio);

                picture.Width = newWidth;
                picture.Height = newHeight;
            }

            picture.Source = image;

        }


        internal static void SelectAnotherElement<T>(DependencyObject obj) where T : FrameworkElement
        {
            FrameworkElement parent = (FrameworkElement)(obj as T)?.Parent;
            while (parent != null && !((IInputElement)parent).Focusable)
            {
                parent = (FrameworkElement)parent.Parent;
            }

            DependencyObject scope = FocusManager.GetFocusScope((obj as T) ?? throw new InvalidOperationException());
            FocusManager.SetFocusedElement(scope, parent);
        }

        internal static bool isDirectory(string fullPath)
        {
            FileAttributes attr = File.GetAttributes(fullPath);

            //detect whether its a directory or file
            if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                return true;

            return false;
        }
    }
}