﻿using CLEngine.Editor.windows;

namespace CLEngine.Editor.core
{
    public class LimeScriptEditor
    {
        private static ScriptingEditorWindow _instance;

        public static ScriptingEditorWindow Instance
        {
            get
            {
                if (_instance == null || !_instance.IsVisible)
                {
                    _instance = new ScriptingEditorWindow();
                    _instance.Show();
                }

                return _instance;
            }
        }

        public static void OpenScript(string path)
        {
            Instance.OpenScript(path);
        }
    }
}