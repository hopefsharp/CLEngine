﻿using System;
using System.Collections.Generic;
using System.IO;
using Xceed.Wpf.AvalonDock;
using Xceed.Wpf.AvalonDock.Layout.Serialization;

namespace CLEngine.Editor.core
{
    public static class LayoutHelper
    {
        private const string layoutPath = @".\Layout\";
        private const string layoutExtension = ".layout";

        public static string LayoutExtension => layoutExtension;

        public static DockingManager DockManager { get; set; }

        public static void LoadCDefaultLayout()
        {
            EditorCommands.ShowOutputMessage("试图加载默认布局");
            if (LoadLayout("Default"))
                EditorCommands.ShowOutputMessage("默认布局已成功加载");
            else
                EditorCommands.ShowOutputMessage("默认布局已成功加载");
        }

        public static bool LoadLayout(string layoutName)
        {
            if (!LayoutExists(layoutName))
            {
                layoutName = "Default";
            }

            try
            {
                var serializer = new XmlLayoutSerializer(DockManager);
                using (var stream = new StreamReader(string.Format(layoutPath + "{0}" + layoutExtension, layoutName)))
                    serializer.Deserialize(stream);

                Properties.Settings.Default.Layout = layoutName;
            }
            catch (Exception)
            {
                return false;
            }
            // loaded successfully
            return true;
        }

        public static bool LayoutExists(string layoutName)
        {
            layoutName = layoutName.Trim();
            string path = layoutPath + layoutName + layoutExtension;
            if (File.Exists(path))
                return true;
            return false;
        }

        public static bool RenameLayout(string newLayoutName)
        {
            if (!newLayoutName.Equals("Default") && !Properties.Settings.Default.Layout.Equals("Default"))
            {
                // 尝试删除当前布局
                if (RemoveLayout(Properties.Settings.Default.Layout))
                    // 尝试使用新名称创建新布局
                    if (CreateNewLayout(newLayoutName))
                        return true; // renamed successfully
            }

            return false;
        }
        public static bool CreateNewLayout(string layoutName)
        {
            if (layoutName.Trim().Equals(string.Empty)) return false;

            EditorCommands.ShowOutputMessage("试图创建一个新的布局");

            if (layoutName.Equals("Default"))
            {
                EditorCommands.ShowOutputMessage("无法覆盖默认布局");
                return false;
            }

            if (RemoveLayout(layoutName))
                EditorCommands.ShowOutputMessage("试图替换布局");

            try
            {
                EditorCommands.ShowOutputMessage("试图保存布局");
                var serializer = new XmlLayoutSerializer(DockManager);
                using (var stream = new StreamWriter(layoutPath + layoutName + LayoutExtension))
                    serializer.Serialize(stream);

                EditorCommands.ShowOutputMessage("布局已成功保存");
            }
            catch (Exception)
            {
                EditorCommands.ShowOutputMessage("布局保存尝试失败");
                return false;
            }

            Properties.Settings.Default.Layout = layoutName;
            return true;
        }

        public static List<string> GetLayouts()
        {
            List<string> Layouts = new List<string>();

            foreach (var layout in Directory.GetFiles(layoutPath, "*.layout"))
            {
                string name = Path.GetFileNameWithoutExtension(layout);
                if (!name.Equals("Default"))
                    Layouts.Add(name);
            }

            return Layouts;
        }


        public static bool RemoveLayout(string layoutName)
        {
            EditorCommands.ShowOutputMessage("试图删除布局");
            if (layoutName.Equals("Default"))
            {
                EditorCommands.ShowOutputMessage("无法删除默认布局");
                return false;
            }

            if (LayoutExists(layoutName))
                try
                {
                    File.Delete(layoutPath + layoutName + layoutExtension);
                    if (Properties.Settings.Default.Layout == layoutName)
                    {
                        Properties.Settings.Default.Layout = string.Empty;
                    }
                }
                catch { EditorCommands.ShowOutputMessage("尝试删除布局失败"); return false; }

            return true;
        }
    }
}