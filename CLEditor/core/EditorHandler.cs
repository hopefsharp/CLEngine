﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using CLEngine.Editor.graphics_device;
using CLEngine.Editor.model;
using CLEngine.Editor.windows;
using CLEngine.Core;
using PropertyGrid = Xceed.Wpf.Toolkit.PropertyGrid.PropertyGrid;

namespace CLEngine.Editor.core
{
    public static class EditorHandler
    {
        internal static PropertyGrid SelectedObjectPG = null;

        public static ObservableCollection<OutputMessage> OutputMessages { get; } = new ObservableCollection<OutputMessage>();

        private static PicturePreview picturePreview;
        internal static PicturePreview PicturePreview
        {
            get
            {
                if (picturePreview == null)
                {
	                picturePreview = new PicturePreview {ShowInTaskbar = false};
	                //picturePreview.Topmost = true;
                }

                return picturePreview;
            }
        }

        //internal static OutputWindow OutputWindow = new OutputWindow();

        /// <summary>
        /// 将属性网格和属性标签更改为输入值
        /// </summary>
        /// <param name="value">The object</param>
        public static bool ChangeSelectedObject(object value)
        {
            foreach (UIElement uiElement in PropertyGridContainer.Children)
            {
                if (uiElement is PropertyBox box)
                {
                    if (box.SelectedObject == value)
                        return false;
                }
            }

            if (value != null)
            {
                PropertyGridContainer.Children.Clear();

                PropertyBox properties = new PropertyBox {SelectedObject = value};
                properties.ToggleExpand();


                PropertyGridContainer.Children.Add(properties);
            }

            return true;
        }

        /// <summary>
        /// 将属性网格和属性标签更改为所选对象
        /// </summary>
        public static void ChangeSelectedObjects()
        {
            TilesetMenuItems.IsEnabled = false;


            if (SelectedGameObjects != null)
            {
                if (SelectedGameObjects.Count == 1)
                {
                    if (!ChangeSelectedObject(SelectedGameObjects[0]))
                        return;

                    if (SelectedGameObjects[0] is Tileset tileset)
                    {
                        TilesetBrushControl.ChangeSelectionSize(tileset.TileWidth, tileset.TileHeight);
                        TilesetBrushControl.ChangeImageSource(tileset.ImageName);

                        SceneManager.ActiveTileset = tileset;
                        TilesetMenuItems.IsEnabled = true;
                    }
                    else
                    {
                        SceneManager.ActiveTileset = null;
                    }

                    EditorCommands.CreatePropertyGridView();
                }
                else if (SelectedGameObjects.Count == 0)
                {
                    SceneManager.ActiveTileset = null;

                    if (SceneTreeView.SelectedItem != null)
                        SceneTreeView.SelectedItem.IsSelected = false;

                    EditorCommands.CreatePropertyGridView();
                }
                else
                {
                    EditorCommands.CreatePropertyGridView();
                }

                TilesetBrushControl?.InvalidateVisual();
            }
        }

        internal static UserPreferences UserPreferences { get; set; }

        internal static ProjectExplorerTreeView ProjectTreeView { get; set; }

        internal static StackPanel PropertyGridContainer { get; set; }

        internal static StackPanel TilesetMenuItems { get; set; }

        /// <summary>
        /// 
        /// </summary>
        internal static SceneViewGameControl SceneViewControl { get; set; }

        /// <summary>
        /// 
        /// </summary>
        internal static SplitContainer EditorSplitterContainer { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public static TilesetBrushControl TilesetBrushControl { get; set; }

        /// <summary>
        /// 
        /// </summary>
        internal static IniFile Settings { get; set; }

        /// <summary>
        /// 
        /// </summary>
        internal static ToolStripStatusLabel StatusLabel { get; set; }

        /// <summary>
        /// 获取或设置活动的PropertyGrid
        /// </summary>
        internal static PropertyGrid PropertyGrid { get; set; }

        /// <summary>
        /// 获取或设置活动的PropertyLabel.
        /// </summary>
        internal static TabPage PropertyPage { get; set; }

        /// <summary>
        /// 获取或设置活动的SceneTreeView.
        /// </summary>
        public static SceneHierarchyTreeView SceneTreeView { get; set; }

        /// <summary>
        /// 获取或设置选定的GameObjects
        /// </summary>
        internal static List<GameObject> SelectedGameObjects { get; set; }

        /// <summary>
        /// 获取或设置活动的UnDoRedo
        /// </summary>
        internal static UndoRedo UnDoRedo { get; set; }
    }
}