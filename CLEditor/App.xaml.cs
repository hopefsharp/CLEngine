﻿using System.Windows;
using System.Windows.Forms;
using System.Windows.Threading;
using CLEngine.Core;
using Enterwell.Clients.Wpf.Notifications;

namespace CLEngine.Editor
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App
    {
        public static NotificationMessageManager Manager = new NotificationMessageManager();

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

			if (SceneManager.GameProject != null &&
                SceneManager.GameProject.EditorSettings != null &&
                !SceneManager.GameProject.EditorSettings.CatchError)
            {
                return;
            }

            DispatcherUnhandledException += Target;
        }

        private void Target(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            if (SceneManager.GameProject != null && 
                SceneManager.GameProject.EditorSettings != null &&
                !SceneManager.GameProject.EditorSettings.CatchError)
            {
                e.Handled = true;
                return;
            }
        }
    }
}
