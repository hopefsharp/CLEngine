﻿using CLEngine.Core;

namespace CLEngine.Editor.model
{
    public class TilesetCommand : ICommand
    {
        private Tileset _element;

        private Tile[,] _change;
        private Tile[,] _beforeChange;

        public TilesetCommand(Tile[,] change, Tile[,] before, Tileset element)
        {
            _change = change;
            _element = element;
            _beforeChange = before;
        }

        public void Execute()
        {
            _element.Tiles = (_change);
        }

        public void UnExecute()
        {
            _element.Tiles = (_beforeChange);
        }
    }
}