﻿using Microsoft.Build.Framework;
using System.Collections.Generic;

namespace CLEngine.Editor.model
{
    public class ErrorLog
    {
        private string fileName;
        private int lineNumber;
        private int columnNumber;
        private string errorMessage;

        public int LineNumber
        {
            get { return lineNumber; }
            set { lineNumber = value; }
        }

        public int ColumnNumber
        {
            get { return columnNumber; }
            set { columnNumber = value; }
        }

        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        public string ErrorMessage
        {
            get { return errorMessage; }
            set { errorMessage = value; }
        }

        public override string ToString()
        {
            return fileName + " - Line: " + lineNumber + " - Error: " + errorMessage;
        }
    }

    /// <summary>
    /// 自定义实现MSBuild ILogger接口记录
    /// 内容构建错误，以便我们以后可以将它们显示给用户
    /// </summary>
    public class ErrorLogger : ILogger
    {
        private List<ErrorLog> errors = new List<ErrorLog>();

        /// <summary> 
        /// 获取已记录的所有错误的列表。
        /// </summary>
        public List<ErrorLog> Errors
        {
            get { return errors; }
        }

        /// <summary>
        /// 初始化自定义记录器，挂钩ErrorRaised通知事件。
        /// </summary>
        public void Initialize(IEventSource eventSource)
        {
	        eventSource.ErrorRaised += ErrorRaised;
        }
        /// <summary>
        /// 关闭自定义记录器
        /// </summary>
        public void Shutdown()
        {
        }


        /// <summary>
        /// 通过存储错误消息字符串来处理错误通知事件
        /// </summary>
        void ErrorRaised(object sender, BuildErrorEventArgs e)
        {
            errors.Add(new ErrorLog()
            {
                ColumnNumber = e.ColumnNumber,
                LineNumber = e.LineNumber,
                ErrorMessage = e.Message,
                FileName = e.File
            });
        }

        #region ILogger Members


        /// <summary>
        /// 实现ILogger.Parameters属性
        /// </summary>
        string ILogger.Parameters
        {
            get { return parameters; }
            set { parameters = value; }
        }

        string parameters;


        /// <summary>
        /// 实现ILogger.Verbosity属性
        /// </summary>
        LoggerVerbosity ILogger.Verbosity
        {
            get { return verbosity; }
            set { verbosity = value; }
        }

        LoggerVerbosity verbosity = LoggerVerbosity.Normal;


        #endregion
    }
}