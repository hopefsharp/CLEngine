﻿using System.IO;

namespace CLEngine.Editor.model
{
    public enum FileTemplate { None, Component };

    public static class FileHelper
    {
        public static string CreateFile(string path, FileTemplate template)
        {
            // 解析所选的文件名
            path = ResolveFilename(path);

            // 创建文件或追加
            using (new FileStream(path, FileMode.Append))
            {

            }

            switch (template)
            {
                case FileTemplate.Component:

                    //File.WriteAllText(path, Properties.Settings.Default.NewComponentTemplate);
                    break;

            }

            return path;
        }

        public static string ResolveFilename(string filename)
        {
            // 该文件不存在？
            if (!File.Exists(filename)) return filename;

            string extension = Path.GetExtension(filename);
            string nameNoExtension = Path.GetFileNameWithoutExtension(filename);
            string _filename = Path.GetDirectoryName(filename) + "\\" + nameNoExtension;

            int cnt = 1;
            while (File.Exists(_filename + cnt + extension)) cnt++;
            _filename += cnt + extension;

            return _filename;
        }
    }
}