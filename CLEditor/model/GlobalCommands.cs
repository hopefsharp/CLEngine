﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using CLEngine.Core;

namespace CLEngine.Editor.model
{
	public static class GlobalCommands
	{
		public static void RemoveEmptyFolders(string path, SearchOption searchOption)
		{
			foreach (var dirPath in Directory.GetDirectories(path, "*", SearchOption.AllDirectories))
			{
				if (!Directory.GetFiles(dirPath).Any() && !Directory.GetDirectories(dirPath).Any())
					Directory.Delete(dirPath);
			}
		}

		[SuppressMessage("ReSharper", "StringLiteralTypo")]
		public static bool DeployProject(string projectPath, string destinationPath, string platform)
		{
			var blockedDirs = new List<string> {"bin", "obj", ".git", ".vs"};

			var blockedFileExtensions = new List<string> {".cs", ".csproj", ".sln"};

			switch (platform.ToLower())
			{
				case "windows":
					// creates shadow directories
					foreach (string dirPath in Directory.GetDirectories(projectPath, "*", SearchOption.AllDirectories))
					{
						Directory.CreateDirectory(dirPath.Replace(projectPath, destinationPath));
					}

					// clear blocked directories from the root folder:
					foreach (string dirPath in Directory.GetDirectories(destinationPath, "*", SearchOption.TopDirectoryOnly))
					{
						if (blockedDirs.Contains(Path.GetFileName(dirPath)))
							Directory.Delete(dirPath, true);
					}

					// copy all the files
					foreach (string path in Directory.GetFiles(projectPath, "*.*", SearchOption.AllDirectories))
					{
						string filename = Path.GetFileName(path);
						string ext = Path.GetExtension(path);
						if (!blockedFileExtensions.Contains(ext) &&
							Directory.Exists(Path.GetDirectoryName(path.Replace(projectPath, destinationPath))))
						{
							if (filename != null && filename.ToLower().Equals("CLEngine.Windows.exe"))
							{
								File.Copy(path, Path.GetDirectoryName(path.Replace(projectPath, destinationPath)) + "\\" + SceneManager.GameProject.ProjectName + ".exe", true);
							}
							else
							{
								File.Copy(path, path.Replace(projectPath, destinationPath), true);
							}
						}
					}

					RemoveEmptyFolders(destinationPath, SearchOption.AllDirectories);
					CHelper.ChangeSettingsToRelease(destinationPath);
					ResBuilder.Build("DesktopGL", projectPath, destinationPath);
					return true;
				case "windowsstore":
					// creates shadow directories
					foreach (string unused in Directory.GetDirectories(projectPath, "*", SearchOption.AllDirectories))
						Directory.CreateDirectory(destinationPath.Replace(projectPath, destinationPath));

					//Copy all the files
					foreach (string path in Directory.GetFiles(projectPath, "*.*", SearchOption.AllDirectories))
					{
						if (!Directory.Exists(Path.GetDirectoryName(path.Replace(projectPath, destinationPath))))
						{
							Directory.CreateDirectory(Path.GetDirectoryName(path.Replace(projectPath, destinationPath)) ?? throw new InvalidOperationException());
						}

						if (path.ToLower().EndsWith(".scene"))
						{
							GameScene scene = (GameScene)CHelper.DeserializeObject(path);
							CHelper.SerializeObjectXML(path.Replace(projectPath, destinationPath), scene);
						}
						else if (path.ToLower().EndsWith(".clengine"))
						{
							CProject gp = (CProject)CHelper.DeserializeObject(path);
							CHelper.SerializeObjectXML(path.Replace(projectPath, destinationPath), gp);
						}
						else
						{
							File.Copy(path, path.Replace(projectPath, destinationPath), true);
						}
					}

					RemoveEmptyFolders(destinationPath, SearchOption.AllDirectories);
					CHelper.ChangeSettingsToRelease(destinationPath);
					ResBuilder.Build("DesktopGL", projectPath, destinationPath);

					return true;
			}

			return false;
		}
	}
}