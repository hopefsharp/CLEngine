﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Xml;
using CLEngine.Core;
using Microsoft.Build.Evaluation;
using Microsoft.Build.Execution;
using Microsoft.Build.Framework;

namespace CLEngine.Editor.model
{
    public static class ScriptsBuilder
    {
        private static readonly object locker = new object();
        private static ErrorLogger logger;

        public static ErrorLogger Logger => logger;

        public static bool IsFileLocked(FileInfo file)
        {
	        FileStream stream = null;

	        try
	        {
		        stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
	        }
	        catch (IOException)
	        {
		        //该文件不可用，因为它是：
		        //仍然被写入
		        //或由另一个线程处理
		        //或者不存在（已经处理过）
		        return true;
	        }
	        finally
	        {
		        if (stream != null)
			        stream.Close();
	        }

	        //file is not locked
	        return false;
        }

		/// <summary>
		/// 
		/// </summary>
		[SuppressMessage("ReSharper", "PossibleNullReferenceException")]
        [SuppressMessage("ReSharper", "CommentTypo")]
        public static bool ReloadScripts()
        {
            if (SceneManager.GameProject == null) return false;

            lock (locker)
            {

                try
                {
                    if (File.Exists(SceneManager.GameProject.ProjectPath + @"\_Scripts.csproj"))
                        File.Delete(SceneManager.GameProject.ProjectPath + @"\_Scripts.csproj");
                    //搜索.sln和.csproj文件
                    foreach (string filename in Directory.GetFiles(SceneManager.GameProject.ProjectPath))
                    {
                        if (Path.GetExtension(filename).ToLower().Equals(".csproj"))
                        {
                            UserPreferences.Instance.ProjectCsProjFilePath = filename;
                        }
                        else if (Path.GetExtension(filename).ToLower().Equals(".sln"))
                        {
                            UserPreferences.Instance.ProjectSlnFilePath = filename;
                        }
                    }

                    // 检查脚本是否已删除：
                    bool removed = false;

                    string projectFileName = UserPreferences.Instance.ProjectCsProjFilePath;
                    XmlDocument doc = new XmlDocument();
                    doc.Load(projectFileName);

                    while (doc.GetElementsByTagName("ItemGroup").Count < 2)
                        doc.GetElementsByTagName("Project").Item(0)?.AppendChild(doc.CreateElement("ItemGroup"));

                    foreach (XmlNode _node in doc.GetElementsByTagName("ItemGroup").Item(1)?.ChildNodes)
                    {
                        if (_node.Name.ToLower().Equals("compile"))
                        {
                            if (_node.Attributes != null && !File.Exists(SceneManager.GameProject.ProjectPath + "\\" + _node.Attributes.GetNamedItem("Include").Value))
                            {
                                doc.GetElementsByTagName("ItemGroup").Item(1)?.RemoveChild(_node);
                                removed = true;
                            }
                        }
                    }

                    if (removed)
                        doc.Save(projectFileName);

                    string tmpProjectFileName = SceneManager.GameProject.ProjectPath + @"\_Scripts.csproj";

                    var hash = CHelper.EncryptMD5(DateTime.Now.ToString(CultureInfo.InvariantCulture));

                    //try
                    //{
                    /* 更改程序集名称 */
                    doc = new XmlDocument();
                    doc.Load(projectFileName);
                    doc.GetElementsByTagName("Project").Item(0).ChildNodes[0]["AssemblyName"].InnerText = hash;
                    doc.Save(tmpProjectFileName);
                    //}
                    //catch (Exception ex)
                    //{
                    //    Console.Info(ex.Message);
                    //}

                    /* Compile project */
                    ProjectCollection projectCollection = new ProjectCollection();
                    Dictionary<string, string> GlobalProperty = new Dictionary<string, string>
                    {
	                    {"Configuration", SceneManager.GameProject.Debug ? "Debug" : "Release"}, {"Platform", "x86"}
                    };

                    //FileLogger logger = new FileLogger() { Parameters = @"logfile=C:\clengine_log.txt" };
                    logger = new ErrorLogger();

                    BuildRequestData buildRequest = new BuildRequestData(tmpProjectFileName, GlobalProperty, null, new[] { "Build" }, null);
                    BuildManager.DefaultBuildManager.Build(
	                    new BuildParameters(projectCollection)
	                    {
		                    BuildThreadPriority = System.Threading.ThreadPriority.AboveNormal,
		                    Loggers = new List<ILogger>() { logger }
	                    },
	                    buildRequest);

                    //foreach (var tr in logger.Errors)
                    //{
                    //    Console.Info(tr.ToString());
                    //}

                    //Console.Info(buildResult.OverallResult);

                    string cPath = SceneManager.GameProject.ProjectPath + @"\bin\" + (SceneManager.GameProject.Debug ? "Debug" : "Release") + "\\" + hash + ".dll";

                    if (File.Exists(cPath))
                    {
                        /* read assembly to memory without locking the file */
                        byte[] readAllBytes = File.ReadAllBytes(cPath);
                        SceneManager.ScriptsAssembly = Assembly.Load(readAllBytes);

                        if (SceneManager.ActiveScene != null)
                        {
                            SceneManager.ActiveScene.SaveComponentValues();
                            SceneManager.ActiveScene.Initialize();
                        }

                        //Console.Info("Path: " + SceneManager.ScriptsAssembly.GetName().Name);
                    }
                    else
                    {
                        File.Delete(tmpProjectFileName);
                        return false;
                    }

                    File.Delete(tmpProjectFileName);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                return true;
            }
        }
    }
}