﻿using System.Windows;

namespace CLEngine.Editor.model
{
    public class ResizeBasedVisibility : System.ComponentModel.INotifyPropertyChanged
    {
        private Visibility maximizeVisibility;
        public Visibility MaximizeVisibility
        {
            get
            {
                return maximizeVisibility;
            }
            set
            {
                maximizeVisibility = value;
                OnPropertyChanged("MaximizeVisibility");
            }
        }

        private Visibility minimizeVisibility;
        public Visibility MinimizeVisibility
        {
            get
            {
                return minimizeVisibility;
            }
            set
            {
                minimizeVisibility = value;
                OnPropertyChanged("MinimizeVisibility");
            }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
        }
    }
}