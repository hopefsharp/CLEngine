﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;

namespace CLEngine.Editor
{
	[SuppressMessage("ReSharper", "StringLiteralTypo")]
	public class ResBuilder
	{
		public static Dictionary<string, bool> image = new Dictionary<string, bool> { { "png", true },{ "jpg", true },{ "bmp", true },
			{"dds",true } };

		public static Dictionary<string, bool> AllSupportTypes = new Dictionary<string, bool>{
			{ "png", true },{ "jpg", true },{ "bmp", true },{ "dds", true },{ "wav", true }, { "mp3", true }, { "ogg", true }
		};
		public static Dictionary<string, bool> sound = new Dictionary<string, bool> { { "wav", true }, { "mp3", true }, { "ogg", true } };
		public static void Build(string platform, string ori, string dest, bool compile = false)
		{
			if (!compile) return;

			var root = new DirectoryInfo(Path.Combine(ori, "Content"));
			var files = root.GetFiles();
			FileInfo file;
			var max = files.Length;
			string Name;
			var d = Path.Combine(dest, "Content");
			var Names = new Dictionary<string, bool>();
			foreach (var t in files)
			{
				file = t;
				Name = Path.GetFileNameWithoutExtension(file.Name);
				if (Names.ContainsKey(Name))
				{
					throw new Exception("有资源重名");
				}

				Names.Add(Name, true);
			}

			using (var sw = new StreamWriter(d + "\\\\Content.mgcb"))
			{
				sw.WriteLine("\n#----------------------------- Global Properties ----------------------------#\n");
				sw.WriteLine("/outputDir:.\\\n/intermediateDir:.\\\n/platform:" + platform);
				sw.WriteLine("/config:\n/profile:Reach\n/compress:True\n");
				sw.WriteLine("#-------------------------------- References --------------------------------#\n");
				sw.WriteLine("#---------------------------------- Content ---------------------------------#");
				for (var i = 0; i < max; i++)
				{
					file = files[i];
					Name = Path.GetExtension(file.Name);
					if (!AllSupportTypes.ContainsKey(Name)) continue;

					sw.WriteLine("\n#begin " + file.Name);
					if (image.ContainsKey(Name))
					{
						sw.WriteLine("/importer:TextureImporter\n/processor:TextureProcessor");
						sw.WriteLine("/processorParam:ColorKeyColor=255,0,255,255");
						sw.WriteLine("/processorParam:ColorKeyEnabled=True");
						sw.WriteLine("/processorParam:GenerateMipmaps=False");
						sw.WriteLine("/processorParam:PremultiplyAlpha=True");
						sw.WriteLine("/processorParam:ResizeToPowerOfTwo=False");
						sw.WriteLine("/processorParam:MakeSquare=False");
						sw.WriteLine("/processorParam:TextureFormat=Color");
					}
					else if (sound.ContainsKey(Name))
					{
						switch (Name)
						{
							case "mp3":
								sw.WriteLine("/importer:Mp3Importer");
								sw.WriteLine("/processor:SongProcessor");
								break;
							case "wav":
								sw.WriteLine("/importer:WavImporter");
								sw.WriteLine("/processor:SoundEffectProcessor");
								break;
							case "ogg":
								sw.WriteLine("/importer:OggImporter");
								sw.WriteLine("/processor:SongProcessor");
								break;
						}
						sw.WriteLine("/processorParam:Quality=Best");
					}
					sw.WriteLine("/build:" + file.Name + ";" + file.Name);
				}
			}

			var p = new Process
			{
				StartInfo = new ProcessStartInfo("C:\\Program Files (x86)\\MSBuild\\MonoGame\\v3.0\\Tools\\MGCB.exe",
					"/w:" + dest + " /@:" + Path.Combine(d, "Content.mgcb"))
				{
					CreateNoWindow = true, UseShellExecute = false, RedirectStandardOutput = true
				}
			};
			p.Exited += (sender, args) =>
			{
				root = new DirectoryInfo(d);
				files = root.GetFiles();
				max = files.Length;
				for (var i = 0; i < max; i++)
				{
					file = files[i];
					Name = Path.GetExtension(file.Name);
					if (Name == ".mgcontent" || AllSupportTypes.ContainsKey(Name.Substring(1)))
					{
						File.Delete(file.FullName);
					}
				}

				File.Delete(Path.Combine(d + "Content.mgcb"));
			};
			p.Start();
			p.WaitForExit();
		}
	}
}
