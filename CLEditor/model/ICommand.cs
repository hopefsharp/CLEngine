﻿namespace CLEngine.Editor.model
{
    public interface ICommand
    {
        void Execute();
        void UnExecute();
    }
}