﻿using System.Collections.Generic;
using CLEngine.Editor.model.commands;
using CLEngine.Core;
using Microsoft.Xna.Framework;

namespace CLEngine.Editor.model
{
    public class UndoRedo
    {
        private Stack<ICommand> _Undocommands = new Stack<ICommand>();
        private Stack<ICommand> _Redocommands = new Stack<ICommand>();

        public void InsertUndoRedo(ICommand cmd)
        {
            _Undocommands.Push(cmd);
            _Redocommands.Clear();
        }

        public void Undo(int levels)
        {
            for (int i = 1; i <= levels; i++)
            {
                if (_Undocommands.Count != 0)
                {
                    ICommand command = _Undocommands.Pop();
                    command.UnExecute();
                    _Redocommands.Push(command);
                }

            }
        }

        public void Redo(int levels)
        {
            for (int i = 1; i <= levels; i++)
            {
                if (_Redocommands.Count != 0)
                {
                    ICommand command = _Redocommands.Pop();
                    command.Execute();
                    _Undocommands.Push(command);
                }

            }
        }

        public void InsertInUnDoRedoForMove(Vector2 position, Vector2 oldPosition, GameObject element)
        {
            ICommand cmd = new MoveCommand(position, oldPosition, element);
            _Undocommands.Push(cmd);
            _Redocommands.Clear();
        }

        public void InsertInUnDoRedoForRotate(float rotate, float oldRotate, GameObject element)
        {
            ICommand cmd = new RotateCommand(rotate, oldRotate, element);
            _Undocommands.Push(cmd);
            _Redocommands.Clear();
        }

        public void InsertInUnDoRedoForScale(Vector2 scale, Vector2 oldScale, GameObject element)
        {
            ICommand cmd = new ScaleCommand(scale, oldScale, element);
            _Undocommands.Push(cmd);
            _Redocommands.Clear();
        }
    }
}