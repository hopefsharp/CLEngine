﻿using System;

namespace CLEngine.Editor.model
{
    [Serializable]
    public class UserPreferences
    {
        private String projectCsProjFilePath;
        private String projectSlnFilePath;
        private string networkcode;

        private static UserPreferences instance;

        public static UserPreferences Instance
        {
            get { return instance; }
            set { instance = value; }
        }

        public String ProjectCsProjFilePath
        {
            get { return projectCsProjFilePath; }
            set { projectCsProjFilePath = value; }
        }

        public String ProjectSlnFilePath
        {
            get { return projectSlnFilePath; }
            set { projectSlnFilePath = value; }
        }

        public string NetworkCode
        {
            get { return networkcode; }
            set { networkcode = value; }
        }
    }
}