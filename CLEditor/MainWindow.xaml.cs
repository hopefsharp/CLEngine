﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Deployment.Application;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using CLEngine.Editor.controls;
using CLEngine.Editor.core;
using CLEngine.Editor.graphics_device;
using CLEngine.Editor.model;
using CLEngine.Editor.windows;
using CLEngine.Core;
using CLEngine.Core.components;
using Enterwell.Clients.Wpf.Notifications;
using Microsoft.Xna.Framework;
using DragDropEffects = System.Windows.DragDropEffects;
using DragEventArgs = System.Windows.DragEventArgs;
using Matrix = Microsoft.Xna.Framework.Matrix;
using MessageBox = System.Windows.MessageBox;
using MouseEventArgs = System.Windows.Forms.MouseEventArgs;
// ReSharper disable All

namespace CLEngine.Editor
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow
    {
        static bool fullscreen;
        public static MainWindow Instance;

        public MainWindow(string projectPath)
        {
            Instance = this;
            projectPathToLoad = string.Empty;
            lastLatestProjects = string.Empty;
            InitializeComponent();

            SetFullScreen(Properties.Settings.Default.StartOnFullScreen);

            Initialize();

            projectPathToLoad = projectPath;
        }

        string lastLatestProjects;
        string projectPathToLoad;
        public SceneViewGameControl sceneViewGameControl;
        public System.Windows.Forms.Timer timer;
        System.Windows.Forms.Timer timerUI;
        EditorModes lastEditorMode;
        ApplicationDeployment deployment;
        string versionInfo;
        ContextMenu gameViewContextMenu;

        private BackgroundWorker backgroundWorker;

        private void Initialize()
        {
            OutputDataGrid.ItemsSource = EditorHandler.OutputMessages;
            ((ObservableCollection<OutputMessage>) OutputDataGrid.ItemsSource).CollectionChanged += MainWindow_CollectionChanged;

            sceneViewGameControl = new SceneViewGameControl();
            SceneViewFormContainer.Children.Add(sceneViewGameControl);

            EditorHandler.SceneViewControl = sceneViewGameControl;
            EditorHandler.PropertyGridContainer = PropertyGridContainer;
            EditorHandler.SceneTreeView = sceneTreeView;
            EditorHandler.ProjectTreeView = projectTreeView;
            EditorHandler.UnDoRedo = new UndoRedo();
            EditorHandler.TilesetMenuItems = tilesetMenuItems;
            // Expander's Context Menu
            OutputExpander.ContextMenu = new ContextMenu();

            // TODO 改变图像源
            MenuItem ClearItem = EditorUtils.CreateMenuItem("Clear");

            OutputExpander.ContextMenu.Items.Add(ClearItem);

            // event handlers
            ClearItem.Click += ClearItem_Click;
            Closed += MainWindow_Closed;
            ContentRendered += MainWindow_ContentRendered;

            sceneViewGameControl.AllowDrop = true;
            sceneViewGameControl.DragEnter += SceneViewGameControlOnDragEnter;
            sceneViewGameControl.DragOver += SceneViewGameControlOnDragOver;

            App.Manager.CreateMessage()
                .Accent("#1751C3")
                .Animates(true)
                .AnimationInDuration(0.75)
                .AnimationOutDuration(2)
                .Background("#333")
                .HasBadge("提示")
                .HasMessage("欢迎使用CL游戏引擎.")
                .Dismiss().WithButton("检查更新", button => { checkforUpdate(); })
                .Dismiss().WithButton("暂不检查", button =>
                {
                }).Dismiss().WithDelay(TimeSpan.FromSeconds(5))
                .Queue();

            backgroundWorker = new BackgroundWorker();
			backgroundWorker.DoWork += BackgroundWorkerOnDoWork;
			backgroundWorker.RunWorkerAsync();
		}

        private void BackgroundWorkerOnDoWork(object sender, DoWorkEventArgs e)
        {
	        // 日志检查
	        while (true)
	        {
		        if (Logger.Messages.Count > 0)
		        {
			        var log = Logger.Messages.Dequeue();
			        Dispatcher.Invoke(() =>
			        {
				        var message = App.Manager.CreateMessage();

				        if (log.Type == LogType.Info)
				        {
					        message.Accent("#1751C3")
						        .HasBadge("提示")
						        .Background("#333");
				        }
				        else if (log.Type == LogType.Warn)
				        {
					        message.Accent("#E0A030")
						        .HasBadge("警告")
						        .Background("#333");
				        }
				        else if (log.Type == LogType.Error)
				        {
					        message.Accent("#F15B19")
						        .HasBadge("错误")
						        .Background("#F15B19");
				        }

				        message.Animates(true)
					        .AnimationInDuration(0.75)
					        .AnimationOutDuration(2)
					        .HasMessage(log.Message)
					        .Dismiss().WithButton("确定", button => { })
					        .Dismiss().WithDelay(TimeSpan.FromSeconds(10))
					        .Queue();
					});
				}
	        }
        }

        private void SceneViewGameControlOnDragOver(object sender, DragEventArgs e)
        {
            if (SceneManager.ActiveScene == null) return;

            if (e.Data.GetDataPresent(typeof(ExplorerTreeViewItem)))
            {
                FieldInfo info;
                object obj;
                info = e.Data.GetType().GetField("innerData", BindingFlags.NonPublic | BindingFlags.Instance);
                if (info == null)
                    return;

                obj = info.GetValue(e.Data);
                info = obj.GetType().GetField("innerData", BindingFlags.NonPublic | BindingFlags.Instance);
                DataObject dataObj = info.GetValue(obj) as DataObject;
                ExplorerTreeViewItem item = dataObj.GetData(typeof(ExplorerTreeViewItem)) as ExplorerTreeViewItem;

                if (EditorUtils.isDirectory(item.FullPath)) return;

                string relativePath = item.FullPath.Replace(SceneManager.GameProject.ProjectPath + "\\", string.Empty);
                string name = Path.GetFileNameWithoutExtension(relativePath);


                string[] extensions = item.Text.ToLower().Split('.');


                string secundaryExtension = string.Empty;
                if (extensions.Length > 2)
                    secundaryExtension = extensions[extensions.Length - 2];

                GameObject objToAdd = null;

                if (secundaryExtension.ToLower().Equals(Properties.Settings.Default.secundaryExtension))
                {
                    objToAdd = new AnimatedSprite() { ImageName = relativePath, Name = name.ToLower().Replace("." + Properties.Settings.Default.secundaryExtension, ""), TotalFramesPerRow = 1, TotalRows = 1 };
                }
                else
                {
                    string extension = extensions.Last();

                    switch (extension)
                    {
                        case "png":
                        case "jpeg":
                        case "jpg":
                        case "gif":
                        case "bmp":
                            objToAdd = new Sprite() { ImageName = relativePath, Name = name };
                            break;
                        case "mp3":
                        case "wav":
                            objToAdd = new AudioObject() { FilePath = relativePath, Name = name };
                            break;
                    }
                }

                if (objToAdd != null)
                {
                    EditorHandler.SceneTreeView.AddGameObject(objToAdd, "", true);
                    EditorHandler.ChangeSelectedObjects();
                }


            }
        }

        private void shiftUpTileset_Click(object sender, RoutedEventArgs e)
        {
            if (EditorHandler.SelectedGameObjects.Count == 0 || !(EditorHandler.SelectedGameObjects[0] is Tileset)) return;

            Tile[,] _tiles = (EditorHandler.SelectedGameObjects[0] as Tileset).DeepCopy();

            (EditorHandler.SelectedGameObjects[0] as Tileset).ShiftUp(1);

            TilesetCommand tc = new TilesetCommand((EditorHandler.SelectedGameObjects[0] as Tileset).Tiles, _tiles, (EditorHandler.SelectedGameObjects[0] as Tileset));
            EditorHandler.UnDoRedo.InsertUndoRedo(tc);
        }

        private void refreshTileset_Click(object sender, RoutedEventArgs e)
        {
            if (EditorHandler.SelectedGameObjects.Count > 0 && EditorHandler.SelectedGameObjects[0] is Tileset)
            {
                EditorHandler.TilesetBrushControl.ChangeImageSource((EditorHandler.SelectedGameObjects[0] as Tileset).ImageName);
            }
        }

        private void convertToCollision_Click(object sender, RoutedEventArgs e)
        {
            ToCollisionBlock();
        }

        private void clearTextureBuffBtn_Click(object sender, RoutedEventArgs e)
        {
            TextureLoader.Clear();
        }

        private void selectAll_Click(object sender, RoutedEventArgs e)
        {
            EditorHandler.SelectedGameObjects = GameObject.GetAllGameObjects();
            EditorHandler.ChangeSelectedObjects();
        }

        private void clearSelection_Click(object sender, RoutedEventArgs e)
        {
            EditorHandler.SelectedGameObjects.Clear();
            EditorHandler.ChangeSelectedObjects();
        }

        private void rotate180º_Click(object sender, RoutedEventArgs e)
        {
            foreach (GameObject gameObject in EditorHandler.SelectedGameObjects)
            {
                EditorHandler.UnDoRedo.InsertInUnDoRedoForRotate(gameObject.Transform.Rotation + (float)Math.PI,
                    gameObject.Transform.Rotation, gameObject);

                gameObject.Transform.Rotation += (float)Math.PI;
            }
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            LayoutHelper.DockManager = dockManager;

            // Attempts to load the latest used layout
            //if (Properties.Settings.Default.Layout.Equals("Default"))
            //{
            //    // attempts to load Gibbo's default layout
            //    LayoutHelper.LoadGibbsoDefaultLayout();
            //    return;
            //}

            //if (LayoutHelper.LayoutExists(Properties.Settings.Default.Layout))
            //{
            EditorCommands.ShowOutputMessage("试图加载用户保存的布局");
            if (LayoutHelper.LoadLayout(Properties.Settings.Default.Layout))
            {
                EditorCommands.ShowOutputMessage("用户已保存的布局已成功加载");
            }
            else
                EditorCommands.ShowOutputMessage("用户保存的布局加载尝试失败");
            //}
        }

        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            LayoutHelper.CreateNewLayout(Properties.Settings.Default.Layout);
            Properties.Settings.Default.Save();

            if (SceneManager.ActiveScene != null)
            {
                MessageBoxResult r = MessageBox.Show("是否要保存当前场景?", "警告", MessageBoxButton.YesNoCancel);

                if (r == MessageBoxResult.Yes)
                {
                    SceneManager.SaveActiveScene();
                }
                else if (r == MessageBoxResult.Cancel)
                {
                    e.Cancel = true;
                }
            }

            if (!e.Cancel)
            {
                LimeScriptEditor.Instance.Close();
                // Dispose opengl context (GraphicsDevice.OpenGL.PlatformDispose())
                sceneViewGameControl.GraphicsDevice.Dispose();
                Application.Current.Shutdown();
            }
        }

        private void Window_KeyDown_1(object sender, KeyEventArgs e)
        {

            // 
            // shortcuts
            switch (e.Key)
            {
                case Key.F11:
                    SetFullScreen(!fullscreen);
                    break;
                case Key.F5:
                    debugProject();
                    break;
                case Key.F6:
                    compileProject();
                    break;
                default:
                    break;
            }

        }

        private void rotate90ºInverse_Click(object sender, RoutedEventArgs e)
        {
            foreach (GameObject gameObject in EditorHandler.SelectedGameObjects)
            {
                EditorHandler.UnDoRedo.InsertInUnDoRedoForRotate(gameObject.Transform.Rotation + (float)Math.PI / 2,
                    gameObject.Transform.Rotation, gameObject);

                gameObject.Transform.Rotation -= (float)Math.PI / 2;
            }
        }

        private void numericScale_Click(object sender, RoutedEventArgs e)
        {
            if (EditorHandler.SelectedGameObjects.Count > 0)
            {
                // 检查：我认为还没有完成numericScale
                //new NumericScale().ShowDialog();
            }
            else
            {
                MessageBox.Show("没有选择对象", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ResetLayoutClick(object sender, RoutedEventArgs e)
        {
            EditorCommands.ShowOutputMessage("试图重置布局");
            LayoutHelper.LoadCDefaultLayout();
        }

        private void MenuItem_tutorial_click(object sender, RoutedEventArgs e)
        {
            TutorialsListWindow tutoListWindow = new TutorialsListWindow();
            tutoListWindow.ShowDialog();
        }

        private void fullDebug_Click_1(object sender, RoutedEventArgs e)
        {
            fullDebugProject();
        }

        private void scaleBtn_Click(object sender, RoutedEventArgs e)
        {
            sceneViewGameControl.EditorMode = EditorModes.Scale;
        }

        private void rotateBtn_Click(object sender, RoutedEventArgs e)
        {
            sceneViewGameControl.EditorMode = EditorModes.Rotate;
        }

        private void translateBtn_Click(object sender, RoutedEventArgs e)
        {
            sceneViewGameControl.EditorMode = EditorModes.Move;
        }

        private void selectBtn_Click(object sender, RoutedEventArgs e)
        {
            sceneViewGameControl.EditorMode = EditorModes.Select;
        }

        private void checkForUpdatesBtn_Click(object sender, RoutedEventArgs e)
        {
	        checkforUpdate();
        }

        private void checkforUpdate()
        {
	        int num = CHelper.CheckNewFiles();
			if (num > 0)
			{
				App.Manager.CreateMessage()
					.Accent("#1751C3")
					.Animates(true)
					.AnimationInDuration(0.75)
					.AnimationOutDuration(2)
					.Background("#333")
					.HasBadge("提示")
					.HasMessage("在网站上发现的更新，现在进行升级?")
					.Dismiss().WithButton("现在更新", button =>
					{
						CHelper.RunUpdater();
						Application.Current.Shutdown();
					}).Dismiss().WithButton("稍后再说", button =>
					{

					}).Queue();
			}
			else
			{
				Logger.Info("已是最新版本, 无需更新");
			}
		}

        private void websiteBtn_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("http://www.hyuan.org/");
        }

        private void aboutBtn_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("http://www.hyuan.org/");
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            Process.Start("http://www.hyuan.org/Help");
        }

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            Process.Start("http://doc.hyuan.org:4999/web/#/7?page_id=392");
        }

        private void DebugResetClick(object sender, RoutedEventArgs e)
        {
            var serializer = new Xceed.Wpf.AvalonDock.Layout.Serialization.XmlLayoutSerializer(dockManager);
            using (var stream = new StreamWriter(@".\Layout\Default.layout"))
                serializer.Serialize(stream);
        }

        private void LayoutMenuItem_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            CurrentLayoutMenuItem.Header = "目前设定: " + Properties.Settings.Default.Layout;
        }

        private void MenuItem_Click_4(object sender, RoutedEventArgs e)
        {
            EditorHandler.SceneViewControl.TakeScreenshot();
        }

        private void bmFont_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = "bmfont\\bmfont.exe";
                startInfo.Arguments = "bmfont\\bmsettings.bmfc";
                Process.Start(startInfo);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void reset_Click(object sender, RoutedEventArgs e)
        {
            foreach (GameObject gameObject in EditorHandler.SelectedGameObjects)
            {
                EditorHandler.UnDoRedo.InsertInUnDoRedoForRotate(0,
                    gameObject.Transform.Rotation, gameObject);

                gameObject.Transform.Rotation = 0;
            }
        }

        private void rotate45ºInverse_Click(object sender, RoutedEventArgs e)
        {
            foreach (GameObject gameObject in EditorHandler.SelectedGameObjects)
            {
                EditorHandler.UnDoRedo.InsertInUnDoRedoForRotate(gameObject.Transform.Rotation - (float)Math.PI / 4,
                    gameObject.Transform.Rotation, gameObject);

                gameObject.Transform.Rotation -= (float)Math.PI / 4;
            }
        }

        private void rotate45º_Click(object sender, RoutedEventArgs e)
        {
            foreach (GameObject gameObject in EditorHandler.SelectedGameObjects)
            {
                EditorHandler.UnDoRedo.InsertInUnDoRedoForRotate(gameObject.Transform.Rotation + (float)Math.PI / 4,
                    gameObject.Transform.Rotation, gameObject);

                gameObject.Transform.Rotation += (float)Math.PI / 4;
            }
        }


        private void rotate90º_Click(object sender, RoutedEventArgs e)
        {
            foreach (GameObject gameObject in EditorHandler.SelectedGameObjects)
            {
                EditorHandler.UnDoRedo.InsertInUnDoRedoForRotate(gameObject.Transform.Rotation + (float)Math.PI / 2,
                    gameObject.Transform.Rotation, gameObject);

                gameObject.Transform.Rotation += (float)Math.PI / 2;
            }
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            fullDebugProject();
        }

        private void debugBtn_Click(object sender, RoutedEventArgs e)
        {
            debugProject();
        }

        private void sceneSettingsBtn_Click(object sender, RoutedEventArgs e)
        {
            EditorHandler.ChangeSelectedObject(SceneManager.ActiveScene);
        }

        private void compileBtn_Click(object sender, RoutedEventArgs e)
        {
            compileProject();
        }

        private void CommandBinding_Executed_1(object sender, ExecutedRoutedEventArgs e)
        {
            fullDebugProject();
        }

        private void fullDebugProject()
        {
            if (compileProject())
                debugProject();
        }

        private void debugProject()
        {
            EditorCommands.ApplyBlurEffect(this);
            EditorCommands.DebugGame();
            EditorCommands.ClearEffect(this);
        }

        private bool compileProject()
        {
            EditorCommands.ApplyBlurEffect(this);

            CompilerWindow cw = new CompilerWindow();

            cw.ShowDialog();
            EditorCommands.ClearEffect(this);
            EditorCommands.CreatePropertyGridView();

            return cw.Result;
        }

        private void CommandBinding_Executed_2(object sender, ExecutedRoutedEventArgs e)
        {
            ToCollisionBlock();
        }

        private void ToCollisionBlock()
        {
            if (sceneViewGameControl.SelectionArea != Rectangle.Empty)
            {
                GameObject gameObject = new GameObject();
                gameObject.Name = "Collision Block";

                gameObject.Transform.Position =
                    new Vector2(sceneViewGameControl.SelectionArea.X + sceneViewGameControl.SelectionArea.Width / 2,
                        sceneViewGameControl.SelectionArea.Y + sceneViewGameControl.SelectionArea.Height / 2);

                SceneManager.ActiveScene.GameObjects.Add(gameObject);

                RectangleBody body = new RectangleBody();
                body.EditorExpanded = true;

                gameObject.AddComponent(body);

                body.Width = sceneViewGameControl.SelectionArea.Width;
                body.Height = sceneViewGameControl.SelectionArea.Height;
                body.BodyType = FarseerPhysics.Dynamics.BodyType.Static;

                sceneTreeView.CreateView();

                EditorHandler.SelectedGameObjects = new List<GameObject>();
                EditorHandler.SelectedGameObjects.Add(gameObject);
                EditorHandler.ChangeSelectedObjects();
            }
            else
                MessageBox.Show("没有提供有效选择，请在执行此命令之前选择一个场景", "错误");
        }

        private void CopyKeyBinding(object sender, ExecutedRoutedEventArgs e)
        {
            if (SceneViewFormContainer.IsFocused || sceneTreeView.canCopyPaste)
            {
                EditorCommands.CopySelectedObjects();
            }
        }

        private void PasteKeyBinding(object sender, ExecutedRoutedEventArgs e)
        {
            if (SceneViewFormContainer.IsFocused || sceneTreeView.canCopyPaste)
            {
                EditorCommands.PasteSelectedObjects();
            }
        }

        private void SceneViewGameControlOnDragEnter(object sender, DragEventArgs e)
        {
            if (SceneManager.ActiveScene == null) return;

            if (e.Data.GetDataPresent(typeof(ExplorerTreeViewItem)))
            {
                e.Effects = DragDropEffects.Move;
            }
        }

        void MainWindow_ContentRendered(object sender, EventArgs e)
        {
            //overlay = new WindowsFormsHostOverlay(SceneViewFormContainer, sceneViewGameControl);

            //this.DataContext = new ButtonVisibilityViewModel(this);

            //System.Windows.Controls.Canvas.SetZIndex(overlay, 1);

            if (projectPathToLoad != string.Empty)
            {
                EditorCommands.LoadProject(projectPathToLoad);
                projectPathToLoad = string.Empty;
            }

            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);

            timerUI = new System.Windows.Forms.Timer();
            timerUI.Interval = 100;
            timerUI.Tick += timerUI_Tick;
            timerUI.Enabled = true;

            EditorHandler.TilesetBrushControl = TilesetControl;

            //propertyGrid.SelectedObject = new AnimatedSprite();

            if (ApplicationDeployment.IsNetworkDeployed)
            {
                deployment = ApplicationDeployment.CurrentDeployment;
                versionInfo = deployment.CurrentVersion.ToString();
            }

            gameViewContextMenu = new ContextMenu();

            CMenuItem panelCreateObjectItem = EditorUtils.CreateMenuItem("创建新对象...", (ImageSource)FindResource("GameObjectIcon_Sprite"));
            CMenuItem panelAddFromStateItem = EditorUtils.CreateMenuItem("从某状态...", null);
            CMenuItem panelPasteItem = EditorUtils.CreateMenuItem("粘贴", (ImageSource)FindResource("PasteIcon"));

            gameViewContextMenu.Items.Add(panelCreateObjectItem);
            gameViewContextMenu.Items.Add(new Separator());
            gameViewContextMenu.Items.Add(panelAddFromStateItem);
            gameViewContextMenu.Items.Add(new Separator());
            gameViewContextMenu.Items.Add(panelPasteItem);

            SceneViewFormContainer.ContextMenu = gameViewContextMenu;

            panelCreateObjectItem.Click += createObjectItem_Click;
            panelAddFromStateItem.Click += addFromStateItem_Click;
            panelPasteItem.Click += panelPasteItem_Click;
        }

        static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            //仅当公共语言运行库尝试绑定到程序集并失败时，才会调用此处理程序

            //检索AssemblyName数组中引用的程序集列表
            Assembly objExecutingAssemblies;
            string strTempAssmbPath = "";

            objExecutingAssemblies = Assembly.GetExecutingAssembly();
            AssemblyName[] arrReferencedAssmbNames = objExecutingAssemblies.GetReferencedAssemblies();

            //循环遍历引用的程序集名称数组
            foreach (AssemblyName strAssmbName in arrReferencedAssmbNames)
            {
                //Console.Info("search: " + SceneManager.GameProject.ProjectPath + "\\" + args.Name.Substring(0, args.Name.IndexOf(",")) + ".dll");
                //Console.Info("aa: " + args.Name.Substring(0, args.Name.IndexOf(",")));
                //检查引发“AssemblyResolve”事件的程序集名称
                if (strAssmbName.FullName.Substring(0, strAssmbName.FullName.IndexOf(",")) == args.Name.Substring(0, args.Name.IndexOf(",")))
                {
                    //从必须加载的位置构建程序集的路径				
                    //strTempAssmbPath = "C:\\Myassemblies\\" + args.Name.Substring(0, args.Name.IndexOf(",")) + ".dll";
                    strTempAssmbPath = SceneManager.GameProject.ProjectPath + "\\" + args.Name.Substring(0, args.Name.IndexOf(",")) + ".dll";
                    break;
                }
            }

            if (strTempAssmbPath == "")
            {
                foreach (string fileName in Directory.GetFiles(SceneManager.GameProject.ProjectPath + "\\libs\\"))
                {
                    string asmName = Path.GetFileName(fileName);
                    //Console.Info("search: " + asmName.Replace(".dll", "") + " == " + args.Name.Substring(0, args.Name.IndexOf(",")));
                    if (asmName.Replace(".dll", "") == args.Name.Substring(0, args.Name.IndexOf(",")))
                    {
                        // Console.Info("entrei");
                        strTempAssmbPath = SceneManager.GameProject.ProjectPath + "\\libs\\" + asmName;
                        break;
                    }
                }
            }

            if (strTempAssmbPath == "")
            {
                return SceneManager.ScriptsAssembly;
            }

            //加载并返回加载的组件
            return Assembly.LoadFrom(strTempAssmbPath);
        }

        void timerUI_Tick(object sender, EventArgs e)
        {
            if (SceneManager.GameProject != null)
            {
                string sceneName = (SceneManager.ActiveScene != null) ? sceneName = Path.GetFileNameWithoutExtension(SceneManager.ActiveScenePath) : "";

                Title = string.Format("{0} - {1} - {2}", "CLEngine 2D [" + versionInfo + "]",
                    SceneManager.GameProject.ProjectName, sceneName);

                showGridBtn.IsChecked = SceneManager.GameProject.EditorSettings.ShowGrid;
                gridSnappingBtn.IsChecked = SceneManager.GameProject.EditorSettings.SnapToGrid;
                showCollisionsBtn.IsChecked = SceneManager.GameProject.EditorSettings.ShowCollisions;
                debugViewBtn.IsChecked = Properties.Settings.Default.ShowDebugView;
                // Console.Info(sceneEditorControl1.EditorMode + ":" + lastEditorMode);

                if (sceneViewGameControl.EditorMode != lastEditorMode)
                {
                    selectBtn.IsChecked = false;
                    translateBtn.IsChecked = false;
                    rotateBtn.IsChecked = false;
                    scaleBtn.IsChecked = false;

                    if (sceneViewGameControl.EditorMode == EditorModes.Select)
                    {
                        selectBtn.IsChecked = true;
                    }
                    else if (sceneViewGameControl.EditorMode == EditorModes.Move)
                    {
                        translateBtn.IsChecked = true;
                    }
                    else if (sceneViewGameControl.EditorMode == EditorModes.Rotate)
                    {
                        rotateBtn.IsChecked = true;
                    }
                    else if (sceneViewGameControl.EditorMode == EditorModes.Scale)
                    {
                        scaleBtn.IsChecked = true;
                    }

                    lastEditorMode = sceneViewGameControl.EditorMode;
                }
            }
        }

        private void projectSettingsBtn_Click(object sender, RoutedEventArgs e)
        {
            new SettingsWindow().ShowDialog();
        }

        private void MenuItem_PreviewMouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            if (lastLatestProjects == Properties.Settings.Default.LastLoadedProjects)
                return;

            lastLatestProjects = Properties.Settings.Default.LastLoadedProjects;

            loadRecentMenuItem.Items.Clear();

            if (Properties.Settings.Default.LastLoadedProjects.Trim() != string.Empty)
            {
                string[] splitted = Properties.Settings.Default.LastLoadedProjects.Split('|');
                int c = 0;
                foreach (string split in splitted)
                {
                    if (split.Trim() != string.Empty && File.Exists(split))
                    {
                        MenuItem item = new MenuItem();

                        item.Header = CHelper.SplitCamelCase(Path.GetDirectoryName(split).Split('\\').Last());
                        item.Width = 200;

                        item.Click += ((s, _e) =>
                        {
                            EditorCommands.AddToProjectHistory(split);
                            EditorCommands.LoadProject(split);
                        });

                        loadRecentMenuItem.Items.Add(item);
                    }

                    c++;
                    if (c == 8)
                        break;
                }
            }
            else
            {
                loadRecentMenuItem.Visibility = Visibility.Collapsed;
            }
        }

        private void saveSceneBtn_Click(object sender, RoutedEventArgs e)
        {
            SceneManager.SaveActiveScene();
            EditorCommands.ShowOutputMessage("游戏场景已保存");
        }

        private void exportSceneXMLBtn_Click(object sender, RoutedEventArgs e)
        {
            EditorCommands.SaveProject();

            System.Windows.Forms.SaveFileDialog sfd = new System.Windows.Forms.SaveFileDialog();
            sfd.InitialDirectory = SceneManager.GameProject.ProjectPath;
            sfd.Filter = "(*.xml)|*.xml";
            System.Windows.Forms.DialogResult dr = sfd.ShowDialog();

            if (dr == System.Windows.Forms.DialogResult.Yes || dr == System.Windows.Forms.DialogResult.OK)
            {
                SceneManager.SaveActiveScene(sfd.FileName, true);
            }
        }

        private void deployBtn_Click(object sender, RoutedEventArgs e)
        {
            //foreach (string dirPath in Directory.GetDirectories(SceneManager.GameProject.ProjectPath, "*",
            //    SearchOption.AllDirectories))
            //    Directory.CreateDirectory(dirPath.Replace(SceneManager.GameProject.ProjectPath, @"C:\Users\John\Desktop\output\"));

            ////Copy all the files
            //foreach (string newPath in Directory.GetFiles(SceneManager.GameProject.ProjectPath, "*.*",
            //    SearchOption.AllDirectories))
            //{
            //    if (newPath.ToLower().EndsWith(".scene"))
            //    {
            //        GameScene scene = (GameScene)CHelper.DeserializeObject(newPath);
            //        CHelper.SerializeObjectXML(newPath.Replace(SceneManager.GameProject.ProjectPath, @"C:\Users\John\Desktop\output\"), scene);
            //    }
            //    else if (newPath.ToLower().EndsWith(".clengine"))
            //    {
            //        CProject gp = (CProject)CHelper.DeserializeObject(newPath);
            //        CHelper.SerializeObjectXML(newPath.Replace(SceneManager.GameProject.ProjectPath, @"C:\Users\John\Desktop\output\"), gp);
            //    }
            //    else
            //    {
            //        File.Copy(newPath, newPath.Replace(SceneManager.GameProject.ProjectPath, @"C:\Users\John\Desktop\output\"), true);
            //    }
            //}

            new DeploymentWindow().ShowDialog();
        }

        private void exitBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void loadMenuItem_Click(object sender, RoutedEventArgs e)
        {
            EditorCommands.LoadProject();
        }

        private void addNewItemSceneBtn_Click(object sender, RoutedEventArgs e)
        {
            if (SceneManager.ActiveScene == null)
            {
                MessageBox.Show("没有场景加载!", "错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                AddNewItemWindow window = new AddNewItemWindow(sceneTreeView.SelectedItem);
                window.ShowDialog();
            }
        }

        void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            //if (sender != null)
            //{

            //    System.Windows.Controls.DataGridRow row = sender as System.Windows.Controls.DataGridRow;
            //    if (row != null)
            //    {
            //        OutputMessage item = row.Item as OutputMessage;
            //        string time = item.Time;
            //        string message = item.Message;
            //    }
            //}
        }

        private void pencilTilesetBtn_Click(object sender, RoutedEventArgs e)
        {
            sceneViewGameControl.TilesetMode = TilesetModes.Pencil;
            UpdateTilesetModes();
        }

        private void UpdateTilesetModes()
        {
            pencilTilesetBtn.IsChecked = false;
            rectangleTileset.IsChecked = false;
            rectangleRemoveTileset.IsChecked = false;
            addColumnTileset.IsChecked = false;
            addRowTileset.IsChecked = false;
            removeColumnTileset.IsChecked = false;
            removeRowTileset.IsChecked = false;

            switch (sceneViewGameControl.TilesetMode)
            {
                case TilesetModes.Pencil:
                    pencilTilesetBtn.IsChecked = true;
                    break;
                case TilesetModes.Rectangle:
                    rectangleTileset.IsChecked = true;
                    break;
                case TilesetModes.Eraser:
                    rectangleRemoveTileset.IsChecked = true;
                    break;
                case TilesetModes.AddColumn:
                    addColumnTileset.IsChecked = true;
                    break;
                case TilesetModes.RemoveColumn:
                    removeColumnTileset.IsChecked = true;
                    break;
                case TilesetModes.AddRow:
                    addRowTileset.IsChecked = true;
                    break;
                case TilesetModes.RemoveRow:
                    removeRowTileset.IsChecked = true;
                    break;
            }
        }

        private void shiftLeftTileset_Click(object sender, RoutedEventArgs e)
        {
            if (EditorHandler.SelectedGameObjects.Count == 0 || !(EditorHandler.SelectedGameObjects[0] is Tileset)) return;

            Tile[,] _tiles = (EditorHandler.SelectedGameObjects[0] as Tileset).DeepCopy();

            (EditorHandler.SelectedGameObjects[0] as Tileset).ShiftLeft(1);

            TilesetCommand tc = new TilesetCommand((EditorHandler.SelectedGameObjects[0] as Tileset).Tiles, _tiles, (EditorHandler.SelectedGameObjects[0] as Tileset));
            EditorHandler.UnDoRedo.InsertUndoRedo(tc);
        }

        private void removeRowTileset_Click(object sender, RoutedEventArgs e)
        {
            sceneViewGameControl.TilesetMode = TilesetModes.RemoveRow;
            UpdateTilesetModes();
        }

        private void removeColumnTileset_Click(object sender, RoutedEventArgs e)
        {
            sceneViewGameControl.TilesetMode = TilesetModes.RemoveColumn;
            UpdateTilesetModes();
        }

        private void addRowTileset_Click(object sender, RoutedEventArgs e)
        {
            sceneViewGameControl.TilesetMode = TilesetModes.AddRow;
            UpdateTilesetModes();
        }

        private void addColumnTileset_Click(object sender, RoutedEventArgs e)
        {
            sceneViewGameControl.TilesetMode = TilesetModes.AddColumn;
            UpdateTilesetModes();
        }

        private void rectangleRemoveTileset_Click(object sender, RoutedEventArgs e)
        {
            sceneViewGameControl.TilesetMode = TilesetModes.Eraser;
            UpdateTilesetModes();
        }

        private void rectangleTileset_Click(object sender, RoutedEventArgs e)
        {
            sceneViewGameControl.TilesetMode = TilesetModes.Rectangle;
            UpdateTilesetModes();
        }

        private void OutputExpander_Expanded(object sender, RoutedEventArgs e)
        {
            SceneViewContainer.RowDefinitions[3].Height = new GridLength(100);
        }

        private void OutputExpander_Collapsed(object sender, RoutedEventArgs e)
        {
            SceneViewContainer.RowDefinitions[3].Height = new GridLength(20);
        }

        private void centerCameraObjectBtn_Click(object sender, RoutedEventArgs e)
        {
            sceneViewGameControl.CenterCameraObject();
        }

        private void refreshSceneBtn_Click(object sender, RoutedEventArgs e)
        {
            sceneTreeView.CreateView();
        }

        void panelPasteItem_Click(object sender, RoutedEventArgs e)
        {
            sceneTreeView.Paste();
        }

        private void refreshProjectExplorerBtn_Click(object sender, RoutedEventArgs e)
        {
            projectTreeView.CreateView();
        }

        private void centerCameraBtn_Click(object sender, RoutedEventArgs e)
        {
            sceneViewGameControl.CenterCamera();
        }

        private void debugViewBtn_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.ShowDebugView = !Properties.Settings.Default.ShowDebugView;
            Properties.Settings.Default.Save();
        }

        private void gridSnappingBtn_Click(object sender, RoutedEventArgs e)
        {
            SceneManager.GameProject.EditorSettings.SnapToGrid = !SceneManager.GameProject.EditorSettings.SnapToGrid;
        }

        private void showCollisionsBtn_Click(object sender, RoutedEventArgs e)
        {
            SceneManager.GameProject.EditorSettings.ShowCollisions = !SceneManager.GameProject.EditorSettings.ShowCollisions;
        }

        private void showGridBtn_Click(object sender, RoutedEventArgs e)
        {
            SceneManager.GameProject.EditorSettings.ShowGrid = !SceneManager.GameProject.EditorSettings.ShowGrid;
        }

        void addFromStateItem_Click(object sender, RoutedEventArgs e)
        {
            sceneTreeView.AddFromState();
        }

        void createObjectItem_Click(object sender, RoutedEventArgs e)
        {
            new AddNewItemWindow(null).ShowDialog();
        }

        void MainWindow_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }

        void ClearItem_Click(object sender, RoutedEventArgs e)
        {
            if (EditorHandler.OutputMessages.Count > 0)
                EditorHandler.OutputMessages.Clear();
        }

        void SceneViewGameControl_MouseMove(object sender, MouseEventArgs e)
        {
            SetMousePosition(new Vector2(e.X, e.Y));
        }

        private Vector2 mouseWorldPosition;

        public void SetMousePosition(Vector2 pos)
        {
            controlPosition = pos;
            mouseWorldPosition = Vector2.Transform(pos, (Matrix) Matrix.Invert(SceneManager.ActiveCamera.TransformMatrix));
        }

        Vector2 controlPosition;

        static MainWindow()
        {
            fullscreen = false;
        }

        void MainWindow_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            OutputDataGrid.Items.Refresh();
            if (OutputDataGrid.Items.Count > 0)
                OutputDataGrid.ScrollIntoView(OutputDataGrid.Items[OutputDataGrid.Items.Count - 1]);
        }

        public void SetFullScreen(bool setFullscreen)
        {
            fullscreen = setFullscreen;
            setFullScreenName(fullscreen);

            if (fullscreen)
                WindowStyle = WindowStyle.None;
            else
                WindowStyle = WindowStyle.SingleBorderWindow;

            WindowState = WindowState.Minimized;
            WindowState = WindowState.Maximized;


        }

        public void setFullScreenName(bool setFullScreen)
        {
            Grid grid = (MainGrid.FindName("TopRightButtonsGrid") as Grid);

            if (setFullScreen)
                (grid.FindName("fullScreenBtn") as RoundedButton).Content = "退出全屏";
            else
                (grid.FindName("fullScreenBtn") as RoundedButton).Content = "切换到全屏";
        }

        private void fullScreenBtn_Click(object sender, RoutedEventArgs e)
        {
            SetFullScreen(!fullscreen);
        }

        private void FairyGuiBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = "FairyGUI-Editor\\FairyGUI-Editor.exe";
                Process.Start(startInfo);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void AnimationBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = "http://dragonbones.com/cn/index.html";
                Process.Start(startInfo);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void LoginOrRegisterBtn_Click(object sender, MouseButtonEventArgs e)
        {
            //var loginOrRegisteWin = new LoginOrRegister();
            //loginOrRegisteWin.ShowDialog();
			Logger.Error("不允许登陆");
        }

        private void QuickCreateCollision_Click(object sender, RoutedEventArgs e)
        {
            if (collisionBtn.IsChecked == true)
            {
                sceneViewGameControl.EditorMapMode = EditorMapModes.AddCollison;
                opacityBtn.IsChecked = false;
            }
            else
            {
                sceneViewGameControl.EditorMapMode = EditorMapModes.None;
            }
        }

        private void QuickCreateOpacity_Click(object sender, RoutedEventArgs e)
        {
            if (opacityBtn.IsChecked == true)
            {
                sceneViewGameControl.EditorMapMode = EditorMapModes.Opacity;
                collisionBtn.IsChecked = false;
            }
            else
            {
                sceneViewGameControl.EditorMapMode = EditorMapModes.None;
            }
        }
    }
}
