﻿using CLEngine.Core;

namespace CLEngine.Editor.graphics_device
{
    public class GameControl : GraphicsDeviceControl
    {
        private Camera camera = new Camera();

        internal Camera Camera
        {
            get { return camera; }
            set { camera = value; }
        }
    }
}