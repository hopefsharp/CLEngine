﻿using CLEngine.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Framework.WpfInterop;
using MonoGame.Framework.WpfInterop.Input;

namespace CLEngine.Editor.graphics_device
{
    public abstract class GraphicsDeviceControl : WpfGame
    {
        public IGraphicsDeviceService graphicsDeviceManager;
        public WpfKeyboard keyboard;
        public WpfMouse mouse;

        protected GraphicsDeviceControl()
        {
           
        }

        protected override void Initialize()
        {
            graphicsDeviceManager = new WpfGraphicsDeviceService(this);
            keyboard = new WpfKeyboard(this);
            mouse = new WpfMouse(this);

            base.Initialize();
        }
    }
}