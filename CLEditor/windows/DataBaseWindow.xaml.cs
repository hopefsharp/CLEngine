﻿using System.Windows;
using System.Windows.Controls;
using CLEngine.Core.framework;
using CLEngine.Editor.viewmodel;

namespace CLEngine.Editor.windows
{
    /// <summary>
    /// DataBaseWindow.xaml 的交互逻辑
    /// </summary>
    public partial class DataBaseWindow : Window
    {
	    public static DataBaseWindow Instance;

		public DataBaseWindow()
        {
            InitializeComponent();
            Instance = this;
			ClassificationComboBox.SelectionChanged += ClassificationComboBoxOnSelectionChanged;
			ItemList.SelectionChanged += ItemListOnSelectionChanged;
		}

		/// <summary>
		/// 物品列表选择发生改变
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ItemListOnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (DataContext is DataBaseViewModel dataBase)
			{
				dataBase.IconPath = (ItemList.SelectedValue as ItemObject)?.IconPath;
				dataBase.DropIconPath = (ItemList.SelectedValue as ItemObject)?.DropIconPath;
			}
		}

		/// <summary>
		/// 分类下拉框值发生改变
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void ClassificationComboBoxOnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			(DataContext as DataBaseViewModel)?.ClearItemChildTypes(ClassificationComboBox.SelectedValue);
		}
    }
}
