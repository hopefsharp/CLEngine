﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using CLEngine.Core;

namespace CLEngine.Editor.windows
{
    /// <summary>
    /// NewProjectWindow.xaml 的交互逻辑
    /// </summary>
    public partial class NewProjectWindow : Window
    {
        public string ProjectPath { get; set; }

        private string defaultProjectsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\CLEngine";


        /// <summary>
        /// New Project Window's Default Constructor
        /// </summary>
        public NewProjectWindow()
        {
            InitializeComponent();
            ProjectPath = string.Empty;

        }

        /// <summary>
        /// 将路径设置为默认路径
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!Directory.Exists(defaultProjectsPath))
                Directory.CreateDirectory(defaultProjectsPath);

            this.pathTxt.Text = defaultProjectsPath;

            ToolTip tooltip = new ToolTip { Content = pathTxt.Text };
            this.pathBtn.ToolTip = tooltip;
        }

        /// <summary>
        /// 尝试使用给定的信息创建项目
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void createBtn_Click(object sender, RoutedEventArgs e)
        {
            // 是否已完成预先要求？
            if (nameTxt.Text.Trim() != string.Empty && pathTxt.Text != string.Empty)
            {
                nameTxt.Text = nameTxt.Text.Trim();
                string path = pathTxt.Text;

                if (createFolderCheck.IsChecked.HasValue && createFolderCheck.IsChecked.Value)
                    path += "\\" + nameTxt.Text;

                // 该项目是否已经存在？
                if (Directory.Exists(path))
                {
                    MessageBox.Show("已经有一个名称的项目，请选择另一个", "错误", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                CProject gp = new CProject(nameTxt.Text, pathTxt.Text);
                gp.Save();

                File.Copy("Project Templates\\CLEngine.Windows.exe", path + "\\CLEngine.Windows.exe", true);
                File.Copy("Project Templates\\CLEngine.Windows.csproj", path + "\\Scripts.csproj", true);
                File.Copy("Project Templates\\settings.ini", path + "\\settings.ini", true);


                // 解决方案准备
                string slnFile = File.ReadAllText("Project Templates\\GameProject.sln");
                slnFile = slnFile.Replace("{%P_NAME%}", nameTxt.Text);

                // 解决方案保存
                File.WriteAllText(path + "\\Scripts.sln", slnFile);

                File.Copy("CLEngine.Core.dll", path + "\\CLEngine.Core.dll", true);

                CHelper.CopyDirectory("Project Templates\\Content", path + "\\Content", true);
                CHelper.CopyDirectory("Project Templates\\libs", path + "", true);
                CHelper.CopyDirectory("Project Templates\\samples", path + "\\samples", true);
                
                //File.Copy("MonoGame.Framework.dll", path + "\\MonoGame.Framework.dll", true);
                //File.Copy("MonoGame.Framework.Net.dll", path + "\\MonoGame.Framework.Net.dll", true);

                ProjectPath = gp.ProjectFilePath;
                DialogResult = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("请填写所有必填字段.", "错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 关闭对话框
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 打开浏览器对话框，以便用户可以选择其项目的文件夹
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pathBtn_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog();
            fbd.Description = "选择并清空目录";
            fbd.ShowNewFolderButton = true;

            if (pathTxt.Text == string.Empty)
                fbd.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            else
                fbd.SelectedPath = pathTxt.Text;

            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (Directory.GetFiles(fbd.SelectedPath).Count() > 0)
                {
                    MessageBox.Show("请选择一个空目录.", "错误", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                pathTxt.Text = fbd.SelectedPath;
            }
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void samplesBtn_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("功能尚未实施.");
        }
    }
}
