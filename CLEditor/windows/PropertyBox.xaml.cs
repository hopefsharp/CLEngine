﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using CLEngine.Editor.core;
using CLEngine.Core;

namespace CLEngine.Editor.windows
{
    /// <summary>
    /// PropertyBox.xaml 的交互逻辑
    /// </summary>
    public partial class PropertyBox : UserControl
    {
        object selected;

        public object SelectedObject
        {
            get { return PropertyGrid.SelectedObject; }
            set
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    selected = value;
                    PropertyGrid.SelectedObject = value;
                    Title.Content = CHelper.SplitCamelCase(value.ToString());

                    if (value is ObjectComponent)
                    {
                        SettingsBtn.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        SettingsBtn.Visibility = System.Windows.Visibility.Hidden;
                    }
                }));
            }
        }

        public PropertyBox()
        {
            InitializeComponent();
        }

        void PropertyGrid_SelectedPropertyItemChanged(object sender, RoutedPropertyChangedEventArgs<Xceed.Wpf.Toolkit.PropertyGrid.PropertyItemBase> e)
        {

        }

        void PropertyGrid_SelectedObjectChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {

        }

        private void VisibilityHandlerBtn_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ToggleExpand();
        }

        public void ToggleExpand()
        {
            //var disp = this.Dispatcher;
            //disp.BeginInvoke(DispatcherPriority.Background, (Action)(() =>
            //{

            bool expanded = false;
            if (PropertyGridContainer.Visibility == System.Windows.Visibility.Collapsed)
            {
                PropertyGridContainer.Visibility = System.Windows.Visibility.Visible;
                VisibilityHandlerBtn.Source = (ImageSource)new ImageSourceConverter().ConvertFrom("content/_arrow_down.png");
                expanded = true;
            }
            else if (PropertyGridContainer.Visibility == System.Windows.Visibility.Visible)
            {
                PropertyGridContainer.Visibility = System.Windows.Visibility.Collapsed;
                VisibilityHandlerBtn.Source = (ImageSource)new ImageSourceConverter().ConvertFrom("content/_arrow_right.png");
            }

            if (SelectedObject is ObjectComponent)
                (SelectedObject as ObjectComponent).EditorExpanded = expanded;
            //}));
        }

        private void PropertyGrid_MouseEnter(object sender, MouseEventArgs e)
        {
            Dispatcher.Invoke((Action)(() =>
            {
                PropertyGrid.Update();
            }));
        }

        private void SettingsBtn_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (MessageBox.Show("您确定要删除此组件吗？", "警告!", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                ObjectComponent oc = this.PropertyGrid.SelectedObject as ObjectComponent;
                oc.Transform.GameObject.RemoveComponent(oc);

                this.Visibility = System.Windows.Visibility.Collapsed;

                EditorCommands.CheckPropertyGridConsistency();
            }
        }

        private void TagTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string text = (sender as TextBox).Text.Trim();
                if (!text.Equals(string.Empty) && text.StartsWith("+"))
                {
                    text = text.Remove(0, 1);

                    if (!text.Trim().Equals(string.Empty))
                    {

                        if (SceneManager.ActiveScene.CommonTags.Contains(text))
                            SceneManager.ActiveScene.CommonTags.Remove(text);

                        SceneManager.ActiveScene.CommonTags.Insert(0, text);

                        (sender as TextBox).Text = text;
                    }
                }
            }


        }
    }
}
