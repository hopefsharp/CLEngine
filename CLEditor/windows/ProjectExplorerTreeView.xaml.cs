﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml;
using CLEngine.Editor.controls;
using CLEngine.Editor.core;
using CLEngine.Editor.model;
using CLEngine.Core;
using CLEngine.Editor.nodeNetwork;
using Microsoft.Win32;
// ReSharper disable All

namespace CLEngine.Editor.windows
{
    /// <summary>
    /// ProjectExplorerTreeView.xaml 的交互逻辑
    /// </summary>
    public partial class ProjectExplorerTreeView
    {
        const int MARGIN = 4;
        const int TIMER_DELAY = 1000;

        ContextMenu rootContextMenu;
        ContextMenu directoryContextMenu;
        ContextMenu fileContextMenu;

        private FileSystemWatcher watcher;
        private System.Windows.Forms.Timer timer;

        string beforeEditingPath = string.Empty;
        bool firstTick = true;

        ExplorerTreeViewItem selectedForEditing;

        EnvDTE80.DTE2 dte;

        List<string> AcceptedExtensions = new List<string>();

        internal DragDropTreeViewItem SelectedItem
        {
            get
            {
                return treeView.SelectedItem as DragDropTreeViewItem;
            }
        }

        public ProjectExplorerTreeView()
        {
            InitializeComponent();
            Initialize();

            treeView.Items.SortDescriptions.Clear();
            treeView.Items.SortDescriptions.Add(new SortDescription("PriorityIndex", ListSortDirection.Ascending));
            treeView.Items.SortDescriptions.Add(new SortDescription("Text", ListSortDirection.Ascending));

            treeView.OnDragDropSuccess += treeView_OnDragDropSuccess;
            treeView.MouseRightButtonDown += treeView_MouseRightButtonDown;
            treeView.MouseLeftButtonDown += treeView_MouseLeftButtonDown;
        }

        void treeView_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            SelectOnClick(e);
        }

        void treeView_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            SelectOnClick(e);
        }

        private void SelectOnClick(MouseButtonEventArgs e)
        {
            DragDropTreeViewItem ClickedTreeViewItem = new DragDropTreeViewItem();

            //find the original object that raised the event
            UIElement ClickedItem = VisualTreeHelper.GetParent(e.OriginalSource as UIElement) as UIElement;

            //find the clicked TreeViewItem
            while ((ClickedItem != null) && !(ClickedItem is DragDropTreeViewItem))
            {
                ClickedItem = VisualTreeHelper.GetParent(ClickedItem) as UIElement;
            }

            ClickedTreeViewItem = ClickedItem as DragDropTreeViewItem;
            if (ClickedTreeViewItem != null)
            {
                ClickedTreeViewItem.IsSelected = true;
                ClickedTreeViewItem.Focus();
            }
        }

        void treeView_OnDragDropSuccess(TreeViewItem source, TreeViewItem target, CancelEventArgs e)
        {
            string sourceType = (string)source.Tag;
            string targetType = (string)target.Tag;

            // the target node is not a directory?
            if (!targetType.Equals("directory"))
            {
                e.Cancel = true;
                return;
            }

            string targetPath = (target as ExplorerTreeViewItem).FullPath;
            if (sourceType.Equals("directory"))
            {
                targetPath += "\\" + (source as ExplorerTreeViewItem).Text;

                if (!Directory.Exists(targetPath))
                    Directory.CreateDirectory(targetPath);
            }

            if (sourceType.Equals("directory"))
            {
                try
                {
                    CHelper.CopyDirectory((source as ExplorerTreeViewItem).FullPath, targetPath, true);
                    Directory.Delete((source as ExplorerTreeViewItem).FullPath, true);
                }
                catch (Exception ex)
                {
                    e.Cancel = true;
                    MessageBox.Show("Error: " + ex.Message, "Error!");
                }
            }
            else
            {
                try
                {
                    File.Move((source as ExplorerTreeViewItem).FullPath, targetPath + "\\" + (source as ExplorerTreeViewItem).Text);
                }
                catch (Exception ex)
                {
                    e.Cancel = true;
                    MessageBox.Show("Error: " + ex.Message, "Error!");
                }
            }
        }

        public void CreateView()
        {
            treeView.Items.Clear();

            ExplorerTreeViewItem rootnode = AddNode(null, SceneManager.GameProject.ProjectPath, 0, (ImageSource)FindResource("FolderIcon"));

            rootnode.Tag = "directory";
            rootnode.PriorityIndex = 0;
            rootnode.ContextMenu = rootContextMenu;

            treeView.Items.Add(rootnode);

            //rootnode.Items.SortDescriptions.Clear();
            //rootnode.Items.SortDescriptions.Add(new SortDescription("PriorityIndex", ListSortDirection.Ascending));
            //rootnode.Items.SortDescriptions.Add(new SortDescription("Text", ListSortDirection.Ascending));

            FillChildNodes(rootnode);

            //rootnode.Items.Refresh();
            SortNode(rootnode);
            timer.Enabled = true;

            if (SceneManager.GameProject.ProjectPath != watcher.Path)
                watcher.Path = SceneManager.GameProject.ProjectPath;

            watcher.EnableRaisingEvents = true;

            firstTick = true;
        }


        private ExplorerTreeViewItem AddNode(ExplorerTreeViewItem root, string text, int priorityIndex, ImageSource imageSource = null)
        {
            ExplorerTreeViewItem _node = new ExplorerTreeViewItem();
            _node.Style = (Style)FindResource("IgniteTreeViewItem");
            _node.PriorityIndex = priorityIndex;
            _node.Text = text;
            _node.MouseDoubleClick += newNode_MouseDoubleClick;

            StackPanel sp = new StackPanel();
            sp.Orientation = Orientation.Horizontal;
            sp.Children.Add(new Image() { Source = imageSource });
            sp.Children.Add(element: new TextBlock() { Text = Path.GetFileName(text), Margin = new Thickness(MARGIN, 0, 0, 0) });

            _node.Header = sp;

            if (root != null)
            {
                root.Items.Add(_node);
                root.Items.Refresh();
            }

            return _node;
        }

        void newNode_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            HandleOpenFile(sender as ExplorerTreeViewItem);
        }

       
        private void Initialize()
        {
            // Settings
            AcceptedExtensions = new List<string>(Properties.Settings.Default.AcceptedExtensions.Split('|'));

            // Root Context Menu
            rootContextMenu = new ContextMenu();

            // TODO change image sources
            MenuItem createItemRoot = EditorUtils.CreateMenuItem("创建...");
            MenuItem sceneItemRoot = EditorUtils.CreateMenuItem("游戏场景", (ImageSource)FindResource("SceneIcon"));
            MenuItem scriptItemRoot = EditorUtils.CreateMenuItem("C#脚本", (ImageSource)FindResource("CSFileIcon"));
            MenuItem addFromFolderItemRoot = EditorUtils.CreateMenuItem("从文件夹中添加");
            MenuItem createFolderItemRoot = EditorUtils.CreateMenuItem("创建文件夹", (ImageSource)FindResource("FolderAddIcon"));
            MenuItem openFolderItemRoot = EditorUtils.CreateMenuItem("在文件浏览器中打开文件夹");
            MenuItem copyFullPathItemRoot = EditorUtils.CreateMenuItem("复制完整路径");

            createItemRoot.Items.Add(sceneItemRoot);
            createItemRoot.Items.Add(new Separator());
            createItemRoot.Items.Add(scriptItemRoot);

            rootContextMenu.Items.Add(createItemRoot);
            rootContextMenu.Items.Add(createFolderItemRoot);
            rootContextMenu.Items.Add(new Separator());
            rootContextMenu.Items.Add(addFromFolderItemRoot);
            rootContextMenu.Items.Add(new Separator());
            rootContextMenu.Items.Add(copyFullPathItemRoot);
            rootContextMenu.Items.Add(openFolderItemRoot);

            sceneItemRoot.Click += createSceneItem_Click;
            createFolderItemRoot.Click += createFolderItem_Click;
            addFromFolderItemRoot.Click += addFromFolder_Click;
            openFolderItemRoot.Click += openFolderItem_Click;
            copyFullPathItemRoot.Click += copyFullPath_Click;
            scriptItemRoot.Click += scriptItem_Click;

            // Directory Context Menu
            directoryContextMenu = new ContextMenu();

            // TODO change image sources
            MenuItem createItem = EditorUtils.CreateMenuItem("创建...");
            MenuItem sceneItem = EditorUtils.CreateMenuItem("游戏场景", (ImageSource)FindResource("SceneIcon"));
            MenuItem scriptItem = EditorUtils.CreateMenuItem("C#脚本", (ImageSource)FindResource("CSFileIcon"));
            MenuItem luaScriptItem = EditorUtils.CreateMenuItem("Lua脚本", (ImageSource) FindResource("LuaFileIcon"));
            MenuItem visualScriptItem = EditorUtils.CreateMenuItem("可视化脚本");
            visualScriptItem.IsEnabled = false;
            MenuItem createFolderItem = EditorUtils.CreateMenuItem("创建文件夹", (ImageSource)FindResource("FolderAddIcon"));
            MenuItem addFromFolderItem = EditorUtils.CreateMenuItem("从文件夹中添加");
            MenuItem renameItem = EditorUtils.CreateMenuItem("重命名", (ImageSource)FindResource("RenameIcon"));
            MenuItem removeItem = EditorUtils.CreateMenuItem("移除");
            MenuItem openFolderItem = EditorUtils.CreateMenuItem("在文件浏览器中打开文件夹");
            MenuItem copyFullPathItemDir = EditorUtils.CreateMenuItem("复制完整路径");
            MenuItem copyRelativePathItemDir = EditorUtils.CreateMenuItem("复制相对路径");

            createItem.Items.Add(sceneItem);
            createItem.Items.Add(new Separator());
            createItem.Items.Add(scriptItem);
            createItem.Items.Add(luaScriptItem);
            createItem.Items.Add(visualScriptItem);

            // other directories

            directoryContextMenu.Items.Add(createItem);
            directoryContextMenu.Items.Add(createFolderItem);
            directoryContextMenu.Items.Add(new Separator());
            directoryContextMenu.Items.Add(addFromFolderItem);
            directoryContextMenu.Items.Add(new Separator());
            directoryContextMenu.Items.Add(copyFullPathItemDir);
            directoryContextMenu.Items.Add(copyRelativePathItemDir);
            directoryContextMenu.Items.Add(openFolderItem);
            directoryContextMenu.Items.Add(new Separator());
            directoryContextMenu.Items.Add(renameItem);
            directoryContextMenu.Items.Add(removeItem);

            scriptItem.Click += scriptItem_Click;
            luaScriptItem.Click += LuaScriptItemOnClick;
            visualScriptItem.Click += VisualScriptItemOnClick;
            sceneItem.Click += createSceneItem_Click;
            createFolderItem.Click += createFolderItem_Click;
            addFromFolderItem.Click += addFromFolder_Click;
            copyFullPathItemDir.Click += copyFullPath_Click;
            copyRelativePathItemDir.Click += copyRelativePathItem_Click;
            openFolderItem.Click += openFolderItem_Click;
            renameItem.Click += renameItem_Click;
            removeItem.Click += removeItem_Click;

            // File Context Menu
            fileContextMenu = new ContextMenu();

            // TODO  change image sources
            MenuItem openItem = EditorUtils.CreateMenuItem("打开");
            MenuItem openContainingFolderItem = EditorUtils.CreateMenuItem("打开所在文件夹");
            MenuItem renameFileItem = EditorUtils.CreateMenuItem("重命名", (ImageSource)FindResource("RenameIcon"));
            MenuItem deleteItem = EditorUtils.CreateMenuItem("删除");
            MenuItem copyFullPathItem = EditorUtils.CreateMenuItem("复制完整路径");
            MenuItem copyRelativePathItem = EditorUtils.CreateMenuItem("复制相对路径");

            fileContextMenu.Items.Add(openItem);
            fileContextMenu.Items.Add(new Separator());
            fileContextMenu.Items.Add(copyFullPathItem);
            fileContextMenu.Items.Add(copyRelativePathItem);
            fileContextMenu.Items.Add(openContainingFolderItem);
            fileContextMenu.Items.Add(new Separator());
            fileContextMenu.Items.Add(renameFileItem);
            fileContextMenu.Items.Add(deleteItem);

            openItem.Click += openItem_Click;
            copyFullPathItem.Click += copyFullPath_Click;
            copyRelativePathItem.Click += copyRelativePathItem_Click;
            openContainingFolderItem.Click += openFolderItem_Click;
            renameFileItem.Click += renameItem_Click;
            deleteItem.Click += deleteItem_Click;

            // timer
            timer = new System.Windows.Forms.Timer();
            timer.Interval = TIMER_DELAY;
            timer.Tick += TimerOnTick;

            // systemWathcer

            watcher = new FileSystemWatcher();
            watcher.IncludeSubdirectories = true;
            watcher.Changed += WatcherEventRaise;
            watcher.Created += WatcherEventRaise;
            watcher.Deleted += WatcherEventRaise;
            watcher.Renamed += WatcherEventRaise;
        }

        private void VisualScriptItemOnClick(object sender, RoutedEventArgs e)
        {
            string tname = "Script"; int c = 1;
            while (File.Exists((SelectedItem as ExplorerTreeViewItem)?.FullPath + "\\" + tname + ".visual"))
            {
                tname = "Script" + c;
                c++;
            }

            tname += ".visual";

            ExplorerTreeViewItem node = AddNode(SelectedItem as ExplorerTreeViewItem, tname, 10);
            ((ExplorerTreeViewItem)SelectedItem).IsExpanded = true;
            node.IsSelected = true;
            node.Tag = "file";
            node.ContextMenu = fileContextMenu;

            BeginEditTextOnSelected();
            beforeEditingPath = ".visual";
        }

        private void TimerOnTick(object sender, EventArgs e)
        {
            if (SceneManager.GameProject.ProjectPath != watcher.Path)
                watcher.Path = SceneManager.GameProject.ProjectPath;
        }

        private void WatcherEventRaise(object sender, FileSystemEventArgs e)
        {
            if (treeView.Items.Count > 0)
            {
                if (firstTick)
                {
                    (treeView.Items[0] as ExplorerTreeViewItem)?.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        ((ExplorerTreeViewItem) treeView.Items[0]).IsExpanded = true;
                    }));
                    firstTick = false;
                }

                try
                {
                    RealtimeFolderUpdater(treeView.Items[0] as ExplorerTreeViewItem);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("实时项目同步错误: " + ex.Message);
                }
            }
        }

        private void LuaScriptItemOnClick(object sender, RoutedEventArgs e)
        {
            string tname = "Script"; int c = 1;
            while (File.Exists((SelectedItem as ExplorerTreeViewItem)?.FullPath + "\\" + tname + ".lua"))
            {
                tname = "Script" + c;
                c++;
            }

            tname += ".lua";

            ExplorerTreeViewItem node = AddNode(SelectedItem as ExplorerTreeViewItem, tname, 10, (ImageSource)FindResource("LuaFileIcon"));
            ((ExplorerTreeViewItem) SelectedItem).IsExpanded = true;
            node.IsSelected = true;
            node.Tag = "file";
            node.ContextMenu = fileContextMenu;

            BeginEditTextOnSelected();
            beforeEditingPath = ".lua";
        }

        private void RealtimeFolderUpdater(ExplorerTreeViewItem folder)
        {
            // 在资源管理器中搜索已删除的文件和目录
            foreach (ExplorerTreeViewItem node in folder.Items)
            {
                if (node.Tag.ToString().ToLower().Equals("directory"))
                {
                    if (!Directory.Exists(node.FullPath))
                        (node.Parent as ExplorerTreeViewItem)?.Items.Remove(node);
                }
                else
                {
                    if (!File.Exists(node.FullPath))
                    {
                        if (!(Path.GetExtension(node.FullPath).ToLower().Equals(".cs") ||
                              Path.GetExtension(node.FullPath).ToLower().Equals(".lua") && !node.CanDrag))
                            (node.Parent as ExplorerTreeViewItem)?.Items.Remove(node);
                    }
                }
            }

            // 在资源管理器中搜索新文件：
            foreach (string filename in Directory.GetFiles(folder.FullPath))
            {
                if (!AcceptedExtensions.Contains(Path.GetExtension(filename))) continue;

                bool found = false;
                foreach (ExplorerTreeViewItem node in folder.Items)
                {
                    //Console.Info(filename + "::::" + node.FullPath.ToLower());
                    if (node.FullPath.ToLower().Equals(filename.ToLower()) && node.Tag.ToString().Equals("file"))
                    {
                        found = true;
                        break;
                    }
                }

                // 文件存在，但它不在树视图上？
                if (!found)
                {
                    // add item
                    ExplorerTreeViewItem newNode = AddNode(folder, Path.GetFileName(filename), 10, GetImageSource(Path.GetExtension(filename)));

                    newNode.Tag = "file";
                    newNode.ContextMenu = fileContextMenu;
                }
            }

            // 在资源管理器中搜索新目录：
            foreach (string folderPath in Directory.GetDirectories(folder.FullPath))
            {
                bool found = false;
                foreach (ExplorerTreeViewItem node in folder.Items)
                {
                    if (node.FullPath.ToLower().Equals(folderPath.ToLower()) && node.Tag.ToString().Equals("directory"))
                    {
                        found = true;
                        break;
                    }
                }

                // 目录存在，但它不在树视图上？
                if (!found)
                {
                    // add item
                    ExplorerTreeViewItem _node = AddNode(folder, Path.GetFileName(folderPath), 5, (ImageSource)FindResource("FolderIcon"));
                    _node.ContextMenu = directoryContextMenu;
                    _node.Tag = "directory";

                    // cur目录是否包含任何文件或子目录？
                    if (Directory.GetFiles(folderPath).Count() > 0 ||
                        Directory.GetDirectories(folderPath).Count() > 0)
                    {
                        AddNode(_node, "*", 0);
                        _node.Expanded += Node_Expanded;

                    }

                    _node.Items.SortDescriptions.Clear();
                    _node.Items.SortDescriptions.Add(new SortDescription("PriorityIndex", ListSortDirection.Ascending));
                    _node.Items.SortDescriptions.Add(new SortDescription("Text", ListSortDirection.Ascending));
                }
            }

            foreach (ExplorerTreeViewItem item in folder.Items)
            {
                if (item.PriorityIndex == 5)
                {
                    if ((item.Items.Count > 0 && (item.Items[0] as ExplorerTreeViewItem).Text != "*") || item.Items.Count == 0)
                        RealtimeFolderUpdater(item);
                }
            }
        }

        void deleteItem_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("确定?", "警告", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                // 脚本文件？ 更改.csproj：
                if (Path.GetExtension((SelectedItem as ExplorerTreeViewItem).FullPath).ToLower().Equals(".cs"))
                {
                    string _toRemove = (SelectedItem as ExplorerTreeViewItem).FullPath.Replace(SceneManager.GameProject.ProjectPath + "\\", "");

                    string projectFileName = SceneManager.GameProject.ProjectPath + @"\Scripts.csproj";

                    XmlDocument doc = new XmlDocument();
                    doc.Load(projectFileName);

                    while (doc.GetElementsByTagName("ItemGroup").Count < 2)
                        doc.GetElementsByTagName("Project").Item(0).AppendChild(doc.CreateElement("ItemGroup"));

                    foreach (XmlNode _node in doc.GetElementsByTagName("ItemGroup").Item(1).ChildNodes)
                    {
                        if (_node.Name.ToLower().Equals("compile"))
                        {
                            if (_node.Attributes.GetNamedItem("Include").Value.ToLower().Equals(_toRemove.ToLower()))
                                doc.GetElementsByTagName("ItemGroup").Item(1).RemoveChild(_node);
                        }
                    }

                    doc.Save(projectFileName);
                }

                File.Delete((SelectedItem as ExplorerTreeViewItem).FullPath);
                ((SelectedItem as ExplorerTreeViewItem).Parent as ExplorerTreeViewItem).Items.Remove(SelectedItem);
            }
        }

        void removeItem_Click(object sender, RoutedEventArgs e)
        {
            ExplorerTreeViewItem selected = (SelectedItem as ExplorerTreeViewItem);
            ExplorerTreeViewItem parent = (selected.Parent as ExplorerTreeViewItem);

            string path = selected.FullPath;

            if (selected == null || parent == null) return;

            MessageBoxResult result = MessageBox.Show("您确定要删除此文件夹吗?\n" + path, "警告", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                EditorCommands.DeleteDirectoryRecursively(path);
                parent.Items.Remove(selected);
            }
        }

        void scriptItem_Click(object sender, RoutedEventArgs e)
        {
            string tname = "Script"; int c = 1;
            while (File.Exists((SelectedItem as ExplorerTreeViewItem).FullPath + "\\" + tname + ".cs"))
            {
                tname = "Script" + c;
                c++;
            }

            tname += ".cs";

            ExplorerTreeViewItem node = AddNode(SelectedItem as ExplorerTreeViewItem, tname, 10, (ImageSource)FindResource("CSFileIcon"));
            (SelectedItem as ExplorerTreeViewItem).IsExpanded = true;
            node.IsSelected = true;
            node.Tag = "file";
            node.ContextMenu = fileContextMenu;

            BeginEditTextOnSelected();
            beforeEditingPath = ".cs";
        }

        private void addFromFolder_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "All Files (*.*)|*.*";
            ofd.Multiselect = true;

            var result = ofd.ShowDialog();

            if (result != true) return;

            ExplorerTreeViewItem selected = (SelectedItem as ExplorerTreeViewItem);

            foreach (string filePath in ofd.FileNames)
            {
                string target = selected.FullPath + "\\" + Path.GetFileName(filePath);
                File.Copy(filePath, target, true);

                if (Path.GetExtension(target).ToLower().Equals(".cs"))
                {
                    string _newValue = target.Replace(SceneManager.GameProject.ProjectPath + "\\", "");

                    AddScriptToSlnFile(_newValue, string.Empty);
                }
            }
        }

        private void AddScriptToSlnFile(string newValue, string oldValue)
        {
            string projectFileName = SceneManager.GameProject.ProjectPath + @"\Scripts.csproj";

            XmlDocument doc = new XmlDocument();
            doc.Load(projectFileName);

            XmlNode node = doc.CreateElement("Compile", doc.DocumentElement.NamespaceURI);
            XmlAttribute attribute = doc.CreateAttribute("Include");
            attribute.Value = newValue;
            node.Attributes.Append(attribute);

            while (doc.GetElementsByTagName("ItemGroup").Count < 2)
                doc.GetElementsByTagName("Project").Item(0).AppendChild(doc.CreateElement("ItemGroup"));

            bool replaced = false;
            if (oldValue != string.Empty)
            {
                foreach (XmlNode _node in doc.GetElementsByTagName("ItemGroup").Item(1).ChildNodes)
                {
                    if (_node.Name.ToLower().Equals("compile"))
                    {
                        //Console.Info("val: " + _node.Attributes.GetNamedItem("Include").Value);
                        if (_node.Attributes.GetNamedItem("Include").Value.ToLower().Equals(oldValue.ToLower()))
                        {
                            _node.Attributes.GetNamedItem("Include").Value = newValue;
                            replaced = true;
                        }
                    }
                }
            }

            if (!replaced)
                doc.GetElementsByTagName("ItemGroup").Item(1).AppendChild(node);

            doc.Save(projectFileName);
        }

        void renameItem_Click(object sender, RoutedEventArgs e)
        {
            BeginEditTextOnSelected();
        }

        internal void BeginEditTextOnSelected()
        {
            ItemLostFocus();

            selectedForEditing = (SelectedItem as ExplorerTreeViewItem);

            if (selectedForEditing == null) return;

            TextBox tb = new TextBox();
            tb.Text = selectedForEditing.Text;
            tb.LostFocus += tb_LostFocus;
            tb.KeyDown += tb_KeyDown;
            tb.Focusable = true;

            beforeEditingPath = selectedForEditing.FullPath;
            (selectedForEditing.Header as StackPanel).Children.RemoveAt(1);
            (selectedForEditing.Header as StackPanel).Children.Add(tb);
            selectedForEditing.CanDrag = false;

            tb.Select(0, tb.Text.Length - (Path.GetExtension(tb.Text).Length));
            tb.Focus();
        }

        void tb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (ItemLostFocus())
                    selectedForEditing.CanDrag = true;
            }
        }

        void tb_LostFocus(object sender, RoutedEventArgs e)
        {
            ItemLostFocus();
            selectedForEditing.CanDrag = true;

            ExplorerTreeViewItem parent = (selectedForEditing.Parent as ExplorerTreeViewItem);
            SortNode(parent);
        }

        bool ItemLostFocus()
        {
            if (selectedForEditing != null && selectedForEditing.Parent != null && selectedForEditing.Header != null && (selectedForEditing.Header as StackPanel).Children.Count >= 2
                && (selectedForEditing.Header as StackPanel).Children[1] is TextBox)
            {
                // update the current name:
                string newValue = ((selectedForEditing.Header as StackPanel).Children[1] as TextBox).Text;

                // prevent nameless folder
                if (newValue.Trim() == "")
                    return false;

                // rename item accordingly 
                string destination = Path.Combine((selectedForEditing.Parent as ExplorerTreeViewItem).FullPath, newValue);

                if (File.Exists(destination))
                    return false;

                // trying to change a script name?
                if (Path.GetExtension(beforeEditingPath).ToLower().Equals(".cs"))
                {
                    if (!Path.GetExtension(destination).ToLower().Equals(".cs"))
                    {
                        destination += ".cs";
                        newValue += ".cs";
                    }
                }

                // remove the text box
                (selectedForEditing.Header as StackPanel).Children.RemoveAt(1);
                (selectedForEditing.Header as StackPanel).Children.Add(new TextBlock() { Text = newValue, Margin = new Thickness(MARGIN, 0, 0, 0) });
                selectedForEditing.Text = newValue;

                // The path was changed?
                if (!(beforeEditingPath == destination))
                {
                    if (selectedForEditing.Tag.ToString().ToLower().Equals("directory") && Directory.Exists(beforeEditingPath) &&
                        !Directory.Exists(destination))
                    {
                        Directory.Move(beforeEditingPath, destination);

                        return true;
                    }
                    else if (!File.Exists(destination) && !selectedForEditing.Tag.ToString().ToLower().Equals("directory")) // tag = "file"
                    {
                        try
                        {
                            if (File.Exists(beforeEditingPath))
                            {
                                File.Move(beforeEditingPath, destination);

                                // if .csproj 
                                if (Path.GetExtension(beforeEditingPath).ToLower().Equals(".csproj"))
                                    UserPreferences.Instance.ProjectCsProjFilePath = destination;

                                // if .sln 
                                if (Path.GetExtension(beforeEditingPath).ToLower().Equals(".sln"))
                                    UserPreferences.Instance.ProjectSlnFilePath = destination;
                            }
                            else if (Path.GetExtension(beforeEditingPath).ToLower().Equals(".cs"))
                            {
                                string script = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\Project Templates\Samples\DefaultComponent.cs");
                                script = script.Replace("NAME", destination.Remove(0, destination.LastIndexOf('\\') + 1).Replace(".cs", string.Empty));

                                File.WriteAllText(destination, script);
                            }
                            else if
                                (Path.GetExtension(beforeEditingPath).ToLower().Equals(".lua"))
                            {
                                string script = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\Project Templates\Samples\DefaultComponent.lua");
                                script = script.Replace("NAME", destination.Remove(0, destination.LastIndexOf('\\') + 1).Replace(".lua", string.Empty));

                                File.WriteAllText(destination, script);
                            }
                            else if (Path.GetExtension(beforeEditingPath).ToLower().Equals(".visual"))
                            {
                                File.WriteAllText(destination, "");
                            }

                            if (Path.GetExtension(beforeEditingPath).ToLower().Equals(".cs"))
                            {
                                string _newValue = selectedForEditing.FullPath.Replace(SceneManager.GameProject.ProjectPath + "\\", "");
                                string _oldValue = beforeEditingPath.Replace(SceneManager.GameProject.ProjectPath + "\\", "");
                                //MessageBox.Show(_newValue + ";\n" + _oldValue);

                                AddScriptToSlnFile(_newValue, _oldValue);
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error");
                        }
                    }

                    return true;
                }
            }

            return false;
        }

        private void openFolderItem_Click(object sender, RoutedEventArgs e)
        {
            ExplorerTreeViewItem selected = (SelectedItem as ExplorerTreeViewItem);

            if (selected == null) return;

            ExplorerTreeViewItem parent = (selected.Parent as ExplorerTreeViewItem);
            string fullPath = string.Empty;

            if (parent == null || selected.Tag.ToString().ToLower().Trim() == "directory") // parent = root
                fullPath = selected.FullPath;
            else // selected = file
                fullPath = parent.FullPath;

            ProcessStartInfo runExplorer = new ProcessStartInfo();
            runExplorer.FileName = "explorer.exe";
            runExplorer.Arguments = fullPath;
            Process.Start(runExplorer);
        }

        private void copyRelativePathItem_Click(object sender, RoutedEventArgs e)
        {
            ExplorerTreeViewItem selected = (SelectedItem as ExplorerTreeViewItem);
            string relativePath = selected.FullPath.Replace(SceneManager.GameProject.ProjectPath + "\\", "");
            Clipboard.SetDataObject(relativePath);
        }

        void copyFullPath_Click(object sender, RoutedEventArgs e)
        {
            ExplorerTreeViewItem selected = (SelectedItem as ExplorerTreeViewItem);
            Clipboard.SetDataObject(selected.FullPath);
        }

        void openItem_Click(object sender, RoutedEventArgs e)
        {
            HandleOpenFile((SelectedItem as ExplorerTreeViewItem));
        }

        void createFolderItem_Click(object sender, RoutedEventArgs e)
        {
            ExplorerTreeViewItem selected = (SelectedItem as ExplorerTreeViewItem);
            if (selected == null) return;

            string path = selected.FullPath + "\\New Folder";
            CreateDirectory(path);
        }

        private void CreateDirectory(string path)
        {
            if (Directory.Exists(path))
            {
                int c = 1;
                while (Directory.Exists(path + c)) c++;
                path += c;
            }

            Directory.CreateDirectory(path);
            ExplorerTreeViewItem newnode = AddNode(null, Path.GetFileName(path), 5, (ImageSource)FindResource("FolderIcon"));
            newnode.ContextMenu = directoryContextMenu;
            newnode.Tag = "directory";

            ExplorerTreeViewItem selected = (SelectedItem as ExplorerTreeViewItem);
            selected.Items.Add(newnode);
            selected.Items.Refresh();
            selected.IsExpanded = true;
        }

        void createSceneItem_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedItem as ExplorerTreeViewItem == null)
            {
                MessageBox.Show("请先选中Scene节点");
                return;
            }

            string fn = CreateFile((SelectedItem as ExplorerTreeViewItem).FullPath + "\\GameScene.scene", FileTemplate.None, (ImageSource)FindResource("SceneIcon"));
            SceneManager.CreateScene(fn);
        }

        private string CreateFile(string path, FileTemplate template, ImageSource imageSource = null)
        {
            if (SelectedItem == null) return string.Empty;

            path = FileHelper.CreateFile(path, template);

            // Add the new tree node to the TreeView
            TreeViewItem _node = AddNode(null, Path.GetFileName(path), 10, imageSource);
            _node.Tag = "file";
            _node.ContextMenu = fileContextMenu;

            if (((SelectedItem as ExplorerTreeViewItem).Items.Count > 0 &&
                 !((SelectedItem as ExplorerTreeViewItem).Items[0] as ExplorerTreeViewItem).Text.Equals("*")) ||
                (SelectedItem as ExplorerTreeViewItem).Items.Count == 0)
            {
                (SelectedItem as ExplorerTreeViewItem).Items.Add(_node);
            }

            (SelectedItem as ExplorerTreeViewItem).IsExpanded = true;

            return path;
        }

        void SortNode(ExplorerTreeViewItem node)
        {
            node.Items.SortDescriptions.Clear();
            node.Items.SortDescriptions.Add(new SortDescription("PriorityIndex", ListSortDirection.Ascending));
            node.Items.SortDescriptions.Add(new SortDescription("Text", ListSortDirection.Ascending));
            node.Items.Refresh();
            node.IsExpanded = true;
        }

        private void FillChildNodes(ExplorerTreeViewItem node)
        {
            try
            {
                DirectoryInfo[] dirs = new DirectoryInfo(node.FullPath).GetDirectories();

                // Add current directory Files:
                string[] files = Directory.GetFiles(node.FullPath);
                foreach (string file in files)
                {
                    if (AcceptedExtensions.Contains(Path.GetExtension(file.ToLower())))
                    {
                        ExplorerTreeViewItem newNode = AddNode(node, Path.GetFileName(file), 10, GetImageSource(Path.GetExtension(file)));

                        newNode.Tag = "file";
                        newNode.ContextMenu = fileContextMenu;
                    }
                }

                node.Items.Refresh();

                // Add Sub Directories:
                foreach (DirectoryInfo dir in dirs)
                {
                    ExplorerTreeViewItem _node = AddNode(node, Path.GetFileName(dir.Name), 5, (ImageSource)FindResource("FolderIcon"));
                    _node.ContextMenu = directoryContextMenu;
                    _node.Tag = "directory";

                    // cur目录是否包含任何文件或子目录？
                    if (Directory.GetFiles(dir.FullName).Count() > 0 ||
                        Directory.GetDirectories(dir.FullName).Count() > 0)
                    {
                        AddNode(_node, "*", 0);
                        _node.Expanded += Node_Expanded;

                    }

                    _node.Items.SortDescriptions.Clear();
                    _node.Items.SortDescriptions.Add(new SortDescription("PriorityIndex", ListSortDirection.Ascending));
                    _node.Items.SortDescriptions.Add(new SortDescription("Text", ListSortDirection.Ascending));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        void Node_Expanded(object sender, RoutedEventArgs e)
        {
            ExplorerTreeViewItem item = (ExplorerTreeViewItem)sender;

            if (item.Items.Count > 0 && (item.Items[0] as ExplorerTreeViewItem).Text.Equals("*"))
            {
                item.Items.RemoveAt(0);

                FillChildNodes(item);
            }
        }

        private ImageSource GetImageSource(string extension)
        {
            switch (extension.ToLower())
            {
                case ".png":
                case ".jpeg":
                case ".jpg":
                case ".bmp":
                case ".gif":
                    return (ImageSource)FindResource("ImageIcon");
                case ".txt":
                case ".ini":
                    return (ImageSource)FindResource("TextFileIcon");
                //case ".clengine":
                //    index = 4;
                //    break;
                case ".cs":
                    return (ImageSource)FindResource("CSFileIcon");
                case ".lua":
                    return (ImageSource) FindResource("LuaFileIcon");
                case ".scene":
                    return (ImageSource)FindResource("SceneIcon");
                case ".sln":
                    return (ImageSource)FindResource("SolutionIcon");
                default:
                    return (ImageSource)FindResource("FileIcon");
            }
        }

        private void HandleOpenFile(ExplorerTreeViewItem explorerTreeViewItem)
        {
            if (explorerTreeViewItem.CanDrag == false)
                return;

            // Optional: implement open file behaviour
            string extension = Path.GetExtension(explorerTreeViewItem.Text).ToLower().Trim();

            // is directory?
            if (extension.Equals(string.Empty)) return;

            if (!AcceptedExtensions.Contains(extension)) return;

            switch (extension)
            {
                case ".clengine":
                    //EditorHandler.ChangeSelectedObject(SceneManager.GameProject);
                    break;
                case ".scene":
                    //System.Threading.ThreadPool.QueueUserWorkItem(o => SceneManager.LoadScene(node.FullPath));
                    if (SceneManager.ActiveScene != null && MessageBox.Show("是否要保存当前场景?", "警告", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        SceneManager.SaveActiveScene();
                    }

                    SceneManager.LoadScene(explorerTreeViewItem.FullPath, true);
                    EditorHandler.ChangeSelectedObject(SceneManager.ActiveScene);
                    EditorHandler.SelectedGameObjects = new List<GameObject>();
                    EditorHandler.SceneTreeView.CreateView();
                    //EditorHandler.EditorSplitterContainer.Panel2Collapsed = true;
                    EditorHandler.ChangeSelectedObjects();
                    break;
                case ".visual":
                    new NodeScriptWindow().ShowDialog();
                    break;
                case ".lua":
                case ".cs":
                    // TODO: 添加打开.cs文件的行为
                    string projectPath = SceneManager.GameProject.ProjectPath;

                    #region SharpDevelop and Xamarin tries

                    // MessageBox.Show(@"C:\Program Files (x86)\SharpDevelop\4.3\bin\SharpDevelop.exe :" + SceneManager.GameProject.ProjectPath + "\\Scripts.sln" + " : " + explorerTreeViewItem.FullPath);
                    //C:\Program Files (x86)\SharpDevelop\4.3\bin\SharpDevelop.exe    
                    // C:\Program Files (x86)\Xamarin Studio\bin\XamarinStudio.exe
                    // if (sharpDevelop == null || sharpDevelop.HasExited)
                    // {
                    //     ProcessStartInfo pinfo = new ProcessStartInfo();
                    //     pinfo.FileName = @"C:\Program Files (x86)\Xamarin Studio\bin\XamarinStudio.exe";
                    //     pinfo.Arguments = "-nologo " + "Scripts.sln " + CHelper.MakeExclusiveRelativePath(SceneManager.GameProject.ProjectPath, explorerTreeViewItem.FullPath);
                    //     pinfo.WorkingDirectory = SceneManager.GameProject.ProjectPath;
                    //     //pinfo.RedirectStandardInput = true;
                    //     //pinfo.UseShellExecute = false;
                    //     sharpDevelop = Process.Start(pinfo);
                    // }
                    // else
                    // {
                    //     SetForegroundWindow(sharpDevelop.MainWindowHandle);
                    //     //sharpDevelop.

                    //     //sharpDevelop.StandardInput.Info(CHelper.MakeExclusiveRelativePath(SceneManager.GameProject.ProjectPath, explorerTreeViewItem.FullPath));

                    //     //ProcessStartInfo pinfo = new ProcessStartInfo();
                    //     //pinfo.FileName = @"C:\Program Files (x86)\SharpDevelop\4.3\bin\SharpDevelop.exe";
                    //     //pinfo.Arguments = CHelper.MakeExclusiveRelativePath(SceneManager.GameProject.ProjectPath, explorerTreeViewItem.FullPath);
                    //     //sharpDevelop.StartInfo = pinfo;
                    //     //sharpDevelop.Start();
                    //     //using (StreamWriter sw = new StreamWriter(sharpDevelop.StandardInput))
                    //     //{
                    //     //    if (sw.BaseStream.CanWrite)
                    //     //    {
                    //     //        sw.Info(CHelper.MakeExclusiveRelativePath(SceneManager.GameProject.ProjectPath, explorerTreeViewItem.FullPath));
                    //     //    }
                    //     //}
                    //}
                    //break;

                    // Process.Start(@"C:\Program Files (x86)\SharpDevelop\4.3\bin\SharpDevelop.exe " + SceneManager.GameProject.ProjectPath + "\\Scripts.sln");

                    #endregion

                    // check if default editor is lime
                    if (Properties.Settings.Default.DefaultScriptEditor.ToLower().Equals("lime"))
                    {
                        LimeScriptEditor.OpenScript(explorerTreeViewItem.FullPath);
                    }
                    else if (Properties.Settings.Default.DefaultScriptEditor.ToLower().Equals("none"))
                    {
                        Process.Start(explorerTreeViewItem.FullPath);
                    }
                    // else, try visual studio
                    else if (EditorUtils.CheckVisualStudioExistance(Properties.Settings.Default.DefaultScriptEditor))
                    {
                        try
                        {
                            string rf = string.Empty;
                            string editor = Properties.Settings.Default.DefaultScriptEditor;
                            switch (editor)
                            {
                                case "VisualStudio2019":
                                    rf = "VisualStudio.DTE.16.0";
                                    break;
                                case "VisualStudio2017":
                                    rf = "VisualStudio.DTE.15.0";
                                    break;
                                case "VisualStudio2015":
                                    rf = "VisualStudio.DTE.14.0";
                                    break;
                                case "VisualStudio2013":
                                    rf = "VisualStudio.DTE.12.0";
                                    break;
                                case "VisualStudio2012":
                                    rf = "VisualStudio.DTE.11.0";
                                    break;
                                case "VisualStudio2010":
                                    rf = "VisualStudio.DTE.10.0";
                                    break;
                                    //case "CSExpress2010":
                                    //    rf = "VCSExpress.DTE.10.0";
                                    //    break;
                            }

                            // 试图找到解决方案
                            EnvDTE.DTE tmp2DTE;
                            if (EditorCommands.TryToRestoreSolution(out tmp2DTE))
                            {
                                // 如果找到解决方案，则指定dte
                                dte = tmp2DTE as EnvDTE80.DTE2;
                            }

                            /*
                            // 如果实例存在，请尝试使其可见
                            if (dte != null)
                            {
                                try
                                {
                                    // attempts to access main window and make it visible
                                    dte.MainWindow.Visible = true;
                                }
                                catch (Exception)
                                {
                                    // if unable, set instance to null in order to create one.
                                    dte = null;
                                }
                            }*/

                            // 如果没有实例，请创建一个
                            if (dte == null)
                            {
                                // 存储指定类型的visual studio实例（2015,2013,2012,2010 ...）
                                Type type = Type.GetTypeFromProgID(rf);
                                // 创建该类型的实例
                                dte = (EnvDTE80.DTE2)Activator.CreateInstance(type);

                                // 创建一个独特的guid并将其设置为dte主窗口标题，以便我们稍后可以搜索它
                                string uniqueGuid = Guid.NewGuid().ToString();

                                // 使用user32.dll SetWindowText更改主窗口标题。 无法使用'set'直接更改»dte.MainWindow.Caption =“x”
                                SetWindowText(new IntPtr(dte.MainWindow.HWnd), uniqueGuid);

                                // 使窗口可见
                                dte.MainWindow.Visible = true;

                                // 获取每个visual studio进程（devenv.exe）
                                Process[] pVSList = Process.GetProcessesByName("DEVENV");
                                // 重置visual studio实例PID
                                EditorCommands.VisualStudioInstancePID = 0;
                                // 遍历每个进程并检查其标题
                                foreach (Process pVS in pVSList)
                                {
                                    // 如果标题是我们之前存储的Guid，则存储它
                                    if (uniqueGuid != string.Empty && pVS.MainWindowTitle.Equals(uniqueGuid))
                                    {
                                        // 存储visual studio实例PID
                                        EditorCommands.VisualStudioInstancePID = pVS.Id;
                                        break;
                                    }
                                }

                                // 打开工程
                                dte.Solution.Open(UserPreferences.Instance.ProjectSlnFilePath);
                            }

                            // 打开文档，将标题（我们设置为Guid）更改为其名称
                            dte.Documents.Open(explorerTreeViewItem.FullPath);
                        }
                        catch (Exception)
                        {
                            Properties.Settings.Default.DefaultScriptEditor = "None";
                            Properties.Settings.Default.Save();

                            MessageBox.Show("您没有选择默认的脚本编辑器！\n在“设置”窗口中选择一个，或者使用其他IDE打开脚本解决方案（.sln文件）来管理脚本", "错误!");
                        }

                    }
                    else
                    {
                        MessageBox.Show("您没有选择默认的脚本编辑器.\n在“设置”窗口中选择一个，或者使用其他IDE打开脚本解决方案（.sln文件）来管理脚本", "错误!");
                    }
                    break;
                case ".sln":
                    // 检查文件是否是有效的解决方案文件（例如，可以放置一个扩展名为.sln的虚拟文件并尝试打开它。在这种情况下，有无需继续）
                    bool validSolution = UserPreferences.Instance.ProjectSlnFilePath.Equals(explorerTreeViewItem.FullPath);

                    // 如果它是有效的解决方案文件，请检查是否已打开解决方案以避免重复实例
                    EnvDTE.DTE tmpDTE;
                    if (validSolution && !EditorCommands.TryToRestoreSolution(out tmpDTE))
                    {
                        // date为null，因为没有找到visual studio解决方案实例
                        //启动解决方案进程
                        Process vsProcess = Process.Start(UserPreferences.Instance.ProjectSlnFilePath);
                        // 存储其ID
                        EditorCommands.VisualStudioInstancePID = vsProcess.Id;

                    }
                    // 如果它不是有效的解决方案，请弹出警告用户的消息
                    else if (!validSolution)
                    {
                        MessageBox.Show("Invalid solution file.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    break;
                default:
                    // 默认行为，尝试使用此类文件的默认用户开放：
                    try
                    {
                        Process.Start(explorerTreeViewItem.FullPath);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    break;
            }
        }

        [DllImport("user32.dll")]
        private static extern bool SetWindowText(IntPtr hWnd, string lpString);
    }
}
