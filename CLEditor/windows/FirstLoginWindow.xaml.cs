﻿using System;
using System.Windows;
using CLEngine.Core;

namespace CLEngine.Editor.windows
{
    /// <summary>
    /// FirstLoginWindow.xaml 的交互逻辑
    /// </summary>
    public partial class FirstLoginWindow : Window
    {
        private bool canClose = false;

        public FirstLoginWindow()
        {
            InitializeComponent();
        }

        private void skipBtn_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.UserEmail = CHelper.EncryptMD5(DateTime.Now.ToString() + CHelper.RandomNumber(1, 1000));
            Properties.Settings.Default.Save();

            canClose = true;
            this.Close();
        }

        private void continueBtn_Click(object sender, RoutedEventArgs e)
        {
            if (IsValidEmail(emailTxtBox.Text))
            {
                Properties.Settings.Default.UserEmail = emailTxtBox.Text;
                Properties.Settings.Default.Save();

                canClose = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("邮箱不正确", "错误");
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!canClose)
                Application.Current.Shutdown();


        }

        private bool IsValidEmail(string strIn)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(strIn,
                @"^(?("")(""[^""]+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");
        }
    }
}
