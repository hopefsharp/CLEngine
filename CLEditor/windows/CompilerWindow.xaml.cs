﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using CLEngine.Editor.core;
using CLEngine.Editor.model;
using CLEngine.Core;
using Timer = System.Windows.Forms.Timer;

namespace CLEngine.Editor.windows
{
    /// <summary>
    /// CompilerWindow.xaml 的交互逻辑
    /// </summary>
    public partial class CompilerWindow
    {
        public bool Result { get; set; }

        public CompilerWindow()
        {
            InitializeComponent();
            ErrorDataGrid.Visibility = Visibility.Collapsed;

            ErrorDataGrid.MouseDoubleClick += ErrorDataGrid_MouseDoubleClick;
        }

        void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender is DataGridRow row)
            {
                if (row.Item is ErrorLog item)
                    LimeScriptEditor.Instance.OpenScriptAndSeek(
                        SceneManager.GameProject.ProjectPath + @"\" + item.FileName, item.LineNumber,
                        item.ColumnNumber);
                Close();
            }
        }

        void ErrorDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //if (sender != null)
            //{
            //    DataGrid grid = sender as DataGrid;
            //    if (grid != null && grid.SelectedItems != null && grid.SelectedItems.Count == 1)
            //    {
            //        //DataGridRow dgr = grid.ItemContainerGenerator.ContainerFromItem(grid.SelectedItem) as DataGridRow;
            //        ErrorLog item = grid.SelectedItem as ErrorLog;

            //        LimeScriptEditor.Instance.OpenScriptAndSeek(SceneManager.GameProject.ProjectPath + @"\" + item.FileName, item.LineNumber, item.ColumnNumber);
            //    }
            //}
        }

        public void SafeClose()
        {
            // 确保我们在UI线程上运行
            //if (this.InvokeRequired)
            //{
            //    BeginInvoke(new Action(SafeClose));
            //    return;
            //}

            if (Dispatcher.CheckAccess())
            {
                Dispatcher.BeginInvoke(new Action(SafeClose));
                return;
            }

            // 现在我们正在UI线程上运行，关闭表单
            // this.DialogResult = System.Windows.Forms.DialogResult.Yes;
            DialogResult = true;
            Close();

        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            if (ScriptsBuilder.ReloadScripts())
            {
                // Sucess:
                progresslbl.Content = "载入中...";

                var timer = new Timer();
                timer.Interval = 5;
                timer.Tick += timer_Tick;
                timer.Enabled = true;

                Result = true;
            }
            else
            {
                progressBar.Value = 80;
                progresslbl.Content = "失败!";
                Result = false;
                EditorCommands.ShowOutputMessage("编译脚本时出错");

                ErrorDataGrid.Visibility = Visibility.Visible;
                ErrorDataGrid.ItemsSource = ScriptsBuilder.Logger.Errors;
            }
        }

        void timer_Tick(object sender, EventArgs e)
        {
            progressBar.Value += CHelper.RandomNumber(4, 5);

            if (progressBar.Value >= 100)
            {
                progresslbl.Content = "Success!";
                ((Timer) sender).Enabled = false;
                Close();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            progresslbl.Content = "收集数据并设置参数以进行编译...";
        }
    }
}
