﻿using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Controls;
using CLEngine.Editor.core;
using CLEngine.Core;
using CLEngine.Editor.model;

namespace CLEngine.Editor.windows
{
    /// <summary>
    /// SettingsWindow.xaml 的交互逻辑
    /// </summary>
    [SuppressMessage("ReSharper", "StringLiteralTypo")]
    [SuppressMessage("ReSharper", "CommentTypo")]
    public partial class SettingsWindow
    {
        private readonly IniFile iniSettings = new IniFile(SceneManager.GameProject.ProjectPath + "\\settings.ini");

        public SettingsWindow()
        {
            InitializeComponent();
            propertyGrid.Tag = "clengine_general";
            propertyGrid.SelectedObject = LoadProperties("clengine_general");
        }

        private ISettingsChannelA LoadProperties(string _ref)
        {
            ISettingsChannelA settings = null;

            switch (_ref)
            {
                case "clengine_general":
                    settings = new CGeneralSettingsDynamic();
                    ((CGeneralSettingsDynamic) settings).AutomaticProjectLoad = Properties.Settings.Default.LoadLastProject;
                    try
                    {
                        ((CGeneralSettingsDynamic) settings).ScriptEditors = (CGeneralSettingsDynamic.ScriptingEditors)Enum.Parse(typeof(CGeneralSettingsDynamic.ScriptingEditors), Properties.Settings.Default.DefaultScriptEditor, true);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    ((CGeneralSettingsDynamic) settings).StartOnFullScreen = Properties.Settings.Default.StartOnFullScreen;
                    ((CGeneralSettingsDynamic) settings).ShowDebugView = Properties.Settings.Default.ShowDebugView;
                    ((CGeneralSettingsDynamic) settings).ReduceConsumption = Properties.Settings.Default.ReduceConsumption;
                    break;

                case "clengine_tileset":
                    settings = new CTilesetSettingsDynamic();
                    ((CTilesetSettingsDynamic) settings).HighlightActiveTileset = Properties.Settings.Default.HighlightActiveTileset;
                    break;

                case "game_general":
                    settings = new GameGeneralSettingsDynamic();
                    ((GameGeneralSettingsDynamic) settings).ProjectName = SceneManager.GameProject.ProjectName;
                    ((GameGeneralSettingsDynamic) settings).CatchError =
                        SceneManager.GameProject.EditorSettings.CatchError;
                    ((GameGeneralSettingsDynamic) settings).NetworkCode = UserPreferences.Instance.NetworkCode;
                    break;

                case "game_grid":
                    settings = new GameGridSettingsDynamic();
                    ((GameGridSettingsDynamic) settings).GridSpacing = SceneManager.GameProject.EditorSettings.GridSpacing;
                    ((GameGridSettingsDynamic) settings).GridThickness = SceneManager.GameProject.EditorSettings.GridThickness;
                    ((GameGridSettingsDynamic) settings).GridColor = SceneManager.GameProject.EditorSettings.GridColor;
                    ((GameGridSettingsDynamic) settings).DisplayLines = SceneManager.GameProject.EditorSettings.GridNumberOfLines;
                    break;

                case "game_debug":
                    settings = new GameDebugDynamic();
                    ((GameDebugDynamic) settings).ShowConsole = iniSettings.IniReadValue("Console", "Visible").ToLower().Trim().Equals("true");
                    ((GameDebugDynamic) settings).Attach = Properties.Settings.Default.AttachVisualStudio;

                    try
                    {
                        ((GameDebugDynamic) settings).DebugMode = (GameDebugDynamic.DebugModes)Enum.Parse(typeof(GameDebugDynamic.DebugModes), (SceneManager.GameProject.Debug ? "Debug" : "Release"), true);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }

                    break;

                case "game_screen":
                    settings = new GameScreenDynamic();
                    ((GameScreenDynamic) settings).MouseVisible = iniSettings.IniReadValue("Mouse", "Visible").ToLower().Trim().Equals("true");
                    ((GameScreenDynamic) settings).StartOnFullScreen = iniSettings.IniReadValue("Window", "StartFullScreen").ToLower().Trim().Equals("true");
                    ((GameScreenDynamic) settings).ScreenWidth = SceneManager.GameProject.Settings.ScreenWidth;
                    ((GameScreenDynamic) settings).ScreenHeight = SceneManager.GameProject.Settings.ScreenHeight;
                    //(settings as GameScreenDynamic).VSync = SceneManager.GameProject.ProjectSettings.VSyncEnabled;
                    break;
            }

            return settings;
        }

        private void SaveCurrent()
        {
            if (propertyGrid.Tag == null) return;

            switch (propertyGrid.Tag.ToString())
            {
                case "clengine_general":
                    Properties.Settings.Default.LoadLastProject = ((CGeneralSettingsDynamic) propertyGrid.SelectedObject).AutomaticProjectLoad;
                    Properties.Settings.Default.StartOnFullScreen = ((CGeneralSettingsDynamic) propertyGrid.SelectedObject).StartOnFullScreen;
                    Properties.Settings.Default.ShowDebugView = ((CGeneralSettingsDynamic) propertyGrid.SelectedObject).ShowDebugView;
                    Properties.Settings.Default.ReduceConsumption = ((CGeneralSettingsDynamic) propertyGrid.SelectedObject).ReduceConsumption;
                    string appName = ((CGeneralSettingsDynamic) propertyGrid.SelectedObject).ScriptEditors.ToString();

                    EditorUtils.StoreInstalledApplications();

                    //bool isInstalled = false;
                    //foreach (string name in EditorUtils.InstalledApps.Keys)
                    //{
                    //    if (name.Replace(" ", "").ToLower().Contains(appName.ToLower()))
                    //    {
                    //        isInstalled = true;
                    //        MessageBox.Show("Found::::" + EditorUtils.InstalledApps[name]);
                    //        //break;
                    //    }
                    //}

                    //C:\Program Files (x86)\SharpDevelop\4.3\bin\SharpDevelop.exe    
                    // C:\Program Files (x86)\Xamarin Studio\bin\XamarinStudio.exe

                    //Process ide;
                    //    ProcessStartInfo pinfo = new ProcessStartInfo();
                    //    pinfo.FileName = "SharpDevelop.exe";
                    //    pinfo.WorkingDirectory = SceneManager.GameProject.ProjectPath;
                    //    pinfo.UseShellExecute = true;
                    //    ide = Process.Start(pinfo);

                    if (appName.ToLower().Equals("lime"))
                    {
                        Properties.Settings.Default.DefaultScriptEditor = ((CGeneralSettingsDynamic) propertyGrid.SelectedObject)?.ScriptEditors.ToString();
                    }

                    if (((CGeneralSettingsDynamic) propertyGrid.SelectedObject)?.ScriptEditors.ToString().ToLower() != "lime")
                        if (EditorUtils.CheckVisualStudioExistance(((CGeneralSettingsDynamic) propertyGrid.SelectedObject)?.ScriptEditors.ToString()))
                            Properties.Settings.Default.DefaultScriptEditor = ((CGeneralSettingsDynamic) propertyGrid.SelectedObject)?.ScriptEditors.ToString();
                        else
                            MessageBox.Show("您没有选定的Visual Studio IDE");
                    else
                        Properties.Settings.Default.DefaultScriptEditor = ((CGeneralSettingsDynamic) propertyGrid.SelectedObject).ScriptEditors.ToString();


                    Properties.Settings.Default.Save();
                    break;

                case "clengine_tileset":
                    Properties.Settings.Default.HighlightActiveTileset = ((CTilesetSettingsDynamic) propertyGrid.SelectedObject).HighlightActiveTileset;
                    break;

                case "game_general":
                    SceneManager.GameProject.ProjectName = (propertyGrid.SelectedObject as GameGeneralSettingsDynamic)?.ProjectName;
                    SceneManager.GameProject.EditorSettings.CatchError = ((GameGeneralSettingsDynamic) propertyGrid.SelectedObject).CatchError;
                    UserPreferences.Instance.NetworkCode =
                        (propertyGrid.SelectedObject as GameGeneralSettingsDynamic)?.NetworkCode;
                    break;

                case "game_grid":
                    SceneManager.GameProject.EditorSettings.GridSpacing = ((GameGridSettingsDynamic) propertyGrid.SelectedObject).GridSpacing;
                    SceneManager.GameProject.EditorSettings.GridThickness = ((GameGridSettingsDynamic) propertyGrid.SelectedObject).GridThickness;
                    SceneManager.GameProject.EditorSettings.GridColor = ((GameGridSettingsDynamic) propertyGrid.SelectedObject).GridColor;
                    SceneManager.GameProject.EditorSettings.GridNumberOfLines = ((GameGridSettingsDynamic) propertyGrid.SelectedObject).DisplayLines;
                    break;

                case "game_debug":
                    iniSettings.IniWriteValue("Console", "Visible", (propertyGrid.SelectedObject as GameDebugDynamic)?.ShowConsole.ToString());
                    Properties.Settings.Default.AttachVisualStudio = ((GameDebugDynamic) propertyGrid.SelectedObject).Attach;
                    Properties.Settings.Default.Save();
                    SceneManager.GameProject.Debug = ((GameDebugDynamic) propertyGrid.SelectedObject).DebugMode == GameDebugDynamic.DebugModes.Debug;
                    break;

                case "game_screen":
                    iniSettings.IniWriteValue("Mouse", "Visible", (propertyGrid.SelectedObject as GameScreenDynamic)?.MouseVisible.ToString());
                    iniSettings.IniWriteValue("Window", "StartFullScreen", (propertyGrid.SelectedObject as GameScreenDynamic)?.StartOnFullScreen.ToString());
                    SceneManager.GameProject.Settings.ScreenWidth = ((GameScreenDynamic) propertyGrid.SelectedObject).ScreenWidth;
                    SceneManager.GameProject.Settings.ScreenHeight = ((GameScreenDynamic) propertyGrid.SelectedObject).ScreenHeight;
                    //SceneManager.GameProject.ProjectSettings.VSyncEnabled = (propertyGrid.SelectedObject as GameScreenDynamic).VSync;
                    break;
            }
        }

        private void ProjectsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((ProjectsListBox.SelectedItem as ListBoxItem)?.Tag == null) return;

            SaveCurrent();

            propertyGrid.Tag = ((ListBoxItem) ProjectsListBox.SelectedItem).Tag.ToString();
            propertyGrid.SelectedObject = LoadProperties(propertyGrid.Tag.ToString());
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            SaveCurrent();
        }
    }

    interface ISettingsChannelA { }

    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "ConvertToAutoProperty")]
    [SuppressMessage("ReSharper", "ArrangeAccessorOwnerBody")]
    class CGeneralSettingsDynamic : ISettingsChannelA
    {
        public enum ScriptingEditors
        {
	        None,
	        Lime,
	        VisualStudio2019,
	        VisualStudio2017,
	        VisualStudio2015,
	        VisualStudio2013,
	        VisualStudio2012,
	        VisualStudio2010
        } //CSExpress2010

        private bool automaticProjectLoad;
        private bool startOnFullScreen;
        private bool showDebugView;
        private bool reduceConsumption;
        private ScriptingEditors scriptEditors;

        [Category("通用")]
        [DisplayName("显示调试视图")]
        public bool ShowDebugView
        {
            get { return showDebugView; }
            set { showDebugView = value; }
        }

        [Category("通用")]
        [DisplayName("降低CPU消耗")]
        public bool ReduceConsumption
        {
            get { return reduceConsumption; }
            set { reduceConsumption = value; }
        }

        [Category("通用")]
        [DisplayName("全屏启动")]
        public bool StartOnFullScreen
        {
            get { return startOnFullScreen; }
            set { startOnFullScreen = value; }
        }

        [Category("通用")]
        [DisplayName("脚本编辑器")]
        public ScriptingEditors ScriptEditors
        {
            get { return scriptEditors; }
            set { scriptEditors = value; }
        }

        [Category("通用")]
        [DisplayName("在开始时加载上一个项目")]
        public bool AutomaticProjectLoad
        {
            get { return automaticProjectLoad; }
            set { automaticProjectLoad = value; }
        }
    }

    [SuppressMessage("ReSharper", "StringLiteralTypo")]
    [SuppressMessage("ReSharper", "ConvertToAutoProperty")]
    [SuppressMessage("ReSharper", "ArrangeAccessorOwnerBody")]
    class CTilesetSettingsDynamic : ISettingsChannelA
    {
        private bool highlightActiveTileset;

        [Category("Tileset")]
        [DisplayName("突出显示活动Tileset")]
        public bool HighlightActiveTileset
        {
            get { return highlightActiveTileset; }
            set { highlightActiveTileset = value; }
        }
    }

    [SuppressMessage("ReSharper", "ConvertToAutoProperty")]
    [SuppressMessage("ReSharper", "ArrangeAccessorOwnerBody")]
    class GameGeneralSettingsDynamic : ISettingsChannelA
    {
        private string projectName;

        [Category("通用")]
        [DisplayName("工程名")]
        public string ProjectName
        {
            get { return projectName; }
            set { projectName = value; }
        }

        private bool catchError;

        [Category("通用")]
        [DisplayName("捕获错误")]
        public bool CatchError
        {
            get { return catchError; }
            set { catchError = value; }
        }

        private string networkCode;

        [Category("网络")]
        [DisplayName("网络通行证")]
        public string NetworkCode
        {
            get { return networkCode; }
            set { networkCode = value; }
        }
    }

    [SuppressMessage("ReSharper", "ConvertToAutoProperty")]
    [SuppressMessage("ReSharper", "ArrangeAccessorOwnerBody")]
    class GameGridSettingsDynamic : ISettingsChannelA
    {
        private int gridSpacing;
        private int gridThickness;
        private int displayLines;

        private Microsoft.Xna.Framework.Color gridColor;

        [Category("网格")]
        [DisplayName("网格间距")]
        public int GridSpacing
        {
            get { return gridSpacing; }
            set { gridSpacing = value; }
        }

        [Category("网格")]
        [DisplayName("网格厚度")]
        public int GridThickness
        {
            get { return gridThickness; }
            set { gridThickness = value; }
        }

        [Category("网格")]
        [DisplayName("显示行")]
        public int DisplayLines
        {
            get { return displayLines; }
            set { displayLines = value; }
        }

        [Category("网格")]
        [DisplayName("网格颜色")]
        public Microsoft.Xna.Framework.Color GridColor
        {
            get { return gridColor; }
            set { gridColor = value; }
        }
    }

    [SuppressMessage("ReSharper", "ArrangeAccessorOwnerBody")]
    [SuppressMessage("ReSharper", "ConvertToAutoProperty")]
    class GameDebugDynamic : ISettingsChannelA
    {
        public enum DebugModes { Debug, Release }

        private DebugModes _debugMode;
        private bool _showConsole;
        private bool _attach;

        [Category("调试")]
        [DisplayName("调试模式")]
        public DebugModes DebugMode
        {
            get { return _debugMode; }
            set { _debugMode = value; }
        }

        [Category("调试")]
        [DisplayName("显示控制台")]
        public bool ShowConsole
        {
            get { return _showConsole; }
            set { _showConsole = value; }
        }

        [Category("调试")]
        [DisplayName("使用Visual Studio进行调试")]
        [Description("如果找到相应的visual studio实例，将尝试附加解决方案")]
        public bool Attach
        {
            get { return _attach; }
            set { _attach = value; }
        }
    }

    [SuppressMessage("ReSharper", "CommentTypo")]
    [SuppressMessage("ReSharper", "ConvertToAutoProperty")]
    [SuppressMessage("ReSharper", "ArrangeAccessorOwnerBody")]
    class GameScreenDynamic : ISettingsChannelA
    {
        private bool startOnFullScreen;
        private bool mouseVisible;
        private int screenWidth;
        private int screenHeight;
        //private bool vsync;

        //[Category("Screen")]
        //[DisplayName("Vertical Sync (VSync)")]
        //public bool VSync
        //{
        //    get { return vsync; }
        //    set { vsync = value; }
        //}

        [Category("屏幕")]
        [DisplayName("全屏启动")]
        public bool StartOnFullScreen
        {
            get { return startOnFullScreen; }
            set { startOnFullScreen = value; }
        }

        [Category("屏幕")]
        [DisplayName("鼠标可见")]
        public bool MouseVisible
        {
            get { return mouseVisible; }
            set { mouseVisible = value; }
        }

        [Category("屏幕")]
        [DisplayName("屏幕宽度")]
        public int ScreenWidth
        {
            get { return screenWidth; }
            set { screenWidth = value; }
        }

        [Category("屏幕")]
        [DisplayName("屏幕高度")]
        public int ScreenHeight
        {
            get { return screenHeight; }
            set { screenHeight = value; }
        }
    }
}
