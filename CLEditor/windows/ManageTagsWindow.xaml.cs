﻿using System.Windows;
using System.Windows.Input;
using CLEngine.Editor.core;
using CLEngine.Core;

namespace CLEngine.Editor.windows
{
    /// <summary>
    /// ManageTagsWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ManageTagsWindow : Window
    {
        public ManageTagsWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.refreshList();
        }

        private void AddTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string text = this.AddTextBox.Text.Trim();
                if (text.Equals(string.Empty)) return;

                if (!SceneManager.ActiveScene.CommonTags.Contains(text))
                    SceneManager.ActiveScene.CommonTags.Add(text);

                (sender as Xceed.Wpf.Toolkit.WatermarkTextBox).Text = "";

                EditorUtils.SelectAnotherElement<Xceed.Wpf.Toolkit.WatermarkTextBox>(sender as Xceed.Wpf.Toolkit.WatermarkTextBox);

                this.refreshList();
            }
        }

        private void removeBtn_Click(object sender, RoutedEventArgs e)
        {
            if (this.TagsListBox.SelectedItem == null) return;
            SceneManager.ActiveScene.CommonTags.Remove(this.TagsListBox.SelectedItem.ToString());
            this.refreshList();
        }

        private void refreshList()
        {
            this.TagsListBox.Items.Clear();

            foreach (var tag in SceneManager.ActiveScene.CommonTags)
            {
                this.TagsListBox.Items.Add(tag);
            }
        }

        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            string text = this.AddTextBox.Text.Trim();
            if (text.Equals(string.Empty)) return;

            if (!SceneManager.ActiveScene.CommonTags.Contains(text))
                SceneManager.ActiveScene.CommonTags.Add(text);

            AddTextBox.Text = "";

            this.refreshList();
        }
    }
}
