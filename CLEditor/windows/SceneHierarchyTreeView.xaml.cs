﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using CLEngine.Editor.controls;
using CLEngine.Editor.core;
using CLEngine.Core;
using CLEngine.Core.attributes;
using CLEngine.Core.components;
using Sprite = CLEngine.Core.Sprite;
using CLEngine.Core.framework;

namespace CLEngine.Editor.windows
{
    /// <summary>
    /// SceneHierarchyTreeView.xaml 的交互逻辑
    /// </summary>
    [SuppressMessage("ReSharper", "LocalizableElement")]
    public partial class SceneHierarchyTreeView
    {
        #region fields

        DragDropTreeViewItem selectedForEditing;
        ContextMenu contextMenu, panelContextMenu;
        CMenuItem addComponentItem;

        const int MARGIN = 4;

        DragDropTreeViewItem lastSelectedItem;
        ImageSource visibleItemIcon;
        ImageSource hiddenItemIcon;

        internal bool canCopyPaste;

        MenuItem renameItem;
        MenuItem moveUpItem;
        MenuItem moveDownItem;
        MenuItem saveStateItem;

        #endregion

        #region properties

        internal DragDropTreeViewItem SelectedItem
        {
            get
            {
                return treeView.SelectedItem as DragDropTreeViewItem;
            }
        }

        #endregion

        #region constructors

        public SceneHierarchyTreeView()
        {
            InitializeComponent();
            Initialize();
            visibleItemIcon = (ImageSource)FindResource("VisibleItemIcon");
            hiddenItemIcon = (ImageSource)FindResource("HiddenItemIcon");

            treeView.OnDragDropSuccess += treeView_OnDragDropSuccess;
            treeView.MouseRightButtonDown += treeView_MouseRightButtonDown;
            treeView.MouseLeftButtonDown += treeView_MouseLeftButtonDown;
        }

        #endregion

        #region methods

        void Initialize()
        {
            contextMenu = new ContextMenu();
            panelContextMenu = new ContextMenu();
            contextMenu.Opened += contextMenu_Opened;
            // Context menu:
            addComponentItem = EditorUtils.CreateMenuItem("添加组件", (ImageSource)FindResource("ComponentIcon"));
            MenuItem createObjectItem = EditorUtils.CreateMenuItem("创建新对象...", (ImageSource)FindResource("GameObjectIcon_Sprite"));
            MenuItem panelCreateObjectItem = EditorUtils.CreateMenuItem("创建新对象...", (ImageSource)FindResource("GameObjectIcon_Sprite"));
            MenuItem addFromStateItem = EditorUtils.CreateMenuItem("从状态添加...");
            MenuItem panelAddFromStateItem = EditorUtils.CreateMenuItem("从状态添加...");
            saveStateItem = EditorUtils.CreateMenuItem("保存状态...", (ImageSource)FindResource("SaveIcon"));
            MenuItem cutItem = EditorUtils.CreateMenuItem("剪切", (ImageSource)FindResource("CutIcon"));
            MenuItem copyItem = EditorUtils.CreateMenuItem("复制", (ImageSource)FindResource("CopyIcon"));
            MenuItem pasteItem = EditorUtils.CreateMenuItem("粘贴", (ImageSource)FindResource("PasteIcon"));
            MenuItem panelPasteItem = EditorUtils.CreateMenuItem("粘贴", (ImageSource)FindResource("PasteIcon"));
            MenuItem deleteItem = EditorUtils.CreateMenuItem("删除");
            renameItem = EditorUtils.CreateMenuItem("重命名", (ImageSource)FindResource("RenameIcon"));
            moveUpItem = EditorUtils.CreateMenuItem("上移", (ImageSource)FindResource("MoveUpIcon"));
            moveDownItem = EditorUtils.CreateMenuItem("下移", (ImageSource)FindResource("MoveDownIcon"));

            contextMenu.Items.Add(addComponentItem);
            contextMenu.Items.Add(createObjectItem);
            contextMenu.Items.Add(new Separator());
            contextMenu.Items.Add(addFromStateItem);
            contextMenu.Items.Add(saveStateItem);
            contextMenu.Items.Add(new Separator());
            contextMenu.Items.Add(cutItem);
            contextMenu.Items.Add(copyItem);
            contextMenu.Items.Add(pasteItem);
            contextMenu.Items.Add(deleteItem);
            contextMenu.Items.Add(new Separator());
            contextMenu.Items.Add(renameItem);
            //contextMenu.Items.Add(new Separator());
            //contextMenu.Items.Add(moveUpItem);
            //contextMenu.Items.Add(moveDownItem);

            panelContextMenu.Items.Add(panelCreateObjectItem);
            panelContextMenu.Items.Add(new Separator());
            panelContextMenu.Items.Add(panelAddFromStateItem);
            panelContextMenu.Items.Add(new Separator());
            panelContextMenu.Items.Add(panelPasteItem);

            createObjectItem.Click += createObjectItem_Click;
            addFromStateItem.Click += addFromStateItem_Click;
            renameItem.Click += renameItem_Click;
            saveStateItem.Click += saveStateItem_Click;
            cutItem.Click += cutItem_Click;
            copyItem.Click += copyItem_Click;
            pasteItem.Click += pasteItem_Click;
            deleteItem.Click += deleteItem_Click;
            moveUpItem.Click += moveUpItem_Click;
            moveDownItem.Click += moveDownItem_Click;

            panelCreateObjectItem.Click += createObjectItem_Click;
            panelAddFromStateItem.Click += addFromStateItem_Click;
            panelPasteItem.Click += panelPasteItem_Click;
        }

        void panelPasteItem_Click(object sender, RoutedEventArgs e)
        {
            Paste();
        }

        internal void Paste()
        {
            List<GameObject> gameObjects = (List<GameObject>)Clipboard.GetData("GameObject");

            if (gameObjects != null)
            {
                foreach (GameObject obj in gameObjects)
                {
                    AddGameObject(obj, string.Empty, true, false, true);
                }
            }

            EditorHandler.ChangeSelectedObjects();
        }

        private void SelectOnClick(MouseButtonEventArgs e)
        {
	        //find the original object that raised the event
            UIElement ClickedItem = VisualTreeHelper.GetParent((UIElement) e.OriginalSource) as UIElement;

            //find the clicked TreeViewItem
            while ((ClickedItem != null) && !(ClickedItem is DragDropTreeViewItem))
            {
                ClickedItem = VisualTreeHelper.GetParent(ClickedItem) as UIElement;
            }

            var ClickedTreeViewItem = (DragDropTreeViewItem) ClickedItem;
            if (ClickedTreeViewItem != null)
            {
                ClickedTreeViewItem.IsSelected = true;
                ClickedTreeViewItem.Focus();
            }
        }

        internal void AddGameObject(GameObject gameObject, string type, bool autoselect = true, bool addToSelectedParent = false, bool expand = false)
        {
            if (gameObject == null)
            {
                switch (type.ToLower())
                {
                    case "empty":
                        gameObject = new GameObject();
                        gameObject.Name = "空对象";

                        break;

                    case "sprite":
                        gameObject = new Sprite();
                        gameObject.Name = "图片精灵";

                        break;

                    case "animatedsprite":
                        gameObject = new AnimatedSprite();
                        gameObject.Name = "动画精灵";

                        break;

                    case "audio":
                        gameObject = new AudioObject();
                        gameObject.Name = "音频";

                        break;

                    case "tileset":
                        gameObject = new Tileset();
                        gameObject.Name = "地图块";

                        break;

                    case "bmfont":
                        gameObject = new BMFont();
                        gameObject.Name = "位图字体";

                        break;

                    case "particleemitter":
                        gameObject = new ParticleEmitter();
                        gameObject.Name = "粒子发射器";

                        break;

                    case "animationobject":
                        gameObject = new AnimationObject();
                        gameObject.Name = "龙骨动画";
                        break;

                }
            }
            else
            {
                gameObject.Initialize();
            }

            if (SelectedItem == null)
            {
                SceneManager.ActiveScene.GameObjects.Add(gameObject);
            }
            else
            {
                if (addToSelectedParent)
                {
                    GameObject _gameObject = (GameObject)SelectedItem.Tag;
                    if (_gameObject.Transform.Parent == null)
                        SceneManager.ActiveScene.GameObjects.Add(gameObject);
                    else
                        _gameObject.Transform.Parent.GameObject.Children.Add(gameObject);
                }
                else
                {
                    GameObject _gameObject = (GameObject)SelectedItem.Tag;
                    _gameObject.Children.Add(gameObject);
                }
            }

            if (gameObject != null)
            {
                var _parent = SelectedItem;
                if (addToSelectedParent)
                {
                    var tmp = EditorUtils.FindVisualParent<DragDropTreeViewItem>(_parent);

                    _parent = tmp;
                }

                var node = AddNode(_parent, gameObject, GameObjectImageSource(gameObject));

                if (_parent != null && expand)
                {
                    //_parent.ExpandSubtree();
                    _parent.IsExpanded = true;
                }

                node.ContextMenu = contextMenu;
                // AddNodes(gameObject);
                //node.IsSelected = true;

                AttachChildren(node);
                //foreach (GameObject _obj in gameObject.Children)
                //    AddGameObject(_obj, string.Empty);

                if (autoselect)
                {
                    node.IsSelected = true;
                    EditorHandler.SelectedGameObjects = new List<GameObject>();
                    EditorHandler.SelectedGameObjects.Add(gameObject);
                }
            }
        }

        private void AttachChildren(DragDropTreeViewItem node)
        {
            var gameObjectCollection = (node.Tag as GameObject)?.Children;
            if (gameObjectCollection != null)
                foreach (GameObject _obj in gameObjectCollection)
                {
                    DragDropTreeViewItem _node = AddNode(node, _obj, GameObjectImageSource(_obj));
                    AttachChildren(_node);
                }
        }

        //private void AddNodes(GameObject obj)
        //{
        //    foreach (GameObject _obj in obj.Children)
        //    {
        //        AddGameObject(_obj.Transform.GameObject, null);
        //        //if (_obj.Children.Count > 0)
        //        //    AddNodes(_obj);
        //    }
        //}

        internal void AddFromState()
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Title = "Open State";
            ofd.Filter = @"(*.state)|*.state";

            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    GameObject gameObject = (GameObject)CHelper.DeserializeObject(ofd.FileName);
                    gameObject.Initialize();

                    AddGameObject(gameObject, string.Empty);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        internal void BeginEditTextOnSelected()
        {
            ItemLostFocus();

            selectedForEditing = SelectedItem;

            if (selectedForEditing == null) return;

            TextBox tb = new TextBox();
            tb.LostFocus += tb_LostFocus;
            tb.KeyDown += tb_KeyDown;
            tb.Text = lastSelectedItem.Tag.ToString();
            tb.Focusable = true;

            (selectedForEditing.Header as StackPanel)?.Children.RemoveAt(2);
            (selectedForEditing.Header as StackPanel)?.Children.Add(tb);
            selectedForEditing.CanDrag = false;

            tb.Select(0, tb.Text.Length);
            tb.Focus();
        }

        internal DragDropTreeViewItem AddNode(DragDropTreeViewItem parent, object tag, ImageSource imageSource)
        {
            DragDropTreeViewItem node = new DragDropTreeViewItem();
            node.Style = (Style)FindResource("IgniteMultiTreeViewItem");

            Image visibilityToggleImage = new Image();
            visibilityToggleImage.Cursor = Cursors.Hand;
            visibilityToggleImage.Tag = tag;
            visibilityToggleImage.Width = 8;
            visibilityToggleImage.Height = 8;
            visibilityToggleImage.Margin = new Thickness(0, 0, MARGIN, 0);
            visibilityToggleImage.MouseUp += visibilityToggleImage_MouseUp;
            if (tag is GameObject)
                visibilityToggleImage.Source = ((tag as GameObject).Visible ? visibleItemIcon : hiddenItemIcon);

            StackPanel sp = new StackPanel();
            sp.Orientation = Orientation.Horizontal;
            sp.Children.Add(visibilityToggleImage);
            sp.Children.Add(new Image() { Source = imageSource });
            sp.Children.Add(new TextBlock() { Text = tag.ToString(), Margin = new Thickness(MARGIN, 0, 0, 0) });

            node.Header = sp;
            node.MouseUp += node_MouseUp;
            node.Tag = tag;
            node.MouseDoubleClick += node_MouseDoubleClick;
            node.ContextMenu = contextMenu;

            //node.ContextMenu = objectContextMenuStrip;

            if (parent == null)
            {
                treeView.Items.Insert(0, node);
            }
            else
            {
                parent.Items.Insert(0, node);
                //parent.ExpandSubtree();
                //parent.IsExpanded = true;
            }

            return node;
        }

        public void SetSelectedItem(ref TreeView control, object item)
        {
            try
            {
                DependencyObject dObject = control
                    .ItemContainerGenerator
                    .ContainerFromItem(item);

                //uncomment the following line if UI updates are unnecessary
                //((TreeViewItem)dObject).IsSelected = true;               

                MethodInfo selectMethod =
                   typeof(TreeViewItem).GetMethod("Select",
                   BindingFlags.NonPublic | BindingFlags.Instance);

                selectMethod?.Invoke(dObject, new object[] { true });
            }
            catch
            {
	            // ignored
            }
        }

        void node_MouseUp(object sender, MouseButtonEventArgs e)
        {
            canCopyPaste = true;
        }

        /// <summary>
        /// 
        /// </summary>
        public void CreateView()
        {
            treeView.Items.Clear();

            if (SceneManager.ActiveScene == null)
            {
                DragDropTreeViewItem noScene = new DragDropTreeViewItem();

                noScene.Header = "内存中没有场景.";
                noScene.CanDrag = false;
                noScene.IsEnabled = false;
                noScene.Style = (Style)FindResource("IgniteMultiTreeViewItem");
                // noScene.can

                treeView.Items.Add(noScene);
                return;
            }

            ContextMenu = panelContextMenu;

            // 设置所有这些图层游戏对象
            foreach (GameObject gameObject in SceneManager.ActiveScene.GameObjects)
            {
                string name = gameObject.Name == null ? "Game Object" : gameObject.Name;
                gameObject.Name = name;

                DragDropTreeViewItem _node = AddNode(null, gameObject, GameObjectImageSource(gameObject));
                //_node.ContextMenuStrip = objectContextMenuStrip;

                FillGameObjects(gameObject, _node);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_gameObject"></param>
        /// <param name="node"></param>
        private void FillGameObjects(GameObject _gameObject, DragDropTreeViewItem node)
        {
            foreach (GameObject gameObject in _gameObject.Children)
            {
                string name = gameObject.Name == null ? "Game Object" : gameObject.Name;
                gameObject.Name = name;

                DragDropTreeViewItem _node = AddNode(node, gameObject, GameObjectImageSource(gameObject));
                //_node.ContextMenuStrip = objectContextMenuStrip;

                FillGameObjects(gameObject, _node);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameObject"></param>
        /// <returns></returns>
        internal ImageSource GameObjectImageSource(GameObject gameObject)
        {
            if ((gameObject is Sprite) || gameObject is AnimationObject)
                return (ImageSource)FindResource("GameObjectIcon_Sprite");
            else if (gameObject is BMFont)
                return (ImageSource)FindResource("GameObjectIcon_Text");
            else if (gameObject is AudioObject)
                return (ImageSource)FindResource("GameObjectIcon_Audio");
            else if (gameObject is Tileset)
                return (ImageSource)FindResource("GameObjectIcon_Tileset");
            else if (gameObject is ParticleEmitter)
                return (ImageSource)FindResource("GameObjectIcon_Particle");
            else if (gameObject is EventObject)
                return (ImageSource) FindResource("EventObject");

            // Empty game object
            return (ImageSource)FindResource("GameObjectIcon_Empty");
        }

        private void ItemLostFocus()
        {
            if (lastSelectedItem != null && lastSelectedItem.Header != null && ((StackPanel) lastSelectedItem.Header).Children.Count >= 2
                && (lastSelectedItem.Header as StackPanel).Children[2] is TextBox)
            {
                // update the current name:
                if (lastSelectedItem.Tag is GameObject)
                    (lastSelectedItem.Tag as GameObject).Name = ((lastSelectedItem.Header as StackPanel).Children[2] as TextBox)?.Text;

                // remove the text box
                (lastSelectedItem.Header as StackPanel).Children.RemoveAt(2);
                (lastSelectedItem.Header as StackPanel).Children.Add(new TextBlock() { Text = lastSelectedItem.Tag.ToString(), Margin = new Thickness(MARGIN, 0, 0, 0) });
            }
        }

        #endregion

        #region events

        void renameItem_Click(object sender, RoutedEventArgs e)
        {
            BeginEditTextOnSelected();
        }

        void tb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ItemLostFocus();
                selectedForEditing.CanDrag = true;
            }
        }

        void addFromStateItem_Click(object sender, RoutedEventArgs e)
        {
            AddFromState();
        }

        [SuppressMessage("ReSharper", "RedundantNameQualifier")]
        void contextMenu_Opened(object sender, RoutedEventArgs e)
        {
            // check disabled :
            if (TreeViewExtension.GetSelectedTreeViewItems(treeView).Count > 1)
            {
                // more than one selected:
                renameItem.IsEnabled = false;
                moveUpItem.IsEnabled = false;
                moveDownItem.IsEnabled = false;
                saveStateItem.IsEnabled = false;
            }
            else
            {
                renameItem.IsEnabled = true;
                moveUpItem.IsEnabled = true;
                moveDownItem.IsEnabled = true;
                saveStateItem.IsEnabled = true;
            }

            addComponentItem.Items.Clear();
            CMenuItem item;
            
            if (SceneManager.ScriptsAssembly == null)
            {
                addComponentItem.Visibility = System.Windows.Visibility.Collapsed;
                return;
            }

            addComponentItem.Visibility = System.Windows.Visibility.Visible;

            //ObjectComponent dummy = new ObjectComponent();
            try
            {
	            foreach (var type in SceneManager.ScriptsAssembly.GetTypes())
	            {
		            // 是否是gameobjectcomponent?
		            if (!type.IsSubclassOf(typeof(ObjectComponent))) continue;

		            var fullname = type.FullName;
		            var lastItem = addComponentItem;

		            var contains = false;
		            const char c = '.';
		            if (fullname != null)
		            {
			            foreach (var c1 in fullname)
			            {
				            if (!Equals(c1, c)) continue;

				            contains = true;
				            break;
			            }

			            if (fullname != type.Name && contains)
			            {
				            var splitted = fullname.Split('.');
				            var scount = splitted.Count() - 1;

				            for (var i = 0; i < scount; i++)
				            {
					            var camelCaseFix = CHelper.SplitCamelCase(splitted[i]);

					            item = (from object t in lastItem.Items where ((CMenuItem) t).TagText.Equals(camelCaseFix) select t).Cast<CMenuItem>().FirstOrDefault();

					            if (item == null)
					            {
						            item = EditorUtils.CreateMenuItem(camelCaseFix);
						            item.TagText = camelCaseFix;
						            lastItem.Items.Insert(0, item);
					            }

					            lastItem = item;
				            }
			            }
		            }

		            var camelCase = CHelper.SplitCamelCase(type.Name);
		            var newItem = EditorUtils.CreateMenuItem(camelCase, (ImageSource)FindResource("ComponentItemIcon"));
		            newItem.Tag = type;
		            newItem.TagText = camelCase;
		            newItem.Click += newItem_Click;

		            SearchAndAttachInfo(newItem, type);

		            lastItem.Items.Add(newItem);
	            }
            }
            catch (Exception exception)
            {
	            Console.WriteLine(exception);
            }

            var luaSubItem = EditorUtils.CreateMenuItem("Lua脚本",
	            (ImageSource) FindResource("ComponentItemIcon"));
            luaSubItem.Tag = typeof(LuaObject);
            luaSubItem.TagText = typeof(LuaObject).Name;
            luaSubItem.Click += newItem_Click;

            SearchAndAttachInfo(luaSubItem, typeof(LuaObject));

            addComponentItem.Items.Add(luaSubItem);

            // System Components :

            addComponentItem.Items.Add(new Separator());
            item = EditorUtils.CreateMenuItem("物理");

            var subItem = EditorUtils.CreateMenuItem("刚体", (ImageSource)FindResource("ComponentItemIcon"));
            subItem.Tag = typeof(PhysicalBody);
            subItem.TagText = typeof(PhysicalBody).Name;
            subItem.Click += newItem_Click;

            SearchAndAttachInfo(subItem, typeof(PhysicalBody));

            item.Items.Add(subItem);

            subItem = EditorUtils.CreateMenuItem("矩形碰撞", (ImageSource)FindResource("ComponentItemIcon"));
            subItem.Tag = typeof(RectangleBody);
            subItem.TagText = typeof(RectangleBody).Name;
            subItem.Click += newItem_Click;

            SearchAndAttachInfo(subItem, typeof(RectangleBody));

            item.Items.Add(subItem);

            subItem = EditorUtils.CreateMenuItem("圆形碰撞", (ImageSource)FindResource("ComponentItemIcon"));
            subItem.Tag = typeof(CircleBody);
            subItem.TagText = typeof(CircleBody).Name;
            subItem.Click += newItem_Click;

            SearchAndAttachInfo(subItem, typeof(CircleBody));

            item.Items.Add(subItem);

            subItem = EditorUtils.CreateMenuItem("纹理碰撞", (ImageSource)FindResource("ComponentItemIcon"));
            subItem.Tag = typeof(TextureBody);
            subItem.TagText = typeof(TextureBody).Name;
            subItem.Click += newItem_Click;

            SearchAndAttachInfo(subItem, typeof(TextureBody));

            item.Items.Add(subItem);

            addComponentItem.Items.Add(item);

            addComponentItem.Items.Add(new Separator());

            var eventItem = EditorUtils.CreateMenuItem("事件", (ImageSource)FindResource("EventObject"));
            eventItem.Tag = typeof(EventObject);
            eventItem.TagText = typeof(EventObject).Name;
            eventItem.Click += newItem_Click;

            SearchAndAttachInfo(eventItem, typeof(EventObject));

            addComponentItem.Items.Add(eventItem);

        }

        private void SearchAndAttachInfo(FrameworkElement item, MemberInfo type)
        {
	        MemberInfo info = type;
            var attributes = info.GetCustomAttributes(typeof(Info), true);

            var count = attributes.Length;
            if (count > 0)
                item.ToolTip = (attributes[0] as Info)?.Value.Replace("\\n", Environment.NewLine);
        }

        void newItem_Click(object sender, RoutedEventArgs e)
        {
            var _sender = (CMenuItem)sender;

            foreach (var ti in TreeViewExtension.GetSelectedTreeViewItems(treeView))
            {
                var component = (ObjectComponent)Activator.CreateInstance((Type)_sender.Tag, new object[] { /* params here */ });
                var gameObject = (GameObject)ti.Tag;
                if (!gameObject.AddComponent(component))
                {
                    MessageBox.Show("该组件未添加到 " + gameObject.Name + ", 要求未得到满足", "错误", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    EditorCommands.CreatePropertyGridView();
                }
            }
        }

        private void createObjectItem_Click(object sender, RoutedEventArgs e)
        {
            new AddNewItemWindow(SelectedItem).ShowDialog();
        }

        private void treeView_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            SelectOnClick(e);
        }

        private void treeView_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            SelectOnClick(e);
        }

        private void treeView_OnDragDropSuccess(DragDropTreeViewItem source, DragDropTreeViewItem target, System.ComponentModel.CancelEventArgs e)
        {
            if (!(source.Tag is GameObject))
            {
                e.Cancel = true;
                return;
            }

            var movedItems = TreeViewExtension.GetSelectedTreeViewItems(treeView);
            // 验证移动的物品是否不会放在他们的任何子物体或自己的子物体身上
            foreach (var ti in movedItems)
            {
                var tx = (DragDropTreeViewItem)ti;
                if (ti == target)
                {
                    // 试图放下其中一个移动的物品
                    e.Cancel = true;
                    return;
                }

                if (!DragDropTreeView.TreeContainsNode(treeView, tx, target)) continue;
                e.Cancel = true;
                return;
            }

            if (target.Tag is GameObject tag)
            {
                foreach (var ti in movedItems)
                {
                    var _source = ti.Tag as GameObject;

                    // no parent?
                    if (_source != null && _source.Transform.Parent == null)
                    {
                        SceneManager.ActiveScene.GameObjects.Remove(_source, false);
                    }
                    else
                    {
	                    var parent = (ti.Parent as DragDropTreeViewItem)?.Tag as GameObject;
	                    parent?.Children.Remove(_source, false);
                    }

                    if (DragDropHelper.insertionPlace == DragDropHelper.InsertionPlace.Center)
                    {
                        tag.Children.Insert(0, _source);
                    }
                    else
                    {
	                    // no parent?
	                    var index = 0;
	                    if (tag.Transform.Parent == null)
                        {
                            index = SceneManager.ActiveScene.GameObjects.IndexOf(tag);
                        }
                        else
	                    {
		                    if ((target.Parent as DragDropTreeViewItem)?.Tag is GameObject parent) index = parent.Children.IndexOf(tag);
	                    }

                        if (DragDropHelper.insertionPlace == DragDropHelper.InsertionPlace.Top)
                            index++;

                        if (tag.Transform.Parent == null)
                        {
                            SceneManager.ActiveScene.GameObjects.Insert(index, _source);
                        }
                        else
                        {
	                        var parent = (target.Parent as DragDropTreeViewItem)?.Tag as GameObject;
	                        parent?.Children.Insert(index, _source);
                        }
                    }

                }

            }
            else
            {
                e.Cancel = true;
            }
        }

        [SuppressMessage("ReSharper", "PossibleNullReferenceException")]
        private void visibilityToggleImage_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var tag = (sender as Image)?.Tag;

            if (tag is GameObject gameObject)
            {
                gameObject.Visible = !gameObject.Visible;
            }

            if (tag != null && !(tag as GameObject).Visible)
            {
                ((Image) sender).Source = hiddenItemIcon;
            }
            else
            {
                ((Image) sender).Source = visibleItemIcon;
            }
        }

        private void node_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //if (lastSelectedItem != null && lastSelectedItem.Tag != null)
            //{
            //    TextBox tb = new TextBox();
            //    tb.Text = lastSelectedItem.Tag.ToString();
            //    tb.LostFocus += tb_LostFocus;

            //    (lastSelectedItem.Header as StackPanel).Children.RemoveAt(2);
            //    (lastSelectedItem.Header as StackPanel).Children.Add(tb);
            //    lastSelectedItem.CanDrag = false;
            //}
        }

        private void tb_LostFocus(object sender, RoutedEventArgs e)
        {
            ItemLostFocus();
            lastSelectedItem.CanDrag = true;
        }

        private void treeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            ItemLostFocus();


        }


        private void moveDownItem_Click(object sender, RoutedEventArgs e)
        {
            var gameObject = (GameObject)SelectedItem.Tag;

            // no parent?
            if (!(SelectedItem.Parent is DragDropTreeView))
            {
                var parentNode = SelectedItem.Parent as DragDropTreeViewItem;

                if (parentNode != null)
                {
	                var index = ((GameObject) parentNode.Tag).Children.FindIndex(o => o.GetHashCode() == gameObject.GetHashCode());

	                // already on bottom?
	                if (index == 0)
		                return;

	                var parent = (GameObject)parentNode.Tag;

	                // Shift objects 
	                var topObject = parent.Children[index - 1];
	                parent.Children.Delete(topObject);
	                parent.Children.Insert(index, topObject);
                }

                //// Shift in treeview
                if (parentNode == null) return;

                var topNode = (DragDropTreeViewItem)parentNode.Items[parentNode.Items.IndexOf(SelectedItem) + 1];
                parentNode.Items.Remove(topNode);
                parentNode.Items.Insert(parentNode.Items.IndexOf(SelectedItem), topNode);
            }
            else
            {
                var index = SceneManager.ActiveScene.GameObjects.FindIndex(o => o.GetHashCode() == gameObject.GetHashCode());

                // already on bottom?
                if (index == 0)
                    return;

                // Shift objects 
                var topObject = SceneManager.ActiveScene.GameObjects[index - 1];
                SceneManager.ActiveScene.GameObjects.Delete(topObject);
                SceneManager.ActiveScene.GameObjects.Insert(index, topObject);

                //// Shift in treeview
                var topNode = (DragDropTreeViewItem)treeView.Items[treeView.Items.IndexOf(SelectedItem) + 1];
                treeView.Items.Remove(topNode);
                treeView.Items.Insert(treeView.Items.IndexOf(SelectedItem), topNode);
            }
        }

        private void moveUpItem_Click(object sender, RoutedEventArgs e)
        {
            var gameObject = (GameObject)SelectedItem.Tag;

            // no parent?
            if (!(SelectedItem.Parent is DragDropTreeView))
            {
                var parentNode = SelectedItem.Parent as DragDropTreeViewItem;

                if (parentNode != null)
                {
	                var index = ((GameObject) parentNode.Tag).Children.FindIndex(o => o.GetHashCode() == gameObject.GetHashCode());

	                // already on bottom?
	                if (index == ((GameObject) parentNode.Tag).Children.Count - 1)
		                return;

	                var parent = (GameObject)parentNode.Tag;

	                // Shift objects 
	                var topObject = parent.Children[index + 1];
	                parent.Children.Delete(topObject);
	                parent.Children.Insert(index, topObject);
                }

                //// Shift in treeview
                if (parentNode == null) return;

                var topNode = (DragDropTreeViewItem)parentNode.Items[parentNode.Items.IndexOf(SelectedItem) - 1];
                parentNode.Items.Remove(topNode);
                parentNode.Items.Insert(parentNode.Items.IndexOf(SelectedItem) + 1, topNode);
            }
            else
            {
                int index = SceneManager.ActiveScene.GameObjects.FindIndex(o => o.GetHashCode() == gameObject.GetHashCode());

                // already on bottom?
                if (index == SceneManager.ActiveScene.GameObjects.Count - 1)
                    return;

                // Shift objects 
                GameObject topObject = SceneManager.ActiveScene.GameObjects[index + 1];
                SceneManager.ActiveScene.GameObjects.Delete(topObject);
                SceneManager.ActiveScene.GameObjects.Insert(index, topObject);

                //// Shift in treeview
                DragDropTreeViewItem topNode = (DragDropTreeViewItem)treeView.Items[treeView.Items.IndexOf(SelectedItem) - 1];
                treeView.Items.Remove(topNode);
                treeView.Items.Insert(treeView.Items.IndexOf(SelectedItem) + 1, topNode);
            }
        }

        private void deleteItem_Click(object sender, RoutedEventArgs e)
        {
            var selected = TreeViewExtension.GetSelectedTreeViewItems(treeView);
            var message = "您确定要删除所选的游戏对象吗?";
            if (selected.Count > 1)
            {
                message = "您确定要删除所选的游戏对象吗?";
            }

            if (System.Windows.Forms.MessageBox.Show(message, "警告", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                TreeViewExtension.UnselectAll(treeView);
                foreach (var t in selected)
                {
	                DragDropTreeViewItem parentNode = (t as DragDropTreeViewItem)?.Parent as DragDropTreeViewItem;
                    if (parentNode == null)
                    {
                        SceneManager.ActiveScene.GameObjects.Remove((t as DragDropTreeViewItem)?.Tag as GameObject);
                        treeView.Items.Remove(t);
                    }
                    else
                    {
                        GameObject objParent = (GameObject)parentNode.Tag;
                        objParent.Children.Remove((t as DragDropTreeViewItem).Tag as GameObject);
                        parentNode.Items.Remove(t);
                    }
                }
                EditorHandler.SelectedGameObjects.Clear();
                EditorHandler.ChangeSelectedObjects();
            }
        }

        void pasteItem_Click(object sender, RoutedEventArgs e)
        {
            Paste();
        }

        private void copyItem_Click(object sender, RoutedEventArgs e)
        {
            var toCopy = new List<GameObject>();
            foreach (var ti in TreeViewExtension.GetSelectedTreeViewItems(treeView))
            {
	            if (ti.Tag is GameObject gameObject)
                {
	                gameObject.SaveComponentValues();
	                toCopy.Add(gameObject);
                }
            }

            Clipboard.SetData("GameObject", toCopy);
        }

        private void cutItem_Click(object sender, RoutedEventArgs e)
        {
            var toCopy = new List<GameObject>();
            foreach (TreeViewItem ti in TreeViewExtension.GetSelectedTreeViewItems(treeView))
            {
	            if (ti.Tag is GameObject gameObject)
                {
	                gameObject.SaveComponentValues();
	                toCopy.Add(gameObject);
                }

                DragDropTreeViewItem parentNode = ti.Parent as DragDropTreeViewItem;
                if (parentNode == null)
                {
                    SceneManager.ActiveScene.GameObjects.Remove(ti.Tag as GameObject);
                    treeView.Items.Remove(ti);
                }
                else
                {
                    GameObject objParent = (GameObject)parentNode.Tag;
                    objParent.Children.Remove(ti.Tag as GameObject);
                    parentNode.Items.Remove(ti);
                }
            }
            Clipboard.SetData("GameObject", toCopy);
            TreeViewExtension.UnselectAll(treeView);
            EditorHandler.SelectedGameObjects.Clear();
            EditorHandler.ChangeSelectedObjects();
        }

        void saveStateItem_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog();
            fbd.SelectedPath = SceneManager.GameProject.ProjectPath;

            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                (SelectedItem.Tag as GameObject)?.Save(fbd.SelectedPath + "//" + (SelectedItem.Tag as GameObject).Name + ".state");
            }
        }

        private void treeView_MouseDown(object sender, MouseButtonEventArgs e)
        {
            canCopyPaste = true;

            if (treeView.SelectedItem != null && e.LeftButton == MouseButtonState.Pressed)
            {
                ((TreeViewItem) treeView.SelectedItem).IsSelected = false;
                TreeViewExtension.UnselectAll(treeView);
            }
        }

        private void treeView_MouseLeave(object sender, MouseEventArgs e)
        {
            canCopyPaste = false;
        }

        private void treeView_MouseUp(object sender, MouseButtonEventArgs e)
        {
            // NEW WAY TO DETECT SELECTIONS:
            if ((sender as TreeView)?.SelectedItem != null)
            {
                var it = TreeViewExtension.GetSelectedTreeViewItems(treeView);
                EditorHandler.SelectedGameObjects = new List<GameObject>();

                foreach (var i in it)
                {
                    object tag = (i as DragDropTreeViewItem)?.Tag;

                    if (tag is GameObject gameObject)
                    {
                        // TODO : multiple selection          
                        EditorHandler.SelectedGameObjects.Add(gameObject);
                    }
                }

                EditorHandler.ChangeSelectedObjects();

                lastSelectedItem = ((TreeView) sender).SelectedItem as DragDropTreeViewItem;
            }
            else
            {
                EditorHandler.SelectedGameObjects.Clear();
                EditorHandler.ChangeSelectedObjects();
            }
        }

        public void SelectionUpdate()
        {
            foreach (TreeViewItem item in TreeViewExtension.GetExpandedTreeViewItems(treeView))
            {
                object tag = (item as DragDropTreeViewItem)?.Tag;
                if (tag is GameObject)
                {
                    if (!EditorHandler.SelectedGameObjects.Contains((tag as GameObject)))
                    {
                        TreeViewExtension.SetIsSelected(item, false);
                    }
                    else
                    {
                        TreeViewExtension.SetIsSelected(item, true);
                    }
                }
            }
        }

        #endregion
    }
}
