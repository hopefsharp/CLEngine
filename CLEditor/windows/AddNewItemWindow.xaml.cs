﻿using System.Windows;
using System.Windows.Controls;
using CLEngine.Editor.controls;
using CLEngine.Editor.core;
using CLEngine.Core;

namespace CLEngine.Editor.windows
{
    /// <summary>
    /// AddNewItemWindow.xaml 的交互逻辑
    /// </summary>
    public partial class AddNewItemWindow : Window
    {
        UIElement target = null;
        GameObject targetObject = null;

        public AddNewItemWindow(UIElement target)
        {
            InitializeComponent();

            itemListBox.SelectedIndex = 1;

            this.target = target;
            if ((target as DragDropTreeViewItem)?.Tag is GameObject)
                targetObject = ((DragDropTreeViewItem) target).Tag as GameObject;

            this.KeyUp += AddNewItemWindow_KeyUp;
        }

        void AddNewItemWindow_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                TryAdd();
            }
        }

        private void UpdateText()
        {
            switch (itemListBox.SelectedIndex)
            {
                case 0:
                    itemInfoTextBlock.Text = "Empty：\n\n此对象没有特定属性。 它主要用作容器.";
                    break;
                case 1:
                    itemInfoTextBlock.Text = "Sprite: \n\n使用此游戏对象，您可以在场景中加载和绘制图像.";
                    break;
                case 2:
                    itemInfoTextBlock.Text = "Animated Sprite: \n\n使用此游戏对象，您可以使用动画属性在场景上加载和绘制图像.";
                    break;
                case 3:
                    itemInfoTextBlock.Text = "Tileset: \n\n使用tileset对象，您可以使用tileset图像非常轻松地绘制地图.";
                    break;
                case 4:
                    itemInfoTextBlock.Text = "Audio: \n\n使用音频对象，您可以在游戏中重现声音.";
                    break;
                case 5:
                    itemInfoTextBlock.Text = "Bitmap Font: \n\n使用位图字体，您可以使用位图在游戏中绘制文本.";
                    break;
                case 6:
                    itemInfoTextBlock.Text = "Particle Emiter: \n\n使用粒子发射器，您可以使用粒子为游戏创建出色的视觉效果.";
                    break;
                case 7:
                    itemInfoTextBlock.Text = "DragonBone Sprite: \n\n使用龙骨动画序列帧，您可以使用它为你的游戏添加动画.";
                    break;
                case 8:
                    itemInfoTextBlock.Text = "Map Object: \n\n使用通用该地图，您可以做出更加真实的游戏地图";
                    break;
            }
        }

        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void itemListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateText();
        }

        private void addBtn_Click(object sender, RoutedEventArgs e)
        {
            TryAdd();
        }

        private void nameTextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {

        }

        private void TryAdd()
        {
            try
            {
                var name = (nameTextBox.Text.Trim().Equals(string.Empty) ? "新游戏对象" : nameTextBox.Text);

                var obj = new GameObject();
                switch (itemListBox.SelectedIndex)
                {
                    case 1:
                        obj = new Sprite();
                        break;
                    case 2:
                        obj = new AnimatedSprite();
                        break;
                    case 3:
                        obj = new Tileset();
                        break;
                    case 4:
                        obj = new AudioObject();
                        break;
                    case 5:
                        obj = new BMFont();
                        break;
                    case 6:
                        obj = new ParticleEmitter();
                        break;
                    case 7:
                        obj = new AnimationObject();
                        break;
                    case 8:
                        var createMapWin = new CreateMapWindow();
                        var result = createMapWin.ShowDialog();
                        if (result.HasValue && result.Value)
                            obj = new MapObject(createMapWin.TileWidth, createMapWin.TileHeight);

                        break;
                }

                obj.Name = name;
                if (addToSelectedCheckBox.IsChecked == true)
                {
                    if (targetObject == null)
                        SceneManager.ActiveScene.GameObjects.Add(obj);
                    else
                        targetObject.Children.Add(obj);

                    EditorHandler.SceneTreeView.AddNode(target as DragDropTreeViewItem, obj, EditorHandler.SceneTreeView.GameObjectImageSource(obj));
                }
                else
                {
                    SceneManager.ActiveScene.GameObjects.Add(obj);
                    EditorHandler.SceneTreeView.AddNode(null, obj, EditorHandler.SceneTreeView.GameObjectImageSource(obj));

                }

                EditorHandler.SelectedGameObjects.Clear();
                EditorHandler.SelectedGameObjects.Add(obj);
                EditorHandler.ChangeSelectedObjects();

                Close();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, "错误!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
