﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using CLEngine.Editor.controls;
using CLEngine.Editor.core;

namespace CLEngine.Editor.windows
{
    /// <summary>
    /// TutorialsListWindow.xaml 的交互逻辑
    /// </summary>
    public partial class TutorialsListWindow : Window
    {
        #region Fields

        private List<FileInfo> tutorialFiles = new List<FileInfo>();

        private Dictionary<string, TutorialsCategoryContainer> tutorialsSections =
            new Dictionary<string, TutorialsCategoryContainer>();

        System.Windows.Forms.Timer visualEffectsTimer;

        #endregion

        public TutorialsListWindow()
        {
            InitializeComponent();

            if (!RenderList())
                MessageBox.Show("暂无教程");
        }

        private bool RenderList()
        {
            string tutorialsPath = AppDomain.CurrentDomain.BaseDirectory + @"\Tutorials";
            if (!Directory.Exists(tutorialsPath))
            {
                EditorCommands.ShowOutputMessage("找不到Tutorials文件夹");
                return false;
            }

            DirectoryInfo directory = new DirectoryInfo(tutorialsPath);
            if (!getTutorials(directory))
            {
                EditorCommands.ShowOutputMessage("加载教程时出错");
                return false;
            }

            foreach (FileInfo file in tutorialFiles)
            {
                string xmlFullPath = file.FullName;
                XDocument doc = XDocument.Load(xmlFullPath);
                string category = doc.Element("Tutorial").Element("Info").Element("Category").Value;

                if (!tutorialsSections.ContainsKey(category))
                {
                    tutorialsSections.Add(category, new TutorialsCategoryContainer(category));
                    DockPanel.SetDock(tutorialsSections[category], Dock.Top);
                    this.ContainersDockPanel.Children.Add(tutorialsSections[category]);
                }

                if (!tutorialsSections[category].AddTutorialPreview(xmlFullPath, file.DirectoryName))
                    return false;

            }

            return true;
        }

        #region Methods

        private bool getTutorials(DirectoryInfo dir)
        {
            tutorialFiles.Clear();

            try
            {
                foreach (FileInfo file in dir.GetFiles("*.xml", SearchOption.AllDirectories))
                {
                    Console.WriteLine("File {0}", file.FullName);
                    tutorialFiles.Add(file);
                }
            }
            catch
            {
                Console.WriteLine("文件夹 {0}  \n 无法访问!!!", dir.FullName);
                return false;
            }

            return true;
        }

        #endregion
    }
}
