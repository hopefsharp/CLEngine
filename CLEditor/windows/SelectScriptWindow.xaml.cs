﻿using System.Windows;
using System.Windows.Controls;
using CLEngine.Editor.viewmodel;

namespace CLEngine.Editor.windows
{
	/// <summary>
	/// SelectScriptWindow.xaml 的交互逻辑
	/// </summary>
	public partial class SelectScriptWindow : Window
	{
		public static SelectScriptWindow Instance;

		public SelectScriptWindow()
		{
			InitializeComponent();
			Instance = this;
			ScriptTypeBox.SelectionChanged += ScriptTypeBoxOnSelectionChanged;
		}

		private void ScriptTypeBoxOnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			(DataContext as DataBaseViewModel)?.SetScriptContent();
		}
	}
}
