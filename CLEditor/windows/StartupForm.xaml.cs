﻿using System;
using System.Deployment.Application;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using CLEngine.Core;
using CLEngine.Editor.core;

namespace CLEngine.Editor.windows
{
    /// <summary>
    /// StartupForm.xaml 的交互逻辑
    /// </summary>
    public partial class StartupForm : Window
    {
        [DllImport("user32.dll")]
        static extern uint GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll")]
        static extern int SetWindowLong(IntPtr hWnd, int nIndex, uint dwNewLong);

        private const int GWL_STYLE = -16;

        private const uint WS_SYSMENU = 0x80000;

        protected override void OnSourceInitialized(EventArgs e)
        {
            IntPtr hwnd = new System.Windows.Interop.WindowInteropHelper(this).Handle;
            SetWindowLong(hwnd, GWL_STYLE,
                GetWindowLong(hwnd, GWL_STYLE) & (0xFFFFFFFF ^ WS_SYSMENU));

            base.OnSourceInitialized(e);
        }

        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        public static bool IsConnectedToInternet()
        {
            int Desc;
            return InternetGetConnectedState(out Desc, 0);
        }

        #region constructors

        public StartupForm()
        {
            //if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            //    new SplashWindow().ShowDialog();

            while (Properties.Settings.Default.UserEmail.Equals(string.Empty))
                new FirstLoginWindow().ShowDialog();
            
            if (IsConnectedToInternet() && System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            {
                try
                {
                    Task loginTask = new Task(() =>
                    {
                        string url = "http://www.hyuan.org/login.php?opt=login&email=" +
                            Properties.Settings.Default.UserEmail + "&version=" +
                            ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();

                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    });

                    loginTask.Start();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("登录发送信息时出错: " + ex.Message);
                }
            }

            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }

            this.ContentRendered += StartupForm_ContentRendered;

            if (AppDomain.CurrentDomain.SetupInformation.ActivationArguments != null)
            {
                string[] activationData = AppDomain.CurrentDomain.SetupInformation.ActivationArguments.ActivationData;
                if (activationData != null && activationData.Length > 0)
                {
                    Uri uri = new Uri(activationData[0]);
                    string path = uri.LocalPath.ToString();

                    if (File.Exists(path))
                    {
                        StartEditor(path);
                        return;
                    }
                }
            }

            if (Properties.Settings.Default.LastLoadedProjects.Trim() != string.Empty)
            {
                string[] splitted = Properties.Settings.Default.LastLoadedProjects.Split('|');
                foreach (string split in splitted)
                {
                    if (split.Trim() != string.Empty && File.Exists(split))
                    {
                        ListBoxItem item = new ListBoxItem();

                        item.Content = CHelper.SplitCamelCase(System.IO.Path.GetDirectoryName(split).Split('\\').Last());
                        item.Tag = split;

                        ProjectsListBox.Items.Add(item);
                    }
                }
            }

            if (Properties.Settings.Default.LoadLastProject && ProjectsListBox.Items.Count > 0)
                StartEditor((string)((ProjectsListBox.Items[0] as ListBoxItem).Tag));
        }

        void StartupForm_ContentRendered(object sender, EventArgs e)
        {
            //this.DataContext = new ButtonVisibilityViewModel();
        }

        #endregion

        #region methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectPath"></param>
        private void StartEditor(string projectPath)
        {
            this.Hide();

            new MainWindow(projectPath).ShowDialog();
            this.Close();
        }

        #endregion

        #region events

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void browseBtn_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "打开工程";
            ofd.Filter = @"(*.clengine)|*.clengine";

            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                EditorCommands.AddToProjectHistory(ofd.FileName);
                StartEditor(ofd.FileName);
            }
        }

        private void loadBtn_Click(object sender, RoutedEventArgs e)
        {
            if (ProjectsListBox.SelectedItem != null)
            {
                string path = (string)((ProjectsListBox.SelectedItem as ListBoxItem).Tag);

                EditorCommands.AddToProjectHistory(path);
                StartEditor(path);
            }
            else
            {
                System.Windows.MessageBox.Show("没有选择项目！\n请从列表，浏览器中选择或创建一个新项目..", "Ups", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ProjectsListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ProjectsListBox.SelectedItem != null)
            {
                string path = (string)(ProjectsListBox.SelectedItem as ListBoxItem).Tag;

                EditorCommands.AddToProjectHistory(path);
                StartEditor(path);
            }
        }

        private void newProjectBtn_Click(object sender, RoutedEventArgs e)
        {
            NewProjectWindow np = new NewProjectWindow();
            np.ShowDialog();
            
            if (np.DialogResult.Value)
            {
                EditorCommands.AddToProjectHistory(np.ProjectPath);
                StartEditor(np.ProjectPath);
            }
        }

        #endregion
    }
}
