﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using CLEngine.Editor.core;
using Page = CLEngine.Editor.core.Page;

namespace CLEngine.Editor.windows
{
    /// <summary>
    /// TutorialWindow.xaml 的交互逻辑
    /// </summary>
    public partial class TutorialWindow : Window
    {
        #region Constants

        private const int maxWidth = 640;
        private const int maxHeight = 340;

        #endregion

        #region Fields

        private string title = string.Empty;
        private List<Page> pages = new List<Page>();
        private int currentPage = 1;
        private string rootPath = string.Empty;

        #endregion

        #region Constructors

        public TutorialWindow()
        {
            InitializeComponent();
        }

        public TutorialWindow(string xmlPath, string rootPath, string title)
        {
            InitializeComponent();

            this.title = title;
            Title = title;

            if (!ReadTutorial(xmlPath))
            {
                EditorCommands.ShowOutputMessage("加载教程时出错");
                this.Close();
            }

            this.rootPath = rootPath;
            RenderPage(0);
        }

        #endregion

        #region Methods

        private bool ReadTutorial(string xmlPath)
        {
            pages.Clear();

            XDocument doc = XDocument.Load(xmlPath);

            var tutorial = from al in doc.Element("Tutorial").Element("Pages").Descendants("Page") select al;

            foreach (var page in tutorial)
            {
                Page newPage = new Page(page.Attribute("Title").Value, page.Element("Description").Value, page.Element("Image").Value);
                this.pages.Add(newPage);
            }

            if (this.pages.Count <= 0)
                return false;

            return true;
        }

        private void RenderPage(int n)
        {
            EditorUtils.RenderPicture(ref PagePicture, this.rootPath + @"\Pictures\" + this.pages[n].PicturePath, maxWidth, maxHeight);
            if (PagePicture.Source == null)
            {
                this.Close();
            }
            RenderTitleAndDescription(n);
        }

        private void RenderTitleAndDescription(int n)
        {
            // Render Title
            this.TutorialTitle.Text = string.Format("{0} ({1}/{2})", this.pages[n].Subtitle, this.currentPage, this.pages.Count); // this.pages[n].Subtitle + " (" + this.currentPage + "/" + this.pages.Count + ")";

            // Render Description
            this.txtDescription.Text = this.pages[n].Description;
        }

        #endregion

        #region Events

        private void NextBtn_Click_1(object sender, RoutedEventArgs e)
        {
            if (currentPage < this.pages.Count)
                currentPage++;
            else
                currentPage = 1;

            RenderPage(currentPage - 1);
        }

        private void PreviousBtn_Click_1(object sender, RoutedEventArgs e)
        {
            if (currentPage > 1)
                currentPage--;
            else
                currentPage = this.pages.Count;

            RenderPage(currentPage - 1);
        }

        #endregion
    }
}
