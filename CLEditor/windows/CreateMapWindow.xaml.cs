﻿using System.Windows;

namespace CLEngine.Editor.windows
{
    /// <summary>
    /// CreateMapWindow.xaml 的交互逻辑
    /// </summary>
    public partial class CreateMapWindow : Window
    {
        public int TileWidth { get; set; }
        public int TileHeight { get; set; }

        public CreateMapWindow()
        {
            InitializeComponent();
        }

        private void CreateMap_OnClick(object sender, RoutedEventArgs e)
        {
            int.TryParse(tileWidth.Text, out var rTileWidth);
            int.TryParse(tileHeight.Text, out var rTileHeight);

            TileWidth = rTileWidth;
            TileHeight = rTileHeight;

            DialogResult = true;

            Close();
        }
    }
}
