﻿using System.Windows;
using System.Windows.Forms;
using CLEngine.Editor.controls;
using CLEngine.Editor.core;
using CLEngine.Editor.model;
using CLEngine.Core;

namespace CLEngine.Editor.windows
{
    /// <summary>
    /// DeploymentWindow.xaml 的交互逻辑
    /// </summary>
    public partial class DeploymentWindow : Window
    {
        string selectedOption = "Windows";

        public DeploymentWindow()
        {
            InitializeComponent();
        }

        private void DeploymentBtn_Checked_1(object sender, RoutedEventArgs e)
        {
            foreach (RoundedButtonToggle item in EditorUtils.FindVisualChildren<RoundedButtonToggle>(ContainersDockPanel))
            {
                if (item != (sender as RoundedButtonToggle) && item.IsChecked == true)
                    item.IsChecked = false;
            }

            selectedOption = (sender as RoundedButtonToggle).Tag as string;
        }

        private void DeployBtn_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog()
            {
                ShowNewFolderButton = true,
                Description = "选择目标文件夹"
            };

            if (Properties.Settings.Default.LastDeploymentFolder != string.Empty)
                dialog.SelectedPath = Properties.Settings.Default.LastDeploymentFolder;

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                // path is fine?
                if (dialog.SelectedPath == SceneManager.GameProject.ProjectPath)
                {
                    System.Windows.Forms.MessageBox.Show("您无法选择项目的文件夹.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                Properties.Settings.Default.LastDeploymentFolder = dialog.SelectedPath;
                Properties.Settings.Default.Save();

                bool previousDebugMode = SceneManager.GameProject.Debug;
                SceneManager.GameProject.Debug = false;
                SceneManager.GameProject.Save();

                if (GlobalCommands.DeployProject(SceneManager.GameProject.ProjectPath, dialog.SelectedPath, selectedOption))
                {
                    // deployed with success!
                    if (System.Windows.Forms.MessageBox.Show("发布成功!\n\n打开输出目录?", "Success", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.Yes)
                    {
                        System.Diagnostics.Process.Start(dialog.SelectedPath);
                    }
                }

                SceneManager.GameProject.Debug = previousDebugMode;
                SceneManager.GameProject.Save();
            }
        }

        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
