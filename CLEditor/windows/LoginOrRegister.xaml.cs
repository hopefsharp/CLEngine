﻿#if Server
using System.Windows;
using CLEngine.Core.server;
using Lidgren.Network;

namespace CLEngine.Editor.windows
{
    /// <summary>
    /// LoginOrRegister.xaml 的交互逻辑
    /// </summary>
    public partial class LoginOrRegister : Window
    {
        public LoginOrRegister()
        {
            InitializeComponent();

            if (Remember.IsChecked != null && Remember.IsChecked.Value)
            {
                UserAccount.Text = Properties.Settings.Default.UserAccount;
                UserPassword.Text = Properties.Settings.Default.UserPassword;
            }

            loginBtn.Click += LoginBtnOnClick;
            registerBtn.Click += RegisterBtnOnClick;
        }

        private void RegisterBtnOnClick(object sender, RoutedEventArgs e)
        {
            var serverLR = new Server_LR();
            serverLR.Account = UserAccount.Text;
            serverLR.Password = UserPassword.Text;
            serverLR.Type = LRType.Reg;


            var message = ClientManager.Client.CreateMessage();

            if (ClientManager.Connection == null)
                ClientManager.Connection = ClientManager.Connect(serverLR);
        }

        

        private void LoginBtnOnClick(object sender, RoutedEventArgs e)
        {
            var serverLR = new Server_LR();
            serverLR.Account = UserAccount.Text;
            serverLR.Password = UserPassword.Text;
            serverLR.Type = LRType.Login;

            ClientManager.Connection =  ClientManager.Connect(serverLR);
        }
    }
}
#endif
