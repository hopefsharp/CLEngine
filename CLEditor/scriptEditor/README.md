# 新版本编辑器
对Lime编辑器进行重写

## 未来设计
### C# 
    - 智能感知
    - 颜色标注
    - 文本缩进
    - 函数展开/收缩

### Lua
    - 颜色标注
    - 文本缩进
    - 函数展开/收缩

## 文档

https://github.com/icsharpcode/AvalonEdit