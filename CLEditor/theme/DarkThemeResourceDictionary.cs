﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using CLEngine.Editor.core;
using CLEngine.Core;
using Microsoft.Win32;

namespace CLEngine.Editor.theme
{
    partial class DarkThemeResourceDictionary : ResourceDictionary
    {
        public DarkThemeResourceDictionary()
        {
            InitializeComponent();
        }

        void grid_mouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is Window)
                (sender as Window).DragMove();
        }

        void LayoutTextBox_Loaded(object sender, RoutedEventArgs e)
        {
            (sender as TextBox).Text = Properties.Settings.Default.Layout;
        }

        void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Window win = Application.Current.Windows.OfType<Window>().SingleOrDefault(x => x.IsActive);

            if (win == null || !win.IsVisible) return;

            win.Close();
        }

        void btnMaximize_Click(object sender, RoutedEventArgs e)
        {
            Window win = Application.Current.Windows.OfType<Window>().SingleOrDefault(x => x.IsActive);

            if (win == null || !win.IsVisible || (win.ResizeMode != ResizeMode.CanResize && win.ResizeMode != ResizeMode.CanResizeWithGrip) || win.WindowState == WindowState.Maximized) return;

            if (win is MainWindow) // 全屏
                (win as MainWindow).SetFullScreen(false);
            else
                win.WindowState = WindowState.Maximized;

        }

        void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            Window win = Application.Current.Windows.OfType<Window>().SingleOrDefault(x => x.IsActive);

            win.WindowState = WindowState.Minimized;

        }

        void Layouts_SelectionChanged(object sender, RoutedEventArgs e)
        {
            if ((sender as ComboBox).SelectedItem == null) return;

            string name = ((sender as ComboBox).SelectedItem as TextBlock).Text;
            LayoutHelper.LoadLayout(name);

            DependencyObject parent = EditorUtils.GetParent(sender as ComboBox, 3);
            (parent as TextBox).Text = name;
        }

        void LayoutTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string layoutName = (sender as TextBox).Text.Trim();
                if (layoutName.Equals(string.Empty)) return;

                if (LayoutHelper.RenameLayout(layoutName))
                {
                    (sender as TextBox).Text = "";
                    EditorUtils.SelectAnotherElement<TextBox>(sender as DependencyObject);
                }
                //if (LayoutHelper.LoadLayout(layoutName))
                //{
                //    (sender as TextBox).Text = "";
                //    EditorUtils.SelectAnotherElement<TextBox>(sender as DependencyObject);
                //}
                //else
                //{
                //    if (LayoutHelper.CreateNewLayout(layoutName))
                //    {
                //        (sender as TextBox).Text = "";
                //        EditorUtils.SelectAnotherElement<TextBox>(sender as DependencyObject);
                //    }
                //}
            }
        }

        void LayoutsPreviewMouseDown(object sender, MouseEventArgs e)
        {
            (sender as ComboBox).Items.Clear();

            foreach (var layout in LayoutHelper.GetLayouts())
            {
                (sender as ComboBox).Items.Add(new TextBlock()
                {
                    Margin = new Thickness(4, 0, 0, 0),
                    Text = layout
                });
            }
        }

        void removeLayoutMouseDown(object sender, RoutedEventArgs e)
        {
            DependencyObject parent = EditorUtils.GetParent(sender as TextBlock, 3);

            string layoutName = (parent as TextBox).Text.Trim();

            if (layoutName.Equals(string.Empty)) return;

            if (LayoutHelper.RemoveLayout(layoutName))
            {
                (parent as TextBox).Text = "";
                EditorUtils.SelectAnotherElement<TextBox>(parent);
            }
        }

        void addLayoutMouseDown(object sender, RoutedEventArgs e)
        {
            DependencyObject parent = EditorUtils.GetParent(sender as TextBlock, 3);

            string layoutName = (parent as TextBox).Text.Trim();

            if (layoutName.Equals(string.Empty)) return;

            if (LayoutHelper.CreateNewLayout(layoutName))
            {
                (parent as TextBox).Text = "";
                EditorUtils.SelectAnotherElement<TextBox>(parent);
            }
        }

        void TagPreviewMouseDown(object sender, MouseEventArgs e)
        {
            (sender as ComboBox).Items.Clear();

            foreach (var item in SceneManager.ActiveScene.CommonTags)
            {
                (sender as ComboBox).Items.Add(new TextBlock()
                {
                    Margin = new Thickness(4, 0, 0, 0),
                    Text = item
                });
            }
        }

        void TagItemChanged(object sender, RoutedEventArgs e)
        {
            if ((sender as ComboBox).SelectedItem == null) return;

            DependencyObject parent = EditorUtils.GetParent(sender as ComboBox, 3);
            (parent as TextBox).Text = ((sender as ComboBox).SelectedItem as TextBlock).Text;

            EditorUtils.SelectAnotherElement<TextBox>(parent);
        }

        void GlobalPathMouseDown(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "设置其他文件路径";

            // 拆分每个接受的扩展
            string[] extensions = Properties.Settings.Default.AcceptedExtensions.Split('|');

            // 设置初始过滤内容
            string filter = "接受的扩展程序|";

            // 循环遍历每个扩展并将所有扩展添加到同一类别（接受的扩展）
            foreach (var extension in extensions)
            {
                filter += "*" + extension + ";";
            }

            // 删除最后一个字符（;）
            filter = filter.Remove(filter.Length - 1);

            // 添加所有文件类别
            filter += "|All Files (*.*)|*.*";

            // 将字符串分配给打开文件对话框过滤器
            ofd.Filter = filter;

            this.ProcessDialog(sender, e, ofd, "Misc\\");
        }

        void TexturePathMouseDown(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "设置BMFont纹理路径";
            ofd.Filter = "PNG|*.png";
            this.ProcessDialog(sender, e, ofd, "Fonts\\");
        }

        void FntPathMouseDown(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "设置BMFont文件路径";
            ofd.Filter = "FNT|*.fnt";
            this.ProcessDialog(sender, e, ofd, "Fonts\\");
        }

        void AudioPathMouseDown(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "设置音频文件路径";
            ofd.Filter = "所有支持的音频类型|*.mp3;*.wav|WAV|*.wav|MP3|*.mp3";
            this.ProcessDialog(sender, e, ofd, "Audio\\");
        }

        void UIPathMouseDown(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "设置UI图片文件路径";
            ofd.Filter = "所有支持的UI图片类型|*.png";
            this.ProcessDialog(sender, e, ofd, "UI\\");
        }

        void FUIPathMouseDown(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "设置FUI文件路径";
            ofd.Filter = "所有支持的FUI类型|*.fui";
            this.ProcessDialog(sender, e, ofd, "UI\\");
        }

        void PathMouseDown(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "设置图像路径";
            ofd.Filter = "所有支持的图形类型|*.bmp;*.jpg;*.jpeg;*.png;*.tif;*.tiff" + "|BMP|*.bmp|GIF|*.gif|JPG|*.jpg;*.jpeg|PNG|*.png|TIFF|*.tif;*.tiff";
            this.ProcessDialog(sender, e, ofd, "Images\\");
        }

        void LuaPathMouseDown(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "设置脚本文件路径";
            ofd.Filter = "所有支持的脚本类型|*.lua";
            this.ProcessDialog(sender, e, ofd, "Scripts\\");
        }

        void SkePathMouseDown(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "设置Ske文件路径";
            ofd.Filter = "所有支持的类型|*.json";
            this.ProcessDialog(sender, e, ofd, "Animation\\");
        }

        void TexJsonPathMouseDown(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "设置Tex JSON文件路径";
            ofd.Filter = "所有支持的类型|*.json";
            this.ProcessDialog(sender, e, ofd, "Animation\\");
        }

        void TexImagePathMouseDown(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "设置Tex图片文件路径";
            ofd.Filter = "所有支持的类型|*.png";
            this.ProcessDialog(sender, e, ofd, "Animation\\");
        }

        void MapImagePathMouseDown(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "设置地图图片文件路径";
            ofd.Filter = "所有支持的类型|*.png;*.jpg";
            this.ProcessDialog(sender, e, ofd, "Map\\");
        }

        void VideoMouseDown(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "设置视频文件路径";
            ofd.Filter = "所有支持的类型|*.asf;*.avi;*.flv;*.mov;*.mp4;*.m4a;*.3gp";
            this.ProcessDialog(sender, e, ofd, "video\\");
        }

        void ProcessDialog(object sender, RoutedEventArgs e, OpenFileDialog ofd, string specificFolder = "")
        {
            var result = ofd.ShowDialog();
            if (result.HasValue && result.Value)
            {
                //(EditorUtils.FindVisualChildren<ScrollViewer>(parent).ElementAt(0) as ScrollViewer).Visibility = Visibility.Collapsed;
                DependencyObject parent = EditorUtils.GetParent(sender as TextBlock, 3);

                string destFolder = (SceneManager.GameProject.ProjectPath + "\\Content\\" + specificFolder).Trim();
                string filename = System.IO.Path.GetFileName(ofd.FileName);

                bool fileOnDirectory = ofd.FileName.StartsWith(SceneManager.GameProject.ProjectPath);

                if (!System.IO.Directory.Exists(destFolder) && !fileOnDirectory)
                    System.IO.Directory.CreateDirectory(destFolder);

                if (!System.IO.File.Exists(destFolder + filename) || fileOnDirectory)
                    this.SetNewPath(ofd.FileName, destFolder, specificFolder, filename, parent);
                else
                {
                    MessageBoxResult overwriteResult = MessageBox.Show("带有名称的文件 " + filename + " 已经存在。 你想覆盖它吗?", "警告", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No);
                    if (overwriteResult == MessageBoxResult.Yes)
                        this.SetNewPath(ofd.FileName, destFolder, specificFolder, filename, parent, true);
                }
            }
        }

        void SetNewPath(string srcPath, string destFolder, string specificFolder, string filename, DependencyObject parentDO, bool overwrite = false)
        {
            try
            {
                bool fileOnDirectory = srcPath.StartsWith(SceneManager.GameProject.ProjectPath);

                if (!fileOnDirectory)
                    System.IO.File.Copy(srcPath, destFolder + filename, overwrite);

                string relativePath = (@"\Content\" + specificFolder + filename).Trim();
                if (fileOnDirectory)
                    relativePath = srcPath.Replace(SceneManager.GameProject.ProjectPath, string.Empty);

                if (relativePath.StartsWith("\\"))
                    relativePath = relativePath.Substring(1, relativePath.Length - 1);

                (parentDO as TextBox).Text = relativePath;

                EditorUtils.SelectAnotherElement<TextBox>(parentDO);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void titleBarMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (e.ClickCount == 2)
                {
                    //Window win = Application.Current.Windows.OfType<Window>().SingleOrDefault(x => x.IsActive);

                    //if (win == null || !win.IsVisible || win.ResizeMode != ResizeMode.CanResize) return;

                    //if (win.WindowState == WindowState.Maximized)
                    //{
                    //    win.WindowState = WindowState.Normal;
                    //    if (win is MainWindow)
                    //        (win as MainWindow).setFullScreenName(false);
                    //}
                    //else
                    //{
                    //    if (win is MainWindow)
                    //        (win as MainWindow).SetFullScreen(false);
                    //    else
                    //        win.WindowState = WindowState.Maximized;
                    //}
                }
            }

        }
    }
}