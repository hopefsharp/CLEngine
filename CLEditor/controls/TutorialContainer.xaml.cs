﻿using System.Windows.Controls;
using System.Windows.Input;
using System.Xml.Linq;
using CLEngine.Editor.core;
using CLEngine.Editor.windows;

namespace CLEngine.Editor.controls
{
    /// <summary>
    /// TutorialContainer.xaml 的交互逻辑
    /// </summary>
    public partial class TutorialContainer : UserControl
    {
        #region Fields

        private string xmlPath = string.Empty;
        private string rootPath = string.Empty;
        private string imagePath = string.Empty;
        private string title = string.Empty;

        #endregion

        #region Constructors

        public TutorialContainer()
        {
            InitializeComponent();
        }

        public TutorialContainer(string xmlPath, string rootPath)
        {
            InitializeComponent();
            this.xmlPath = xmlPath;
            this.rootPath = rootPath;
        }

        #endregion

        #region Methods

        public bool ReadInfo()
        {
            XDocument doc = XDocument.Load(xmlPath);

            imagePath = doc.Element("Tutorial").Element("Info").Element("Image").Value;

            EditorUtils.RenderPicture(ref containerPicture, this.rootPath + @"\Pictures\" + imagePath, 200, 180);
            if (containerPicture.Source == null)
                return false;

            containerPicture.Width = 200;
            containerPicture.Height = 180;

            this.title = doc.Element("Tutorial").Element("Info").Element("Title").Value;

            TitleTextBlock.Text = this.title;
            AuthorTextBlock.Text = doc.Element("Tutorial").Element("Info").Element("Author").Value;
            DescriptionTextBlock.Text = doc.Element("Tutorial").Element("Info").Element("Description").Value;

            return true;
        }

        #endregion

        #region Events

        private void Border_MouseUp(object sender, MouseButtonEventArgs e)
        {
            TutorialWindow tutorial = new TutorialWindow(xmlPath, rootPath, this.title);
            tutorial.ShowDialog();
        }

        #endregion
    }
}
