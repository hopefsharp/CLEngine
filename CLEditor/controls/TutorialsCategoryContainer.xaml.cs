﻿using System.Windows.Controls;

namespace CLEngine.Editor.controls
{
    /// <summary>
    /// TutorialsCategoryContainer.xaml 的交互逻辑
    /// </summary>
    public partial class TutorialsCategoryContainer : UserControl
    {
        public TutorialsCategoryContainer()
        {
            InitializeComponent();
        }

        public TutorialsCategoryContainer(string category)
        {
            InitializeComponent();

            CategoryTextBlock.Text = "● " + category;
        }

        public bool AddTutorialPreview(string xmlPath, string rootPath)
        {
            TutorialContainer tutoPreview = new TutorialContainer(xmlPath, rootPath);
            if (tutoPreview.ReadInfo())
            {
                this.TutorialsWrapPanel.Children.Add(tutoPreview);
                return true;
            }

            return false;
        }
    }
}
