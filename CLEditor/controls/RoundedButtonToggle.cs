﻿using System.Windows;
using System.Windows.Controls.Primitives;

namespace CLEngine.Editor.controls
{
    public class RoundedButtonToggle : ToggleButton
    {
        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(CornerRadius),
                typeof(RoundedButtonToggle), new UIPropertyMetadata());
    }
}