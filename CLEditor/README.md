# 编辑器目录
这里存放编辑器所有核心代码/功能。主要用于可视化开发，文件结构可能会不断的发生变化

## 目录结构

- bmfont
    - 字体文件生成工具
- content
    - 编辑器所用到的资源
- controls
    - 编辑器的自定义控件
- core
    - 编辑器的核心功能
- graphics_device
    - 编辑器可视化界面
- Layout
    - 编辑器布局文件
- model
    - 编辑器的模型数据
- Project Templates
    - 游戏模板
- theme
    - 编辑器主题
- Tutorials
    - 编辑器教程
- viewmodel
    - 编辑器视图模型层
- windows
    - 编辑器窗口
- 必装运行库
    - 游戏开发所必要的文件