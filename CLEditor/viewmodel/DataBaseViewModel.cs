﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using CLEngine.Core;
using CLEngine.Core.framework;
using CLEngine.Editor.windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using MessageBox = System.Windows.Forms.MessageBox;

namespace CLEngine.Editor.viewmodel
{
    /// <summary>
    /// 数据库视图模型层
    /// </summary>
    public class DataBaseViewModel : ViewModelBase
    {
        /// <summary>
        /// 物品列表
        /// </summary>
        public ObservableCollection<ItemObject> ItemObjects { get; set; }
        /// <summary>
        /// 添加物品命令
        /// </summary>
        public RelayCommand AddItemCommand { get; set; }
		/// <summary>
		/// 删除物品命令
		/// </summary>
		public RelayCommand RemoveItemCommand { get; set; }
        /// <summary>
        /// 性别列表
        /// </summary>
        public List<string> GenderTypes {
	        get { return FrameworkSettings.GenderTypes; }
        }
		/// <summary>
		/// 物品分类
		/// </summary>
        public List<string> ItemTypes
        {
	        get { return FrameworkSettings.ItemTypes.Keys.ToList(); }
        }

		/// <summary>
		/// 脚本分类
		/// </summary>
		public List<string> ScriptTypes
		{
			get { return FrameworkSettings.ScriptTypes.ToList(); }
		}

		/// <summary>
		/// 物品子类
		/// </summary>
		public ObservableCollection<string> ItemChildTypes { get; set; }

        /// <summary>
        /// 职业列表
        /// </summary>
        public List<string> OccupationTypes {
            get { return FrameworkSettings.OccupationTypes; }
        }

		/// <summary>
		/// 背包图标浏览
		/// </summary>
		public RelayCommand BagBrowserCommand { get; set; }
		/// <summary>
		/// 掉落图标浏览
		/// </summary>
		public RelayCommand DropBrowserCommand { get; set; }
		/// <summary>
		/// 保存数据按钮
		/// </summary>
		public RelayCommand SaveCommand { get; set; }
		/// <summary>
		/// 创建脚本按钮
		/// </summary>
		public RelayCommand CreateScriptCommand { get; set; }
		/// <summary>
		/// 脚本选择按钮
		/// </summary>
		public RelayCommand ScriptSelectCommand { get; set; }
		/// <summary>
		/// 确认脚本选择
		/// </summary>
		public RelayCommand ConfirmScriptSelectCommand { get; set; }
		/// <summary>
		/// 移除脚本
		/// </summary>
		public RelayCommand RemoveScriptCommand { get; set; }
		public RelayCommand UpScriptCommand { get; set; }
		public RelayCommand DownScriptCommand { get; set; }

		private string _iconPath;
		/// <summary>
		/// 背包图标路径
		/// </summary>
		public string IconPath
		{
			get { return _iconPath; }
			set
			{
				_iconPath = value;
				RaisePropertyChanged(() => IconPath);
			}
		}

		private string _dropIconPath;
		/// <summary>
		/// 掉落图标路径
		/// </summary>
		public string DropIconPath
		{
			get { return _dropIconPath; }
			set
			{
				_dropIconPath = value;
				RaisePropertyChanged(() => DropIconPath);
			}
		}

		private string _scriptType;
		/// <summary>
		/// 脚本类型
		/// </summary>
		public string ScriptType
		{
			get { return _scriptType; }
			set
			{
				_scriptType = value;
				RaisePropertyChanged(() => ScriptType);
			}
		}

		public ObservableCollection<string> CanSelectScriptCollection { get; set; }

		private string _selectScript;
		public string SelectScript
		{
			get { return _selectScript;}
			set
			{
				_selectScript = value;
				RaisePropertyChanged(() => SelectScript);
			}
		}

		public DataBaseViewModel()
        {
            ItemObjects = GetItemObjects();
            AddItemCommand = new RelayCommand(AddItemAction);
            ItemChildTypes = new ObservableCollection<string>();
            CanSelectScriptCollection = new ObservableCollection<string>();
			// 我们需要数据通知子类发生变化
			ItemChildTypes.CollectionChanged += (sender, args) => { RaisePropertyChanged(() => ItemChildTypes); };

            BagBrowserCommand = new RelayCommand(BagBrowserAction);
			DropBrowserCommand = new RelayCommand(DropBrowserAction);
			SaveCommand = new RelayCommand(SaveAction);
			RemoveItemCommand = new RelayCommand(RemoveItemAction);
			CreateScriptCommand = new RelayCommand(CreateScriptAction);
			ScriptSelectCommand = new RelayCommand(ScriptSelectAction);
			ConfirmScriptSelectCommand = new RelayCommand(ConfirmScriptSelectAction);
			RemoveScriptCommand = new RelayCommand(RemoveScriptAction);
			UpScriptCommand = new RelayCommand(UpScriptAction);
			DownScriptCommand = new RelayCommand(DownScriptAction);

			ScriptType = "CSharp";
        }

		private void DownScriptAction()
		{
			if (DataBaseWindow.Instance.ScriptListBox.SelectedValue == null)
			{
				Logger.Error("请先选择一个脚本！");
				return;
			}

			if (DataBaseWindow.Instance.ItemList.SelectedValue is ItemObject item)
			{
				var selectItem = DataBaseWindow.Instance.ScriptListBox.SelectedValue.ToString();
				var index = item.ScriptList.IndexOf(selectItem);
				item.ScriptList.Remove(selectItem);
				item.ScriptList.Insert(index + 1 > item.ScriptList.Count ? item.ScriptList.Count : index + 1, selectItem);

				// 刷新列表
				DataBaseWindow.Instance.ScriptListBox.ItemsSource = null;
				DataBaseWindow.Instance.ScriptListBox.ItemsSource = item.ScriptList;
			}
		}

		private void UpScriptAction()
		{
			if (DataBaseWindow.Instance.ScriptListBox.SelectedValue == null)
			{
				Logger.Error("请先选择一个脚本！");
				return;
			}

			if (DataBaseWindow.Instance.ItemList.SelectedValue is ItemObject item)
			{
				var selectItem = DataBaseWindow.Instance.ScriptListBox.SelectedValue.ToString();
				var index = item.ScriptList.IndexOf(selectItem);
				item.ScriptList.Remove(selectItem);
				item.ScriptList.Insert(index - 1 < 0 ? 0 : index - 1, selectItem);

				// 刷新列表
				DataBaseWindow.Instance.ScriptListBox.ItemsSource = null;
				DataBaseWindow.Instance.ScriptListBox.ItemsSource = item.ScriptList;
			}
		}

		private void RemoveScriptAction()
		{
			if (DataBaseWindow.Instance.ScriptListBox.SelectedValue == null)
			{
				Logger.Error("请先选择一个脚本！");
				return;
			}

			if (DataBaseWindow.Instance.ItemList.SelectedValue is ItemObject item)
			{
				var selectItem = DataBaseWindow.Instance.ScriptListBox.SelectedValue.ToString();
				item.ScriptList.Remove(selectItem);

				// 刷新列表
				DataBaseWindow.Instance.ScriptListBox.ItemsSource = null;
				DataBaseWindow.Instance.ScriptListBox.ItemsSource = item.ScriptList;
			}
		}

		private void ConfirmScriptSelectAction()
		{
			if (string.IsNullOrEmpty(SelectScript))
			{
				Logger.Error("必须先选择一个脚本后才能确定!");
				return;
			}

			if (DataBaseWindow.Instance.ItemList.SelectedValue == null)
			{
				Logger.Error("清先选择一个物品后再选择脚本!");
				return;
			}

			if (DataBaseWindow.Instance.ItemList.SelectedValue is ItemObject item)
			{
				// 兼容4.0.3.8之前版本
				if (item.ScriptList == null)
					item.ScriptList = new List<string>();

				if (!item.ScriptList.Contains(SelectScript))
					item.ScriptList.Add(SelectScript);
				else
					Logger.Warn("您已添加该脚本，已自动忽略该脚本");

				// 刷新列表
				DataBaseWindow.Instance.ScriptListBox.ItemsSource = null;
				DataBaseWindow.Instance.ScriptListBox.ItemsSource = item.ScriptList;
			}

			SelectScriptWindow.Instance.Close();
		}

		private void ScriptSelectAction()
		{
			// 先设置内容再打开
			SetScriptContent();

			var scriptSelect = new SelectScriptWindow();
			scriptSelect.ShowDialog();
		}

		/// <summary>
		/// 设置脚本内容
		/// </summary>
		public void SetScriptContent()
		{
			string[] files = {};
			if (ScriptType == "CSharp")
			{
				files = Directory.GetFiles(SceneManager.GameProject.ProjectPath, "*.cs", SearchOption.AllDirectories);
			}
			else if (ScriptType == "Lua")
			{
				files = Directory.GetFiles(SceneManager.GameProject.ProjectPath, "*.lua", SearchOption.AllDirectories);
			}

			CanSelectScriptCollection.Clear();

			foreach (var file in files)
			{
				CanSelectScriptCollection.Add(file);
			}

			RaisePropertyChanged(() => CanSelectScriptCollection);
		}

		private void RemoveItemAction()
		{
			var itemList = DataBaseWindow.Instance.ItemList;
			if (itemList.SelectedValue is ItemObject item)
			{
				ItemManager.RemoveItem(item);
				ItemObjects.Remove(item);
			}
		}

		private ObservableCollection<ItemObject> GetItemObjects()
		{
			var itemObjects = new ObservableCollection<ItemObject>();
			if (ItemManager.WorldItem.Count <= 0) return itemObjects;

			foreach (var itemObject in ItemManager.WorldItem)
			{
				itemObjects.Add(itemObject.Value);
			}

			return itemObjects;
		}

		private void SaveAction()
        {
	        FrameworkManager.SaveData("item");
        }

        private void DropBrowserAction()
        {
			var fileSelectDialog = new OpenFileDialog();
			var itemObject = (ItemObject)DataBaseWindow.Instance.ItemList.SelectedItem;
			if (itemObject != null)
			{
				itemObject.DropIconPath = ProcessDialog(fileSelectDialog, "Icon\\");
				DropIconPath = itemObject.DropIconPath;
			}
		}

		private void BagBrowserAction()
        {
	        var fileSelectDialog = new OpenFileDialog();
	        var itemObject = (ItemObject) DataBaseWindow.Instance.ItemList.SelectedItem;
	        if (itemObject != null)
	        {
		        itemObject.IconPath = ProcessDialog(fileSelectDialog, "Icon\\");
		        IconPath = itemObject.IconPath;
	        }
		}

		[SuppressMessage("ReSharper", "LocalizableElement")]
		private string ProcessDialog(OpenFileDialog ofd, string specificFolder = "")
        {
	        var result = ofd.ShowDialog();
	        if (result == DialogResult.OK || result == DialogResult.Yes)
	        {
		        string destFolder = (SceneManager.GameProject.ProjectPath + "\\Content\\" + specificFolder).Trim();
		        string filename = Path.GetFileName(ofd.FileName);

		        bool fileOnDirectory = ofd.FileName.StartsWith(SceneManager.GameProject.ProjectPath);

		        if (!Directory.Exists(destFolder) && !fileOnDirectory)
			        Directory.CreateDirectory(destFolder);

		        if (!File.Exists(destFolder + filename) || fileOnDirectory)
			        return SetNewPath(ofd.FileName, destFolder, specificFolder, filename);

		        var overwriteResult = MessageBox.Show("带有名称的文件 " + filename + " 已经存在。 你想覆盖它吗?", "警告", MessageBoxButtons.YesNoCancel);
		        if (overwriteResult == DialogResult.Yes)
			        return SetNewPath(ofd.FileName, destFolder, specificFolder, filename, true);
	        }

	        return null;
        }

        private string SetNewPath(string srcPath, string destFolder, string specificFolder, string filename, bool overwrite = false)
        {
	        try
	        {
		        bool fileOnDirectory = srcPath.StartsWith(SceneManager.GameProject.ProjectPath);

		        if (!fileOnDirectory)
			        File.Copy(srcPath, destFolder + filename, overwrite);

		        string relativePath = (@"\Content\" + specificFolder + filename).Trim();
		        if (fileOnDirectory)
			        relativePath = srcPath.Replace(SceneManager.GameProject.ProjectPath, string.Empty);

		        if (relativePath.StartsWith("\\"))
			        relativePath = relativePath.Substring(1, relativePath.Length - 1);

		        return relativePath;
	        }
	        catch (Exception ex)
	        {
		        MessageBox.Show(ex.Message);
		        return null;
	        }
        }

        /// <summary>
		/// 清除子类数据并重新赋值
		/// </summary>
		/// <param name="value"></param>
        public void ClearItemChildTypes(object value)
		{
			ItemChildTypes.Clear();

			if (value == null)
		        return;

	        foreach (var itemType in FrameworkSettings.ItemTypes[value.ToString()])
		        ItemChildTypes.Add(itemType);
		}

        private void AddItemAction()
        {
            var item = ItemManager.CreateItem("物品" + ItemManager.WorldId);
            ItemObjects.Add(item);
        }

        /// <summary>
        /// 创建脚本
        /// </summary>
        private void CreateScriptAction()
        {
	        var FileName = DataBaseWindow.Instance.FileNameInput.Text;
	        var path = Path.Combine(SceneManager.GameProject.ProjectPath, "Scripts");
	        var ScriptName = Path.Combine(path, FileName);
	        if (!File.Exists(ScriptName))
	        {
	        }
        }
	}
}