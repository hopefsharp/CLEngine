﻿using System.Linq;
using System.Windows;
using CLEngine.Editor.model;

namespace CLEngine.Editor.viewmodel
{
    public class ButtonVisibilityViewModel
    {
        public ResizeBasedVisibility ButtonVisibility { get; set; }

        public ButtonVisibilityViewModel()
        {

            Window win = Application.Current.Windows.OfType<Window>().SingleOrDefault(x => x.IsActive);
            if (win != null)
            {
                ResizeBasedVisibility bar = new ResizeBasedVisibility();
                switch (win.ResizeMode)
                {
                    case ResizeMode.CanMinimize:
                        bar.MaximizeVisibility = Visibility.Hidden;
                        bar.MinimizeVisibility = Visibility.Visible;
                        break;
                    case ResizeMode.CanResize:
                    case ResizeMode.CanResizeWithGrip:
                        bar.MaximizeVisibility = Visibility.Visible;
                        bar.MaximizeVisibility = Visibility.Visible;
                        break;
                    default:
                        bar.MaximizeVisibility = Visibility.Collapsed;
                        bar.MinimizeVisibility = Visibility.Collapsed;
                        break;
                }
            }
        }
    }
}