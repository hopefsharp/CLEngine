using CLEngine.Core;
using CLEngine.Core.components;
using CLEngine.Editor.core;
using CLEngine.Editor.model;
using CLEngine.Editor.windows;
using Enterwell.Clients.Wpf.Notifications;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Windows.Media;
using CLEngine.Core.framework;

namespace CLEngine.Editor.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        public RelayCommand ShiftRightTilesetCommand { get; set; }
        public RelayCommand ShiftDownTilesetCommand { get; set; }
        public RelayCommand SaveProjectCommand { get; set; }
        public RelayCommand UndoKeyCommand { get; set; }
        public RelayCommand RedoKeyCommand { get; set; }
        public RelayCommand FullDebugCommand { get; set; }
        public RelayCommand ToCollisionBlockCommand { get; set; }
        public RelayCommand CopyKeyCommand { get; set; }
        public RelayCommand PasteKeyCommand { get; set; }
        public RelayCommand ThumbScreenshotCommand { get; set; }
        public RelayCommand NewProjectCommand { get; set; }
        public RelayCommand LoadProjectCommand { get; set; }
        public RelayCommand SaveSceneCommand { get; set; }
        public RelayCommand ManageTagsCommand { get; set; }
        public RelayCommand ProjectSettingsCommand { get; set; }
        public RelayCommand SceneSettingsCommand { get; set; }
        public RelayCommand CompileCommand { get; set; }
        public RelayCommand DebugCommand { get; set; }
        public RelayCommand ClearTextureBuffCommand { get; set; }
        /// <summary>
        /// 打开游戏数据库
        /// </summary>
        public RelayCommand DataBaseCommand { get; set; }
		/// <summary>
		/// 事件通知管理器
		/// </summary>
        public NotificationMessageManager Manager { get; set; } = App.Manager;

        public MainViewModel()
        {
            ShiftRightTilesetCommand = new RelayCommand(ShiftRightTilesetAction);
            ShiftDownTilesetCommand = new RelayCommand(ShiftDownTilesetAction);
            SaveProjectCommand = new RelayCommand(SaveProjectAction);
            UndoKeyCommand = new RelayCommand(UndoKeyAction);
            RedoKeyCommand = new RelayCommand(RedoKeyAction);
            FullDebugCommand = new RelayCommand(FullDebugAction);
            ToCollisionBlockCommand = new RelayCommand(ToCollisionBlockAction);
            CopyKeyCommand = new RelayCommand(CopyKeyAction);
            PasteKeyCommand = new RelayCommand(PasteKeyAction);
            ThumbScreenshotCommand = new RelayCommand(ThumbScreenshotAction);
            NewProjectCommand = new RelayCommand(NewProjectAction);
            LoadProjectCommand = new RelayCommand(LoadProjectAction);
            SaveSceneCommand = new RelayCommand(SaveSceneAction);
            ManageTagsCommand = new RelayCommand(ManageTagsAction);
            ProjectSettingsCommand = new RelayCommand(ProjectSettingsAction);
            SceneSettingsCommand = new RelayCommand(SceneSettingsAction);
            CompileCommand = new RelayCommand(CompileAction);
            DebugCommand = new RelayCommand(DebugAction);
            ClearTextureBuffCommand = new RelayCommand(ClearTextureBuffAction);
            DataBaseCommand = new RelayCommand(OpenDataBaseAction, CanDataBaseAction);
        }

        private void OpenDataBaseAction()
        {
            new DataBaseWindow().ShowDialog();
        }

        private bool CanDataBaseAction()
        {
            return true;
        }

        private void ClearTextureBuffAction()
        {
            TextureLoader.Clear();
        }

        private void DebugAction()
        {
            debugProject();
        }

        private void CompileAction()
        {
            compileProject();
        }

        private void SceneSettingsAction()
        {
            EditorHandler.ChangeSelectedObject(SceneManager.ActiveScene);
        }

        private void ProjectSettingsAction()
        {
            new SettingsWindow().ShowDialog();
        }

        private void ManageTagsAction()
        {
            ManageTagsWindow window = new ManageTagsWindow();

            window.ShowDialog();
        }

        private void SaveSceneAction()
        {
            SceneManager.SaveActiveScene();
            EditorCommands.ShowOutputMessage("游戏场景已保存");
        }

        private void LoadProjectAction()
        {
            EditorCommands.LoadProject();
        }

        private void NewProjectAction()
        {
            NewProjectWindow np = new NewProjectWindow();
            np.ShowDialog();

            // 一个项目被创建了
            if (np.DialogResult.Value)
            {
                //Reload();
                EditorCommands.LoadProject(np.ProjectPath);
                EditorCommands.AddToProjectHistory(np.ProjectPath);
            }
        }

        private void ThumbScreenshotAction()
        {
            EditorHandler.SceneViewControl.TakeScreenshot();
        }

        private void PasteKeyAction()
        {
            var SceneViewFormContainer = MainWindow.Instance.SceneViewFormContainer;
            var sceneTreeView = MainWindow.Instance.sceneTreeView;
            if (SceneViewFormContainer.IsFocused || sceneTreeView.canCopyPaste)
            {
                EditorCommands.PasteSelectedObjects();
            }
        }

        private void CopyKeyAction()
        {
            var SceneViewFormContainer = MainWindow.Instance.SceneViewFormContainer;
            var sceneTreeView = MainWindow.Instance.sceneTreeView;
            if (SceneViewFormContainer.IsFocused || sceneTreeView.canCopyPaste)
            {
                EditorCommands.CopySelectedObjects();
            }
        }

        private void ToCollisionBlockAction()
        {
            var sceneViewGameControl = MainWindow.Instance.sceneViewGameControl;
            var sceneTreeView = MainWindow.Instance.sceneTreeView;
            if (sceneViewGameControl.SelectionArea != Rectangle.Empty)
            {
                GameObject gameObject = new GameObject();
                gameObject.Name = "Collision Block";

                gameObject.Transform.Position =
                    new Vector2(sceneViewGameControl.SelectionArea.X + sceneViewGameControl.SelectionArea.Width / 2,
                        sceneViewGameControl.SelectionArea.Y + sceneViewGameControl.SelectionArea.Height / 2);

                SceneManager.ActiveScene.GameObjects.Add(gameObject);

                RectangleBody body = new RectangleBody();
                body.EditorExpanded = true;

                gameObject.AddComponent(body);

                body.Width = sceneViewGameControl.SelectionArea.Width;
                body.Height = sceneViewGameControl.SelectionArea.Height;
                body.BodyType = FarseerPhysics.Dynamics.BodyType.Static;

                sceneTreeView.CreateView();

                EditorHandler.SelectedGameObjects = new List<GameObject>();
                EditorHandler.SelectedGameObjects.Add(gameObject);
                EditorHandler.ChangeSelectedObjects();
            }
            else
                MessageBox.Show("没有提供有效选择，请在执行此命令之前选择一个场景", "错误");
        }

        private void FullDebugAction()
        {
            if (compileProject())
                debugProject();
        }

        private void debugProject()
        {
            EditorCommands.ApplyBlurEffect(MainWindow.Instance);
            EditorCommands.DebugGame();
            EditorCommands.ClearEffect(MainWindow.Instance);
        }

        private bool compileProject()
        {
            EditorCommands.ApplyBlurEffect(MainWindow.Instance);

            CompilerWindow cw = new CompilerWindow();

            cw.ShowDialog();
            EditorCommands.ClearEffect(MainWindow.Instance);
            EditorCommands.CreatePropertyGridView();

            return cw.Result;
        }

        private void RedoKeyAction()
        {
            EditorHandler.UnDoRedo.Redo(1);
        }

        private void UndoKeyAction()
        {
            EditorHandler.UnDoRedo.Undo(1);
        }

        private void SaveProjectAction()
        {
            EditorCommands.SaveProject();
            EditorCommands.SaveScene(false);
        }

        private void ShiftDownTilesetAction()
        {
            if (EditorHandler.SelectedGameObjects.Count == 0 || !(EditorHandler.SelectedGameObjects[0] is Tileset)) return;

            Tile[,] _tiles = (EditorHandler.SelectedGameObjects[0] as Tileset).DeepCopy();

            (EditorHandler.SelectedGameObjects[0] as Tileset).ShiftDown(1);

            TilesetCommand tc = new TilesetCommand((EditorHandler.SelectedGameObjects[0] as Tileset).Tiles, _tiles, (EditorHandler.SelectedGameObjects[0] as Tileset));
            EditorHandler.UnDoRedo.InsertUndoRedo(tc);
        }

        private void ShiftRightTilesetAction()
        {
            if (EditorHandler.SelectedGameObjects.Count == 0 || !(EditorHandler.SelectedGameObjects[0] is Tileset)) return;

            Tile[,] _tiles = (EditorHandler.SelectedGameObjects[0] as Tileset).DeepCopy();

            (EditorHandler.SelectedGameObjects[0] as Tileset).ShiftRight(1);

            TilesetCommand tc = new TilesetCommand((EditorHandler.SelectedGameObjects[0] as Tileset).Tiles, _tiles, (EditorHandler.SelectedGameObjects[0] as Tileset));
            EditorHandler.UnDoRedo.InsertUndoRedo(tc);
        }
    }
}