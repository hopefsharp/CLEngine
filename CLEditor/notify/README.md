# 编辑器新版提示
优化原有的提示功能，修改`MessageBox.Show` 为 `NotificationMessageManager`.

## 基础使用
在窗口XAML中，放置NotificationMessageContainer控件。 您可以将管理器绑定到视图模型，并通过DI或其他机制获取管理器; 或者可以从后端分配管理器实例。

```cs
this.Manager = new NotificationMessageManager();
```

```xml
<controls:NotificationMessageContainer Manager="{Binding Manager}" />
```

### 简单的“信息”通知

```cs
manager.CreateMessage()
       .Accent("#1751C3")
       .Background("#333")
       .HasBadge("信息")
       .HasMessage("将在下次应用程序重新启动时安装更新.")
       .Dismiss().WithButton("现在更新", button => { })
       .Dismiss().WithButton("更新日志", button => { })
       .Dismiss().WithButton("稍后", button => { })
       .Queue(); 
```

管理器上的`CreateMessage`创建一个空通知消息。 然后我们设置重点和背景画笔。 `HasBadge`和`HasMessage`将填充通知标记和消息内容。

`WithButton`将创建一个具有指定内容的按钮（内容不必是字符串），并在单击时指定操作回调。 如果您在WithButton之前放置Dismiss，您的按钮回调将首先被解雇通知操作拦截 - 通知将在用户输入时被取消。

队列将排队消息。 这会将消息传播到通知消息容器控件，然后显示消息（当显示多条消息时，新消息排队到堆栈的底部）

### 简单的“警告”通知

```cs
manager.CreateMessage()
       .Accent("#E0A030")
       .Background("#333")
       .HasBadge("警告")
       .HasMessage("无法检索数据.")
       .WithButton("重试", button => { })
       .Dismiss().WithButton("忽略", button => { })
       .Queue();
```

### 启用通知消息动画（选择加入功能）

```cs
manager
    ...
    .Animates(true)
    .AnimationInDuration(0.75)
    .AnimationOutDuration(2)
    ...
```

## 高级用法

### 自定义控件叠加通知

```cs
manager.CreateMessage()
       .Accent("#F15B19")
       .Background("#F15B19")
       .HasHeader("丢失了与服务器的连接")
       .HasMessage("重连中...")
       .WithOverlay(new ProgressBar
       {
           VerticalAlignment = VerticalAlignment.Bottom,
           HorizontalAlignment = HorizontalAlignment.Stretch,
           Height = 3,
           BorderThickness = new Thickness(0),
           Foreground = new SolidColorBrush(Color.FromArgb(128, 255, 255, 255)),
           Background = Brushes.Transparent,
           IsIndeterminate = true,
           IsHitTestVisible = false
       })
       .Queue();
```

`WithOverlay`允许您设置自定义叠加内容。 在此示例中，进度条位于通知控件的底部。 请注意，`IsHitTextVisible`设置为`false`，以便通知消息按钮不会因为覆盖控件位于按钮底部而失去焦点

### 自定义附加通知内容

```cs
manager.CreateMessage()
       .Accent("#1751C3")
       .Background("#333")
       .HasBadge("信息")
       .HasHeader("可用更新")
       .HasMessage("将在下次应用程序重新启动时安装更新.")
       .Dismiss().WithButton("现在更新", button => { })
       .Dismiss().WithButton("稍后", button => { })
       .WithAdditionalContent(ContentLocation.Bottom,
       new Border
       {
           BorderThickness = new Thickness(0,1,0,0),
           BorderBrush = new SolidColorBrush(Color.FromArgb(128, 28, 28, 28)),
           Child = new CheckBox
           {
               Margin = new Thickness(12, 8, 12, 8),
               HorizontalAlignment = HorizontalAlignment.Left,
               Content = "不再显示"
           }
       })
       .Queue();
```

在此示例中，自定义“不再显示”复选框位于邮件的底部。

您可以将其他内容添加到以下位置：Top，Bottom，Left，Right，Main，AboveBadge。

### 多个通知

通知消息容器具有内置支持，可同时显示多个通知。 新通知将显示在邮件堆栈的底部。

#### 不喜欢扩展方法？

扩展方法不包含用于实例化通知的任何复杂逻辑，因此您仍然可以直接使用`NotificationMessageBuilder`类来构建通知消息，而无需扩展方法。

```cs
var builder = NotificationMessageBuilder.CreateMessage();
builder.Manager = this.Manager;
builder.Message = this.Manager.Factory.GetMessage();
builder.SetAccent(Brushes.DodgerBlue);
builder.SetBackground(Brushes.DimGray);
builder.SetBadge("信息");
builder.HasMessage("此通知不使用扩展方法构建.");

var notificationButton = this.Manager.Factory.GetButton();
notificationButton.Content = "内容";
notificationButton.Callback = (button) =>
{
    this.Manager.Dismiss(builder.Message);
    // ... 做其余的事
};

builder.AddButton(notificationButton);
builder.Manager.Queue(builder.Message);
```

#### 不喜欢builder？
也支持标准控件实例化。

```cs
this.Manager.Queue(new NotificationMessage
{
    Message = "没有消息构建器的Works事件",
    BadgeText = "消息",
    AccentBrush = Brushes.Orange,
    Background = Brushes.Black,
    Buttons = new ObservableCollection<object>
    {
        new NotificationMessageButton()
        {
            Content = "很棒!",
            Callback = button => { }
        }
    }
});
```