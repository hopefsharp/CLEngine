-- 游戏初始化
function init()
	GlobalLogger.GetLogger("lua"):Info("游戏初始化")
end

--[[
	* 游戏逻辑更新
	* gameTime: 游戏循环时间
]]
function update(gameTime)

end

--[[
	* 游戏帧更新
	* 图形绘制更新
	* gameTime: 游戏循环时间
]]
function draw(gameTime)

end

-- 游戏加载资源
function loadContent()
	GlobalLogger.GetLogger("lua"):Info("游戏加载资源")
end

-- 游戏卸载资源
function unLoadContent()

end