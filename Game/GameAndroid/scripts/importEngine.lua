import("System")
import("System.TimeSpan")

import("CLEngine")
import("CLEditor")
import("CLEditor.Core")
import("CLEditor.Core.Diagnostics")
import("CLEditor.Core.Diagnostics.GlobalLogger")

luanet.load_assembly("MonoGame.Framework")
import("Microsoft.Xna.Framework")
import("Microsoft.Xna.Framework.Graphics")
import("Microsoft.Xna.Framework.Graphics.SamplerState")
import("Microsoft.Xna.Framework.Color")
import("Microsoft.Xna.Framework.Texture2D")
import("Microsoft.Xna.Framework.Graphics.GraphicsProfile")
import("Microsoft.Xna.Framework.Input")
import("Microsoft.Xna.Framework.Input.Keys")
import("Microsoft.Xna.Framework.Audio")

luanet.load_assembly("MonoGame.Extended")
import("MonoGame.Extended")
import("MonoGame.Extended.HslColor")
import("MonoGame.Extended.Camera2D")
import("MonoGame.Extended.Sprites")
import("MonoGame.Extended.BitmapFonts")
import("MonoGame.Extended.TextureAtlases")
import("MonoGame.Extended.TextureAtlases.TextureRegion2D")

import("MonoGame.Extended.Gui")
import("MonoGame.Extended.Gui.Controls")

import("MonoGame.Extended.Particles")
import("MonoGame.Extended.Particles.ParticleEffect")
import("MonoGame.Extended.Particles.Profiles")
import("MonoGame.Extended.Particles.Modifiers")
import("MonoGame.Extended.Particles.Modifiers.Interpolators")

import("MonoGame.Extended.Tiled")
import("MonoGame.Extended.Tiled.TiledMap")
import("MonoGame.Extended.Tiled.Graphics")
import("MonoGame.Extended.Tiled.Graphics.TiledMapRenderer")

import("MonoGame.Extended.Graphics")

import("MonoGame.Extended.Entities")

import("MonoGame.Extended.Animations")

import("MonoGame.Extended.SceneGraphs")

import("MonoGame.Extended.Tweening")

import("MonoGame.Extended.Collisions")

import("MonoGame.Extended.Input")

luanet.load_assembly("DragonBonesMG")
import("DragonBonesMG")
import("DragonBonesMG.Core")

luanet.load_assembly("VelcroPhysics.MonoGame")
import("VelcroPhysics.Dynamics")
import("VelcroPhysics.Factories")
import("VelcroPhysics.Dynamics.BodyType")