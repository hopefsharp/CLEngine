﻿using System;
using System.IO;
using System.Text;

namespace game_win
{
    class MyTextWriterArgs : EventArgs
    {
        public String Text { get; set; }
    }

    public class MyTextWriter : TextWriter
    {
        public event EventHandler ConsoleOutput;
        private string curLine = string.Empty;

        public override void Write(char value)
        {
            base.Write(value);

            curLine += value.ToString();

            if (value.Equals('\n') || value.Equals('\r') || value.ToString().Equals(Environment.NewLine))
            {
                if (ConsoleOutput != null)
                {
                    ConsoleOutput(this, new MyTextWriterArgs() { Text = curLine });
                }

                curLine = string.Empty;
            }
        }

        public override Encoding Encoding
        {
            get { return Encoding.UTF8; }
        }
    }
}