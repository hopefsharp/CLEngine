﻿using System;
using System.IO;
using CLEngine.Core;

namespace game_win
{
    class Program
    {
        public static MyTextWriter textWritter;

        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                textWritter = new MyTextWriter();
                textWritter.ConsoleOutput += textWritter_ConsoleOutput;

                using (var game = new MainGame())
                {
                    if (args.Length > 0)
                    {
                        Console.WriteLine("arg count: " + args.Length);

                        if (args.Length >= 2)
                        {
                            int px = int.Parse(args[0]);
                            int py = int.Parse(args[1]);
                            game.preferredPositionX = px;
                            game.preferredPositionY = py;
                        }

                        int cnt = 0;
                        foreach (var arg in args)
                        {
                            cnt++;
                            Console.WriteLine("ARG[" + cnt + "]: " + arg);
                        }
                    }

                    SceneManager.GameArgs = args;

                    game.Run();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message, "error");
                throw new Exception(e.Message);
            }
            
        }

        static void textWritter_ConsoleOutput(object sender, EventArgs e)
        {
            if (SceneManager.GameProject != null)
                using (StreamWriter w = File.AppendText(Path.Combine(SceneManager.GameProject.ProjectPath, "log.txt")))
                {
                    w.WriteLine((e as MyTextWriterArgs)?.Text);
                }
            else
                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    w.WriteLine((e as MyTextWriterArgs)?.Text);
                }
        }
    }
}
