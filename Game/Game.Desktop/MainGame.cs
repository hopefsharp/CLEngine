﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using CLEngine.Core;
using FairyGUI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NvgSharp;
using PerfStat;

namespace game_win
{
    public class MainGame : Core
    {
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;

        private GraphicsDeviceManager graphics;
        internal int preferredPositionX = -1;
        internal int preferredPositionY = -1;
        private string originalTitle;
        private string projectFilePath;
        private SpriteBatch spriteBatch;
        private bool fullscreenSet = false;
        private bool showProfile = true;
        private float deltaFPSTime;
        private float fps;
        private float fpsCount;

        private NvgContext _nvgContext;
        private readonly PerfGraphWidget _perfGraph = new PerfGraphWidget();

        private bool preferredFullScreenMode = false;

        public MainGame()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreparingDeviceSettings += GraphicsOnPreparingDeviceSettings;

            Content.RootDirectory = "Content";
            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);
        }

        protected override void Initialize()
        {
            InitializeSettings();

            SceneManager.ScriptsAssembly = Assembly.LoadFile(Environment.CurrentDirectory + "\\libs\\Scripts.dll");

            if (!File.Exists(projectFilePath)) Exit();

            SceneManager.GameProject = CProject.Load(projectFilePath);
            SceneManager.GameWindow = this;

            //IsFixedTimeStep = (SceneManager.GameProject.ProjectSettings.VSyncEnabled) ? true : false;

            if (SceneManager.GameProject.Debug)
            {
                Console.WriteLine("CLEngine 2D - 游戏引擎控制台");
                Console.WriteLine("场景加载成功!");
                Console.WriteLine("工程路径: " + SceneManager.GameProject.ProjectPath);
                Console.WriteLine("程序集路径: " + SceneManager.ScriptsAssembly.Location.ToString());
            }

            Window.Title = SceneManager.GameProject.ProjectName;
            originalTitle = SceneManager.GameProject.ProjectName;

            Components.Add(new Stage(this));

            base.Initialize();
        }

        protected override void Draw(GameTime gameTime)
        {
            if (SceneManager.ActiveScene != null)
            {
                GraphicsDevice.Clear(SceneManager.ActiveScene.BackgroundColor);
            }
            else
            {
                GraphicsDevice.Clear(Color.CornflowerBlue);
            }

            try
            {
                if (SceneManager.ActiveScene != null)
                {
                    SceneManager.Draw(gameTime);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ups: " + ex.ToString() + "\nTarget:>" + ex.TargetSite);
                throw new Exception(ex.Message);
            }

            base.Draw(gameTime);

            if (showProfile)
            {
                _nvgContext.BeginFrame(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight, 1.0f);
                _perfGraph.Render(_nvgContext, 5, 5);
                _nvgContext.EndFrame();
            }
        }

        protected override void Update(GameTime gameTime)
        {
            if (preferredFullScreenMode && !fullscreenSet)
            {
                SceneManager.Graphics.IsFullScreen = true;
                SceneManager.Graphics.ApplyChanges();
                fullscreenSet = true;
            }

            if (showProfile)
                _perfGraph.Update(gameTime.ElapsedGameTime.TotalSeconds);

            try
            {
                if (SceneManager.ActiveScene != null)
                {
                    SceneManager.Update(gameTime);

                    if (!showProfile)
                        if (SceneManager.GameProject.Debug)
                        {
                            deltaFPSTime += (float)gameTime.ElapsedGameTime.TotalSeconds;
                            if (deltaFPSTime >= 1)
                            {
                                fps = fpsCount;
                                fpsCount = 0;
                                deltaFPSTime = 0;

                                Window.Title = originalTitle + " - FPS: " + fps.ToString(CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                fpsCount++;
                            }
                        }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ups: " + ex + "\nTarget:>" + ex.TargetSite);
                throw new Exception(ex.Message);
            }

            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            // 创建一个新的SpriteBatch，可用于绘制纹理。
            spriteBatch = new SpriteBatch(GraphicsDevice);
            _nvgContext = new NvgContext(GraphicsDevice);

            if (!File.Exists(SceneManager.GameProject.ProjectPath + "\\" + SceneManager.GameProject.SceneStartPath)) Exit();

            SceneManager.Content = Content;
            SceneManager.SpriteBatch = spriteBatch;
            SceneManager.GraphicsDevice = GraphicsDevice;
            SceneManager.Graphics = graphics;
            SceneManager.IsEditor = false;



            if (!SceneManager.LoadScene(SceneManager.GameProject.SceneStartPath))
                Exit();
        }

        private void InitializeSettings()
        {
            //AllocConsole();           
            IniFile settings = new IniFile(AppDomain.CurrentDomain.BaseDirectory + "\\settings.ini");

            try
            {
                var isEnbaleConsole = settings.IniReadValue("Console", "WriteToConsole").ToLower().Trim().Equals("true");
                if (!isEnbaleConsole)
                {
                    Console.SetOut(Program.textWritter);
                }

                // arguments
                string arguments = settings.IniReadValue("Console", "Arguments");
                //Console.Info("arguments: " + arguments);
                if (arguments.Trim() != string.Empty && SceneManager.GameArgs.Length == 0)
                {
                    SceneManager.GameArgs = arguments.Trim().Split(',');
                }

                string extra = settings.IniReadValue("Console", "Extra");
                Console.WriteLine("Extra: " + extra);
                if (arguments.Trim() != string.Empty)
                {
                    SceneManager.GameExtra = extra.Trim().Split(',');
                }

                // Console
                bool showConsole = settings.IniReadValue("Console", "Visible").ToLower().Trim().Equals("true") ? true : false;
                if (!showConsole)
                {
                    var handle = GetConsoleWindow();

                    // Hide
                    ShowWindow(handle, SW_HIDE);
                }

                bool isBorderLess = settings.IniReadValue("Window", "IsBorderLess").ToLower().Trim().Equals("true");
                if (isBorderLess)
                {
                    Window.IsBorderless = true;
                    Console.WriteLine("BorderLess模式已激活");
                }
                else
                {
                    Console.WriteLine("没有激活边框模式");
                }

                // Cursor
                bool showCursor = settings.IniReadValue("Mouse", "Visible").ToLower().Trim().Equals("true");
                if (showCursor)
                    IsMouseVisible = true;

                graphics.PreferredBackBufferWidth = Convert.ToInt32(settings.IniReadValue("Window", "Width").Trim());
                graphics.PreferredBackBufferHeight = Convert.ToInt32(settings.IniReadValue("Window", "Height").Trim());

                // Full Screen
                bool fullScreen = settings.IniReadValue("Window", "StartFullScreen").ToLower().Trim().Equals("true");
                if (fullScreen)
                {
                    //graphics.ToggleFullScreen();
                    //graphics.IsFullScreen = true;

                    preferredFullScreenMode = true;
                }
                else
                {
                    /*
                    Type type = typeof(OpenTKGameWindow);
                    System.Reflection.FieldInfo field = type.GetField("window", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                    OpenTK.GameWindow window = (OpenTK.GameWindow)field.GetValue(Window);

                    string posx = settings.IniReadValue("Window", "PositionX").Trim();
                    string posy = settings.IniReadValue("Window", "PositionY").Trim();

                    if (preferredPositionX != -1)
                    {
                        window.X = preferredPositionX;
                    }
                    else if (posx != string.Empty)
                    {
                        int px = int.Parse(posx);
                        window.X = px;
                    }
                    else
                    {
                        window.X = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width / 2 - graphics.PreferredBackBufferWidth / 2;
                    }

                    if (preferredPositionY != -1)
                    {
                        window.Y = preferredPositionY;
                    }
                    else if (posy != string.Empty)
                    {
                        int py = int.Parse(posy);
                        window.Y = py;
                    }
                    else
                    {
                        window.Y = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height / 2 - graphics.PreferredBackBufferHeight / 2;
                    }*/
                }

                // Profile
                this.showProfile = settings.IniReadValue("Profile", "Visible").ToLower().Trim().Equals("true");

                // Update Settings
                graphics.ApplyChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw new Exception(ex.Message);
            }

            // 搜索项目文件
            foreach (string filePath in Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory))
            {
                if (System.IO.Path.GetExtension(filePath).ToLower().Equals(".clengine"))
                {
                    projectFilePath = filePath;
                    break;
                }
            }
        }

        static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            Assembly objExecutingAssemblies;
            string strTempAssmbPath = "";

            objExecutingAssemblies = Assembly.GetExecutingAssembly();
            AssemblyName[] arrReferencedAssmbNames = objExecutingAssemblies.GetReferencedAssemblies();

            //循环遍历引用的程序集名称数组
            foreach (AssemblyName strAssmbName in arrReferencedAssmbNames)
            {
                //检查引发“AssemblyResolve”事件的程序集名称
                if (strAssmbName.FullName.Substring(0, strAssmbName.FullName.IndexOf(",")) == args.Name.Substring(0, args.Name.IndexOf(",")))
                {
                    //从必须加载的位置构建程序集的路径		
                    strTempAssmbPath = SceneManager.GameProject.ProjectPath + "\\" + args.Name.Substring(0, args.Name.IndexOf(",")) + ".dll";
                    break;
                }
            }

            if (strTempAssmbPath == "")
            {
                foreach (string fileName in Directory.GetFiles(SceneManager.GameProject.ProjectPath + "\\libs\\"))
                {
                    string asmName = System.IO.Path.GetFileName(fileName);
                    if (asmName.Replace(".dll", "") == args.Name.Substring(0, args.Name.IndexOf(",")))
                    {
                        strTempAssmbPath = SceneManager.GameProject.ProjectPath + "\\libs\\" + asmName;
                        break;
                    }
                }
            }

            if (strTempAssmbPath == "")
            {
                return SceneManager.ScriptsAssembly;
            }

            //加载并返回加载的组件
            return Assembly.LoadFrom(strTempAssmbPath);
        }

        private void GraphicsOnPreparingDeviceSettings(object sender, PreparingDeviceSettingsEventArgs e)
        {
            try
            {
                e.GraphicsDeviceInformation.PresentationParameters.PresentationInterval = PresentInterval.Two;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw new Exception(exception.Message);
            }
        }
    }
}