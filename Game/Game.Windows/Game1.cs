﻿using CLEngine;

namespace Game.Windows
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : CGame
    {
        public Game1()
        {
            Content.RootDirectory = "Content";
        }
    }
}
