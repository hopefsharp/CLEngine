﻿using System;

namespace CLEngine.Core
{
    public class Timer : ITimer
    {
        public object Context { get; set; }

        float _timeInSeconds;
        bool _repeats;
        Action<ITimer> _onTime;
        bool _isDone;
        float _elapsedTime;


        public void Stop()
        {
            _isDone = true;
        }

        public void Reset()
        {
            _elapsedTime = 0f;
        }

        public T GetContext<T>()
        {
            return (T)Context;
        }

        internal bool tick()
        {
            // 如果在tick之前调用stop，则isDone将为true
            if (!_isDone && _elapsedTime > _timeInSeconds)
            {
                _elapsedTime -= _timeInSeconds;
                _onTime(this);

                if (!_isDone && !_repeats)
                    _isDone = true;
            }

            _elapsedTime += Time.deltaTime;

            return _isDone;
        }

        internal void initialize(float timeInSeconds, bool repeats, object context, Action<ITimer> onTime)
        {
            _timeInSeconds = timeInSeconds;
            _repeats = repeats;
            Context = context;
            _onTime = onTime;
        }

        /// <summary>
        /// 将对象引用置零，以便GC可以根据需要进行拾取
        /// </summary>
        internal void unload()
        {
            Context = null;
            _onTime = null;
        }
    }
}