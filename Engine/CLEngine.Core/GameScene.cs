﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using CLEngine.Core.farseer;
using FarseerPhysics;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace CLEngine.Core
{
    /// <summary>
    /// 游戏场景
    /// </summary>
#if WIN
    [Serializable]
#endif
    [DataContract(Namespace = "")]
    public class GameScene : IDisposable
    {
#if WIN
        [NonSerialized]
      
#endif
        [IgnoreDataMember, XmlIgnore]
        protected ContentManager content;

        [DataMember]
        private string name = "游戏场景";

        [DataMember]
        private Color backgroundColor = Color.FromNonPremultiplied(50, 50, 50, 255);

#if WIN
        [NonSerialized]       
#endif
        [IgnoreDataMember, XmlIgnore]
        protected GraphicsDeviceManager graphics;

#if WIN
        [NonSerialized]     
#endif
        [IgnoreDataMember, XmlIgnore]
        protected SpriteBatch spriteBatch;

        [DataMember]
        private List<string> commonTags = new List<string>();

        [DataMember]
        private GameObjectCollection gameObjects = new GameObjectCollection(null);

        [DataMember]
        private Camera camera = new Camera();

#if WIN
        [Browsable(false)]
#endif
        [IgnoreDataMember, XmlIgnore]
        public ContentManager Content
        {
            get { return content; }
            set { content = value; }
        }

#if WIN
        [Browsable(false)]
#endif
        [IgnoreDataMember, XmlIgnore]
        public World World
        {
            get { return world; }
            set { world = value; }
        }

#if WIN
        [Browsable(false)]
#endif
        [IgnoreDataMember, XmlIgnore]
        public SpriteBatch SpriteBatch
        {
            get { return spriteBatch; }
            set { spriteBatch = value; }
        }

#if WIN
        [Browsable(false)]
#endif
        [IgnoreDataMember, XmlIgnore]
        public GraphicsDeviceManager Graphics
        {
            get { return graphics; }
            set { graphics = value; }
        }

#if WIN
        [Category("场景基础属性"), Browsable(false)]
#endif
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

#if WIN
        [EditorAttribute(typeof(XNAColorEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("场景基础属性")]
        [DisplayName("清屏颜色"), Description("清屏颜色")]
#endif
        public Color BackgroundColor
        {
            get { return backgroundColor; }
            set { backgroundColor = value; }
        }

#if WIN
        [Category("场景基础属性")]
        [DisplayName("重力"), Description("重力")]
#endif
        public Vector2 Gravity
        {
            get { return gravity; }
            set
            {
                gravity = value;
                world.Gravity = value;
            }
        }

#if WIN
        [Category("场景基础属性")]
        [DisplayName("相机"), Description("激活相机")]
#endif
        public Camera Camera
        {
            get { return camera; }
            set { camera = value; }
        }

#if WIN
        [Browsable(false)]
#endif
        public List<string> CommonTags
        {
            get { return commonTags; }
            set { commonTags = value; }
        }

#if WIN
        [Category("场景属性"), Browsable(false)]
#endif
        public GameObjectCollection GameObjects
        {
            get { return gameObjects; }
            set { gameObjects = value; }
        }

#if WIN
        [NonSerialized]
#endif
        [IgnoreDataMember, XmlIgnore]
        private World world;

#if WIN
        [NonSerialized]
#endif
        [IgnoreDataMember, XmlIgnore]
        internal List<GameObject> markedForRemoval;

#if WIN
        [NonSerialized]
#endif
        [IgnoreDataMember, XmlIgnore]
        private DebugView2D debugView;

#if WIN
        [NonSerialized]
#endif
        [IgnoreDataMember, XmlIgnore]
        private List<RenderView> renderViews = new List<RenderView>();

        [DataMember]
        private Vector2 gravity = Vector2.UnitY * 10;

        public GameScene()
        {

        }

        /// <summary>
        /// 初始化游戏场景。
        ///此场景的图层，游戏对象和组件也已初始化
        /// </summary>
        public virtual void Initialize()
        {
            if (renderViews == null)
                renderViews = new List<RenderView>();

            if (CommonTags == null)
                CommonTags = new List<string>();

            markedForRemoval = new List<GameObject>();

            if (world != null)
            {
                //foreach (var obj in world.BodyList)
                //    world.RemoveBody(obj);

                //world.Step(0);
            }
            else
            {
                world = new World(gravity);
            }

            debugView = new DebugView2D(world);
            debugView.RemoveFlags(DebugViewFlags.Controllers);
            debugView.LoadContent(SceneManager.GraphicsDevice, Content);

            //if(SceneManager.IsEditor)
            //    debugView.Flags = DebugViewFlags.PerformanceGraph | DebugViewFlags.DebugPanel | DebugViewFlags.Joint | DebugViewFlags.Shape;

            for (int i = 0; i < gameObjects.Count; i++)
                gameObjects[i].Initialize();
        }

        public void Update(GameTime gameTime)
        {
            for (int i = 0; i < gameObjects.Count; i++)
                if (!gameObjects[i].Disabled)
                    gameObjects[i].Update(gameTime);

            if (SceneManager.IsEditor)
                world.Step(0);
            else
            {
                world.Step((float)gameTime.ElapsedGameTime.TotalMilliseconds * 0.001f);
                //Console.Info((float)gameTime.ElapsedGameTime.TotalMilliseconds * 0.001f);
            }

            for (int i = markedForRemoval.Count - 1; i >= 0; i--)
            {
                markedForRemoval[i].Delete();
                markedForRemoval.RemoveAt(i);
            }
        }

        public void Draw(GameTime gameTime)
        {
            SceneManager.drawPass = 0;

            if (renderViews.Count == 0)
            {
                for (int i = 0; i < gameObjects.Count; i++)
                    if (!gameObjects[i].Disabled)
                        gameObjects[i].Draw(gameTime, this.SpriteBatch);

                DrawCollisionView(SceneManager.GraphicsDevice.Viewport, SceneManager.ActiveCamera);
            }
            else
            {
                // save defaults
                Viewport defaultViewport = SceneManager.GraphicsDevice.Viewport;
                Camera defaultCamera = SceneManager.ActiveCamera;

                foreach (RenderView v in renderViews)
                {
                    SceneManager.GraphicsDevice.Viewport = v.Viewport;
                    SceneManager.ActiveScene.Camera = v.Camera;
                    SceneManager.ActiveCamera = v.Camera;

                    for (int i = 0; i < gameObjects.Count; i++)
                        if (!gameObjects[i].Disabled)
                            gameObjects[i].Draw(gameTime, this.SpriteBatch);

                    if (v.Camera != null)
                        DrawCollisionView(v.Viewport, v.Camera);

                    SceneManager.drawPass++;
                }

                // load defaults
                SceneManager.GraphicsDevice.Viewport = defaultViewport;
                SceneManager.ActiveScene.camera = defaultCamera;
                SceneManager.ActiveCamera = defaultCamera;
            }
        }

        private void DrawCollisionView(Viewport viewport, Camera camera)
        {
            if ((SceneManager.IsEditor && SceneManager.GameProject.EditorSettings.ShowCollisions)
                || (!SceneManager.IsEditor && SceneManager.GameProject.Debug && SceneManager.GameProject.EditorSettings.ShowCollisions))
            {
                var projection = Matrix.CreateOrthographicOffCenter(
                    0f,
                    ConvertUnits.ToSimUnits(viewport.Width),
                    ConvertUnits.ToSimUnits(viewport.Height), 0f, 0f,
                    1f);

                var view = Matrix.CreateTranslation(
                               -ConvertUnits.ToSimUnits(camera.Position.X),
                               -ConvertUnits.ToSimUnits(camera.Position.Y), 0.0f) *
                           Matrix.CreateRotationZ(camera.Rotation) *
                           Matrix.CreateScale(
                               new Vector3((float)camera.Zoom,
                                   (float)camera.Zoom, 1)) *
                           Matrix.CreateTranslation(
                               ConvertUnits.ToSimUnits(viewport.Width / 2),
                               ConvertUnits.ToSimUnits(viewport.Height / 2), 0.0f);

                debugView.RenderDebugData(ref projection, ref view);
            }
        }

        public void SaveComponentValues()
        {
            foreach (GameObject obj in gameObjects)
                obj.SaveComponentValues();
        }

        public virtual void LoadContent() { }

        public override string ToString()
        {
            return this.Name;
        }

        public void Dispose()
        {
            foreach (GameObject obj in this.gameObjects)
                obj.Dispose();

            GC.SuppressFinalize(this);
        }
    }
}