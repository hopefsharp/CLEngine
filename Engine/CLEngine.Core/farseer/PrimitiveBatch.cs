﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CLEngine.Core.farseer
{
    public class PrimitiveBatch : IDisposable
    {
        private const int DefaultBufferSize = 500;

        /// <summary>
        /// 一个基本效果，包含我们将用于绘制我们的着色器原语
        /// </summary>
        private BasicEffect _basicEffect;

        /// <summary>
        /// 我们将发出绘图调用的设备
        /// </summary>
        private GraphicsDevice _device;

        /// <summary>
        /// 一旦调用了Begin，hasBegun就会被转换为true，并且用于制作
        /// 确保用户在调用Begin之前不调用End
        /// </summary>
        private bool _hasBegun;

        private bool _isDisposed;
        private int _triangleVertsCount;
        private int _lineVertsCount;

        private VertexPositionColor[] _lineVertices;
        private VertexPositionColor[] _triangleVertices;

        public PrimitiveBatch(GraphicsDevice graphicsDevice)
            : this(graphicsDevice, DefaultBufferSize)
        {
        }

        public PrimitiveBatch(GraphicsDevice graphicsDevice, int bufferSize)
        {
            if (graphicsDevice == null)
            {
                throw new ArgumentNullException("graphicsDevice");
            }
            _device = graphicsDevice;

            _triangleVertices = new VertexPositionColor[bufferSize - bufferSize % 3];
            _lineVertices = new VertexPositionColor[bufferSize - bufferSize % 2];

            // 设置新的基本效果，并启用顶点颜色
            _basicEffect = new BasicEffect(graphicsDevice);
            _basicEffect.VertexColorEnabled = true;
        }

        public void Begin(ref Matrix projection, ref Matrix view)
        {
            if (_hasBegun)
            {
                throw new InvalidOperationException("必须先调用End才能再次调用Begin.");
            }

            //告诉我们开始的基本效果
            _basicEffect.Projection = projection;
            _basicEffect.View = view;
            _basicEffect.CurrentTechnique.Passes[0].Apply();

            // 翻转错误检查布尔值。 现在可以调用AddVertex，Flush，
            // 结束
            _hasBegun = true;
        }

        public void End()
        {
            if (!_hasBegun)
            {
                throw new InvalidOperationException("必须在调用End之前调用Begin.");
            }

            // 绘制用户希望我们绘制的内容
            FlushTriangles();
            FlushLines();

            _hasBegun = false;
        }

        public void AddVertex(Vector2 vertex, Color color, PrimitiveType primitiveType)
        {
            if (!_hasBegun)
            {
                throw new InvalidOperationException("必须先调用Begin，然后才能调用AddVertex.");
            }
            if (primitiveType == PrimitiveType.LineStrip ||
                primitiveType == PrimitiveType.TriangleStrip)
            {
                throw new NotSupportedException("PrimitiveBatch不支持指定的primitiveType.");
            }

            if (primitiveType == PrimitiveType.TriangleList)
            {
                if (_triangleVertsCount >= _triangleVertices.Length)
                {
                    FlushTriangles();
                }
                _triangleVertices[_triangleVertsCount].Position = new Vector3(vertex, -0.1f);
                _triangleVertices[_triangleVertsCount].Color = color;
                _triangleVertsCount++;
            }
            if (primitiveType == PrimitiveType.LineList)
            {
                if (_lineVertsCount >= _lineVertices.Length)
                {
                    FlushLines();
                }
                _lineVertices[_lineVertsCount].Position = new Vector3(vertex, 0f);
                _lineVertices[_lineVertsCount].Color = color;
                _lineVertsCount++;
            }
        }

        public bool IsReady()
        {
            return _hasBegun;
        }

        private void FlushLines()
        {
            if (!_hasBegun)
            {
                throw new InvalidOperationException("必须先调用Begin才能调用Flush.");
            }
            if (_lineVertsCount >= 2)
            {
                int primitiveCount = _lineVertsCount / 2;
                // 将绘图调用提交给图形卡
                _device.SamplerStates[0] = SamplerState.AnisotropicClamp;
                _device.DrawUserPrimitives(PrimitiveType.LineList, _lineVertices, 0, primitiveCount);
                _lineVertsCount -= primitiveCount * 2;
            }
        }

        private void FlushTriangles()
        {
            if (!_hasBegun)
            {
                throw new InvalidOperationException("必须先调用Begin才能调用Flush.");
            }
            if (_triangleVertsCount >= 3)
            {
                int primitiveCount = _triangleVertsCount / 3;
                // submit the draw call to the graphics card
                _device.SamplerStates[0] = SamplerState.AnisotropicClamp;
                _device.DrawUserPrimitives(PrimitiveType.TriangleList, _triangleVertices, 0, primitiveCount);
                _triangleVertsCount -= primitiveCount * 3;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && !_isDisposed)
            {
                if (_basicEffect != null)
                    _basicEffect.Dispose();

                _isDisposed = true;
            }
        }
    }
}