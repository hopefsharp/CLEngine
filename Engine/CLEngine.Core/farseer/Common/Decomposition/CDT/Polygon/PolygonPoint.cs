

// Changes from the Java version
//   Replaced get/set Next/Previous with attributes
// Future possibilities
//   Documentation!

namespace FarseerPhysics.Common.Decomposition.CDT.Polygon
{
    internal class PolygonPoint : TriangulationPoint
    {
        public PolygonPoint(double x, double y) : base(x, y)
        {
        }

        public PolygonPoint Next { get; set; }
        public PolygonPoint Previous { get; set; }
    }
}