

// Changes from the Java version
//   Replaced getPolygons with attribute
// Future possibilities
//   Replace Add(Polygon) with exposed container?
//   Replace entire class with HashSet<Polygon> ?

using System.Collections.Generic;

namespace FarseerPhysics.Common.Decomposition.CDT.Polygon
{
    internal class PolygonSet
    {
        protected List<Polygon> _polygons = new List<Polygon>();

        public PolygonSet()
        {
        }

        public PolygonSet(Polygon poly)
        {
            _polygons.Add(poly);
        }

        public IEnumerable<Polygon> Polygons
        {
            get { return _polygons; }
        }

        public void Add(Polygon p)
        {
            _polygons.Add(p);
        }
    }
}