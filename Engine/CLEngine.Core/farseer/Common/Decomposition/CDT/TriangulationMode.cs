
namespace FarseerPhysics.Common.Decomposition.CDT
{
    internal enum TriangulationMode
    {
        Unconstrained,
        Constrained,
        Polygon
    }
}