
using System.Collections.Generic;

namespace FarseerPhysics.Common.Decomposition.CDT.Delaunay.Sweep
{
    internal class DTSweepPointComparator : IComparer<TriangulationPoint>
    {
        #region IComparer<TriangulationPoint> Members

        public int Compare(TriangulationPoint p1, TriangulationPoint p2)
        {
            if (p1.Y < p2.Y)
            {
                return -1;
            }
            else if (p1.Y > p2.Y)
            {
                return 1;
            }
            else
            {
                if (p1.X < p2.X)
                {
                    return -1;
                }
                else if (p1.X > p2.X)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }

        #endregion
    }
}