

// Changes from the Java version
//   Removed getters
//   Has* turned into attributes
// Future possibilities
//   Comments!

namespace FarseerPhysics.Common.Decomposition.CDT.Delaunay.Sweep
{
    internal class AdvancingFrontNode
    {
        public AdvancingFrontNode Next;
        public TriangulationPoint Point;
        public AdvancingFrontNode Prev;
        public DelaunayTriangle Triangle;
        public double Value;

        public AdvancingFrontNode(TriangulationPoint point)
        {
            Point = point;
            Value = point.X;
        }

        public bool HasNext
        {
            get { return Next != null; }
        }

        public bool HasPrev
        {
            get { return Prev != null; }
        }
    }
}