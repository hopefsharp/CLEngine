

/**
 * Forces a triangle edge between two points p and q
 * when triangulating. For example used to enforce
 * Polygon Edges during a polygon triangulation.
 * 
 * @author Thomas Åhlén, thahlen@gmail.com
 */
namespace FarseerPhysics.Common.Decomposition.CDT
{
    internal class TriangulationConstraint
    {
        public TriangulationPoint P;
        public TriangulationPoint Q;
    }
}