
namespace FarseerPhysics.Common.Decomposition.CDT
{
    internal enum Orientation
    {
        CW,
        CCW,
        Collinear
    }
}