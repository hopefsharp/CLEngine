﻿using System;
using System.Reflection;
using CLEngine.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace CLEngine.Core
{
    /// <summary>
    /// 场景管理器
    /// 该类负责管理活动场景
    /// </summary>
    public static class SceneManager
    {
        private static Game _gameWindow;

        private static GraphicsDevice graphicsDevice;

        private static Camera activeCamera;
        internal static int drawPass = 0;

        /// <summary>
        /// 获取或设置当前游戏项目
        /// </summary>
        public static CProject GameProject { get; set; }

        private static Assembly scriptsAssembly;
        private static Game gameWindow;
        private static bool isEditor = true;
        private static string[] gameArgs = new string[0];
        private static string[] gameExtra = new string[0];

        public static string[] GameExtra
        {
            get { return SceneManager.gameExtra; }
            set { SceneManager.gameExtra = value; }
        }

        public static string[] GameArgs
        {
            get { return SceneManager.gameArgs; }
            set { SceneManager.gameArgs = value; }
        }

        public static GraphicsDeviceManager Graphics
        {
            get { return SceneManager.graphics; }
            set { SceneManager.graphics = value; }
        }

        /// <summary>
        /// 当前活动的场景路径
        /// </summary>
        public static string ActiveScenePath { get; set; }

        /// <summary>
        /// 确定我们是否在编辑器模式下运行
        /// </summary>
        public static bool IsEditor
        {
            get { return isEditor; }
            set { isEditor = value; }
        }

        public static SpriteBatch SpriteBatch
        {
            get { return SceneManager.spriteBatch; }
            set { SceneManager.spriteBatch = value; }
        }

        /// <summary>
        /// 活跃的游戏窗口。
        /// 仅在游戏模式下可用
        /// </summary>
        public static Game GameWindow
        {
            get { return SceneManager.gameWindow; }
            set { SceneManager.gameWindow = value; }
        }

        public static GraphicsDevice GraphicsDevice
        {
            get { return graphicsDevice; }
            set { graphicsDevice = value; }
        }

        public static Assembly ScriptsAssembly
        {
            get { return scriptsAssembly; }
            set { scriptsAssembly = value; }
        }

        public static Camera ActiveCamera
        {
            get
            {
                if (!IsEditor && ActiveScene != null)
                {
                    return ActiveScene.Camera;
                }
                else
                {
                    return activeCamera;
                }
            }
            set
            {
                activeCamera = value;

                if (!IsEditor && ActiveScene != null)
                {
                    ActiveScene.Camera = value;
                }
            }
        }

        public static ContentManager Content
        {
            get { return SceneManager.content; }
            set { SceneManager.content = value; }
        }

        private static GameScene activeScene;

        private static ContentManager content;

        private static SpriteBatch spriteBatch;

        private static GraphicsDeviceManager graphics;

        private static Tileset activeTileset = null;
        private static float deltaFPSTime = 0f;
        private static int fpsCount = 0;
        private static int fps = 0;

        public static GameScene ActiveScene
        {
            get { return activeScene; }
            set
            {
                if (value == null)
                {
                    activeScene = null;
                    return;
                }

                if (content == null)
                    throw new Exception("无法添加场景，内容管理器未分配");

                if (spriteBatch == null)
                    throw new Exception("无法添加场景，未分配精灵批处理");

                if (activeScene != null)
                    activeScene.Dispose();

                activeScene = value;

                value.Content = content;
                value.SpriteBatch = spriteBatch;
                value.Graphics = graphics;

                value.Initialize();
                value.LoadContent();

                activeTileset = null;
            }
        }

        /// <summary>
        /// 活动的tileset
        /// </summary>
        public static Tileset ActiveTileset
        {
            get { return activeTileset; }
            set { activeTileset = value; }
        }

        public static bool CreateScene(string filename)
        {
            GameScene gameScene = new GameScene();
            CHelper.SerializeObject(filename, gameScene);

            return true;
        }

        public static void Update(GameTime gameTime)
        {
            GameInput.Update();

            deltaFPSTime += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (deltaFPSTime >= 1)
            {
                fps = fpsCount;
                fpsCount = 0;
                deltaFPSTime = 0;
            }
            else
            {
                fpsCount++;
            }

            if (activeScene != null)
            {
                activeScene.Update(gameTime);
            }
        }

        public static bool SaveActiveScene()
        {
            if (ActiveScene == null) return false;

            activeScene.SaveComponentValues();
            CHelper.SerializeObject(ActiveScenePath, ActiveScene);

            return true;
        }

        /// <summary>
        /// 保存当前场景
        /// </summary>
        /// <param name="path">路径</param>
        /// <param name="xml">是否xml文件</param>
        /// <returns>是否保存成功</returns>
        public static bool SaveActiveScene(string path, bool xml)
        {
            if (ActiveScene == null) return false;

            activeScene.SaveComponentValues();

            if (!xml)
                CHelper.SerializeObject(path, ActiveScene);
            else
                CHelper.SerializeObjectXML(path, activeScene);

            return true;
        }

        public static void Draw(GameTime gameTime)
        {
            if (activeScene != null)
            {
                activeScene.Draw(gameTime);
            }
        }

        /// <summary>
        /// 根据标签寻找对象
        /// </summary>
        /// <param name="src">标签名</param>
        /// <returns>对象</returns>
        public static GameObject FindByTag(string src)
        {
            return GameObject.FindByTag(src);
        }

        public static GameObject Remove(string src)
        {
            return GameObject.Remove(src);
        }
        
        public static GameObject Remove(GameObject gameObject)
        {
            return GameObject.Remove(gameObject);
        }

        /// <summary>
        /// 从文件加载对象
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static GameObject LoadFromState(string path)
        {
            return (GameObject)CHelper.DeserializeObject(path);
        }

        /// <summary>
        /// 加载场景
        /// </summary>
        /// <param name="scenePath">场景路径</param>
        /// <returns>是否加载成功</returns>
        public static bool LoadScene(string scenePath)
        {
            return LoadScene(scenePath, false);
        }

        /// <summary>
        /// 加载场景
        /// </summary>
        /// <param name="scenePath">场景路径</param>
        /// <param name="saveHistory">保存为历史路径</param>
        /// <returns>是否加载成功</returns>
        public static bool LoadScene(string scenePath, bool saveHistory)
        {
            try
            {
#if WIN
                GameScene gameScene = (GameScene)CHelper.DeserializeObject(scenePath);
#elif WINRT
                GameScene gameScene = (GameScene)CHelper.DeserializeObject(typeof(GameScene), scenePath);
#endif
                ActiveScene = gameScene;
                ActiveScenePath = scenePath;

                // 更新上次保存的场景：
                if (GameProject != null && !scenePath.StartsWith("_") && saveHistory)
                    GameProject.EditorSettings.LastOpenScenePath = CHelper.MakeExclusiveRelativePath(GameProject.ProjectPath, ActiveScenePath);

                Time.SceneChanged();

                // 加载成功，通知：
                return true;
            }
            catch (Exception exception)
            {
                Console.WriteLine("加载场景时出错: " + exception.Message + "\n>" + exception.ToString());
                // 未加载，通知：
                return false;
            }
        }
    }
}