﻿using System;
using System.Runtime.Serialization;

namespace CLEngine.Core
{
#if WIN
    [Serializable]
#endif
    [DataContract(IsReference = true)]
    public class SystemObject
    {
        
    }
}