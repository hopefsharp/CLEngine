﻿using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Security.Permissions;
using FarseerPhysics;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;

#if WIN
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
#endif

namespace CLEngine.Core
{
    /// <summary>
    /// 变换类
    /// </summary>
#if WIN
    [ExpandableObject]
    [Serializable, TypeConverter(typeof(ExpandableObjectConverter))]
#endif
    [DataContract(Namespace = "")]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class Transform
#if WIN
    : ICloneable
#endif
    {
        [DataMember]
        internal GameObject gameObject;

        [DataMember]
        internal Transform parent;

        [DataMember]
        internal Vector2 position;

        [DataMember]
        internal float rotation;

        [DataMember]
        internal Vector2 scale = Vector2.One;

        [DataMember]
        private GameObjectCollection children;

#if WIN
        [NonSerialized]
#endif
        private Vector2 desiredPosition = Vector2.Zero;

        /// <summary>
        /// 根节点的属性
        /// </summary>
#if WIN
        [Browsable(false)]
#endif
        public Transform Root
        {
            get
            {
                Transform root = this;
                while (root.parent != null)
                    root = root.parent;

                return root;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Transform()
        {

        }

#if WIN
        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <exception cref="ArgumentNullException"></exception>
        protected Transform(SerializationInfo info)
        {
            if (info == null)
                throw new ArgumentNullException(nameof(info));

            gameObject = info.GetValue("gameObject", typeof(GameObject)) as GameObject;
            parent = info.GetValue("parent", typeof(Transform)) as Transform;
            position = (Vector2)info.GetValue("position", typeof(Vector2));
            rotation = (float)info.GetDouble("rotation");

            object obj = info.GetValue("scale", typeof(object));
            if (obj is Vector2)
                scale = (Vector2)info.GetValue("scale", typeof(Vector2));
            else
                scale = Vector2.One;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        /// <exception cref="ArgumentNullException"></exception>
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException(nameof(info));

            info.AddValue("gameObject", gameObject);
            info.AddValue("position", position);
            info.AddValue("rotation", rotation);
            info.AddValue("scale", scale);
            info.AddValue("parent", parent);
        }
#endif

        /// <summary>
        /// 父变换
        /// </summary>
#if WIN
        [Browsable(false)]
#endif
        public Transform Parent
        {
            get { return parent; }
            set { parent = value; }
        }

        /// <summary>
        /// 所依附的对象
        /// </summary>
#if WIN
        [Browsable(false)]
#endif
        public GameObject GameObject
        {
            get { return gameObject; }
            set { gameObject = value; }
        }

        /// <summary>
        /// 子对象列表
        /// </summary>
#if WIN
        [Browsable(false)]
#endif
        public GameObjectCollection Children
        {
            get { return children; }
            set { children = value; }
        }

#if WIN
        [NonSerialized]
#endif
        internal bool physicsPositionReached = true;

        /// <summary>
        /// 对象的相对位置。
        /// 如果对象没有父对象，则返回世界位置
        /// </summary>
#if WIN
        [NotifyParentProperty(true)]
        [DisplayName("位置"), Description("当前位置")]
#endif
        public Vector2 RelativePosition
        {
            get
            {
                if (gameObject.Body == null)
                {
                    if (parent != null)
                        return position - parent.Position;
                    else
                        return position;
                }
                else
                {
                    if (parent != null)
                        return ConvertUnits.ToDisplayUnits(gameObject.Body.Position) - parent.Position;
                    else
                        return ConvertUnits.ToDisplayUnits(gameObject.Body.Position);
                }
            }
            set
            {
                TranslateChildren(gameObject, value - RelativePosition, ConvertUnits.ToSimUnits(value - RelativePosition));

                if (parent != null)
                    position = value + parent.Position;
                else
                    position = value;

                if (gameObject.Body != null)
                {
                    if (parent != null)
                        desiredPosition = value + parent.Position;
                    else
                        desiredPosition = value;

                    desiredPosition = ConvertUnits.ToSimUnits(desiredPosition);

                    if (SceneManager.IsEditor || gameObject.Body.BodyType == BodyType.Static || gameObject.Body.BodyType == BodyType.Kinematic)
                    {
                        gameObject.Body.Position = desiredPosition;
                    }
                    else
                    {
                        physicsPositionReached = false;
                    }
                }
            }
        }

        /// <summary>
        /// 当前缩放
        /// </summary>
#if WIN
        [NotifyParentProperty(true)]
        [DisplayName("缩放"), Description("当前缩放")]
#endif
        public Vector2 Scale
        {
            get { return scale; }
            set
            {
                if (gameObject != null)
                {
                    // scale children along
                    foreach (GameObject go in gameObject.Children)
                    {
                        go.Transform.Scale = value;
                    }
                }

                scale = value;

                if (gameObject?.Body != null && gameObject.physicalBody != null)
                {
                    gameObject.physicalBody.ResetBody();
                    gameObject.Body.Awake = true;
                }
            }
        }

        /// <summary>
        /// 当前旋转
        /// </summary>
#if WIN
        [DisplayName("旋转"), Description("当前旋转")]
#endif
        public float Rotation
        {
            get
            {
                if (gameObject?.Body != null)
                {
                    //if (parent != null)
                    //    return gameObject.Body.Rotation + parent.Rotation;
                    //else
                    if (parent != null && !gameObject.RotationIndependent)
                        return gameObject.Body.Rotation + parent.Rotation;
                    else
                        return gameObject.Body.Rotation;
                }
                else
                {
                    if (gameObject != null && (parent != null && !gameObject.RotationIndependent))
                        return rotation + parent.Rotation;
                    else
                        return rotation;
                }
            }
            set
            {
                RotateChildren(gameObject, (value - rotation));

                rotation = value;

                if (gameObject?.Body != null)
                {
                    gameObject.Body.Rotation = rotation;
                    gameObject.Body.Awake = true;
                }
            }
        }

        /// <summary>
        /// 子对象数量
        /// </summary>
        public int ChildCount => children?.Count ?? 0;

        /// <summary>
        /// 设置父变换
        /// </summary>
        /// <param name="transform">变换</param>
        public void SetParent(Transform transform)
        {
            parent = transform;
        }

        /// <summary>
        /// 看向某个点
        /// </summary>
        /// <param name="point">坐标点</param>
        /// <param name="immediate">是否立即改变</param>
        public float LookAt(Vector2 point, bool immediate = true)
        {
            Vector2 direction = point - Position;
            direction.Normalize();

            float _rotation = (float)Math.Atan2(direction.Y, direction.X);
            if (Math.Abs(_rotation - Rotation) > 0 && immediate)
                Rotation = _rotation;

            return _rotation;
        }

        /// <summary>
        /// 根据名称寻找对象的属性
        /// </summary>
        /// <param name="name">名称</param>
        /// <returns>属性</returns>
        public Transform Find(string name)
        {
            if (children == null)
                return null;

            foreach (var child in children)
            {
                if (child.Name == name)
                    return child.Transform;
            }

            return null;
        }

        private void RotateChildren(GameObject obj, float dif)
        {
            if (obj != null)
            {
                foreach (GameObject _obj in obj.Children)
                {
                    if (_obj.RotationIndependent) continue;

                    if (_obj.Body != null && _obj.Body.BodyType != BodyType.Dynamic)
                        _obj.Body.Rotation = _obj.Body.Rotation + _obj.Transform.rotation;

                    _obj.Transform.RelativePosition = _obj.Transform.RelativePosition.Rotate(dif);

                    RotateChildren(_obj, dif);
                }
            }
        }

        /// <summary>
        /// 对象的世界位置
        /// </summary>
#if WIN
        [Browsable(false)]
#endif
        public Vector2 Position
        {
            get
            {
                if (gameObject?.Body != null)
                {
                    return ConvertUnits.ToDisplayUnits(gameObject.Body.Position);
                }

                return position;
            }
            set
            {
                TranslateChildren(gameObject, value - position, ConvertUnits.ToSimUnits(value - position));

                position = value;

                if (gameObject?.Body != null)
                {
                    desiredPosition = ConvertUnits.ToSimUnits(value);
                    gameObject.Body.Position = desiredPosition;
                    gameObject.Body.Awake = true;
                }
            }
        }

        internal void TranslateChildren(GameObject obj, Vector2 dif, Vector2 difSim)
        {
            if (obj != null)
            {
                foreach (GameObject _obj in obj.Children)
                {
                    _obj.Transform.position += dif;

                    if (_obj.Body != null)
                    {
                        _obj.Body.Position += difSim;
                        if (!SceneManager.IsEditor)
                            _obj.Body.Awake = true; //wake the body for physics detection
                    }

                    TranslateChildren(_obj, dif, difSim);
                }
            }
        }

        /// <summary>
        /// 移动对象
        /// </summary>
        /// <param name="translation">移动方向及速度</param>
        public void Translate(Vector2 translation)
        {
            Translate(translation.X, translation.Y);
        }

        /// <summary>
        /// 移动对象
        /// </summary>
        /// <param name="x">x轴的方向及速度</param>
        /// <param name="y">y轴的方向及速度</param>
        public void Translate(float x, float y)
        {
            //if (gameObject.Body == null)
            //{
            //    this.position.X += x;
            //    this.position.Y += y;
            //}
            //else
            //{
            Position = new Vector2(Position.X + x, Position.Y + y);
            //}
        }

        /// <summary>
        /// 深度复制
        /// </summary>
        /// <param name="copyGameObject">是否复制对象</param>
        /// <returns>复制对象的属性</returns>
        public Transform DeepCopy(bool copyGameObject = false)
        {
            if (Clone() is Transform result)
            {
                result.gameObject = (copyGameObject ? gameObject.Copy() : gameObject);
                result.Position = new Vector2(Position.X, Position.Y);
                result.scale = new Vector2(scale.X, scale.Y);
                result.rotation = rotation;

                return result;
            }

            return null;
        }

        /// <summary>
        /// 转字符串
        /// </summary>
        /// <returns>空</returns>
        public override string ToString()
        {
            return "";
        }


        /// <summary>
        /// 复制  
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}