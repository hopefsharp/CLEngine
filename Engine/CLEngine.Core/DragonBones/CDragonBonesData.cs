﻿using System;
using Microsoft.Xna.Framework.Graphics;

namespace CLEngine.Core.DragonBones
{
    [Serializable]
    public class CDragonBonesData
    {
        [System.Serializable]
        public class TextureAtlas
        {
            public string textureAtlasJSON;
            public Texture2D texture;
            public Sprite material;
            public Sprite uiMaterial;
        }

        public string dataName;
        public string dragonBonesJSON;
        public TextureAtlas[] textureAtlas;

        public void RemoveFromFactory(bool disposeData = true)
        {
            CFactory.factory.RemoveDragonBonesData(dataName, disposeData);
            if (textureAtlas != null)
            {
                foreach (TextureAtlas ta in textureAtlas)
                {
                    if (ta != null && ta.texture != null)
                    {
                        CFactory.factory.RemoveTextureAtlasData(ta.texture.Name, disposeData);
                    }
                }
            }
        }
    }
}