﻿using CLEngine.Core.components;
using DragonBones;
using Microsoft.Xna.Framework;

namespace CLEngine.Core.DragonBones
{
    public class CSlot : Slot
    {
        internal GameObject _renderDisplay;
        private CArmatureComponent _proxy;

        internal MeshBuffer _meshBuffer;

        protected override void _InitDisplay(object value, bool isRetain)
        {
            
        }

        protected override void _OnClear()
        {
            base._OnClear();

            if (this._meshBuffer != null)
                this._meshBuffer.Dispose();

            this._meshBuffer = null;
        }

        protected override void _DisposeDisplay(object value, bool isRelease)
        {
            if (!isRelease)
                CFactoryHelper.DestroyUnityObject(value as GameObject);
        }

        protected override void _OnUpdateDisplay()
        {
            _renderDisplay = (_display != null ? _display : _rawDisplay) as GameObject;

            _proxy = _armature.proxy as CArmatureComponent;

            if (this._meshBuffer == null)
            {
                this._meshBuffer = new MeshBuffer();
                this._meshBuffer.sharedMesh = MeshBuffer.GenerateMesh();
                this._meshBuffer.sharedMesh.Name = this.name;
            }
        }

        protected override void _AddDisplay()
        {
            _proxy = _armature.proxy as CArmatureComponent;
            var container = _proxy;
            if (_renderDisplay.Transform.parent != container.Transform)
            {
                _renderDisplay.Transform.SetParent(container.Transform);

                _SetZorder(0);
            }
        }

        protected override void _ReplaceDisplay(object value)
        {
            var container = _proxy;
        }

        protected override void _RemoveDisplay()
        {
            throw new System.NotImplementedException();
        }

        protected override void _UpdateZOrder()
        {
            throw new System.NotImplementedException();
        }

        internal override void _UpdateVisible()
        {
            throw new System.NotImplementedException();
        }

        internal override void _UpdateBlendMode()
        {
            throw new System.NotImplementedException();
        }

        protected override void _UpdateColor()
        {
            throw new System.NotImplementedException();
        }

        protected override void _UpdateFrame()
        {
            var currentVerticesData = (this._deformVertices != null && this._display == this._meshDisplay)
                ? this._deformVertices.verticesData
                : null;
            var currentTextureData = this._textureData as CTextureData;

        }

        protected override void _UpdateMesh()
        {
            throw new System.NotImplementedException();
        }

        protected override void _UpdateTransform()
        {
            throw new System.NotImplementedException();
        }

        protected override void _IdentityTransform()
        {
            throw new System.NotImplementedException();
        }
    }
}