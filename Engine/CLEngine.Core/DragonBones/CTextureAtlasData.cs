﻿using System;
using System.Collections.Generic;
using System.Linq;
using DragonBones;
using Microsoft.Xna.Framework.Graphics;

namespace CLEngine.Core.DragonBones
{
    public class CTextureAtlasData : TextureAtlasData
    {
        internal bool _disposeEnabled;

        public Sprite texture;
        public Sprite uiTexture;

        protected override void _OnClear()
        {
            base._OnClear();

            if (_disposeEnabled && texture != null)
                CFactoryHelper.DestroyUnityObject(texture);

            if (_disposeEnabled && uiTexture != null)
                CFactoryHelper.DestroyUnityObject(texture);

            _disposeEnabled = false;
            texture = null;
            uiTexture = null;
        }

        public override TextureData CreateTexture()
        {
            return BaseObject.BorrowObject<CTextureData>();
        }
    }
}