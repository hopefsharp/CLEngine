﻿using System;
using System.Collections.Generic;

namespace CLEngine.Core.DragonBones
{
    public class MeshBuffer : IDisposable
    {
        public readonly List<CSlot> combineSlots = new List<CSlot>();
        public CMesh sharedMesh;

        public static CMesh GenerateMesh()
        {
            var mesh = new CMesh();

            return mesh;
        }

        public void Dispose()
        {
            if (this.sharedMesh != null)
                CFactoryHelper.DestroyUnityObject(this.sharedMesh);

            this.combineSlots.Clear();
            this.sharedMesh = null;
        }
    }
}