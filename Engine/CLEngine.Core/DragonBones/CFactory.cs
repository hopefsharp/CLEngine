﻿using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using CLEngine.Core.components;
using DragonBones;
using Microsoft.Xna.Framework.Graphics;

namespace CLEngine.Core.DragonBones
{
    public class CFactory : BaseFactory
    {
        private GameObject _armatureGameObject = null;

        private static CFactory _factory = null;
        public static CFactory factory
        {
            get
            {
                if (_factory == null)
                {
                    _factory = new CFactory();
                }

                return _factory;
            }
        }

        public CTextureAtlasData LoadTextureAtlasData(string textureAtlasJSONPath, string name = "", float scale = 1.0f)
        {
            var text = File.ReadAllText(textureAtlasJSONPath);

            if (!string.IsNullOrEmpty(text))
            {
                Dictionary<string, object> textureJSONData = (Dictionary<string, object>)MiniJSON.Json.Deserialize(text);
                CTextureAtlasData textureAtlasData = ParseTextureAtlasData(textureJSONData, null, name, scale) as CTextureAtlasData;

                return textureAtlasData;
            }

            return null;
        }

        public DragonBonesData LoadDragonBonesData(string dragonBonesJsonPath, string name = "", float scale = 0.01f)
        {
            var dragonBonesJSON = File.ReadAllText(dragonBonesJsonPath);

            if (string.IsNullOrEmpty(dragonBonesJSON))
                return null;

            if (!string.IsNullOrEmpty(name))
            {
                var existedData = GetDragonBonesData(name);
                if (existedData != null)
                    return existedData;
            }

            DragonBonesData data = null;
            if (dragonBonesJSON == "DBDT")
            {
                BinaryDataParser.jsonParseDelegate = MiniJSON.Json.Deserialize;
                data = ParseDragonBonesData(byte.Parse(dragonBonesJSON), name, scale);
            }
            else
            {
                data = ParseDragonBonesData((Dictionary<string, object>) MiniJSON.Json.Deserialize(dragonBonesJSON),
                    name, scale);
            }

            name = !string.IsNullOrEmpty(name) ? name : data.name;

            _dragonBonesDataMap[name] = data;

            return data;
        }

        public CArmatureComponent BuildArmatureComponent(string armatureName, string dragonBonesName = "",
            string skinName = "", string textureAtlasName = "", GameObject gameObject = null)
        {
            _armatureGameObject = gameObject;
            var armature = BuildArmature(armatureName, dragonBonesName, skinName, textureAtlasName);

            if (armature != null)
            {
                _dragonBones.clock.Add(armature);

                var armtureDisplay = armature.display as GameObject;
                var armatureComponent = armtureDisplay.GetComponent<CArmatureComponent>();

                return armatureComponent;
            }

            return null;
        }

        protected override TextureAtlasData _BuildTextureAtlasData(TextureAtlasData textureAtlasData, object textureAtlas)
        {
            if (textureAtlasData != null)
            {
                if (textureAtlas != null)
                {
                    (textureAtlasData as CTextureAtlasData).uiTexture =
                        (textureAtlas as CDragonBonesData.TextureAtlas).uiMaterial;
                    (textureAtlasData as CTextureAtlasData).texture = (textureAtlas as CDragonBonesData.TextureAtlas).material;
                }
            }
            else
            {
                textureAtlasData = BaseObject.BorrowObject<CTextureAtlasData>();
            }

            return textureAtlasData;
        }

        protected override Armature _BuildArmature(BuildArmaturePackage dataPackage)
        {
            var armature = BaseObject.BorrowObject<Armature>();
            var armatureDisplay = _armatureGameObject == null
                ? new GameObject() {Name = dataPackage.armature.name}
                : _armatureGameObject;

            var armatureComponent = (CArmatureComponent)armatureDisplay.GetComponent<CArmatureComponent>();
            if (armatureComponent == null)
            {
                armatureComponent = (CArmatureComponent)armatureDisplay.AddComponent<CArmatureComponent>();
            }
            else
            {
                var slotRoot = armatureDisplay.Transform.Find("Slots");
                if (slotRoot != null)
                {
                    foreach (var transformChild in slotRoot.Children)
                    {
                        transformChild.SetParent(armatureDisplay.Transform);
                    }

                    CFactoryHelper.DestroyUnityObject(slotRoot.GameObject);
                }
            }

            armatureComponent._armature = armature;
            armature.Init(dataPackage.armature, armatureComponent, armatureDisplay, this._dragonBones);
            _armatureGameObject = null;

            return armature;
        }

        protected override Slot _BuildSlot(BuildArmaturePackage dataPackage, SlotData slotData, Armature armature)
        {
            var slot = BaseObject.BorrowObject<CSlot>();
            var armatureDisplay = armature.display as GameObject;
            var transform = armatureDisplay.Transform.Find(slotData.name);
            var gameObject = transform == null ? null : transform.gameObject;
            var isNeedIngoreCombineMesh = false;
            if (gameObject == null)
                gameObject = new GameObject(slotData.name);

            slot.Init(slotData, armature, gameObject, gameObject);

            return slot;
        }

        public override void Clear(bool disposeData = true)
        {
            base.Clear(disposeData);

            _armatureGameObject = null;
        }
    }

    internal static class CFactoryHelper
    {
        /// <summary>
        /// 检查路径合法性
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        internal static string CheckResourecdPath(string path)
        {
            var index = path.LastIndexOf("Resources");
            if (index > 0)
            {
                path = path.Substring(index + 10);
            }

            index = path.LastIndexOf(".");
            if (index > 0)
            {
                path = path.Substring(0, index);
            }

            return path;
        }

        /// <summary>
        /// 根据贴图JSON文件的路径和JSON文件中贴图名称获得贴图路径
        /// </summary>
        /// <param name="textureAtlasJSONPath">贴图JSON文件路径:NewDragon/NewDragon_tex</param>
        /// <param name="textureAtlasImageName">贴图名称:NewDragon.png</param>
        /// <returns></returns>
        internal static string GetTextureAtlasImagePath(string textureAtlasJSONPath, string textureAtlasImageName)
        {
            var index = textureAtlasJSONPath.LastIndexOf("Resources");
            if (index > 0)
            {
                textureAtlasJSONPath = textureAtlasJSONPath.Substring(index + 10);
            }

            index = textureAtlasJSONPath.LastIndexOf("/");

            string textureAtlasImagePath = textureAtlasImageName;
            if (index > 0)
            {
                textureAtlasImagePath = textureAtlasJSONPath.Substring(0, index + 1) + textureAtlasImageName;
            }

            index = textureAtlasImagePath.LastIndexOf(".");
            if (index > 0)
            {
                textureAtlasImagePath = textureAtlasImagePath.Substring(0, index);
            }

            return textureAtlasImagePath;
        }

        /// <summary>
        /// 根据贴图路径获得贴图名称
        /// </summary>
        /// <param name="textureAtlasJSONPath"></param>
        /// <returns></returns>
        internal static string GetTextureAtlasNameByPath(string textureAtlasJSONPath)
        {
            string name = string.Empty;
            int index = textureAtlasJSONPath.LastIndexOf("/") + 1;
            int lastIdx = textureAtlasJSONPath.LastIndexOf("_tex");

            if (lastIdx > -1)
            {
                if (lastIdx > index)
                {
                    name = textureAtlasJSONPath.Substring(index, lastIdx - index);
                }
                else
                {
                    name = textureAtlasJSONPath.Substring(index);
                }
            }
            else
            {
                if (index > -1)
                {
                    name = textureAtlasJSONPath.Substring(index);
                }

            }

            return name;
        }

        internal static void DestroyUnityObject(GameObject obj)
        {
            if (obj == null)
            {
                return;
            }

            GameObject.Remove(obj);
        }
    }

    internal static class LogHelper
    {
        internal static void LogWarning(object message)
        {
            Logger.Warn("[DragonBone]" + message);
        }
    }
}