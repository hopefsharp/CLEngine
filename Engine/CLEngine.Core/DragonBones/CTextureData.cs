﻿using DragonBones;
using Microsoft.Xna.Framework.Graphics;

namespace CLEngine.Core.DragonBones
{
    public class CTextureData : TextureData
    {
        public Texture2D renderTexture = null;
        public bool rotated = false;

        protected override void _OnClear()
        {
            base._OnClear();

            if (this.renderTexture != null)
                this.renderTexture.Dispose();

            this.renderTexture = null;
        }
    }
}