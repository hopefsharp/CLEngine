﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CLEngine.Core.DragonBones
{
    public class CMesh : GameObject
    {
        private BasicEffect _effect;
        private VertexBuffer _vertexBuffer;
        private IndexBuffer _indexBuffer;
        private VertexPositionColorTexture[] _vertices;

        private float[] _originalVertices;
        private short[] _indices;
        private float[] _uvs;

        public override void Initialize()
        {
            base.Initialize();

            for (int i = 0; i < _originalVertices.Length; i += 2)
            {
                var v = new VertexPositionColorTexture(
                    new Vector3(_originalVertices[i], _originalVertices[i + 1], 0f),
                    Color.White,
                    new Vector2(_uvs[i], _uvs[i + 1]));

                _vertices[i / 2] = v;
            }

            _indexBuffer = new IndexBuffer(SceneManager.GraphicsDevice, typeof(short), _indices.Length,
                BufferUsage.WriteOnly);

            _vertexBuffer = new DynamicVertexBuffer(SceneManager.GraphicsDevice, typeof(VertexPositionColorTexture),
                _vertices.Length, BufferUsage.WriteOnly);

            _effect = new BasicEffect(SceneManager.GraphicsDevice)
            {
                World = Matrix.Identity,
                View = Matrix.Identity,
                VertexColorEnabled = true,
                TextureEnabled = true,
            };
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);

            var reverseCull = Transform.scale.X * Transform.scale.Y < 0;
            var rasterizerState = spriteBatch.GraphicsDevice.RasterizerState;

            if (reverseCull)
            {
                spriteBatch.GraphicsDevice.RasterizerState = new RasterizerState()
                {
                    CullMode = CullMode.CullCounterClockwiseFace
                };
            }

            _indexBuffer.SetData(_indices);
            _vertexBuffer.SetData(_vertices);
            spriteBatch.GraphicsDevice.SetVertexBuffer(_vertexBuffer);
            spriteBatch.GraphicsDevice.Indices = _indexBuffer;

            foreach (var pass in _effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                spriteBatch.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, _indices.Length / 3);
            }

            spriteBatch.GraphicsDevice.RasterizerState = rasterizerState;
        }

        public static int CalcPrimitives(PrimitiveType type, int vertexCount)
        {
            if (vertexCount == 0)
                return 0;

            switch (type)
            {
                case PrimitiveType.LineList:
                    if (vertexCount == 1 || vertexCount % 2 != 0)
                        throw new ArgumentException("LineList requires a vertex-count that is a multiple of 2");
                    return vertexCount / 2;

                case PrimitiveType.LineStrip:
                    if (vertexCount < 1)
                        throw new ArgumentException("LineStrip requires a vertex-count greater 1");

                    return vertexCount - 1;

                case PrimitiveType.TriangleStrip:
                    if (vertexCount < 3)
                        throw  new ArgumentException("Not enough triangles, at least 3 required");

                    return vertexCount - 2;

                default:
                    throw new InvalidOperationException($"Unknown primitive type: {type}");
            }
        }
    }
}