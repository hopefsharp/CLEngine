﻿using System;
using System.Collections.Generic;
using CLEngine.Core;
using CLEngine.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CLEngine.Editor.core
{
    public static class Primitives
    {
        #region fields

        private static Texture2D pixel;
        private static readonly Dictionary<String, List<Vector2>> circleCache = new Dictionary<string, List<Vector2>>();

        #endregion

        #region properties



        #endregion

        #region constructors

        /// <summary>
        /// 默认构造函数
        /// </summary>
        static Primitives()
        {
            pixel = new Texture2D(SceneManager.GraphicsDevice, 1, 1);
            pixel.SetData(new[] { Color.White });
        }

        #endregion

        #region methods

        /// <summary>
        /// 在输入位置绘制一个像素
        /// </summary>
        /// <param name="sb">spritebatch</param>
        /// <param name="x">位置 X</param>
        /// <param name="y">位置 Y</param>
        /// <param name="c">颜色</param>
        public static void DrawPixel(SpriteBatch sb, int x, int y, Color c)
        {
            sb.Draw(pixel, new Vector2(x, y), c);
        }

        /// <summary>
        /// 在输入位置绘制一个框
        /// </summary>
        /// <param name="sb">spritebatch</param>
        /// <param name="r">目标矩形</param>
        /// <param name="c">颜色</param>
        /// <param name="linewidth">边框宽度</param>
        public static void DrawBox(SpriteBatch sb, Rectangle r, Color c, int linewidth)
        {
            DrawLine(sb, r.Left, r.Top, r.Right, r.Top, c, linewidth);
            DrawLine(sb, r.Right, r.Y, r.Right, r.Bottom, c, linewidth);
            DrawLine(sb, r.Right, r.Bottom, r.Left, r.Bottom, c, linewidth);
            DrawLine(sb, r.Left, r.Bottom, r.Left, r.Top, c, linewidth);
        }

        /// <summary>
        /// 在输入位置绘制一个框
        /// </summary>
        /// <param name="sb">spritebatch</param>
        /// <param name="upperLeft">左上角位置</param>
        /// <param name="lowerRight">右下位置</param>
        /// <param name="c">颜色</param>
        public static void DrawBox(SpriteBatch sb, Vector2 upperLeft, Vector2 lowerRight, Color c, int linewidth)
        {
            Rectangle r = MathExtension.RectangleFromVectors(upperLeft, lowerRight);
            DrawLine(sb, r.Left, r.Top, r.Right, r.Top, c, linewidth);
            DrawLine(sb, r.Right, r.Y, r.Right, r.Bottom, c, linewidth);
            DrawLine(sb, r.Right, r.Bottom, r.Left, r.Bottom, c, linewidth);
            DrawLine(sb, r.Left, r.Bottom, r.Left, r.Top, c, linewidth);
        }

        /// <summary>
        /// 在输入位置绘制一个填充框
        /// </summary>
        /// <param name="sb">spritebatch</param>
        /// <param name="x">位置 X</param>
        /// <param name="y">位置 Y</param>
        /// <param name="w">宽度</param>
        /// <param name="h">高度</param>
        /// <param name="c">颜色</param>
        public static void DrawBoxFilled(SpriteBatch sb, float x, float y, float w, float h, Color c)
        {
            sb.Draw(pixel, new Rectangle((int)x, (int)y, (int)w, (int)h), c);
        }

        /// <summary>
        /// 在输入位置绘制一个填充框
        /// </summary>
        /// <param name="sb">spritebatch</param>
        /// <param name="upperLeft">左上角位置</param>
        /// <param name="lowerRight">右下位置</param>
        /// <param name="c">颜色</param>
        public static void DrawBoxFilled(SpriteBatch sb, Vector2 upperLeft, Vector2 lowerRight, Color c)
        {
            Rectangle r = MathExtension.RectangleFromVectors(upperLeft, lowerRight);
            sb.Draw(pixel, r, c);
        }

        /// <summary>
        /// 在输入位置绘制一个填充框
        /// </summary>
        /// <param name="sb">spritebatch</param>
        /// <param name="r">目标矩形</param>
        /// <param name="c">颜色</param>
        public static void DrawBoxFilled(SpriteBatch sb, Rectangle r, Color c)
        {
            sb.Draw(pixel, r, c);
        }

        /// <summary>
        /// 在输入位置画一条线
        /// </summary>
        /// <param name="sb">spritebatch</param>
        /// <param name="x1">位置 X1</param>
        /// <param name="y1">位置 Y1</param>
        /// <param name="x2">位置 X2</param>
        /// <param name="y2">位置 Y2</param>
        /// <param name="c">颜色</param>
        /// <param name="linewidth">行宽</param>
        public static void DrawLine(SpriteBatch sb, float x1, float y1, float x2, float y2, Color c, int linewidth)
        {
            Vector2 v = new Vector2(x2 - x1, y2 - y1);
            float rot = (float)Math.Atan2(y2 - y1, x2 - x1);
            sb.Draw(pixel, new Vector2(x1, y1), new Rectangle(1, 1, 1, linewidth), c, rot,
                new Vector2(0, linewidth / 2), new Vector2(v.Length(), 1), SpriteEffects.None, 0);
        }

        /// <summary>
        /// 在输入位置画一条线
        /// </summary>
        /// <param name="sb">spritebatch</param>
        /// <param name="startpos">开始的位置</param>
        /// <param name="endpos">结束位置</param>
        /// <param name="c">颜色</param>
        /// <param name="linewidth">行宽</param>
        public static void DrawLine(SpriteBatch sb, Vector2 startpos, Vector2 endpos, Color c, int linewidth)
        {
            DrawLine(sb, startpos.X, startpos.Y, endpos.X, endpos.Y, c, linewidth);
        }

        private static void DrawPoints(SpriteBatch spriteBatch, Vector2 position, List<Vector2> points, Color color, int thickness)
        {
            if (points.Count < 2)
                return;

            for (int i = 1; i < points.Count; i++)
            {
                DrawLine(spriteBatch, points[i - 1] + position, points[i] + position, color, thickness);
            }
        }

        /// <summary>
        /// 绘制圆形
        /// </summary>
        /// <param name="spriteBatch">目标绘图表面</param>
        /// <param name="center">圆的中心</param>
        /// <param name="radius">圆的半径</param>
        /// <param name="sides">要生成的边数</param>
        /// <param name="color">圆的颜色</param>
        public static void DrawCircle(SpriteBatch spriteBatch, Vector2 center, float radius, int sides, Color color)
        {
            DrawPoints(spriteBatch, center, CreateCircle(radius, sides), color, 1);
        }

        private static List<Vector2> CreateCircle(double radius, int sides)
        {
            // Look for a cached version of this circle
            String circleKey = radius + "x" + sides;
            if (circleCache.ContainsKey(circleKey))
            {
                return circleCache[circleKey];
            }

            List<Vector2> vectors = new List<Vector2>();

            const double max = 2.0 * Math.PI;
            double step = max / sides;

            for (double theta = 0.0; theta < max; theta += step)
            {
                vectors.Add(new Vector2((float)(radius * Math.Cos(theta)), (float)(radius * Math.Sin(theta))));
            }

            // then add the first vector again so it's a complete loop
            vectors.Add(new Vector2((float)(radius * Math.Cos(0)), (float)(radius * Math.Sin(0))));

            // Cache this circle so that it can be quickly drawn next time
            circleCache.Add(circleKey, vectors);

            return vectors;
        }

        #endregion
    }
}