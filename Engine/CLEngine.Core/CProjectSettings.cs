﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace CLEngine.Core
{
#if WIN
    [Serializable, TypeConverter(typeof(ExpandableObjectConverter))]
#endif
    [DataContract]
    public class CProjectSettings
    {
        #region fields
        [DataMember]
        private bool defaultCollisionDetectionEnabled = true;
        [DataMember]
        private bool highlightActiveTilesetInEditor = true;
        [DataMember]
        private bool highlightActiveTilesetInGame = false;
        //[DataMember]
        //private bool vsyncEnabled = true;

        #endregion

        #region properties

        //        /// <summary>
        //        /// Determines if the vertical sync is enabled. If you use the physics system, you should keep this enabled.
        //        /// </summary>
        //#if WINDOWS
        //        [DisplayName("VSync"), Description("Determines if the vertical sync is enabled")]
        //#endif
        //        public bool VSyncEnabled
        //        {
        //            get { return vsyncEnabled; }
        //            set { vsyncEnabled = value; }
        //        }

        /// <summary>
        /// Determines if the active tileset is going to be highlighted in the editor
        /// </summary>
#if WIN
        [DisplayName("在编辑器中突出显示Active Tileset"), Description("确定是否将在编辑器中突出显示活动切片集")]
#endif
        public bool HighlightActiveTilesetInEditor
        {
            get { return highlightActiveTilesetInEditor; }
            set { highlightActiveTilesetInEditor = value; }
        }

        /// <summary>
        /// 定活动的tileset是否将在游戏中突出显示
        /// </summary>
#if WIN
        [DisplayName("突出游戏中的活动Tileset"), Description("确定活动的tileset是否将在游戏中突出显示")]
#endif
        public bool HighlightActiveTilesetInGame
        {
            get { return highlightActiveTilesetInGame; }
            set { highlightActiveTilesetInGame = value; }
        }

        /// <summary>
        /// 确定引擎是否将使用默认的碰撞引擎
        /// </summary>
#if WIN
        [DisplayName("默认碰撞引擎"), Description("确定引擎是否使用默认的碰撞引擎")]
#endif
        public bool DefaultCollisionDetectionEnabled
        {
            get { return defaultCollisionDetectionEnabled; }
            set { defaultCollisionDetectionEnabled = value; }
        }

        #endregion

        #region methods

        /// <summary>
        /// To String
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Empty;
        }

        #endregion
    }
}