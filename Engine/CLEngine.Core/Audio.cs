﻿using System;
using System.Collections.Generic;
using NAudio.Wave;
// ReSharper disable All

namespace CLEngine.Core
{
    class MappedWaveChannel
    {
        public WaveChannel32 WaveChannel { get; set; }
        public long LastPosition { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class Audio
    {
        #region fields

        private static bool initialized;
        private static int bufferCount;

#if WIN
        private static IWavePlayer waveOutDevice;
        private static LoopStream mainOutputStream;
        private static WaveMixerStream32 mixer;
        private static Dictionary<int, MappedWaveChannel> buffer = new Dictionary<int, MappedWaveChannel>();
#endif

        #endregion

        #region methods

        static Audio()
        {
            if (!initialized)
            {
#if WIN
                try
                {
                    mixer = new WaveMixerStream32();
                    mixer.AutoStop = false;

                    waveOutDevice = new DirectSoundOut(50);
                    waveOutDevice.Init(mixer);

                    initialized = true;
                }
                catch (Exception driverCreateException)
                {
                    Console.WriteLine($"{driverCreateException.Message}");
                }
#endif
            }
        }

        internal static void Initialize()
        {

        }

        /// <summary>
        /// 清除音频缓冲区
        /// </summary>
        public static void ClearBuffer()
        {
#if WIN
            buffer.Clear();
#endif
            bufferCount = 0;
        }

        /// <summary>
        /// 卸载音频波
        /// </summary>
        public static void Unload()
        {
#if WIN
            if (waveOutDevice != null)
            {
                waveOutDevice.Dispose();
            }

            if (mainOutputStream != null)
            {
                mainOutputStream.Dispose();
            }
#endif
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="volume"></param>
        /// <returns></returns>
        public static int LoadSoundToBuffer(string filepath, float volume)
        {
#if WIN
            WaveChannel32 channel = CreateInputStream(filepath, volume, true);

            if (channel == null) return -1;

            buffer[bufferCount] = new MappedWaveChannel() { WaveChannel = channel, LastPosition = 0 };

            bufferCount++;

            return (bufferCount - 1);
#elif WINRT
            return -1;
#endif
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        public static void PlayFromBuffer(int key)
        {
#if WIN
            if (buffer.ContainsKey(key))
            {
                PlayAudioFromBuffer(buffer[key]);
            }
#endif
        }

#if WIN
        internal static void PlayAudioFromBuffer(MappedWaveChannel param)
        {
            param.WaveChannel.Position = param.LastPosition;
            //param.WaveChannel.Position = 0;

            if (!(waveOutDevice.PlaybackState == PlaybackState.Playing))
                waveOutDevice.Play();
        }
#endif

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        public static void StopFromBuffer(int key)
        {
#if WIN
            if (buffer.ContainsKey(key))
            {
                StopAudioFromBuffer(buffer[key]);
            }
#endif
        }

#if WIN
        internal static void StopAudioFromBuffer(MappedWaveChannel param)
        {
            param.WaveChannel.Position = param.WaveChannel.Length;
        }
#endif

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        public static void PauseFromBuffer(int key)
        {
#if WIN
            if (buffer.ContainsKey(key))
            {
                buffer[key].LastPosition = buffer[key].WaveChannel.Position;
                StopFromBuffer(key);
            }
#endif
        }

        /// <summary>
        /// 播放.mp3或.wav音频文件
        /// </summary>
        /// <param name="filePath">The relative path to the audio file</param>
        /// <param name="loop">Determine if the sound should loop</param>
        public static void PlayAudio(string filePath, bool loop) // testa isto  <- pelos scripts
        {
            if (!initialized) return;

            // Add the project path
            filePath = SceneManager.GameProject.ProjectPath + "//" + filePath;

#if WIN
            WaveChannel32 stream = CreateInputStream(filePath, 1, false);
            
            if (stream != null)
            {
                mainOutputStream = new LoopStream(stream);
                mainOutputStream.EnableLooping = loop;

                try
                {
                    waveOutDevice.Init(mainOutputStream);
                }
                catch (Exception initException)
                {
                    Console.WriteLine(String.Format("{0}", initException.Message), "初始化输出时出错");
                    return;
                }

                waveOutDevice.Play();
            }
#endif
        }

        /// <summary>
        /// 播放.mp3或.wav音频文件
        /// </summary>
        /// <param name="filePath">The relative path to the audio file</param>
        public static void PlayAudio(string filePath)
        {
            PlayAudio(filePath, false);
        }

#if WIN
        /// <summary>
        /// 从选定文件创建波流
        /// </summary>
        /// <param name="fileName">The file path to the audio file (.mp3 or .wav)</param>
        /// <param name="volume">The default desired volume</param>
        /// <param name="addToBuffer">Determines if the audio file is added to the buffer</param>
        /// <returns></returns>
        public static WaveChannel32 CreateInputStream(string fileName, float volume, bool addToBuffer)
    {
        WaveStream stream;
        WaveOffsetStream offset;
        WaveChannel32 channel;

        if (fileName.ToLower().EndsWith(".wav"))
        {
            stream = new WaveFileReader(fileName);
        }
        else if (fileName.ToLower().EndsWith(".mp3"))
        {
            stream = new Mp3FileReader(fileName);
        }
        else
        {
            Console.WriteLine("不支持音频格式");
            return null;
        }

        stream = new WaveFormatConversionStream(new WaveFormat(44100, 1), stream);
        offset = new WaveOffsetStream(stream);
        channel = new WaveChannel32(offset);
        channel.Volume = volume;

        if(addToBuffer)
            mixer.AddInputStream(channel);

        return channel;
    }
#endif

        #endregion
    }
}