﻿using System.Text;
using NLua;

namespace CLEngine.Core
{
    /// <summary>
    /// 脚本引擎
    /// </summary>
    public static class ScriptEngine
    {
        /// <summary>
        /// 脚本引擎
        /// </summary>
        public static readonly Lua LuaEngine = new Lua();

        static ScriptEngine()
        {
            LuaEngine.LoadCLRPackage();
            LuaEngine.State.Encoding = Encoding.UTF8;

            LuaEngine.DoString("luanet.load_assembly('CLEngine.Core')");
            LuaEngine.DoString("luanet.load_assembly('MonoGame.Framework')");

            LuaEngine.DoString("import('System')");
            LuaEngine.DoString("import('FairyGUI', 'CLEngine.Core')");
            LuaEngine.DoString("import('FairyGUI.Utils', 'CLEngine.Core')");
            LuaEngine.DoString("import('CLEngine.Core', 'CLEngine.Core')");
            LuaEngine.DoString("import('Microsoft.Xna.Framework', 'MonoGame.Framework')");
            LuaEngine.DoString("import('Microsoft.Xna.Framework.Input', 'MonoGame.Framework')");
            LuaEngine.DoString("import('Microsoft.Xna.Framework.Graphics', 'MonoGame.Framework')");
            LuaEngine.DoString("import('Steamworks', 'CLEngine.Core')");

            LuaEngine["SpriteEffects"] = LuaEngine.DoString("return luanet.import_type('Microsoft.Xna.Framework.Graphics.SpriteEffects')")[0];
            LuaEngine["Keys"] = LuaEngine.DoString("return luanet.import_type('Microsoft.Xna.Framework.Input.Keys')")[0];
            LuaEngine["ButtonState"] = LuaEngine.DoString("return luanet.import_type('Microsoft.Xna.Framework.Input.ButtonState')")[0];
            LuaEngine["Keyboard"] = LuaEngine.DoString("return luanet.import_type('Microsoft.Xna.Framework.Input.Keyboard')")[0];
            LuaEngine["Mouse"] = LuaEngine.DoString("return luanet.import_type('Microsoft.Xna.Framework.Input.Mouse')")[0];
            LuaEngine["Vector2"] = LuaEngine.DoString("return luanet.import_type('Microsoft.Xna.Framework.Vector2')")[0];
            LuaEngine["MathHelper"] =
                LuaEngine.DoString("return luanet.import_type('Microsoft.Xna.Framework.MathHelper')")[0];
        }
    }
}