﻿using System;
using System.Collections.Generic;

namespace CLEngine.Core
{
    public class TimerManager
    {
        private static List<Timer> _timers = new List<Timer>();

        internal static void Update()
        {
            for (var i = _timers.Count - 1; i >= 0; i--)
            {
                // 如果它返回true则完成，所以我们删除它
                if (_timers[i].tick())
                {
                    _timers[i].unload();
                    _timers.RemoveAt(i);
                }
            }
        }

        /// <summary>
        /// 安排一次性或重复计时器，调用传入的Action
        /// </summary>
        /// <param name="timeInSeconds">时间以秒为单位.</param>
        /// <param name="repeats">If set to <c>true</c> repeats.</param>
        /// <param name="context">Context.</param>
        /// <param name="onTime">On time.</param>
        internal static ITimer Schedule(float timeInSeconds, bool repeats, object context, Action<ITimer> onTime)
        {
            var timer = new Timer();
            timer.initialize(timeInSeconds, repeats, context, onTime);
            _timers.Add(timer);

            return timer;
        }
    }
}