﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;

namespace CLEngine.Core
{
#if WIN
    [Serializable, TypeConverter(typeof(ExpandableObjectConverter))]
#endif
    [DataContract]
    public class CProjectEditorSettings
    {
        [DataMember]
        private bool showGrid;

        [DataMember]
        private bool snapToGrid;

        [DataMember]
        private bool showCollisions;

        [DataMember]
        private string lastOpenScenePath = string.Empty;

        [DataMember]
        private int gridSpacing = 32;

        [DataMember]
        private int gridThickness = 1;

        [DataMember]
        private Color gridColor = new Color(0, 0, 0, 80);

        [DataMember]
        private int gridNumberOfLines = 100;

        [DataMember] private bool catchError = true;

#if WIN
        [DisplayName("格子显示"), Description("确定网格是否可见")]
#endif
        public bool ShowGrid
        {
            get { return showGrid; }
            set { showGrid = value; }
        }

#if WIN
        [DisplayName("对齐网格"), Description("确定移动工具是否应捕捉到最近的网格点")]
#endif
        public bool SnapToGrid
        {
            get { return snapToGrid; }
            set { snapToGrid = value; }
        }

#if WIN
        [DisplayName("显示碰撞"), Description("确定是否将绘制碰撞模型")]
#endif
        public bool ShowCollisions
        {
            get { return showCollisions; }
            set { showCollisions = value; }
        }

#if WIN
        [DisplayName("网格单元大小"), Description("网格单元大小，以像素为单位")]
#endif
        public int GridSpacing
        {
            get { return gridSpacing; }
            set { gridSpacing = value; }
        }

#if WIN
        [DisplayName("网格颜色"), Description("网格的颜色")]
#endif
        public Color GridColor
        {
            get { return gridColor; }
            set { gridColor = value; }
        }

#if WIN
        [DisplayName("网格线"), Description("要显示的最大网格线")]
#endif
        public int GridNumberOfLines
        {
            get { return gridNumberOfLines; }
            set
            {
                if (value > 500) gridNumberOfLines = 500;
                else gridNumberOfLines = value;
            }
        }

#if WIN
        [DisplayName("网格厚度"), Description("网格的厚度")]
#endif
        public int GridThickness
        {
            get { return gridThickness; }
            set
            {
                if (value > 4) gridThickness = 4;
                else gridThickness = value;
            }
        }

#if WIN
        [DisplayName("错误捕获"), Description("是否捕获错误")]
#endif
        public bool CatchError
        {
            get { return catchError; }
            set { catchError = value; }
        }


#if WIN
        [Browsable(false)]
#endif
        public string LastOpenScenePath
        {
            get { return lastOpenScenePath; }
            set { lastOpenScenePath = value; }
        }
    }
}