﻿using System;

namespace CLEngine.Core.components
{
    /// <summary>
    /// 如果希望在编辑器中更新和绘制对象组件，则使用该类
    /// </summary>
#if WIN
    [Serializable]
#elif WINRT
    [DataContract]
#endif
    public class ExtendedObjectComponent : ObjectComponent
    {
        
    }
}