﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using CLEngine.Core.attributes;
using FarseerPhysics;
using FarseerPhysics.Common;
using FarseerPhysics.Common.Decomposition;
using FarseerPhysics.Common.PolygonManipulation;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CLEngine.Core.components
{
    [Info("Texture Body:\n具有基于给定纹理生成的动态形状的主体.")]
    public class TextureBody : PhysicalBody
    {
        #region fields

        private string texturePath = "";
        private float density = 1;

#if WIN
        [NonSerialized]
#endif
        private Texture2D texture;

#if WIN
        [NonSerialized]
#endif
        private uint[] data;

        #endregion

        #region properties

        /// <summary>
        /// 纹理的相对路径
        /// </summary>
#if WIN
        [Category("刚体属性")]
        [DisplayName("图片路径"), Description("纹理的相对路径")]
#endif
        public string ImageName
        {
            get { return texturePath; }
            set { texturePath = value; this.LoadTexture(); }
        }

        /// <summary>
        /// 刚体的密度
        /// </summary>
#if WIN
        [Category("刚体属性"), DisplayName("密度")]
#endif
        public float Density
        {
            get { return density; }
            set
            {
                density = value;

                if (Transform.GameObject.Body != null)
                    foreach (Fixture s in Transform.gameObject.Body.FixtureList)
                        s.Shape.Density = value;
            }
        }

        #endregion

        #region methods

        public void SetTexture(Texture2D texture)
        {
            this.texture = texture;
            UpdateTextureData();
        }

        private void LoadTexture()
        {
            texture = TextureLoader.FromContent(texturePath);

            UpdateTextureData();
        }

        private void UpdateTextureData()
        {
            if (texture != null)
            {
                data = new uint[texture.Width * texture.Height];
                texture.GetData(data);

                ResetBody();
            }
        }

        /// <summary>
        /// Initializes the body
        /// </summary>
        public override void Initialize()
        {
            Transform.gameObject.physicalBody = this;

            //Transform.GameObject.Body = BodyFactory.CreateCircle(SceneManager.ActiveScene.World, ConvertUnits.ToSimUnits(radius), density);
            LoadTexture();
        }

        internal override void ResetBody()
        {
            if (texture != null)
            {
                //Console.Info("aa");
                Vertices outline = PolygonTools.CreatePolygon(data, texture.Width, true);
                Vector2 centroid = -new Vector2(texture.Width / 2, texture.Height / 2);//-outline.GetCentroid();
                outline.Translate(ref centroid);
                outline = SimplifyTools.DouglasPeuckerSimplify(outline, 0.1f);
                List<Vertices> result = Triangulate.ConvexPartition(outline, TriangulationAlgorithm.Bayazit);
                Vector2 scale = ConvertUnits.ToSimUnits(Transform.Scale);
                foreach (Vertices vertices in result)
                {
                    vertices.Scale(ref scale);
                }

                Transform.GameObject.Body = BodyFactory.CreateCompoundPolygon(SceneManager.ActiveScene.World, result, density);

                Transform.Position = Transform.position;
                Transform.Rotation = Transform.rotation;

                UpdateBodyProperties();
            }
        }

        #endregion
    }
}