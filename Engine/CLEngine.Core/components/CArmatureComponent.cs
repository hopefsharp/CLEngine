﻿using System;
using CLEngine.Core.DragonBones;
using DragonBones;

namespace CLEngine.Core.components
{
    public class CArmatureComponent : DragonBoneEventDispatcher, IArmatureProxy
    {
        internal Armature _armature = null;
        private bool _disposeProxy = true;

        public void Dispose(bool disposeProxy)
        {
            _disposeProxy = disposeProxy;

            if (_armature != null)
                _armature.Dispose();
        }

        public Armature armature => _armature;

        public Animation animation => _armature != null ? _armature.animation : null;

        public void DBInit(Armature armature)
        {
            this._armature = armature;
        }

        public void DBClear()
        {
            if (this._armature != null)
            {
                this._armature = null;
                if (this._disposeProxy)
                {
                    try
                    {
                        var go = GameObject;
                        CFactoryHelper.DestroyUnityObject(GameObject);
                    }
                    catch (Exception e) { }
                }
            }
        }

        public void DBUpdate()
        {
        }
    }
}