﻿using System.ComponentModel;
using CLEngine.Core.attributes;
using FarseerPhysics;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Factories;

namespace CLEngine.Core.components
{
    [Info("Circle Body:\n附有圆形的主体.")]
    public class CircleBody : PhysicalBody
    {
        private float radius = 100;
        private float density = 1;

#if WIN
        [Category("刚体属性"), DisplayName("半径")]
#endif
        public float Radius
        {
            get { return radius; }
            set
            {
                radius = value;

                if (radius <= 0)
                    radius = 1;

                if (Transform.GameObject.Body != null)
                    (Transform.GameObject.Body.FixtureList[0].Shape as CircleShape).Radius = ConvertUnits.ToSimUnits(radius * Transform.Scale.X);
            }
        }

#if WIN
        [Category("刚体属性"), DisplayName("密度")]
#endif
        public float Density
        {
            get { return density; }
            set
            {
                density = value;

                if (Transform.GameObject.Body != null)
                    (Transform.GameObject.Body.FixtureList[0].Shape as CircleShape).Density = density;
            }
        }

        public override void Initialize()
        {
            Transform.GameObject.Body = BodyFactory.CreateCircle(SceneManager.ActiveScene.World, ConvertUnits.ToSimUnits(radius * Transform.scale.X), density);
            Transform.gameObject.physicalBody = this;

            Transform.Position = Transform.position;
            Transform.Rotation = Transform.rotation;

            // Update the body properties:
            UpdateBodyProperties();
        }

        internal override void ResetBody()
        {
            if (Transform.GameObject.Body == null || Transform.gameObject.Body.FixtureList == null) return;

            (Transform.GameObject.Body.FixtureList[0].Shape as CircleShape).Radius = ConvertUnits.ToSimUnits(radius * Transform.Scale.X);
            (Transform.GameObject.Body.FixtureList[0].Shape as CircleShape).Density = density;
        }
    }
}