﻿using System.ComponentModel;
using CLEngine.Core.attributes;
using FarseerPhysics;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common;
using FarseerPhysics.Factories;

namespace CLEngine.Core.components
{
    [Info("Rectangle Body:\n附有矩形形状的主体.")]
    public class RectangleBody : PhysicalBody
    {
        private int width = 100;
        private int height = 100;
        private float density = 1;

#if WIN
        [Category("刚体属性"), DisplayName("宽度")]
#endif
        public int Width
        {
            get { return width; }
            set
            {
                width = value;

                if (Transform.GameObject.Body != null)
                    ResetBody();
            }
        }

#if WIN
        [Category("刚体属性"), DisplayName("高度")]
#endif
        public int Height
        {
            get { return height; }
            set
            {
                height = value;

                if (Transform.GameObject.Body != null)
                    ResetBody();
            }
        }

#if WIN
        [Category("刚体属性"), DisplayName("密度")]
#endif
        public float Density
        {
            get { return density; }
            set
            {
                density = value;

                if (Transform.GameObject.Body != null)
                    (Transform.GameObject.Body.FixtureList[0].Shape as PolygonShape).Density = value;
            }
        }

        public override void Initialize()
        {
            Transform.GameObject.Body = BodyFactory.CreateRectangle(SceneManager.ActiveScene.World, ConvertUnits.ToSimUnits(width * Transform.scale.X), ConvertUnits.ToSimUnits(height * Transform.scale.Y), density);
            Transform.gameObject.physicalBody = this;

            Transform.Position = Transform.position;
            Transform.Rotation = Transform.rotation;

            // Update the body properties:
            UpdateBodyProperties();
        }

        internal override void ResetBody()
        {
            if (Transform.GameObject.Body.FixtureList[0].Shape != null && Transform.GameObject.Body.FixtureList[0].Shape is PolygonShape)
            {
                Vertices newVertices = PolygonTools.CreateRectangle((ConvertUnits.ToSimUnits(width) / 2) * Transform.scale.X, (ConvertUnits.ToSimUnits(height) / 2) * Transform.scale.Y);
                (Transform.GameObject.Body.FixtureList[0].Shape as PolygonShape).Vertices = newVertices;
                (Transform.GameObject.Body.FixtureList[0].Shape as PolygonShape).Density = density;
            }
        }
    }
}