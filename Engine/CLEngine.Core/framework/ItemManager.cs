﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace CLEngine.Core.framework
{
	/// <summary>
	/// 物品管理器
	/// </summary>
#if WIN
	[Serializable]
#endif
	[DataContract]
	[SuppressMessage("ReSharper", "UnusedMember.Global")]
	public class ItemManager : IDataFramework
	{
		/// <summary>
		/// 全局物品
		/// </summary>
		[DataMember] private static Dictionary<string, ItemObject> _worldItem;
		/// <summary>
		/// 玩家背包
		/// </summary>
		[DataMember] private static List<ItemObject> _playerItem;
		/// <summary>
		/// 全局物品Id
		/// </summary>
		[DataMember] public static int WorldId { get; private set; }

		/// <summary>
		/// 全局物品
		/// </summary>
		public static Dictionary<string, ItemObject> WorldItem => _worldItem;

		static ItemManager()
		{
			_worldItem = new Dictionary<string, ItemObject>();
			_playerItem = new List<ItemObject>();
			WorldId = 1;
		}

		/// <summary>
		/// 创建物品
		/// </summary>
		public static ItemObject CreateItem(string name)
		{
			var item = new ItemObject(name) { Id = WorldId };
			_worldItem.Add(name, item);

			WorldId++;

			return item;
		}

		/// <summary>
		/// 删除物品
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public static ItemObject RemoveItem(string name)
		{
			var item = new ItemObject(name);
			_worldItem.Remove(name);

			return item;
		}

		/// <summary>
		/// 删除物品
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public static bool RemoveItem(ItemObject item)
		{
			foreach (var itemObject in _worldItem)
			{
				if (itemObject.Value == item)
				{
					_worldItem.Remove(itemObject.Key);
					return true;
				}
			}

			return false;
		}

		/// <summary>
		/// 保存全局物品
		/// </summary>
		public void SaveData()
		{
			FrameworkManager.SaveFrameworkData("worldItem", _worldItem);
			FrameworkManager.SaveFrameworkData("playerItem", _playerItem);
			FrameworkManager.SaveFrameworkData("worldId", WorldId);
		}

		/// <summary>
		/// 加载全局物品
		/// </summary>
		public void LoadData()
		{
			_worldItem = (Dictionary<string, ItemObject>)FrameworkManager.LoadFrameworkData("worldItem") ?? new Dictionary<string, ItemObject>();
			_playerItem = (List<ItemObject>)FrameworkManager.LoadFrameworkData("playerItem") ?? new List<ItemObject>();
			var worldId = FrameworkManager.LoadFrameworkData("worldId");
			if (worldId != null)
				WorldId = (int)worldId;
		}

		/// <summary>
		/// 世界物品中通过名称找物品
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public static ItemObject WorldFindItemByName(string name)
		{
			return _worldItem[name];
		}
		/// <summary>
		/// 世界物品中通过编号找物品
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public static ItemObject WorldFindItemById(int id)
		{
			foreach (var item in _worldItem)
			{
				if (item.Value.Id == id)
				{
					return item.Value;
				}
			}
			return null;
		}
		/// <summary>
		/// 背包中通过名字找物品，返回第一个找到的
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public static ItemObject BagFindItemByName(string name)
		{
			var max = _playerItem.Count;
			for (int i = 0; i < max; i++)
			{
				var item = _playerItem[i];
				if (item.Name == name)
				{
					return item;
				}
			}
			return null;
		}
		/// <summary>
		/// 背包中通过名字找物品，返回所有符合的物品
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public static List<ItemObject> BagFindAllItemByName(string name)
		{
			var max = _playerItem.Count;
			var itemlist = new List<ItemObject>();
			for (int i = 0; i < max; i++)
			{
				var item = _playerItem[i];
				if (item.Name == name)
				{
					itemlist.Add(item);
				}
			}
			return itemlist;
		}
		/// <summary>
		/// 背包中通过编号找物品，返回第一个找到的
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public static ItemObject BagFindItemById(int id)
		{
			var max = _playerItem.Count;
			for (int i = 0; i < max; i++)
			{
				var item = _playerItem[i];
				if (item.Id == id)
				{
					return item;
				}
			}
			return null;
		}
		/// <summary>
		/// 背包中通过编号找物品，返回所有找到的
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public static List<ItemObject> BagFindAllItemById(int id)
		{
			var max = _playerItem.Count;
			var itemlist = new List<ItemObject>();
			for (int i = 0; i < max; i++)
			{
				var item = _playerItem[i];
				if (item.Id == id)
				{
					itemlist.Add(item);
				}
			}
			return itemlist;
		}
		/// <summary>
		/// 拾取物品时以编号作为获取参数
		/// </summary>
		/// <param name="id"></param>
		/// <param name="number"></param>
		public static void GetItemById(int id, int number)//需要注意检索是否装备唯一
		{
			var onget = WorldFindItemById(id);
			var max = _playerItem.Count;
			var is_finish = false;
			for (var i = 0; i < max; i++)
			{
				var item = _playerItem[i];
				if (item.Id == id)
				{
					if (item.Number + number <= item.MaxNumber)
					{
						item.Number += number;
						is_finish = true;
						break;
					}
				}
			}
			if (!is_finish)
			{
				onget.Number = number;
				_playerItem.Add(onget);
			}
			onget.GetItem();
		}
		/// <summary>
		/// 拾取物品时以名称作为获取参数
		/// </summary>
		/// <param name="name"></param>
		/// <param name="number"></param>
		public static void GetItemByName(string name, int number)
		{
			var onget = _worldItem[name];
			var max = _playerItem.Count;
			var is_finish = false;
			for (var i = 0; i < max; i++)
			{
				var item = _playerItem[i];
				if (item.Name != name) continue;
				if (item.Number + number > item.MaxNumber) continue;
				item.Number += number;
				is_finish = true;
				break;
			}
			if (!is_finish)
			{
				onget.Number = number;
				_playerItem.Add(onget);
			}
			onget.GetItem();
		}
	}
}