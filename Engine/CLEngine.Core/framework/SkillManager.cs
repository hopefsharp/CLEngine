﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace CLEngine.Core.framework
{
	/// <summary>
	/// 技能管理器
	/// </summary>
#if WIN
	[Serializable]
#endif
	[DataContract]
	[SuppressMessage("ReSharper", "UnusedMember.Global")]
	public class SkillManager : IDataFramework
	{
		/// <summary>
		/// 全局技能
		/// </summary>
		[DataMember] private static Dictionary<string, SkillObject> _worldSkill;

		static SkillManager()
		{
			_worldSkill = new Dictionary<string, SkillObject>();
		}

		/// <summary>
		/// 添加技能
		/// </summary>
		/// <param name="name">技能名</param>
		/// <returns>技能</returns>
		public static SkillObject AddSkill(string name)
		{
			var skill = new SkillObject();
			_worldSkill.Add(name, skill);

			return skill;
		}

		/// <summary>
		/// 删除技能
		/// </summary>
		/// <param name="name">技能名</param>
		public static void RemoveSkill(string name)
		{
			_worldSkill.Remove(name);
		}

		/// <summary>
		/// 获取技能列表
		/// </summary>
		/// <returns>技能列表</returns>
		public static List<SkillObject> GetSkillList()
		{
			var skillList = new List<SkillObject>();
			foreach (var skill in _worldSkill)
				skillList.Add(skill.Value);

			return skillList;
		}

		/// <summary>
		/// 通过名称获取世界技能
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public static SkillObject GetWorldSkill(string name)
		{
			return _worldSkill[name];
		}

		/// <summary>
		/// 加载数据
		/// </summary>
		public void LoadData()
		{
			_worldSkill = (Dictionary<string, SkillObject>)FrameworkManager.LoadFrameworkData("skills") ?? new Dictionary<string, SkillObject>();
		}

		/// <summary>
		/// 保存数据
		/// </summary>
		public void SaveData()
		{
			FrameworkManager.SaveFrameworkData("skills", _worldSkill);
		}
	}
}