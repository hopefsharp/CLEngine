﻿namespace CLEngine.Core.framework
{
	/// <summary>
	/// 数据框架接口
	/// 用于读取/加载数据
	/// </summary>
	public interface IDataFramework
	{
		/// <summary>
		/// 加载数据
		/// </summary>
		void LoadData();

		/// <summary>
		/// 保存数据
		/// </summary>
		void SaveData();
	}
}