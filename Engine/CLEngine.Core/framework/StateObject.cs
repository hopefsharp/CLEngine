﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CLEngine.Core.framework
{
#if WIN
	[Serializable]
#endif
	[DataContract]
	public class StateObject
	{
		/// <summary>
		/// 当buff被添加
		/// </summary>
		public EventHandler OnAdd;
		/// <summary>
		/// 当buff被移除
		/// </summary>
		public EventHandler OnRemove;
		/// <summary>
		/// 当buff被刷新
		/// </summary>
		public EventHandler OnRefresh;

		[DataMember] private int _continueTime;
		[DataMember] private bool _isAffectOnce;
		[DataMember] private string _name;
		[DataMember] private int _affectHp;
		[DataMember] private int _affectMp;
		[DataMember] private int _affectSpeed;
		[DataMember] private int _id;
		[DataMember] private List<string> _scriptList;
		/// <summary>
		/// 脚本列表
		/// </summary>
		public List<string> ScriptList { get { return _scriptList; } set { _scriptList = value; } }
		/// <summary>
		/// ID
		/// </summary>
		public int Id { get { return _id; } internal set { _id = value; } }
		/// <summary>
		/// 持续时间
		/// </summary>
		public int ContinueTime { get { return _continueTime; } set { _continueTime = value; } }
		/// <summary>
		/// 是否只生效一次
		/// </summary>
		public bool IsAffectOnce { get { return _isAffectOnce; } set { _isAffectOnce = value; } }
		/// <summary>
		/// 名称
		/// </summary>
		public string Name { get { return _name; } set { _name = value; } }
		/// <summary>
		/// 对生命值影响
		/// </summary>
		public int AffectHp { get { return _affectHp; } set { _affectHp = value; } }
		/// <summary>
		/// 对法力值影响
		/// </summary>
		public int AffectMp { get { return _affectMp; } set { _affectMp = value; } }
		/// <summary>
		/// 对速度影响
		/// </summary>
		public int AffectSpeed { get { return _affectSpeed; } set { _affectSpeed = value; } }
		public StateObject(string name)
		{
			Name = name;
		}
	}
}
