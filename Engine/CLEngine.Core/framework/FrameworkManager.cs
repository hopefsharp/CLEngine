﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace CLEngine.Core.framework
{
	/// <summary>
	/// 框架管理器
	/// </summary>
	public static class FrameworkManager
	{
		private static readonly Dictionary<string, IDataFramework> _dataFrameworks;

		static FrameworkManager()
		{
			_dataFrameworks = new Dictionary<string, IDataFramework>();
			RegisterDataFramework("item", new ItemManager());
			RegisterDataFramework("skill", new SkillManager());
		}

		/// <summary>
		/// 注册事件框架
		/// </summary>
		/// <param name="name">事件名</param>
		/// <param name="framework">事件数据</param>
		public static void RegisterDataFramework(string name, IDataFramework framework)
		{
			_dataFrameworks.Add(name, framework);
		}

		/// <summary>
		/// 取消注册事件框架
		/// </summary>
		/// <param name="name">框架名</param>
		/// <returns>是否成功</returns>
		public static bool UnRegisterDataFramework(string name)
		{
			return _dataFrameworks.Remove(name);
		}

		/// <summary>
		/// 获取数据框架
		/// </summary>
		/// <typeparam name="T">数据类型</typeparam>
		/// <param name="name">数据名称</param>
		/// <returns>数据</returns>
		public static T GetDataFramework<T>(string name) where T : IDataFramework
		{
			return (T)_dataFrameworks[name];
		}

		/// <summary>
		/// 加载所有数据
		/// </summary>
		[SuppressMessage("ReSharper", "ForCanBeConvertedToForeach")]
		public static void LoadAllData()
		{
			var dataFrameworkList = _dataFrameworks.ToList();
			// 不要使用foreach 列表发生更改时会报错
			for (int i = 0; i < dataFrameworkList.Count; i++)
				dataFrameworkList[i].Value.LoadData();
		}

		/// <summary>
		/// 保存所有数据
		/// </summary>
		public static void SaveAllData()
		{
			foreach (var dataFramework in _dataFrameworks)
				dataFramework.Value.SaveData();
		}

		/// <summary>
		/// 保存指定数据
		/// </summary>
		/// <param name="name"></param>
		public static void SaveData(string name)
		{
			try
			{
				_dataFrameworks[name].SaveData();
			}
			catch (Exception e)
			{
				MessageBox.Show(e.StackTrace);
			}
		}

		/// <summary>
		/// 改变指定数据
		/// </summary>
		/// <param name="name">名称</param>
		/// <param name="data">数据</param>
		public static void ChangeData<T>(string name, T data) where T : IDataFramework
		{
			_dataFrameworks[name] = data;
		}

		/// <summary>
		/// 加载数据
		/// </summary>
		/// <param name="name">文件名</param>
		public static object LoadFrameworkData(string name)
		{
			if (string.IsNullOrEmpty(SceneManager.GameProject.ProjectPath))
				throw new Exception("未加载工程前无法加载游戏数据");

			var dataPath = Path.Combine(SceneManager.GameProject.ProjectPath, "Content", "Data");
			try
			{
				var path = Path.Combine(dataPath, name + ".data");

				return File.Exists(path) ? CHelper.DeserializeObject(path) : null;
			}
			catch (Exception e)
			{
				throw new Exception(e.Message, e);
			}
		}

		/// <summary>
		/// 保存数据
		/// </summary>
		/// <param name="name">文件名</param>
		/// <param name="type">数据</param>
		public static void SaveFrameworkData(string name, object type)
		{
			if (string.IsNullOrEmpty(SceneManager.GameProject.ProjectPath))
				throw new Exception("未加载工程前无法保存游戏数据");

			var dataPath = Path.Combine(SceneManager.GameProject.ProjectPath, "Content", "Data");
			try
			{
				Directory.CreateDirectory(dataPath);
				CHelper.SerializeObject(Path.Combine(dataPath, name + ".data"), type);
			}
			catch (Exception e)
			{
				throw new Exception(e.Message, e);
			}
		}

		/// <summary>
		/// 获取全局事件
		/// </summary>
		/// <returns></returns>
		public static IGlobalEvent GetGlobalEvent()
		{
			var assembly = Assembly.GetAssembly(typeof(IGlobalEvent));
			var types = assembly.GetTypes();
			return (from item in types
				where !item.IsInterface
				from i in item.GetInterfaces()
				where i == typeof(IGlobalEvent)
				select (IGlobalEvent) Activator.CreateInstance(i)).FirstOrDefault();
		}

		/// <summary>
		/// 执行Lua代码
		/// </summary>
		/// <param name="fileName">文件名</param>
		/// <param name="funcName">函数名</param>
		/// <param name="param">参数</param>
		public static void DoLua(string fileName, string funcName, params object[] param)
		{

		}
	}
}