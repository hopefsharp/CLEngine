﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace CLEngine.Core.framework
{
	/// <summary>
	/// 
	/// </summary>
#if WIN
	[Serializable]
#endif
	[DataContract]
	[SuppressMessage("ReSharper", "UnusedMember.Local")]
	[SuppressMessage("ReSharper", "ArrangeAccessorOwnerBody")]
	[SuppressMessage("ReSharper", "UnusedMember.Global")]
	public class EventObject : GameObject
	{
		[DataMember] private string _name;
		[DataMember] private List<ItemObject> _bindItem;
		[DataMember] private GameObject _gameObj;
		[DataMember] private int _hp;
		[DataMember] private int _maxHp;
		[DataMember] private int _mp;
		[DataMember] private int _maxMp;
		[DataMember] private int _lv;
		[DataMember] private int _maxLv;
		[DataMember] private int _exp;
		[DataMember] private int _maxExp;
		[DataMember] private string _gender;
		[DataMember] private string _occupation;
		[DataMember] private Dictionary<string, SkillObject> _skill;
		[DataMember] private Dictionary<string, StateObject> _state;
		[DataMember] private SkillObject _onSkill;

		/// <summary>
		/// 触发使用技能事件，处理
		/// </summary>
		/// <param name="name"></param>
		public void UseSkill(string name)
		{
			var skill = _skill[name];

			if (skill == null)
			{
				if (GameEvent.UseSkill != null)
					GameEvent.UseSkill.Invoke(this, new SkillUseEventArgs(name, SkillEventType.NotFound));
				return;
			}
			if (skill.CurrentTime > 0)
			{
				if (GameEvent.UseSkill != null)
					GameEvent.UseSkill.Invoke(this, new SkillUseEventArgs(name, SkillEventType.Cd));
				return;
			}
			if (_mp < skill.UseMp)
			{
				if (GameEvent.UseSkill != null)
					GameEvent.UseSkill.Invoke(this, new SkillUseEventArgs(name, SkillEventType.NoMp));
				return;
			}
			if (_hp < skill.UseHp)
			{
				if (GameEvent.UseSkill != null)
					GameEvent.UseSkill.Invoke(this, new SkillUseEventArgs(name, SkillEventType.NoHp));
				return;
			}

			_mp -= skill.UseMp;
			_hp -= skill.UseHp;
			skill.CurrentTime = skill.CoolingTime;
			skill.TrigUse();
		}
		/// <summary>
		/// 添加技能
		/// </summary>
		/// <param name="name"></param>
		public void AddSkill(string name)
		{
			var skill = SkillManager.GetWorldSkill(name);
			if (skill == null)
			{
				throw new Exception("技能'" + name + "'不存在！");
			}
			_skill.Add(name, skill);
		}
		/// <summary>
		/// 删除技能
		/// </summary>
		/// <param name="name"></param>
		public void RemoveSkill(string name)
		{
			_skill.Remove(name);
		}
		/// <summary>
		/// 通过列表添加技能
		/// </summary>
		/// <param name="list"></param>
		public void AddSkillByList(List<string> list)
		{
			foreach (var skill in list)
			{
				AddSkill(skill);
			}
		}
		/// <summary>
		/// 通过列表删除技能
		/// </summary>
		/// <param name="list"></param>
		public void RemoveSkillByList(List<string> list)
		{
			foreach (var skill in list)
			{
				RemoveSkill(skill);
			}
		}

		/// <summary>
		/// 状态表
		/// </summary>
		public Dictionary<string, StateObject> State { get { return _state; } private set { _state = value; } }
		/// <summary>
		/// 技能表
		/// </summary>
		public Dictionary<string, SkillObject> Skill { get { return _skill; } private set { _skill = value; } }
		/// <summary>
		/// 升级所需最大经验值
		/// </summary>
		public int MaxExp { get { return _maxExp; } set { _maxExp = value; } }
		/// <summary>
		/// 当前经验值
		/// </summary>
		public int Exp { get { return _exp; } set { _exp = value; } }
		/// <summary>
		/// 性别
		/// </summary>
		public string Gender { get { return _gender; } set { _gender = value; } }
		/// <summary>
		/// 职业
		/// </summary>
		public string Occupation { get { return _occupation; } set { _occupation = value; } }
		/// <summary>
		/// 角色最大等级
		/// </summary>
		public int MaxLv { get { return _maxLv; } set { _maxLv = value; } }
		/// <summary>
		/// 角色等级
		/// </summary>
		public int Lv { get { return _lv; } set { _lv = value; } }
		/// <summary>
		/// 最大法力值
		/// </summary>
		public int MaxMp { get { return _maxMp; } set { _maxMp = value; } }
		/// <summary>
		/// 蓝量
		/// </summary>
		public int Mp { get { return _mp; } set { _mp = value; } }
		/// <summary>
		/// 最大生命值
		/// </summary>
		public int MaxHp { get { return _maxHp; } set { _maxHp = value; } }
		/// <summary>
		/// 血量
		/// </summary>
		public int Hp { get { return _hp; } set { _hp = value; } }
		/// <summary>
		/// 游戏对象
		/// </summary>
		public GameObject GameObject { get { return _gameObj; } set { _gameObj = value; } }
		/// <summary>
		/// 事件名
		/// </summary>
		public new string Name { get { return _name; } set { _name = value; } }
		/// <summary>
		/// 绑定的物品
		/// </summary>
		public List<ItemObject> BindItem { get { return _bindItem; } set { _bindItem = value; } }
	}
}
