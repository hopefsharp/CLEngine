﻿using FairyGUI;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.Serialization;

namespace CLEngine.Core.framework
{
	/// <summary>
	/// 
	/// </summary>
#if WIN
	[Serializable]
#endif
	[DataContract]
	public class SkillEventArgs : EventArgs
	{
		/// <summary>
		/// 
		/// </summary>
		public int userId;
		/// <summary>
		/// 技能是否可以使用
		/// </summary>
		public bool canUse;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		public SkillEventArgs(int id)
		{
			userId = id;
		}
	}

	/// <summary>
	/// 
	/// </summary>
#if WIN
	[Serializable]
#endif
	[DataContract]
	[SuppressMessage("ReSharper", "ArrangeAccessorOwnerBody")]
	[SuppressMessage("ReSharper", "UnusedMember.Global")]
	public class SkillObject
	{
		/// <summary>
		/// 使用技能事件
		/// </summary>
		public EventHandler Use;
		/// <summary>
		/// 技能冷却改变事件
		/// </summary>
		public EventHandler Cooling;
		/// <summary>
		/// 冷却完成事件
		/// </summary>
		public EventHandler FinishCooling;

		[DataMember] private string _name;
		[DataMember] private string _iconPath;
		[DataMember] private float _coolingTime;
		[DataMember] private float _currentTime;
		[DataMember] private int _useHp;
		[DataMember] private int _useMp;
		[DataMember] private string _description;
		[DataMember] private int _castingDistance;
		[DataMember] private float _castingSpeed;
		[DataMember] private int _userId;
		[DataMember] private List<string> _scriptList;
		[NonSerialized] private Image _icon;
		[NonSerialized] private Texture2D _iconTex;
		/// <summary>
		/// 脚本列表
		/// </summary>
		public List<string> ScriptList { get { return _scriptList; } set { _scriptList = value; } }
		/// <summary>
		/// 当前冷却时间
		/// </summary>
		public float CurrentTime { get { return _currentTime; } set { _currentTime = value; } }
		/// <summary>
		/// 技能使用者id
		/// </summary>
		public int UserId { get { return _userId; } set { _userId = value; } }
		/// <summary>
		/// 游戏图标
		/// </summary>
		public Texture2D IconTex { get { return _iconTex; } set { _iconTex = value; } }
		/// <summary>
		/// fui图标
		/// </summary>
		public Image Icon { get { return _icon; } set { _icon = value; } }
		/// <summary>
		/// 技能名称
		/// </summary>
		public string Name
		{
			get { return _name; }
			set { _name = value; }
		}
		/// <summary>
		/// 技能图标路径
		/// </summary>
		public string IconPath
		{
			get { return _iconPath; }
			set
			{
				_iconPath = value;
				if (string.IsNullOrEmpty(_iconPath))
				{
					_iconTex = null;
					return;
				}
				var path = Path.Combine(SceneManager.GameProject.ProjectPath, _iconPath);
				if (File.Exists(path))
				{
					IconTex = TextureLoader.FromContent(_iconPath);
				}
				else
				{
					throw new Exception("文件" + path + "不存在");
				}
			}
		}
		/// <summary>
		/// 技能冷却时间
		/// </summary>
		public float CoolingTime
		{
			get { return _coolingTime; }
			set { _coolingTime = value; }
		}
		/// <summary>
		/// 消耗Hp
		/// </summary>
		public int UseHp
		{
			get { return _useHp; }
			set { _useHp = value; }
		}
		/// <summary>
		/// 消耗Mp
		/// </summary>
		public int UseMp
		{
			get { return _useMp; }
			set { _useMp = value; }
		}
		/// <summary>
		/// 技能描述
		/// </summary>
		public string Description
		{
			get { return _description; }
			set { _description = value; }
		}
		/// <summary>
		/// 施法距离
		/// </summary>
		public int CastingDistance
		{
			get { return _castingDistance; }
			set { _castingDistance = value; }
		}
		/// <summary>
		/// 施法速度
		/// </summary>
		public float CastingSpeed
		{
			get { return _castingSpeed; }
			set { _castingSpeed = value; }
		}
		/// <summary>
		/// 技能类型
		/// </summary>
		public enum SkillType
		{
			/// <summary>
			/// 
			/// </summary>
			Consumable,
			/// <summary>
			/// 
			/// </summary>
			Bullet
		}
		/// <summary>
		/// 目标类型
		/// </summary>
		public enum TargetType
		{
			/// <summary>
			/// 
			/// </summary>
			Self,
			/// <summary>
			/// 
			/// </summary>
			Enemy,
			/// <summary>
			/// 
			/// </summary>
			Friendly
		}
		/// <summary>
		/// 释放位置
		/// </summary>
		public enum ReleasePosition
		{
			/// <summary>
			/// 
			/// </summary>
			Self,
			/// <summary>
			/// 
			/// </summary>
			Target,
			/// <summary>
			/// 
			/// </summary>
			Mouse
		}
		/// <summary>
		/// 触发使用技能事件
		/// </summary>
		public void TrigUse()
		{
			if (Use != null)
			{
				Use.Invoke(this, new SkillEventArgs(_userId));
			}
		}
		/// <summary>
		/// 触发技能冷却刷新事件
		/// </summary>
		public void TrigCooling()
		{
			if (Cooling != null)
			{
				Cooling.Invoke(this, new SkillEventArgs(_userId));
			}
		}
		/// <summary>
		/// 触发技能冷却完成事件
		/// </summary>
		public void TrigFinishCooling()
		{
			if (FinishCooling != null)
			{
				FinishCooling.Invoke(this, new SkillEventArgs(_userId));
			}
		}
	}
}
