﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Microsoft.Xna.Framework.Graphics;
using System.IO;
using Microsoft.Xna.Framework;
using FairyGUI;

namespace CLEngine.Core.framework
{
	/// <summary>
	/// 
	/// </summary>
#if WIN
	[Serializable]
#endif
	[DataContract]
	[SuppressMessage("ReSharper", "ArrangeAccessorOwnerBody")]
	[SuppressMessage("ReSharper", "UnusedMember.Global")]
	[SuppressMessage("ReSharper", "UnusedMember.Global")]
	public class ItemObject
	{
		[DataMember] private string _iconPath;
		[DataMember] private string _name;
		[DataMember] private string _showName;
		[DataMember] private string _description;
		[DataMember] private string _type;
		[DataMember] private int _userId;
		[DataMember] private int _id;
		[DataMember] private string _belongBag;
		[DataMember] private int _number;
		[DataMember] private int _maxNumber;
		[DataMember] private float _salePercent;
		[DataMember] private int _buyPrice;
		[DataMember] private Color _color;
		[DataMember] private int _durability;
		[DataMember] private int _maxDurability;
		[DataMember] private int _addMoveSpeed;
		[DataMember] private int _hp;
		[DataMember] private int _mp;
		[DataMember] private int _addHp;
		[DataMember] private int _addMp;
		[DataMember] private int _addReturnHp;
		[DataMember] private int _addReturnMp;
		[DataMember] private int _addAtkSpeed;
		[DataMember] private int _addAtkRange;
		[DataMember] private Dictionary<string, object> _customProp;
		[DataMember] private bool _isGuiShow;
		[DataMember] private int _lv;
		[DataMember] private string _child;
		[DataMember] private string _occupation;
		[DataMember] private string _gender;
		[DataMember] private bool _canSale;
		[DataMember] private bool _canTrade;
		[DataMember] private bool _canThrow;
		[DataMember] private int _strength;
		[DataMember] private int _quick;
		[DataMember] private int _intelligence;
		[DataMember] private int _luck;
		[DataMember] private int _san;
		[DataMember] private int _addAD;
		[DataMember] private int _addAP;
		[DataMember] private int _addADDef;
		[DataMember] private int _addAPDef;
		[DataMember] private int _cd;
		[DataMember] private bool _onlyGetOne;
		[DataMember] private float _hitAccuracy;
		[DataMember] private float _missPercent;
		[DataMember] private float _magicUseSpeed;
		[DataMember] private string _classification;
		[DataMember] private bool _onlyStoreOne;
		[DataMember] private string _dropIconPath;
		[DataMember] private List<string> _scriptList;
		[NonSerialized] private Image _guiIcon;
		[NonSerialized] private Texture2D _texture;
		[NonSerialized] private Texture2D _dropIcon;

		private int _positionInBag;
		/// <summary>
		/// 脚本列表
		/// </summary>
		public List<string> ScriptList { get { return _scriptList; } set { _scriptList = value; } }
		/// <summary>
		/// 掉落图标路径
		/// </summary>
		public string DropIconPath
		{
			get { return _dropIconPath; }
			set
			{
				_dropIconPath = value;
				if (string.IsNullOrEmpty(_dropIconPath))
				{
					_dropIcon = null;
					return;
				}
				var path = Path.Combine(SceneManager.GameProject.ProjectPath, _dropIconPath);
				if (File.Exists(path))
				{
					_dropIcon = TextureLoader.FromContent(_dropIconPath);
				}
				else
				{
					_dropIcon = null;
					Console.WriteLine("文件" + _dropIconPath + "不存在");
				}
			}
		}
		/// <summary>
		/// 掉落图标
		/// </summary>
		public Texture2D DropIcon { get { return _dropIcon; } set { _dropIcon = value; } }
		/// <summary>
		/// 仓库唯一
		/// </summary>
		public bool OnlyStoreOne { get { return _onlyStoreOne; } set { _onlyStoreOne = value; } }
		/// <summary>
		/// 分类
		/// </summary>
		public string Classification { get { return _classification; } set { _classification = value; } }
		/// <summary>
		/// 施法速率
		/// </summary>
		public float MagicUseSpeed { get { return _magicUseSpeed; } set { _magicUseSpeed = value; } }
		/// <summary>
		/// 闪避率
		/// </summary>
		public float MissPercent { get { return _missPercent; } set { _missPercent = value; } }
		/// <summary>
		/// 命中率
		/// </summary>
		public float HitAccuracy { get { return _hitAccuracy; } set { _hitAccuracy = value; } }
		/// <summary>
		/// 背包唯一
		/// </summary>
		public bool OnlyGetOne { get { return _onlyGetOne; } set { _onlyGetOne = value; } }
		/// <summary>
		/// 冷却时间
		/// </summary>
		public int CD { get { return _cd; } set { _cd = value; } }
		/// <summary>
		/// 回复法力值
		/// </summary>
		public int Mp { get { return _mp; } set { _mp = value; } }
		/// <summary>
		/// 附加属性：魔法防御
		/// </summary>
		public int AddAPDef { get { return _addAPDef; } set { _addAPDef = value; } }
		/// <summary>
		/// 附加属性：物理防御
		/// </summary>
		public int AddADDef { get { return _addADDef; } set { _addADDef = value; } }
		/// <summary>
		/// 附加属性：魔法攻击
		/// </summary>
		public int AddAP { get { return _addAP; } set { _addAP = value; } }
		/// <summary>
		/// 附加属性：物理攻击
		/// </summary>
		public int AddAD { get { return _addAD; } set { _addAD = value; } }
		/// <summary>
		/// 回复生命值
		/// </summary>
		public int Hp { get { return _hp; } set { _hp = value; } }
		/// <summary>
		/// 精神
		/// </summary>
		public int San { get { return _san; } set { _san = value; } }
		/// <summary>
		/// 运气
		/// </summary>
		public int Luck { get { return _luck; } set { _luck = value; } }
		/// <summary>
		/// 智力
		/// </summary>
		public int Intelligence { get { return _intelligence; } set { _intelligence = value; } }
		/// <summary>
		/// 敏捷
		/// </summary>
		public int Quick { get { return _quick; } set { _quick = value; } }
		/// <summary>
		/// 力量
		/// </summary>
		public int Strength { get { return _strength; } set { _strength = value; } }
		/// <summary>
		/// 是否可丢弃
		/// </summary>
		public bool CanThrow { get { return _canThrow; } set { _canThrow = value; } }
		/// <summary>
		/// 是否可以交易
		/// </summary>
		public bool CanTrade { get { return _canTrade; } set { _canTrade = value; } }
		/// <summary>
		/// 物品是否可以出售
		/// </summary>
		public bool CanSale { get { return _canSale; } set { _canSale = value; } }
		/// <summary>
		/// 物品性别
		/// </summary>
		public string Gender { get { return _gender; } set { _gender = value; } }
		/// <summary>
		/// 物品职业
		/// </summary>
		public string Occupation { get { return _occupation; } set { _occupation = value; } }
		/// <summary>
		/// 物品子类
		/// </summary>
		public string Child { get { return _child; } set { _child = value; } }
		/// <summary>
		/// 物品等级
		/// </summary>
		public int Lv { get { return _lv; } set { _lv = value; } }
		/// <summary>
		/// 显示名
		/// </summary>
		public string ShowName { get { return _showName; } set { _showName = value; } }
		/// <summary>
		/// 属于哪个背包（名）
		/// </summary>
		public string BelongBag { get { return _belongBag; } set { _belongBag = value; } }
		/// <summary>
		/// 物品颜色级别
		/// </summary>
		public Color ItemColor { get { return _color; } set { _color = value; } }
		/// <summary>
		/// 购买价格
		/// </summary>
		public int BuyPrice { get { return _buyPrice; } set { _buyPrice = value; } }
		/// <summary>
		/// 出售价格折算比
		/// </summary>
		public float SalePercent { get { return _salePercent; } set { _salePercent = value; } }
		/// <summary>
		/// 是否gui显示
		/// </summary>
		public bool IsGuiShow { get { return _isGuiShow; } set { _isGuiShow = value; } }
		/// <summary>
		/// GUI中的图标
		/// </summary>
		public Image GuiIcon { get { return _guiIcon; } set { _guiIcon = value; } }
		/// <summary>
		/// 在背包内的位置
		/// </summary>
		public int PositionInBag { get { return _positionInBag; } set { _positionInBag = value; } }
		/// <summary>
		/// 物品类型
		/// </summary>
		public string Type { get { return _type; } set { _type = value; } }
		/// <summary>
		/// 物品描述
		/// </summary>
		public string Description { get { return _description; } set { _description = value; } }
		/// <summary>
		/// 物品名称
		/// </summary>
		public string Name { get { return _name; } set { _name = value; } }
		/// <summary>
		/// 物品图标路径
		/// </summary>
		public string IconPath
		{
			get { return _iconPath; }
			set
			{
				_iconPath = value;
				if (string.IsNullOrEmpty(_iconPath))
				{
					IconTex = null;
					return;
				}

				var path = Path.Combine(SceneManager.GameProject.ProjectPath, _iconPath);
				if (File.Exists(path))
				{
					IconTex = TextureLoader.FromContent(_iconPath);
				}
				else
				{
					Console.WriteLine("文件" + path + "不存在");
				}
			}
		}
		/// <summary>
		/// 图标图片
		/// </summary>
		public Texture2D IconTex
		{
			get { return _texture; }
			set { _texture = value; }
		}
		/// <summary>
		/// 持有者id
		/// </summary>
		public int userId { get { return _userId; } set { _userId = value; } }
		/// <summary>
		/// 物品id
		/// </summary>
		public int Id { get { return _id; } internal set { _id = value; } }
		/// <summary>
		/// 数量
		/// </summary>
		public int Number { get { return _number; } set { _number = value; } }
		/// <summary>
		/// 最大堆叠数量
		/// </summary>
		public int MaxNumber { get { return _maxNumber; } set { _maxNumber = value; } }
		/// <summary>
		/// 耐久
		/// </summary>
		public int Durability { get { return _durability; } set { _durability = value; } }
		/// <summary>
		/// 最大耐久
		/// </summary>
		public int MaxDurability { get { return _maxDurability; } set { _maxDurability = value; } }
		/// <summary>
		/// 所在背包的位置
		/// </summary>
		public Vector2 BagPosition { get; set; }

		/// <summary>
		/// 附加属性：移动速度
		/// </summary>
		public int AddMoveSpeed { get { return _addMoveSpeed; } set { _addMoveSpeed = value; } }
		/// <summary>
		/// 附加属性：攻击速度
		/// </summary>
		public int AddAtkSpeed { get { return _addAtkSpeed; } set { _addAtkSpeed = value; } }
		/// <summary>
		/// 附加属性：攻击范围
		/// </summary>
		public int AddAtkRange { get { return _addAtkRange; } set { _addAtkRange = value; } }
		/// <summary>
		/// 附加属性：角色生命值
		/// </summary>
		public int AddHp { get { return _addHp; } set { _addHp = value; } }
		/// <summary>
		/// 附加属性：角色魔法值
		/// </summary>
		public int AddMp { get { return _addMp; } set { _addMp = value; } }
		/// <summary>
		/// 附加属性：角色生命值恢复
		/// </summary>
		public int AddReturnHp { get { return _addReturnHp; } set { _addReturnHp = value; } }
		/// <summary>
		/// 附加属性：角色魔法值恢复
		/// </summary>
		public int AddReturnMp { get { return _addReturnMp; } set { _addReturnMp = value; } }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="name">物品名称</param>
		public ItemObject(string name = "")
		{
			ScriptList = new List<string>();
			BagPosition = Vector2.Zero;
			Name = name;
		}

		/// <summary>
		/// 设置自定义值
		/// </summary>
		/// <param name="name"></param>
		/// <param name="customprop"></param>
		public void SetcustomProp(string name, object customprop)
		{
			if (_customProp.ContainsKey(name))
			{
				Console.WriteLine("自定义值" + name + "不允许重复");
			}
			_customProp[name] = customprop;
		}
		/// <summary>
		/// 获取自定义值
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public object GetcustomProp(string name)
		{
			return _customProp[name];
		}
		/// <summary>
		/// 使用物品
		/// </summary>
		public void UseItem()
		{
			FrameworkManager.GetGlobalEvent()?.UseItem(userId);
		}
		/// <summary>
		/// 获取物品
		/// </summary>
		public void GetItem()
		{
			FrameworkManager.GetGlobalEvent()?.GetItem(userId);
		}
		/// <summary>
		/// 丢掉物品
		/// </summary>
		public void ThrowItem()
		{
			FrameworkManager.GetGlobalEvent()?.ThrowItem(userId);
		}
		/// <summary>
		/// 物品数量改变
		/// </summary>
		public void ItemNumberChange()
		{
			FrameworkManager.GetGlobalEvent()?.ChangePropItem(userId);
		}
		/// <summary>
		/// 返回表示当前对象的字符串
		/// </summary>
		/// <returns>表示当前对象的字符串</returns>
		public override string ToString()
		{
			return _name;
		}
		/// <summary>
		/// 
		/// </summary>
		public ItemObject()
		{
			SalePercent = 1f;
		}
	}
}
