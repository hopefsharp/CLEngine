namespace CLEngine.Core.framework
{
    /// <summary>
    /// 游戏全局事件
    /// </summary>
    public interface IGlobalEvent
    {
         #region 物品事件

         /// <summary>
         /// 使用物品事件
         /// </summary>
         void UseItem(int id);

         /// <summary>
         /// 获取物品事件
         /// </summary>
         void GetItem(int id);

		 /// <summary>
		 /// 丢弃物品事件
		 /// </summary>
		 /// <param name="id"></param>
		 void ThrowItem(int id);

		 /// <summary>
		 /// 物品属性改变事件
		 /// </summary>
		 /// <param name="id"></param>
		 void ChangePropItem(int id);

		 #endregion
    }
}