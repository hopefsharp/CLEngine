﻿using System;

namespace CLEngine.Core.framework
{
	/// <summary>
	/// 技能事件类型
	/// </summary>
	public enum SkillEventType
	{
		/// <summary>
		/// 普通事件
		/// </summary>
		None,
		/// <summary>
		/// 未找到技能
		/// </summary>
		NotFound,
		/// <summary>
		/// 冷却事件
		/// </summary>
		Cd,
		/// <summary>
		/// 无法力
		/// </summary>
		NoMp,
		/// <summary>
		/// 无生命值
		/// </summary>
		NoHp,
	}

	/// <summary>
	/// 技能使用事件
	/// </summary>
	public class SkillUseEventArgs : EventArgs
	{
		/// <summary>
		/// 技能名称
		/// </summary>
		public string name;
		/// <summary>
		/// 技能事件类型
		/// </summary>
		public SkillEventType type;

		/// <summary>
		/// 
		/// </summary>
		public SkillUseEventArgs(string name, SkillEventType type = SkillEventType.None)
		{
			this.name = name;
			this.type = type;
		}
	}

	/// <summary>
	/// 游戏事件类
	/// </summary>
	public class GameEvent
	{
		/// <summary>
		/// 使用技能事件
		/// </summary>
		public static EventHandler UseSkill;
	}
}