﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;

namespace CLEngine.Core.framework
{
	/// <summary>
	/// 框架设置类
	/// 用于设置全局框架内容
	/// </summary>
#if WIN
	[Serializable]
#endif
	[DataContract]
	public static class FrameworkSettings
	{
		[DataMember] private static Vector2 _defaultBagPosition;

		/// <summary>
		/// 默认背包位置
		/// </summary>
		public static Vector2 DefaultBagPosition
		{
			get { return _defaultBagPosition; }
			set { _defaultBagPosition = value; }
		}

		static FrameworkSettings()
		{
			DefaultBagPosition = Vector2.Zero;
		}
		/// <summary>
		/// 物品列表
		/// </summary>
		public static Dictionary<string, List<string>> ItemTypes = new Dictionary<string, List<string>>{
			{"物品",new List<string>{"消耗品","任务用品","材料","礼包" } },
			{"装备",new List<string>{"武器","鞋子","帽子","衣服","裤子","首饰","戒指" } },
		};

		/// <summary>
		/// 性别列表
		/// </summary>
		public static List<string> GenderTypes = new List<string>()
		{
			"无",
			"男",
			"女"
		};

		/// <summary>
		/// 脚本列表
		/// </summary>
		public static List<string> ScriptTypes = new List<string>()
		{
			"CSharp",
			"Lua",
		};

		/// <summary>
		/// 职业列表
		/// </summary>
		public static List<string> OccupationTypes = new List<string>()
		{

		};
	}
}