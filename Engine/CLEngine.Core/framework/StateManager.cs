﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace CLEngine.Core.framework
{
	/// <summary>
	/// 状态管理器
	/// </summary>
#if WIN
	[Serializable]
#endif
	[DataContract]
	[SuppressMessage("ReSharper", "UnusedMember.Global")]
	public class StateManager : IDataFramework
	{
		/// <summary>
		/// 全局状态
		/// </summary>
		[DataMember] private static Dictionary<string, StateObject> _worldState;
		/// <summary>
		/// 玩家状态
		/// </summary>
		[DataMember] private static List<StateObject> _playerState;
		/// <summary>
		/// 全局状态Id
		/// </summary>
		[DataMember] public static int WorldId { get; private set; }

		/// <summary>
		/// 全局状态
		/// </summary>
		public static Dictionary<string, StateObject> WorldState => _worldState;

		static StateManager()
		{
			_worldState = new Dictionary<string, StateObject>();
			_playerState = new List<StateObject>();
			WorldId = 1;
		}

		/// <summary>
		/// 创建状态
		/// </summary>
		public static StateObject CreateState(string name)
		{
			var State = new StateObject(name) { Id = WorldId };
			_worldState.Add(name, State);

			WorldId++;

			return State;
		}

		/// <summary>
		/// 删除状态
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public static StateObject RemoveState(string name)
		{
			var State = new StateObject(name);
			_worldState.Remove(name);

			return State;
		}

		/// <summary>
		/// 删除状态
		/// </summary>
		/// <param name="State"></param>
		/// <returns></returns>
		public static bool RemoveState(StateObject State)
		{
			foreach (var StateObject in _worldState)
			{
				if (StateObject.Value == State)
				{
					_worldState.Remove(StateObject.Key);
					return true;
				}
			}

			return false;
		}

		/// <summary>
		/// 保存全局状态
		/// </summary>
		public void SaveData()
		{
			FrameworkManager.SaveFrameworkData("worldState", _worldState);
			FrameworkManager.SaveFrameworkData("playerState", _playerState);
			FrameworkManager.SaveFrameworkData("worldId", WorldId);
		}

		/// <summary>
		/// 加载全局状态
		/// </summary>
		public void LoadData()
		{
			_worldState = (Dictionary<string, StateObject>)FrameworkManager.LoadFrameworkData("worldState") ?? new Dictionary<string, StateObject>();
			_playerState = (List<StateObject>)FrameworkManager.LoadFrameworkData("playerState") ?? new List<StateObject>();
			var worldId = FrameworkManager.LoadFrameworkData("worldId");
			if (worldId != null)
				WorldId = (int)worldId;
		}

		/// <summary>
		/// 世界状态中通过名称找状态
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public static StateObject WorldFindStateByName(string name)
		{
			return _worldState[name];
		}
		/// <summary>
		/// 世界状态中通过编号找状态
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public static StateObject WorldFindStateById(int id)
		{
			foreach (var State in _worldState)
			{
				if (State.Value.Id == id)
				{
					return State.Value;
				}
			}
			return null;
		}
		/// <summary>
		/// 状态表中通过名字找状态，返回第一个找到的
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public static StateObject PlayerFindStateByName(string name)
		{
			var max = _playerState.Count;
			for (int i = 0; i < max; i++)
			{
				var State = _playerState[i];
				if (State.Name == name)
				{
					return State;
				}
			}
			return null;
		}
		/// <summary>
		/// 状态表中通过名字找状态，返回所有符合的状态
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public static List<StateObject> PlayerFindAllStateByName(string name)
		{
			var max = _playerState.Count;
			var Statelist = new List<StateObject>();
			for (int i = 0; i < max; i++)
			{
				var State = _playerState[i];
				if (State.Name == name)
				{
					Statelist.Add(State);
				}
			}
			return Statelist;
		}
		/// <summary>
		/// 状态表中通过编号找状态，返回第一个找到的
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public static StateObject PlayerFindStateById(int id)
		{
			var max = _playerState.Count;
			for (int i = 0; i < max; i++)
			{
				var State = _playerState[i];
				if (State.Id == id)
				{
					return State;
				}
			}
			return null;
		}
		/// <summary>
		/// 状态表中通过编号找状态，返回所有找到的
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public static List<StateObject> PlayerFindAllStateById(int id)
		{
			var max = _playerState.Count;
			var Statelist = new List<StateObject>();
			for (int i = 0; i < max; i++)
			{
				var State = _playerState[i];
				if (State.Id == id)
				{
					Statelist.Add(State);
				}
			}
			return Statelist;
		}
	}
}