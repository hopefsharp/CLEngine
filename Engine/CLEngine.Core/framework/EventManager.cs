﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace CLEngine.Core.framework
{
	/// <summary>
	/// 事件管理类
	/// </summary>
	public static class EventManager
	{
		private static List<EventObject> eventObjects = new List<EventObject>();

		/// <summary>
		/// 添加事件
		/// </summary>
		/// <returns>事件</returns>
		public static EventObject AddEvent()
		{
			var eventObject = new EventObject();
			eventObjects.Add(eventObject);

			return eventObject;
		}

		/// <summary>
		/// 移除事件
		/// </summary>
		/// <param name="eventObject">事件</param>
		public static void RemoveEvent(EventObject eventObject)
		{
			eventObjects.Remove(eventObject);
		}

		/// <summary>
		/// 绑定对象
		/// </summary>
		/// <param name="eventObject">事件</param>
		/// <param name="gameObject">对象</param>
		public static void BindObject(this EventObject eventObject, GameObject gameObject)
		{
			eventObject.GameObject = gameObject;
		}

		/// <summary>
		/// 释放对象
		/// </summary>
		/// <param name="eventObject">事件</param>
		public static void ReleaseObject(this EventObject eventObject)
		{
			eventObject.GameObject = null;
		}
	}
}