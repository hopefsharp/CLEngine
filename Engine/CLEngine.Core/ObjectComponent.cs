﻿using System;
using System.ComponentModel;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Runtime.Serialization;

namespace CLEngine.Core
{
    /// <summary>
    /// 此类表示一个对象组件
    /// </summary>
#if WIN
    [Serializable]
#elif WINRT
    [DataContract]
#endif
    public class ObjectComponent
#if WIN
    : ICloneable
#endif
    {
        [DataMember]
        private bool disabled;

        [DataMember]
        private bool editorExpanded;

        [DataMember]
        private string name = string.Empty;

#if WIN
        [NonSerialized]
#endif
        private Transform transform;

        /// <summary>
        /// 确定组件是否已禁用
        /// </summary>
#if WIN
        [Browsable(false)]
#endif
        public bool Disabled
        {
            get { return disabled; }
            set { disabled = value; }
        }

        /// <summary>
        /// 组件对象的引用变换
        /// </summary>
#if WIN
        [Browsable(false)]
#endif
        public Transform Transform
        {
            get { return transform; }
            set { transform = value; }
        }

        /// <summary>
        /// 组件的名称
        /// </summary>
#if WIN
        [Browsable(false)]
#endif
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// 编辑属性面板内是否允许展开
        /// </summary>
#if WIN
        [Browsable(false)]
#endif
        public bool EditorExpanded
        {
            get { return editorExpanded; }
            set { editorExpanded = value; }
        }

        /// <summary>
        /// 组件所在的对象
        /// </summary>
#if WIN
        [Browsable(false)]
#endif
        public GameObject GameObject
        {
            get { return Transform.GameObject;}
            set { Transform.GameObject = value; }
        }

        /// <summary>
        /// 从游戏对象中删除脚本时调用此方法
        /// </summary>
        public virtual void Removed()
        {
        }

        /// <summary>
        /// 在没有与其他对象发生碰撞的框架中抛出的事件
        /// </summary>
        public virtual void OnCollisionFree()
        {

        }

        /// <summary>
        /// 绘制
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="spriteBatch"></param>
        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {

        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime"></param>
        public virtual void Update(GameTime gameTime)
        {

        }

        /// <summary>
        /// 鼠标未在对象上时抛出的事件
        /// </summary>
        public virtual void OnMouseOut()
        {

        }

        /// <summary>
        /// 当鼠标与游戏对象发生碰撞时抛出的事件
        /// </summary>
        public virtual void OnMouseEnter()
        {

        }

        /// <summary>
        /// 当鼠标移动碰撞时抛出事件
        /// </summary>
        public virtual void OnMouseMove()
        {

        }

        /// <summary>
        /// 触发游戏对象上的鼠标向上事件时抛出的事件
        /// </summary>
        public virtual void OnMouseUp()
        {

        }

        /// <summary>
        /// 当鼠标单击碰撞时抛出事件
        /// </summary>
        /// <param name="buttonPressed"></param>
        public virtual void OnMouseClick(MouseEventButton buttonPressed)
        {

        }

        /// <summary>
        /// 当鼠标单击碰撞时抛出事件
        /// </summary>
        /// <param name="buttonPressed"></param>
        public virtual void OnMouseDown(MouseEventButton buttonPressed)
        {

        }

        /// <summary>
        /// 发生物体碰撞时抛出的事件
        /// </summary>
        /// <param name="other"></param>
        public virtual void OnCollisionEnter(GameObject other)
        {

        }

        /// <summary>
        /// 转字符串
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return name;
        }

        /// <summary>
        /// 复制
        /// </summary>
        /// <returns>组件</returns>
        public object Clone()
        {
            return MemberwiseClone();
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public virtual void Initialize()
        {

        }
    }
}