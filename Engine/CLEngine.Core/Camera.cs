﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;

namespace CLEngine.Core
{
    /// <summary>
    /// 相机对象
    /// </summary>
#if WIN
    [ExpandableObject]
    [Serializable, TypeConverter(typeof(ExpandableObjectConverter))]
#endif
    [DataContract]
    public class Camera : SystemObject
    {
        private Matrix transformMatrix;

        [DataMember]
        private Vector2 position;

        [DataMember]
        private float zoom;

        /// <summary>
        /// 当前的Camera.tion矩阵
        /// </summary>
#if WIN
        [Browsable(false)]
#endif
        public Matrix TransformMatrix
        {
            get
            {
                transformMatrix = CalculateTransform();

                return transformMatrix;
            }
        }

        /// <summary>
        /// 相机的位置
        /// </summary>
#if WIN
        [Category("相机属性")]
        [DisplayName("位置"), Description("相机的位置")]
#endif
        public Vector2 Position
        {
            get { return position; }
            set
            {
                position = value;
            }
        }

        [DataMember]
        private float rotation = 0;

        /// <summary>
        /// 相机的缩放倍数
        /// </summary>
#if WIN
        [Category("相机属性")]
        [DisplayName("缩放"), Description("相机的缩放倍数")]
#endif
        public float Zoom
        {
            get { return zoom; }
            set
            {
                if (value < 0.05f) value = 0.05f;
                zoom = value;
            }
        }

        /// <summary>
        /// 相机的旋转角度
        /// </summary>
#if WIN
        [Category("相机属性")]
        [DisplayName("旋转"), Description("相机旋转角度")]
#endif
        public float Rotation
        {
            get { return rotation; }
            set { rotation = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Camera()
        {
            //this.transform.GameObject = this;
            this.Position = Vector2.Zero;
            this.zoom = 1.0f;
        }

        /// <summary>
        /// 绘制
        /// </summary>
        public void Draw()
        {

        }

        /// <summary>
        /// 将着色器应用于相机表面
        /// </summary>
        private void ApplyPostprocessing()
        {

        }

        /// <summary>
        /// 计算相机的变换矩阵
        /// </summary>
        /// <returns></returns>
        private Matrix CalculateTransform()
        {
            Vector3 scalingFactor = Vector3.One;
            if (!SceneManager.IsEditor)
            {
                float width = (SceneManager.GameProject.Settings.VirtualScreenWidth != 0 ?
                    SceneManager.GameProject.Settings.VirtualScreenWidth :
                    SceneManager.GameProject.Settings.ScreenWidth);

                float height = (SceneManager.GameProject.Settings.VirtualScreenHeight != 0 ?
                    SceneManager.GameProject.Settings.VirtualScreenHeight :
                    SceneManager.GameProject.Settings.ScreenHeight);

                // 确保我们保持真实的比例（用户输入）
                float widthScale = (float)SceneManager.GraphicsDevice.Viewport.Width / width;
                float heightScale = (float)SceneManager.GraphicsDevice.Viewport.Height / height;

                scalingFactor = new Vector3(widthScale, heightScale, 1);
            }

            Vector2 target = Position;
            float aspectRatio = ((float)SceneManager.GraphicsDevice.Viewport.Width) / ((float)SceneManager.GraphicsDevice.Viewport.Height);
            Matrix result =
                Matrix.CreateTranslation(-(float)Math.Round(target.X, 1), -(float)Math.Round(target.Y), 0.0f) *
                Matrix.CreateRotationZ(rotation) *
                Matrix.CreateScale(new Vector3((float)zoom, (float)zoom, 1)) *
                Matrix.CreateScale(scalingFactor) *
                Matrix.CreateTranslation(SceneManager.GraphicsDevice.Viewport.Width / 2,
                    SceneManager.GraphicsDevice.Viewport.Height / 2, 0);

            return result;
        }

        /// <summary>
        /// 计算给定游戏对象的摄像机的变换矩阵
        /// </summary>
        /// <param name="obj">对象</param>
        /// <returns>变换矩阵</returns>
        public Matrix ObjectTransform(GameObject obj)
        {
            return
                Matrix.CreateTranslation(-obj.Transform.Position.X, -obj.Transform.Position.Y, 0.0f) *
                Matrix.CreateRotationZ(obj.Transform.Rotation) *
                Matrix.CreateTranslation(obj.Transform.Position.X, obj.Transform.Position.Y, 0.0f) *
                CalculateTransform()
                ;
        }

        /// <summary>
        /// 转为屏幕坐标
        /// </summary>
        /// <param name="worldPosition">世界坐标</param>
        /// <returns>屏幕坐标</returns>
        public static Vector2 ToScreen(Vector2 worldPosition)
        {
            return Vector2.Transform(worldPosition, SceneManager.ActiveCamera.TransformMatrix);
        }

        /// <summary>
        /// 转为世界坐标
        /// </summary>
        /// <param name="screenPosition">屏幕坐标</param>
        /// <returns>世界坐标</returns>
        public static Vector2 ToWorld(Vector2 screenPosition)
        {
            return Vector2.Transform(screenPosition,
                Matrix.Invert(SceneManager.ActiveCamera.TransformMatrix));
        }

        /// <summary>
        /// 对象转屏幕坐标
        /// </summary>
        /// <param name="gameObject">对象</param>
        /// <param name="worldPosition">世界坐标</param>
        /// <returns></returns>
        public Vector2 ObjectToScreen(GameObject gameObject, Vector2 worldPosition)
        {
            return Vector2.Transform(worldPosition, ObjectTransform(gameObject));
        }

        /// <summary>
        /// 对象转世界坐标
        /// </summary>
        /// <param name="gameObject">对象</param>
        /// <param name="screenPosition">世界坐标</param>
        /// <returns></returns>
        public Vector2 ObjectToWorld(GameObject gameObject, Vector2 screenPosition)
        {
            return Vector2.Transform(screenPosition, Matrix.Invert(ObjectTransform(gameObject)));
        }

        /// <summary>
        /// 转字符串
        /// </summary>
        /// <returns>Camera名称</returns>
        public override string ToString()
        {
            return string.Empty;
        }
    }
}