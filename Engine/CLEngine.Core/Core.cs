﻿using System;
using System.Collections;
using Microsoft.Xna.Framework;

namespace CLEngine.Core
{
    /// <summary>
    /// 核心游戏类
    /// </summary>
    public class Core : Game
    {
        /// <summary>
        /// 游戏更新逻辑
        /// </summary>
        /// <param name="gameTime"></param>
        protected override void Update(GameTime gameTime)
        {
            TimerManager.Update();
            CoroutineManager.Update();
            Time.Update((float)gameTime.ElapsedGameTime.TotalSeconds);

            base.Update(gameTime);
        }

        /// <summary>
        /// 安排一次性或重复计时器，调用传入的Action
        /// </summary>
        /// <param name="timeInSeconds">时间以秒为单位.</param>
        /// <param name="repeats"></param>
        /// <param name="context"></param>
        /// <param name="onTime"></param>
        public static ITimer Schedule(float timeInSeconds, bool repeats, object context, Action<ITimer> onTime)
        {
            return TimerManager.Schedule(timeInSeconds, repeats, context, onTime);
        }

        /// <summary>
        /// 安排一次性或重复计时器，调用传入的Action
        /// </summary>
        /// <param name="timeInSeconds"></param>
        /// <param name="context"></param>
        /// <param name="onTime"></param>
        /// <returns></returns>
        public static ITimer Schedule(float timeInSeconds, object context, Action<ITimer> onTime)
        {
            return TimerManager.Schedule(timeInSeconds, false, context, onTime);
        }

        /// <summary>
        /// 安排一次性或重复计时器，调用传入的Action
        /// </summary>
        /// <param name="timeInSeconds"></param>
        /// <param name="repeats"></param>
        /// <param name="onTime"></param>
        /// <returns></returns>
        public static ITimer Schedule(float timeInSeconds, bool repeats, Action<ITimer> onTime)
        {
            return TimerManager.Schedule(timeInSeconds, repeats, null, onTime);
        }

        /// <summary>
        /// 安排一次性或重复计时器，调用传入的Action
        /// </summary>
        /// <param name="timeInSeconds"></param>
        /// <param name="onTime"></param>
        /// <returns></returns>
        public static ITimer Schedule(float timeInSeconds, Action<ITimer> onTime)
        {
            return TimerManager.Schedule(timeInSeconds, false, null, onTime);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enumerator"></param>
        /// <returns></returns>
        public static ICoroutine StartCoroutine(IEnumerator enumerator)
        {
            return CoroutineManager.StartCoroutine(enumerator);
        }

    }
}