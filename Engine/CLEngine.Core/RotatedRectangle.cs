﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace CLEngine.Core
{
    public class RotatedRectangle
    {
        private Rectangle collisionRectangle;

        public Rectangle CollisionRectangle
        {
            get
            {
                return collisionRectangle;
            }
            set
            {
                collisionRectangle = value;
                Origin = new Vector2((int)value.Width / 2, (int)value.Height / 2);
            }
        }

        public float Rotation;
        public Vector2 Origin;

        public RotatedRectangle()
        {
            CollisionRectangle = Rectangle.Empty;
            Rotation = 0;
        }

        public RotatedRectangle(int x, int y, int width, int height, float theInitialRotation = 0)
        {
            CollisionRectangle = new Rectangle(x, y, width, height);
            Rotation = theInitialRotation;
        }

        public RotatedRectangle(Rectangle theRectangle, float theInitialRotation = 0)
        {
            CollisionRectangle = theRectangle;
            Rotation = theInitialRotation;
        }

        /// <summary>
        /// 用于更改旋转矩形的X和Y位置
        /// </summary>
        /// <param name="theXPositionAdjustment"></param>
        /// <param name="theYPositionAdjustment"></param>
        public void ChangePosition(int theXPositionAdjustment, int theYPositionAdjustment)
        {
            collisionRectangle.X += theXPositionAdjustment;
            collisionRectangle.Y += theYPositionAdjustment;
        }

        /// <summary>
        /// 此intersects方法可用于检查标准XNA框架Rectangle
        /// object并查看它是否与Rotated Rectangle对象发生碰撞
        /// </summary>
        /// <param name="theRectangle"></param>
        /// <returns></returns>
        public bool Intersects(Rectangle theRectangle)
        {
            return Intersects(new RotatedRectangle(theRectangle, 0.0f));
        }

        /// <summary>
        /// 检查两个旋转矩形是否发生碰撞
        /// </summary>
        /// <param name="theRectangle"></param>
        /// <returns></returns>
        public bool Intersects(RotatedRectangle theRectangle)
        {
            //计算我们将用于确定是否发生碰撞的轴
            //由于对象是矩形，我们只需要生成4个轴（2个用于
            //每个矩形）因为我们知道矩形上的其他2是平行的
            List<Vector2> aRectangleAxis = new List<Vector2>();
            aRectangleAxis.Add(UpperRightCorner() - UpperLeftCorner());
            aRectangleAxis.Add(UpperRightCorner() - LowerRightCorner());
            aRectangleAxis.Add(theRectangle.UpperLeftCorner() - theRectangle.LowerLeftCorner());
            aRectangleAxis.Add(theRectangle.UpperLeftCorner() - theRectangle.UpperRightCorner());

            //循环浏览我们需要检查的所有Axis。 如果没有发生碰撞
            //在所有轴上，然后没有发生碰撞。 我们可以退出
            //立即通知调用函数没有检测到冲突。 如果
            //碰撞发生在所有轴上，然后发生碰撞
            //在旋转的矩形之间。 我们通过分离轴定理知道这是真的
            foreach (Vector2 aAxis in aRectangleAxis)
            {
                if (!IsAxisCollision(theRectangle, aAxis))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 确定是否在其中一个轴上发生了碰撞
        ///平行于Rectangle的平面
        /// </summary>
        /// <param name="theRectangle"></param>
        /// <param name="aAxis"></param>
        /// <returns></returns>
        private bool IsAxisCollision(RotatedRectangle theRectangle, Vector2 aAxis)
        {
            //投影Rectangle的角，我们正在检查Axis和
            //获取该项目的标量值，然后我们可以将其用于比较
            List<int> aRectangleAScalars = new List<int>();
            aRectangleAScalars.Add(GenerateScalar(theRectangle.UpperLeftCorner(), aAxis));
            aRectangleAScalars.Add(GenerateScalar(theRectangle.UpperRightCorner(), aAxis));
            aRectangleAScalars.Add(GenerateScalar(theRectangle.LowerLeftCorner(), aAxis));
            aRectangleAScalars.Add(GenerateScalar(theRectangle.LowerRightCorner(), aAxis));

            //将当前Rectangle的角投影到Axis和
            //获取该项目的标量值，然后我们可以将其用于比较
            List<int> aRectangleBScalars = new List<int>();
            aRectangleBScalars.Add(GenerateScalar(UpperLeftCorner(), aAxis));
            aRectangleBScalars.Add(GenerateScalar(UpperRightCorner(), aAxis));
            aRectangleBScalars.Add(GenerateScalar(LowerLeftCorner(), aAxis));
            aRectangleBScalars.Add(GenerateScalar(LowerRightCorner(), aAxis));

            //获取每个矩形的最大和最小标量值
            int aRectangleAMinimum = aRectangleAScalars.Min();
            int aRectangleAMaximum = aRectangleAScalars.Max();
            int aRectangleBMinimum = aRectangleBScalars.Min();
            int aRectangleBMaximum = aRectangleBScalars.Max();

            //如果我们在矩形之间有重叠（即B的最小值小于A的最大值）
            //然后我们检测到这个轴上的矩形之间的碰撞
            if (aRectangleBMinimum <= aRectangleAMaximum && aRectangleBMaximum >= aRectangleAMaximum)
            {
                return true;
            }
            else if (aRectangleAMinimum <= aRectangleBMaximum && aRectangleAMaximum >= aRectangleBMaximum)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 生成一个标量值，可用于比较角落的位置
        ///已将矩形投影到特定轴上。
        /// </summary>
        /// <param name="theRectangleCorner"></param>
        /// <param name="theAxis"></param>
        /// <returns></returns>
        private int GenerateScalar(Vector2 theRectangleCorner, Vector2 theAxis)
        {
            //使用矢量投影的公式。 转角进入
            //并将其投影到给定的轴上
            float aNumerator = (theRectangleCorner.X * theAxis.X) + (theRectangleCorner.Y * theAxis.Y);
            float aDenominator = (theAxis.X * theAxis.X) + (theAxis.Y * theAxis.Y);
            float aDivisionResult = aNumerator / aDenominator;
            Vector2 aCornerProjected = new Vector2(aDivisionResult * theAxis.X, aDivisionResult * theAxis.Y);

            //现在我们有了我们的投影向量，计算出该投影的标量
            //可以用来更容易地进行比较
            float aScalar = (theAxis.X * aCornerProjected.X) + (theAxis.Y * aCornerProjected.Y);
            return (int)aScalar;
        }

        /// <summary>
        /// 从给定位置旋转一个点并使用Origin我们进行调整
        ///正在转动
        /// </summary>
        /// <param name="thePoint"></param>
        /// <param name="theOrigin"></param>
        /// <param name="theRotation"></param>
        /// <returns></returns>
        private Vector2 RotatePoint(Vector2 thePoint, Vector2 theOrigin, float theRotation)
        {
            Vector2 aTranslatedPoint = new Vector2();
            aTranslatedPoint.X = (float)(theOrigin.X + (thePoint.X - theOrigin.X) * Math.Cos(theRotation)
                - (thePoint.Y - theOrigin.Y) * Math.Sin(theRotation));
            aTranslatedPoint.Y = (float)(theOrigin.Y + (thePoint.Y - theOrigin.Y) * Math.Cos(theRotation)
                + (thePoint.X - theOrigin.X) * Math.Sin(theRotation));
            return aTranslatedPoint;
        }

        public Vector2 UpperLeftCorner()
        {
            Vector2 aUpperLeft = new Vector2(collisionRectangle.Left, collisionRectangle.Top);
            aUpperLeft = RotatePoint(aUpperLeft, aUpperLeft + Origin, Rotation);
            return aUpperLeft;
        }

        public Vector2 UpperRightCorner()
        {
            Vector2 aUpperRight = new Vector2(collisionRectangle.Right, collisionRectangle.Top);
            aUpperRight = RotatePoint(aUpperRight, aUpperRight + new Vector2(-Origin.X, Origin.Y), Rotation);
            return aUpperRight;
        }

        public Vector2 LowerLeftCorner()
        {
            Vector2 aLowerLeft = new Vector2(collisionRectangle.Left, collisionRectangle.Bottom);
            aLowerLeft = RotatePoint(aLowerLeft, aLowerLeft + new Vector2(Origin.X, -Origin.Y), Rotation);
            return aLowerLeft;
        }

        public Vector2 LowerRightCorner()
        {
            Vector2 aLowerRight = new Vector2(collisionRectangle.Right, collisionRectangle.Bottom);
            aLowerRight = RotatePoint(aLowerRight, aLowerRight + new Vector2(-Origin.X, -Origin.Y), Rotation);
            return aLowerRight;
        }

        public int X
        {
            get { return collisionRectangle.X; }
        }

        public int Y
        {
            get { return collisionRectangle.Y; }
        }

        public int Width
        {
            get { return collisionRectangle.Width; }
        }

        public int Height
        {
            get { return collisionRectangle.Height; }
        }

        public override string ToString()
        {
            return string.Format("{0};{1};{2};{3}", X, Y, Width, Height);
        }
    }
}