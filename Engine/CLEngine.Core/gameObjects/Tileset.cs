﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using CLEngine.Core.design;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CLEngine.Core
{
#if WIN
    [Serializable]
#endif
    [DataContract]
    public class Tile
    {
        /// <summary>
        /// 图块的原矩形
        /// </summary>
        public Rectangle Source;

        public Tile DeepCopy()
        {
            return new Tile()
            {
                Source = new Rectangle()
                {
                    X = this.Source.X,
                    Y = this.Source.Y,
                    Width = this.Source.Width,
                    Height = this.Source.Height
                }
            };
        }
    }

#if WIN
    [Serializable]
#endif
    [DataContract]
    public class Tileset : GameObject
    {
        bool useRenderTarget = false;

        [DataMember]
        private int tileWidth = 32;

        [DataMember]
        private Color color = Color.White;

        [DataMember]
        private int tileHeight = 32;

        [NonSerialized]
        bool needsRefresh = false;

        [DataMember]
        private Tile[,] tiles = new Tile[20, 18];

        [Browsable(false)]
        public Tile[,] Tiles
        {
            get { return tiles; }
            set { tiles = value; needsRefresh = true; }
        }

#if WIN
        [NonSerialized]
#endif
        private Color collisionBoundryColor;

#if WIN
        [NonSerialized]
#endif
        private Texture2D texture;

        [NonSerialized]
        private RenderTarget2D tilesetRender;

        [DataMember]
        private string imagePath = string.Empty;

#if WIN
        [Category("图集属性")]
        [DisplayName("颜色"), Description("填充颜色")]
#endif
        public Color Color
        {
            get { return color; }
            set { color = value; }
        }

#if WIN
        [Category("图集属性")]
        [DisplayName("使用渲染目标"), Description("确定在游戏模式下是否应使用渲染目标")]
#endif
        public bool UseRenderTarget
        {
            get { return useRenderTarget; }
            set { useRenderTarget = value; }
        }



#if WIN
        [EditorAttribute(typeof(ContentBrowserEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("图集属性")]
        [DisplayName("图片名称"), Description("图像的相对路径")]
#endif
        public string ImageName
        {
            get { return imagePath; }
            set
            {
                imagePath = value;
                LoadTexture();
            }
        }

        private void LoadTexture()
        {
            texture = TextureLoader.FromContent(imagePath);
        }

        [NonSerialized]
        private SpriteBatch tilesetBatch;

#if WINRT
        [DataMember]
#endif
        private Color dimColor;

#if WIN
        [Category("图集属性")]
        [DisplayName("图块宽度"), Description("图块大小 (宽度)")]
#endif
        public int TileWidth
        {
            get { return tileWidth; }
            set
            {
                tileWidth = value;

                foreach (Tile tile in tiles)
                {
                    if (tile != null)
                        tile.Source.Width = value;
                }
            }
        }

#if WIN
        [Browsable(false)]
#endif
        public Texture2D Texture
        {
            get { return this.texture; }
            set { this.texture = value; }
        }

        [Category("图集属性")]
        [DisplayName("图块高度"), Description("图块大小 (高度)")]
        public int TileHeight
        {
            get { return tileHeight; }
            set
            {
                tileHeight = value;

                foreach (Tile tile in tiles)
                {
                    // 删除旧偏移量并添加新值
                    if (tile != null)
                        tile.Source.Height = value;
                }
            }
        }

#if WIN
        [Category("图集属性")]
        [DisplayName("宽度"), Description("瓷砖数量（宽度）")]
#endif
        public int Width
        {
            get
            {
                return tiles.GetLength(0);
            }
            set
            {
                if (value % 2 != 0)
                    value -= 1;

                if (value > 200)
                    value = 200;

                if (value > 0)
                    ResizeTileset(value, Height);
            }
        }

#if WIN
        [Category("图集属性")]
        [DisplayName("高度"), Description("瓷砖数量（高度）")]
#endif
        public int Height
        {
            get
            {
                return tiles.GetLength(1);
            }
            set
            {
                if (value % 2 != 0)
                    if (value > Height) value += 1;
                    else value -= 1;

                if (value > 200)
                    value = 200;

                if (value > 0)
                    ResizeTileset(Width, value);
            }
        }

#if WIN
        [Browsable(false)]
#endif
        public Rectangle VisibleTiles
        {
            get
            {
                Vector2 topLeft = Vector2.Transform(Vector2.Zero, Matrix.Invert(SceneManager.ActiveCamera.TransformMatrix));
                Vector2 bottomRight = Vector2.Transform(new Vector2(SceneManager.GraphicsDevice.Viewport.Width, SceneManager.GraphicsDevice.Viewport.Height), Matrix.Invert(SceneManager.ActiveCamera.TransformMatrix));

                // position to base the current tiles matrix
                Vector2 startPos = new Vector2()
                {
                    X = (float)Math.Floor((topLeft.X + ((tileWidth * Width) / 2)) / tileWidth) - tileWidth,
                    Y = (float)Math.Floor((topLeft.Y + ((tileHeight * Height) / 2)) / tileHeight) - tileHeight
                };

                Vector2 endPos = new Vector2()
                {
                    X = (float)Math.Floor((bottomRight.X + ((tileWidth * Width) / 2)) / tileWidth) + tileWidth,
                    Y = (float)Math.Floor((bottomRight.Y + ((tileHeight * Height) / 2)) / tileHeight) + tileHeight
                };

                // boundries verification
                if (startPos.X < 0)
                    startPos.X = 0;

                if (startPos.Y < 0)
                    startPos.Y = 0;

                if (endPos.X > Width)
                    endPos.X = Width;
                else if (endPos.X < 0)
                    endPos.X = 0;

                if (endPos.Y > Height)
                    endPos.Y = Height;
                else if (endPos.Y < 0)
                    endPos.Y = 0;

                Rectangle result = new Rectangle((int)startPos.X, (int)startPos.Y, (int)endPos.X - (int)startPos.X, (int)endPos.Y - (int)startPos.Y);

                return result;
            }
        }

        public override void Initialize()
        {
            base.Initialize();

            if (texture == null && imagePath != null && imagePath.Trim() != "")
            {
                LoadTexture();
            }

            collisionBoundryColor = Color.FromNonPremultiplied(255, 64, 0, 120);
            dimColor = Color.FromNonPremultiplied(255, 255, 255, 150);

            tilesetBatch = new SpriteBatch(SceneManager.GraphicsDevice);

            needsRefresh = true;

        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);

            if (texture != null && Visible)
            {
                if (!SceneManager.IsEditor && useRenderTarget)
                {
                    Rectangle visibleTiles = VisibleTiles;

                    if (visibleTiles != Rectangle.Empty)
                    {
                        if (needsRefresh)
                        {
                            needsRefresh = false;

                            if (tilesetRender != null)
                                tilesetRender.Dispose();

                            tilesetRender = new RenderTarget2D(SceneManager.GraphicsDevice, Width * tileWidth,
                                           Height * tileHeight);

                            SceneManager.GraphicsDevice.SetRenderTarget(tilesetRender);
                            SceneManager.GraphicsDevice.Clear(Color.Transparent);

                            tilesetBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied);

                            for (int x = 0; x < Width; x++)
                            {
                                for (int y = 0; y < Height; y++)
                                {
                                    if (tiles[x, y] != null)
                                    {
                                        //  Console.Info(TileWorldPos(x, y));
                                        //Vector2 worldPos = TileWorldPos(x, y);
                                        Vector2 worldPos = new Vector2() { X = x * tileWidth, Y = y * tileHeight };
                                        Rectangle nsrc = tiles[x, y].Source;

                                        // normal mode (everything has the same focus)
                                        tilesetBatch.Draw(this.texture, worldPos, nsrc, this.color);
                                    }
                                }
                            }

                            tilesetBatch.End();

                            SceneManager.GraphicsDevice.SetRenderTarget(null);
                        }

                        spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, null, null, null, null, SceneManager.ActiveCamera.TransformMatrix);

                        Vector2 drawPosition = TileWorldPos(0, 0);

                        //if (((SceneManager.IsEditor && SceneManager.GameProject.ProjectSettings.HighlightActiveTilesetInEditor) ||
                        //                    !SceneManager.IsEditor && SceneManager.GameProject.ProjectSettings.HighlightActiveTilesetInGame) &&
                        //                    SceneManager.ActiveTileset != null)
                        //{
                        //    if (SceneManager.ActiveTileset == this)
                        //    {
                        //        spriteBatch.Draw(tilesetRender, drawPosition, Color.White);
                        //    }
                        //    else
                        //    {
                        //        spriteBatch.Draw(tilesetRender, drawPosition, dimColor);
                        //    }
                        //}
                        //else
                        //{
                        spriteBatch.Draw(tilesetRender, drawPosition, this.color);
                        //}

                        spriteBatch.End();
                    }
                }
                else
                {
                    Rectangle visibleTiles = VisibleTiles;

                    if (visibleTiles != Rectangle.Empty)
                    {
                        spriteBatch.Begin(SpriteSortMode.Texture, BlendState.NonPremultiplied, SamplerState.PointClamp, null, null, null, SceneManager.ActiveCamera.TransformMatrix);

                        for (int x = visibleTiles.X; x < visibleTiles.X + visibleTiles.Width; x++)
                        {
                            for (int y = visibleTiles.Y; y < visibleTiles.Y + visibleTiles.Height; y++)
                            {
                                if (tiles[x, y] != null)
                                {
                                    //  Console.Info(TileWorldPos(x, y));
                                    Vector2 worldPos = TileWorldPos(x, y);

                                    // use highlight mode?
                                    if (((SceneManager.IsEditor && SceneManager.GameProject.ProjectSettings.HighlightActiveTilesetInEditor) ||
                                        !SceneManager.IsEditor && SceneManager.GameProject.ProjectSettings.HighlightActiveTilesetInGame) &&
                                        SceneManager.ActiveTileset != null)
                                    {
                                        if (SceneManager.ActiveTileset == this)
                                        {
                                            spriteBatch.Draw(this.texture, worldPos, tiles[x, y].Source, this.color);
                                        }
                                        else
                                        {
                                            spriteBatch.Draw(this.texture, worldPos, tiles[x, y].Source, dimColor);
                                        }
                                    }
                                    else
                                    {
                                        // normal mode (everything has the same focus)
                                        spriteBatch.Draw(this.texture, worldPos, tiles[x, y].Source, this.color);
                                    }
                                }
                            }
                        }

                        spriteBatch.End();
                    }
                }
            }
        }

        public Vector2 TileWorldPos(int x, int y)
        {
            return new Vector2()
            {
                X = ((x * tileWidth) - ((float)(Width * tileWidth) / 2.0f)),
                Y = ((y * tileHeight) - ((float)(Height * tileHeight) / 2.0f))
            };
        }

        public Tile[,] RemoveTiles(Rectangle area)
        {
            Rectangle _area;
            if ((_area = WorldAreaToMatrix(area)) == Rectangle.Empty)
                return null;

            //Console.Info("valido {0}", endPos);
            Tile[,] result = new Tile[_area.Width, _area.Height];

            //Console.Info(result.GetLength(0) + ":" + result.GetLength(1));

            for (int x = _area.X; x < _area.X + _area.Width; x++)
            {
                for (int y = _area.Y; y < _area.Y + _area.Height; y++)
                {
                    if (tiles[x, y] != null)
                        result[x - _area.X, y - _area.Y] = tiles[x, y].DeepCopy();

                    tiles[x, y] = null;
                }
            }

            needsRefresh = true;

            return result;
        }

        public void RemoveRow(int pointY)
        {
            Vector2 mPos = TileMatrixPos(0, pointY);

            if (ValidPosition(mPos) && mPos.Y < Height - 1)
            {
                for (int x = 0; x < Width; x++)
                {
                    for (int y = (int)mPos.Y + 1; y < Height; y++)
                    {
                        if (tiles[x, y] != null)
                        {
                            tiles[x, y - 1] = tiles[x, y];
                            tiles[x, y] = null;
                        }
                        else
                        {
                            tiles[x, y - 1] = null;
                        }
                    }
                }
            }

            needsRefresh = true;
        }

        public void RemoveColumn(int pointX)
        {
            Vector2 mPos = TileMatrixPos(pointX, 0);

            if (ValidPosition(mPos) && mPos.X < Width - 1)
            {
                for (int x = (int)mPos.X + 1; x < Width; x++)
                {
                    for (int y = 0; y < Height; y++)
                    {
                        if (tiles[x, y] != null)
                        {
                            tiles[x - 1, y] = tiles[x, y];
                            tiles[x, y] = null;
                        }
                        else
                        {
                            tiles[x - 1, y] = null;
                        }
                    }
                }
            }

            needsRefresh = true;
        }

        public void AddRow(int pointY)
        {
            Vector2 mPos = TileMatrixPos(0, pointY);

            if (ValidPosition(mPos))
            {
                for (int x = 0; x < Width; x++)
                {
                    for (int y = Height - 1; y >= (int)mPos.Y; y--)
                    {
                        if (tiles[x, y] != null)
                        {
                            if (y + 1 < Height)
                            {
                                tiles[x, y + 1] = tiles[x, y];
                            }

                            tiles[x, y] = null;
                        }
                    }
                }
            }

            needsRefresh = true;
        }

        public void AddColumn(int pointX)
        {
            Vector2 mPos = TileMatrixPos(pointX, 0);

            if (ValidPosition(mPos))
            {
                for (int x = Width - 1; x >= (int)mPos.X; x--)
                {
                    for (int y = 0; y < Height; y++)
                    {
                        if (tiles[x, y] != null)
                        {
                            if (x + 1 < Width)
                            {
                                tiles[x + 1, y] = tiles[x, y];
                            }

                            tiles[x, y] = null;
                        }
                    }
                }
            }

            needsRefresh = true;
        }

        public Rectangle WorldAreaToMatrix(Rectangle area)
        {
            Vector2 startPos = TileMatrixPos(area.X, area.Y);

            if (!ValidPosition(startPos))
            {
                if (startPos.X < 0)
                    startPos.X = 0;
                else
                    return Rectangle.Empty; // not a valid position

                if (startPos.Y < 0)
                    startPos.Y = 0;
                else
                    return Rectangle.Empty;
            }
            //Console.Info(startPos);
            Vector2 endPos = TileMatrixPos(area.X + area.Width, area.Y + area.Height);

            if (!ValidPosition(endPos))
            {
                if (endPos.X >= Width)
                    endPos.X = Width;

                if (endPos.Y >= Height)
                    endPos.Y = Height;
            }

            return new Rectangle((int)startPos.X, (int)startPos.Y, (int)endPos.X - (int)startPos.X, (int)endPos.Y - (int)startPos.Y);
        }

        public void PlaceTiles(Rectangle source, Rectangle area)
        {
            int sX, sY, cX = 0, cY = 0;
            sX = source.Width / tileWidth;
            sY = source.Height / tileHeight;

            int ax, ay;
            ax = area.Width / TileWidth;
            ay = area.Height / TileHeight;

            for (int x = 0; x < ax; x++)
            {
                cY = 0;

                for (int y = 0; y < ay; y++)
                {
                    PlaceTile(new Vector2(source.X + (cX * TileWidth), source.Y + (cY * TileHeight)),
                        new Vector2(area.X + (x * TileWidth), area.Y + (y * TileHeight)));

                    cY++;
                    if (cY >= sY)
                        cY = 0;
                }

                cX++;
                if (cX >= sX)
                    cX = 0;
            }
        }

        public void PlaceTiles(Rectangle source, Vector2 worldPosition)
        {
            int cX, cY;
            cX = source.Width / tileWidth;
            cY = source.Height / tileHeight;

            for (int i = 0; i < cX; i++)
            {
                for (int l = 0; l < cY; l++)
                {
                    PlaceTile(new Vector2(source.X + (i * tileWidth), source.Y + (l * tileHeight)),
                        new Vector2(worldPosition.X + (i * tileWidth), worldPosition.Y + (l * tileHeight)));
                }
            }
        }

        public void PlaceTile(Vector2 sourcePosition, Vector2 worldPosition)
        {
            Vector2 matrixPos = TileMatrixPos(worldPosition.X, worldPosition.Y);

            // Valid position?
            if (ValidPosition(matrixPos))
            {
                this.tiles[(int)matrixPos.X, (int)matrixPos.Y] = new Tile()
                {
                    Source = new Rectangle((int)sourcePosition.X, (int)sourcePosition.Y, tileWidth, tileHeight)
                };
            }

            needsRefresh = true;
        }

        public Vector2 TileMatrixPos(float x, float y)
        {
            return new Vector2()
            {
                X = (float)Math.Floor((x + ((tileWidth * Width) / 2)) / tileWidth),
                Y = (float)Math.Floor((y + ((tileHeight * Height) / 2)) / tileHeight)
            };
        }

        public void ResizeTileset(int newWidth, int newHeight)
        {
            Tile[,] newTileset = new Tile[newWidth, newHeight];

            // smart update:
            for (int x = 0; x < newWidth; x++)
            {
                for (int y = 0; y < newHeight; y++)
                {
                    Vector2 relativePosition;

                    relativePosition.X = (newWidth < Width) ?
                        (x + (Width / 2 - newWidth / 2)) : (x - (newWidth / 2 - Width / 2));

                    relativePosition.Y = (newHeight < Height) ?
                        (y + (Height / 2 - newHeight / 2)) : (y - (newHeight / 2 - Height / 2));

                    if (ValidPosition(relativePosition))
                    {
                        newTileset[x, y] = tiles[(int)relativePosition.X, (int)relativePosition.Y];
                    }
                }
            }

            needsRefresh = true;

            this.tiles = newTileset;
        }

        public bool ValidPosition(Vector2 position)
        {
            return (position.X >= 0 && position.X < tiles.GetLength(0) && position.Y >= 0 && position.Y < tiles.GetLength(1));
        }

        public Tile[,] DeepCopy()
        {
            Tile[,] result = new Tile[tiles.GetLength(0), tiles.GetLength(1)];

            for (int x = 0; x < tiles.GetLength(0); x++)
            {
                for (int y = 0; y < tiles.GetLength(1); y++)
                {
                    if (tiles[x, y] != null)
                        result[x, y] = tiles[x, y].DeepCopy();
                }
            }

            return result;
        }

        public void ShiftDown(int amount)
        {
            if (amount == 0) return;

            for (int x = 0; x < Width; x++)
            {
                for (int y = Height - amount - 1; y >= 0; y--)
                {
                    if (tiles[x, y] != null)
                    {
                        tiles[x, y + amount] = tiles[x, y];
                        tiles[x, y] = null;
                    }
                    else
                    {
                        tiles[x, y + amount] = null;
                    }
                }
            }

            needsRefresh = true;
        }

        public void ShiftRight(int amount)
        {
            if (amount == 0) return;

            for (int x = Width - amount - 1; x >= 0; x--)
            {
                for (int y = 0; y < Height; y++)
                {
                    if (tiles[x, y] != null)
                    {
                        tiles[x + amount, y] = tiles[x, y];
                        tiles[x, y] = null;
                    }
                    else
                    {
                        tiles[x + amount, y] = null;
                    }
                }
            }

            needsRefresh = true;
        }

        public void ShiftUp(int amount)
        {
            if (amount == 0) return;

            for (int x = 0; x < Width; x++)
            {
                for (int y = amount; y < Height; y++)
                {
                    if (tiles[x, y] != null)
                    {
                        tiles[x, y - amount] = tiles[x, y];
                        tiles[x, y] = null;
                    }
                    else
                    {
                        tiles[x, y - amount] = null;
                    }
                }
            }

            needsRefresh = true;
        }

        public void ShiftLeft(int amount)
        {
            if (amount == 0) return;

            for (int x = amount; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    if (tiles[x, y] != null)
                    {
                        tiles[x - amount, y] = tiles[x, y];
                        tiles[x, y] = null;
                    }
                    else
                    {
                        tiles[x - amount, y] = null;
                    }
                }
            }

            needsRefresh = true;
        }
    }
}