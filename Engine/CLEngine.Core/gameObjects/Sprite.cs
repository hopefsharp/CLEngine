﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using CLEngine.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CLEngine.Core
{
    /// <summary>
    /// 图片对象
    /// </summary>
#if WIN
    [Serializable]
#endif
    [DataContract(IsReference = true)]
    [KnownType(typeof(Sprite))]
    public class Sprite : GameObject
    {
        /// <summary>
        /// 显示模式
        /// </summary>
        public enum DisplayModes { None, Tile, PositionTile, Fill }

        [DataMember]
        private string imageName;

        [DataMember]
        private Vector2 origin;

        [DataMember] private Origins origins = Origins.Center;

        [DataMember] private DisplayModes displayMode = DisplayModes.None;

        [DataMember] private bool useCustomOrigin = false;

        [DataMember]
        private Color color = Color.White;

#if WIN
        [NonSerialized]
#endif
        private Texture2D texture;

        /// <summary>
        /// 图像的相对路径
        /// </summary>
#if WIN
        //[EditorAttribute(typeof(ContentBrowserEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("精灵属性")]
        [DisplayName("图片路径"), Description("图像的相对路径")]
#endif
        public string ImageName
        {
            get { return imageName; }
            set
            {
                imageName = value;
                LoadTexture();
            }
        }

        /// <summary>
        /// 确定如何显示此精灵
        /// </summary>
#if WIN
        [Category("精灵属性")]
        [DisplayName("显示模式"), Description("确定如何显示此精灵")]
#endif
        public DisplayModes DisplayMode
        {
            get { return displayMode; }
            set { displayMode = value; }
        }

#if WIN
        [NonSerialized]
#endif
        private BlendState blendState;

        [DataMember]
        private BlendModes blendMode = BlendModes.NonPremultiplied;

        [DataMember]
        private Rectangle sourceRectangle = Rectangle.Empty;

        [DataMember]
        private SpriteEffects spriteEffect = SpriteEffects.None;

        /// <summary>
        /// 原点
        /// </summary>
#if WIN
        [Category("精灵属性")]
        [DisplayName("锚点"), Description("中心点")]
#endif
        public Vector2 Origin
        {
            get { return origin; }
            set { origin = value; }
        }

        /// <summary>
        /// 原点类型
        /// </summary>
#if WIN
        [Category("精灵属性")]
        [DisplayName("锚点类型"), Description("锚点类型")]
#endif
        public Origins Origins
        {
            get { return origins; }
            set { origins = value; }
        }

        /// <summary>
        /// 是否使用自定义锚点
        /// </summary>
#if WIN
        [Category("精灵属性")]
        [DisplayName("使用自定义锚点"), Description("使用自定义锚点")]
#endif
        public bool UseCustomOrigin
        {
            get { return useCustomOrigin; }
            set { useCustomOrigin = value; }
        }

        /// <summary>
        /// 填充颜色
        /// </summary>
#if WIN
        [Category("精灵属性")]
        [DisplayName("颜色"), Description("填充颜色")]
#endif
        public Color Color
        {
            get { return color; }
            set { color = value; }
        }

        /// <summary>
        /// 图片
        /// </summary>
#if WIN
        [Browsable(false)]
#endif
        public Texture2D Texture
        {
            get { return texture; }
            set { texture = value; }
        }

        /// <summary>
        /// 精灵效果
        /// </summary>
#if WIN
        [Category("精灵属性")]
        [DisplayName("精灵效果"), Description("精灵效果")]
#endif
        public SpriteEffects SpriteEffect
        {
            get { return spriteEffect; }
            set { spriteEffect = value; }
        }

        /// <summary>
        /// 混合模式
        /// </summary>
#if WIN
        [Category("精灵属性")]
        [DisplayName("混合模式"), Description("混合模式")]
#endif
        public BlendModes BlendMode
        {
            get { return blendMode; }
            set { blendMode = value; this.LoadState(); }
        }

        /// <summary>
        /// 要显示的图像区域
        /// </summary>
#if WIN
        [Category("精灵属性")]
        [DisplayName("源矩形"), Description("要显示的图像区域")]
#endif
        public Rectangle SourceRectangle
        {
            get { return sourceRectangle; }
            set { sourceRectangle = value; }
        }

        public override void Initialize()
        {
            if (texture == null && imageName != null && imageName.Trim() != "")
            {
                LoadTexture();
            }

            LoadState();

            base.Initialize();
        }

        private void LoadState()
        {
            switch (this.blendMode)
            {
                case BlendModes.NonPremultiplied:
                    this.blendState = BlendState.NonPremultiplied;
                    break;
                case BlendModes.AlphaBlend:
                    this.blendState = BlendState.AlphaBlend;
                    break;
                case BlendModes.Additive:
                    this.blendState = BlendState.Additive;
                    break;
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);

            if (texture != null && Visible) // && (MeasureDimension().Intersects(SceneManager.ActiveCamera.BoundingBox) || displayMode == DisplayModes.Fill || displayMode == DisplayModes.Tile || displayMode == DisplayModes.PositionTile))
            {
                if (useCustomOrigin)
                {
                    if (Origins == Origins.TopLeft)
                        Origin = Vector2.Zero;
                    else if(Origins == Origins.Top)
                        Origin = new Vector2(texture.Width / 2, 0);
                    else if (Origins == Origins.TopRight)
                        Origin = new Vector2(texture.Width, 0);
                    else if (Origins == Origins.Right)
                        Origin = new Vector2(texture.Width, texture.Height / 2);
                    else if (Origins == Origins.BottomRight)
                        Origin = new Vector2(texture.Width, texture.Height);
                    else if (Origins == Origins.Bottom)
                        Origin = new Vector2(texture.Width / 2, texture.Height);
                    else if (Origins == Origins.BottomLeft)
                        Origin = new Vector2(0, texture.Height);
                    else if(Origins == Origins.Left)
                        Origin = new Vector2(0, texture.Height / 2);
                    else if (Origins == Origins.Center)
                        Origin = new Vector2(texture.Width / 2, texture.Height/ 2);
                }

                if (displayMode == DisplayModes.Tile && MathExtension.IsPowerOfTwo(new Vector2(texture.Width, texture.Height)))
                {
                    //Vector2 initialPosition = Vector2.Transform(Vector2.Zero, Matrix.Invert(SceneManager.ActiveCamera.TransformMatrix));
                    //Vector2 endPos = Vector2.Transform(new Vector2(SceneManager.GraphicsDevice.Viewport.Width, SceneManager.GraphicsDevice.Viewport.Height), Matrix.Invert(SceneManager.ActiveCamera.TransformMatrix));
                    //int sizeWidth = (int)(endPos.X - initialPosition.X);
                    //int sizeHeight = (int)(endPos.Y - initialPosition.Y);

                    spriteBatch.Begin(SpriteSortMode.Deferred, this.blendState, SamplerState.LinearWrap, null, null, null, SceneManager.ActiveCamera.TransformMatrix);

                    //spriteBatch.Draw(texture, new Vector2(initialPosition.X, initialPosition.Y), new Rectangle((int)SceneManager.ActiveCamera.Position.X,
                    //    (int)SceneManager.ActiveCamera.Position.Y, sizeWidth, sizeHeight), Color, 0, Vector2.Zero, 1.0f,
                    //    SpriteEffects.None, 1);

                    float tBorder = Vector2.Transform(new Vector2(0, 0), Matrix.Invert(SceneManager.ActiveCamera.TransformMatrix)).Y;
                    float bBorder = Vector2.Transform(new Vector2(0, SceneManager.GraphicsDevice.Viewport.Height), Matrix.Invert(SceneManager.ActiveCamera.TransformMatrix)).Y;
                    float lBorder = Vector2.Transform(new Vector2(0, 0), Matrix.Invert(SceneManager.ActiveCamera.TransformMatrix)).X;
                    float rBorder = Vector2.Transform(new Vector2(SceneManager.GraphicsDevice.Viewport.Width, 0), Matrix.Invert(SceneManager.ActiveCamera.TransformMatrix)).X;

                    Rectangle dispRect = new Rectangle()
                    {
                        X = (int)lBorder,
                        Y = (int)tBorder,
                        Width = (int)rBorder - (int)lBorder,
                        Height = (int)bBorder - (int)tBorder
                    };

                    //spriteBatch.Draw(texture, dispRect, null, Color, 0, Vector2.Zero, spriteEffect, 1);

                    spriteBatch.Draw(texture, new Vector2(lBorder, tBorder), new Rectangle((int)SceneManager.ActiveCamera.Position.X,
                             (int)SceneManager.ActiveCamera.Position.Y, dispRect.Width, dispRect.Height), Color, 0, Vector2.Zero, Transform.Scale, spriteEffect, 1);
                }
                else if (displayMode == DisplayModes.PositionTile)
                {
                    //Vector2 initialPosition = Vector2.Transform(Vector2.Zero, Matrix.Invert(SceneManager.ActiveCamera.TransformMatrix));
                    //Vector2 endPos = Vector2.Transform(new Vector2(SceneManager.GraphicsDevice.Viewport.Width, SceneManager.GraphicsDevice.Viewport.Height), Matrix.Invert(SceneManager.ActiveCamera.TransformMatrix));
                    //int sizeWidth = (int)(endPos.X - initialPosition.X);
                    //int sizeHeight = (int)(endPos.Y - initialPosition.Y);

                    spriteBatch.Begin(SpriteSortMode.Deferred, this.blendState, SamplerState.LinearWrap, null, null, null, SceneManager.ActiveCamera.TransformMatrix);

                    //spriteBatch.Draw(texture, new Vector2(initialPosition.X, initialPosition.Y), new Rectangle((int)Transform.Position.X,
                    //    (int)Transform.Position.Y, sizeWidth, sizeHeight), Color, 0, Vector2.Zero, 1.0f,
                    //    SpriteEffects.None, 1);

                    spriteBatch.Draw(texture, new Vector2((int)SceneManager.ActiveCamera.Position.X - SceneManager.GraphicsDevice.Viewport.Width, (int)SceneManager.ActiveCamera.Position.Y - SceneManager.GraphicsDevice.Viewport.Height), new Rectangle((int)Transform.Position.X,
                        (int)Transform.Position.Y, SceneManager.GraphicsDevice.Viewport.Width * 2, SceneManager.GraphicsDevice.Viewport.Height * 2), Color, 0, Vector2.Zero, Transform.Scale,
                        spriteEffect, 1);
                }
                else if (displayMode == DisplayModes.Fill)
                {
                    Rectangle fill = new Rectangle(
                         (int)SceneManager.ActiveScene.Camera.Position.X,
                         (int)SceneManager.ActiveScene.Camera.Position.Y - 1,
                         (int)SceneManager.GameProject.Settings.ScreenWidth,
                         (int)SceneManager.GameProject.Settings.ScreenHeight);

                    spriteBatch.Begin(SpriteSortMode.Deferred, this.blendState, SamplerState.LinearClamp, null, null, null, SceneManager.ActiveCamera.TransformMatrix);

                    if (sourceRectangle == Rectangle.Empty)
                        spriteBatch.Draw(texture, fill, null, Color, 0, Origin, spriteEffect, 0);
                    else
                        spriteBatch.Draw(texture, fill, sourceRectangle, Color, 0, Origin, spriteEffect, 0);
                }
                else
                {
                    spriteBatch.Begin(SpriteSortMode.Deferred, this.blendState, SamplerState.LinearClamp, null, RasterizerState.CullNone, null, SceneManager.ActiveCamera.TransformMatrix);

                    //Console.Info("rr: " + Transform.Rotation);

                    if (sourceRectangle == Rectangle.Empty)
                        spriteBatch.Draw(texture, Transform.Position, null, color, Transform.Rotation, Origin, Transform.Scale, spriteEffect, 1);
                    else
                        spriteBatch.Draw(texture, Transform.Position, sourceRectangle, color, Transform.Rotation, Origin, Transform.Scale, spriteEffect, 1);
                }

                spriteBatch.End();
            }
        }

        private void LoadTexture()
        {
            texture = TextureLoader.FromContent(imageName);
        }
    }
}