﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using CLEngine.Core.components;
using CLEngine.Core.design;
using CLEngine.Core.DragonBones;
using DragonBones;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using BlendState = Microsoft.Xna.Framework.Graphics.BlendState;
using Rectangle = Microsoft.Xna.Framework.Rectangle;

namespace CLEngine.Core
{
#if WIN
    [Serializable]
#endif
    [DataContract]
    public class AnimationObject : GameObject
    {
        [DataMember]
        private string skeFilePath = string.Empty;

        [DataMember]
        private string texFilePath = string.Empty;

        [DataMember]
        private string texImageFilePath = string.Empty;

        [DataMember] private string defaultAction = string.Empty;

        [DataMember] private bool loop;

        [DataMember] private bool pingpong;

        [DataMember] private Vector2 origin;

        [DataMember] private SpriteEffects effects;

        [DataMember] private float frameDuration = 0.2f;

        [DataMember] private bool isReversed;

        [DataMember] private SpriteSortMode spriteSortMode = SpriteSortMode.Deferred;

        [DataMember] private BlendModes blendMode = BlendModes.NonPremultiplied;

        [DataMember] private SamplerModes samplerMode = SamplerModes.PointClamp;
        [DataMember] private Color _color = Color.White;

#if WIN
        [NonSerialized]
#endif
        private BlendState blendState;

#if WIN
        [NonSerialized]
#endif
        private SamplerState samplerState;

#if WIN
        [Editor(typeof(ContentBrowserEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("动画属性")]
        [DisplayName("ske文件路径"), Description("ske文件的相对路径")]
#endif
        public string SkeFilePath
        {
            get { return skeFilePath;}
            set
            {
                skeFilePath = value;
                LoadData();
            }
        }

#if WIN
        [Editor(typeof(ContentBrowserEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("动画属性")]
        [DisplayName("tex JSON文件路径"), Description("tex JSON文件的相对路径")]
#endif
        public string TexFilePath
        {
            get { return texFilePath; }
            set
            {
                texFilePath = value;
                LoadData();
            }
        }

#if WIN
        [Category("动画属性")]
        [DisplayName("动画锚点"), Description("动画的锚点")]
#endif
        public Vector2 Origin
        {
            get { return origin; }
            set { origin = value; }
        }

#if WIN
        [Editor(typeof(ContentBrowserEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("动画属性")]
        [DisplayName("tex PNG文件路径"), Description("tex PNG文件的相对路径")]
#endif
        public string TexImageFilePath
        {
            get { return texImageFilePath; }
            set
            {
                texImageFilePath = value;
                LoadData();
            }
        }

#if WIN
        [Category("动画属性")]
        [DisplayName("默认动作"), Description("动画的默认动作")]
#endif
        public string DefaultAction
        {
            get { return defaultAction; }
            set
            {
                defaultAction = value;
                LoadData();
            }
        }

#if WIN
        [Category("动画属性")]
        [DisplayName("循环"), Description("动画的是否循环播放")]
#endif
        public bool Loop
        {
            get { return loop; }
            set { loop = value; }
        }

#if WIN
        [Category("动画属性")]
        [DisplayName("倒序循环"), Description("动画的是否尾播放至头")]
#endif
        public bool PingPong
        {
            get { return pingpong; }
            set { pingpong = value; }
        }

#if WIN
        [Category("动画属性")]
        [DisplayName("正在播放"), Description("动画是否正在播放")]
#endif
        public bool IsPlaying { get; private set; }

#if WIN
        [Category("动画属性")]
        [DisplayName("翻转模式"), Description("动画的翻转模式")]
#endif
        public SpriteEffects SpriteEffect
        {
            get { return effects;}
            set { effects = value; }
        }

#if WIN
        [Category("动画属性")]
        [DisplayName("帧时间"), Description("帧持续时间")]
#endif
        public float FrameDuration {
            get { return frameDuration; }
            set
            {
                if (value > 0f)
                    frameDuration = value;
            }
        }
#if WIN
        [Category("动画属性")]
        [DisplayName("排序模式"), Description("动画的排序模式")]
#endif
        public SpriteSortMode SpriteSortMode
        {
            get { return spriteSortMode; }
            set { spriteSortMode = value; }
        }

#if WIN
        [Category("动画属性")]
        [DisplayName("混合状态"), Description("动画的混合状态")]
#endif
        public BlendModes BlendState
        {
            get { return blendMode; }
            set
            {
                blendMode = value;
                LoadState();
            }
        }

#if WIN
        [Category("动画属性")]
        [DisplayName("采样器状态"), Description("动画的采样器状态")]
#endif
        public SamplerModes SamplerState
        {
            get { return samplerMode; }
            set
            {
                samplerMode = value;
                LoadSamplerState();
            }
        }

#if WIN
       [Category("动画属性")]
       [DisplayName("颜色")]
#endif
        public Color Color
        {
            get { return _color; }
            set { _color = value; }
        }

#if WIN
        [NonSerialized]
#endif
        private Dictionary<string, ArmatureData> _armatureData = new Dictionary<string, ArmatureData>();

#if WIN
        [NonSerialized]
#endif
        private Dictionary<string, TextureData> _textures = new Dictionary<string, TextureData>();

#if WIN
       [NonSerialized]
#endif
       private List<DisplayData> _sheetSlots = new List<DisplayData>();

#if WIN
        [NonSerialized]
#endif
        private Texture2D _texture;

#if WIN
        [NonSerialized]
#endif
        private Rectangle sourceRectangle;

#if WIN
        [NonSerialized]
#endif
        private ArmatureData stageData;

#if WIN
        [NonSerialized]
#endif
        private int frameCount;

#if WIN
        [NonSerialized]
#endif
        private float _currentTime;

#if WIN
        [Category("动画属性")]
        [DisplayName("播放完成"), Description("动画是否播放完毕")]
#endif
        public bool IsComplete => _currentTime >= AnimationDuration;

#if WIN
        [Category("动画属性")]
        [DisplayName("动画持续时间"), Description("动画持续时间")]
#endif
        public float AnimationDuration =>
            PingPong ? (frameCount * 2 - 2) * frameDuration : frameCount * frameDuration;

#if WIN
        [NonSerialized]
#endif
        [Browsable(false)]
        public int CurrentFrameIndex;

#if WIN
        [Category("动画属性")]
        [DisplayName("是否逆转"), Description("动画是否逆转播放")]
#endif
        public bool IsReversed
        {
            get { return isReversed; }
            set { isReversed = value; }
        }

#if WIN
        [NonSerialized]
#endif
        private CTextureAtlasData _atlasData;

#if WIN
        [NonSerialized]
#endif
        private DragonBonesData _dragonBonesData;

        public override void Initialize()
        {
            LoadData();
            LoadState();
            LoadSamplerState();

            if (frameDuration == 0f)
                frameDuration = 0.2f;

            base.Initialize();
        }

        [Browsable(false)]
        public EventHandler OnCompeleted { get; set; }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (Disabled)
                return;

            if (_sheetSlots == null || _texture == null || !IsPlaying)
                return;

            _currentTime += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (IsComplete)
            {
                OnCompeleted?.Invoke(this, EventArgs.Empty);

                if (Loop)
                    _currentTime -= AnimationDuration;
            }

            if (frameCount == 1)
            {
                CurrentFrameIndex = 0;
                return;
            }

            var frameIndex = (int) (_currentTime / frameDuration);

            if (PingPong)
            {
                frameIndex = frameIndex % (frameCount * 2 - 2);

                if (frameIndex >= frameCount)
                    frameIndex = frameCount - 2 - (frameIndex - frameCount);
            }

            if (Loop)
            {
                if (IsReversed)
                {
                    frameIndex = frameIndex % frameCount;
                    frameIndex = frameCount - frameIndex - 1;
                }
                else
                {
                    frameIndex = frameIndex % frameCount;
                }
            }
            else
                frameIndex = IsReversed
                    ? Math.Max(frameCount - frameIndex - 1, 0)
                    : Math.Min(frameCount - 1, frameIndex);

            CurrentFrameIndex = frameIndex;

            var region = _textures[_sheetSlots[CurrentFrameIndex].path].region;
            sourceRectangle = new Rectangle((int)region.x, (int)region.y, (int)region.width, (int)region.height);
        }

        private void LoadState()
        {
            switch (this.blendMode)
            {
                case BlendModes.NonPremultiplied:
                    this.blendState = Microsoft.Xna.Framework.Graphics.BlendState.NonPremultiplied;
                    break;
                case BlendModes.AlphaBlend:
                    this.blendState = Microsoft.Xna.Framework.Graphics.BlendState.AlphaBlend;
                    break;
                case BlendModes.Additive:
                    this.blendState = Microsoft.Xna.Framework.Graphics.BlendState.Additive;
                    break;
            }
        }

        private void LoadSamplerState()
        {
            switch (this.samplerMode)
            {
                case SamplerModes.AnisotropicClamp:
                    this.samplerState = Microsoft.Xna.Framework.Graphics.SamplerState.AnisotropicClamp;
                    break;
                case SamplerModes.AnisotropicWrap:
                    this.samplerState = Microsoft.Xna.Framework.Graphics.SamplerState.AnisotropicWrap;
                    break;
                case SamplerModes.LinearClamp:
                    this.samplerState = Microsoft.Xna.Framework.Graphics.SamplerState.LinearClamp;
                    break;
                case SamplerModes.LinearWrap:
                    this.samplerState = Microsoft.Xna.Framework.Graphics.SamplerState.LinearWrap;
                    break;
                case SamplerModes.PointWrap:
                    this.samplerState = Microsoft.Xna.Framework.Graphics.SamplerState.PointWrap;
                    break;
                case SamplerModes.PointClamp:
                    this.samplerState = Microsoft.Xna.Framework.Graphics.SamplerState.PointClamp;
                    break;
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);
            
            if (Disabled || !Visible)
                return;

            if (_texture == null || _sheetSlots == null || _sheetSlots?.Count == 0)
                return;

            if (_sheetSlots.Count <= CurrentFrameIndex)
                CurrentFrameIndex = 0;

            var frame = _textures[_sheetSlots[CurrentFrameIndex].path].frame ?? new global::DragonBones.Rectangle
            {
                height = sourceRectangle.Height,
                width = sourceRectangle.Width
            };
            var slot = (ImageDisplayData) _sheetSlots[CurrentFrameIndex];
            spriteBatch.Begin(spriteSortMode, blendState, samplerState, null, null,
                null, SceneManager.ActiveCamera.TransformMatrix);
            if (SpriteEffect == SpriteEffects.None)
                spriteBatch.Draw(_texture, Transform.Position - new Vector2(frame.x, frame.y), sourceRectangle,
                    _color,
                    Transform.rotation,
                    Origin + new Vector2(slot.pivot.x, slot.pivot.y) * new Vector2(frame.width, frame.height),
                    Transform.scale, SpriteEffect, 1);
            else if (SpriteEffect == SpriteEffects.FlipHorizontally)
                spriteBatch.Draw(_texture,
                    new Vector2(Transform.Position.X - (sourceRectangle.Width - (frame.x + frame.width)),
                        Transform.Position.Y - frame.y), sourceRectangle, _color,
                    Transform.rotation,
                    Origin + new Vector2(slot.pivot.x, slot.pivot.y) * new Vector2(frame.width, frame.height),
                    Transform.scale, SpriteEffect, 1);
            else if (SpriteEffect == SpriteEffects.FlipVertically)
                spriteBatch.Draw(_texture,
                    new Vector2(Transform.Position.X - frame.x,
                        Transform.Position.Y - (sourceRectangle.Height - (frame.y + frame.height))),
                    sourceRectangle,
                    _color,
                    Transform.rotation,
                    Origin + new Vector2(slot.pivot.x, slot.pivot.y) * new Vector2(frame.width, frame.height),
                    Transform.scale, SpriteEffect, 1);
            spriteBatch.End();
        }
        
        private void Play()
        {
            IsPlaying = true;
        }

        private void LoadTextures(CTextureAtlasData data)
        {
            CheckData();
            _textures.Clear();

            foreach (var dataTexture in data.textures)
            {
                _textures.Add(dataTexture.Key, dataTexture.Value);
            }
        }

        private void LoadArmatureData(DragonBonesData data)
        {
            CheckData();

            _armatureData.Clear();

            foreach (var dataArmature in data.armatures)
            {
                if (string.IsNullOrEmpty(defaultAction))
                {
                    defaultAction = dataArmature.Value.defaultActions[0].name;
                }

                _armatureData.Add(dataArmature.Key, dataArmature.Value);
            }

            if (stageData == null)
                stageData = data.stage;

            LoadFrameData(data);
        }

        private void LoadFrameData(DragonBonesData data)
        {
            if (string.IsNullOrEmpty(defaultAction))
            {
                var sheetSlot = stageData.skins["default"].displays["sheetSlot"];
                _sheetSlots = sheetSlot;
                frameCount = sheetSlot.Count;
            }
            else
            {
                foreach (var dataArmature in data.armatures)
                {
                    if (dataArmature.Key == defaultAction)
                    {
                        var sheetSlot = dataArmature.Value.skins["default"].displays["sheetSlot"];
                        _sheetSlots = sheetSlot;
                        frameCount = sheetSlot.Count;

                        return;
                    }
                }
            }

        }

        private void CheckData()
        {
            if (_armatureData == null)
                _armatureData = new Dictionary<string, ArmatureData>();

            if (_textures == null)
                _textures = new Dictionary<string, TextureData>();

            if (_sheetSlots == null)
                _sheetSlots = new List<DisplayData>();
        }

        private void LoadData()
        {
            string texImage = Path.Combine(SceneManager.GameProject.ProjectPath, texImageFilePath);
            string skePath = Path.Combine(SceneManager.GameProject.ProjectPath, skeFilePath);
            string texData = Path.Combine(SceneManager.GameProject.ProjectPath, texFilePath);

            if (File.Exists(texImage) && File.Exists(skePath) && File.Exists(texData))
            {
                _texture = TextureLoader.FromFile(texImage);
                _atlasData = CFactory.factory.LoadTextureAtlasData(texData);
                _dragonBonesData = CFactory.factory.LoadDragonBonesData(skePath);

                LoadArmatureData(_dragonBonesData);
                LoadTextures(_atlasData);

                Play();
            }

        }
    }
}