﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using CLEngine.Core.design;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CLEngine.Core
{
#if WIN
    [Serializable]
#endif
    [DataContract]
    public class BMFont : GameObject
    {
        [DataMember]
        private TextAlignModes alignMode = TextAlignModes.Left;
        [DataMember]
        private string text = string.Empty;
        [DataMember]
        private string fntFilePath = string.Empty;
        [DataMember]
        private string textureFilePath = string.Empty;
        [DataMember]
        private Vector2 origin;
        [DataMember]
        private int lineSpacing = 0;
        [DataMember]
        private Color overlayColor = Color.White;

#if WIN
        [Category("字体属性")]
        [DisplayName("对齐"), Description("对齐模式")]
#endif
        public TextAlignModes AlignMode
        {
            get { return alignMode; }
            set { alignMode = value; }
        }

#if WIN
        [Category("字体属性")]
        [DisplayName("文字"), Description("要显示的文本")]
#endif
        public string Text
        {
            get { return text; }
            set { text = value; }
        }

#if WIN
        [NonSerialized]
#endif
        private FontFile fontFile;

#if WIN
        [NonSerialized]
#endif
        private Texture2D fontTexture;

#if WIN
        [NonSerialized]
#endif
        private BitmapFontRenderer fontRenderer;

#if WIN
        [EditorAttribute(typeof(ContentBrowserEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("字体属性")]
        [DisplayName("Fnt文件路径"), Description("相对fnt文件路径")]
#endif
        public string FntFilePath
        {
            get { return fntFilePath; }
            set
            {
                if (System.IO.Path.GetExtension(value).Equals(".fnt"))
                {
                    fntFilePath = value;
                    LoadData();
                }
            }
        }

#if WIN
        [Category("字体属性")]
        [DisplayName("图片文件路径"), Description("相对图片文件路径")]
#endif
        public string TextureFilePath
        {
            get { return textureFilePath; }
            set
            {
                if (System.IO.Path.GetExtension(value).Equals(".png"))
                {
                    textureFilePath = value;
                    LoadData();
                }
            }
        }

#if WIN
        [Category("字体属性")]
        [DisplayName("原点"), Description("相对于对象位置的原点")]
#endif
        public Vector2 Origin
        {
            get { return origin; }
            set { origin = value; }
        }

#if WIN
        [Category("字体属性")]
        [DisplayName("行间距"), Description("行之间的间距")]
#endif
        public int LineSpacing
        {
            get { return lineSpacing; }
            set { lineSpacing = value; }
        }

#if WIN
        [EditorAttribute(typeof(XNAColorEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("字体属性")]
        [DisplayName("叠加颜色"), Description("叠加颜色")]
#endif
        public Color OverlayColor
        {
            get { return overlayColor; }
            set { overlayColor = value; }
        }

        public override void Initialize()
        {
            base.Initialize();

            LoadData();
        }

        private void LoadData()
        {
            string _fntFilePath = System.IO.Path.Combine(SceneManager.GameProject.ProjectPath, fntFilePath);
            string _textureFilePath = System.IO.Path.Combine(SceneManager.GameProject.ProjectPath, textureFilePath);

#if WIN
            if (File.Exists(_fntFilePath) && File.Exists(_textureFilePath))
#elif WINRT
            if (MetroHelper.AppDataFileExists(_fntFilePath) && MetroHelper.AppDataFileExists(_textureFilePath))
#endif
            {
                this.fontFile = FontLoader.Load(_fntFilePath);
                this.fontTexture = TextureLoader.FromFile(_textureFilePath);

                this.fontRenderer = new BitmapFontRenderer(this.fontFile, this.fontTexture);
            }
        }

        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);

            if (this.fontRenderer != null && Visible) // && CollisionModel.CollisionBoundry.Intersects(SceneManager.ActiveCamera.BoundingBox))
            {
                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.LinearWrap, null, null, null, SceneManager.ActiveCamera.TransformMatrix);
                
                Vector2 dest = fontRenderer.MeasureString(text, Transform.Scale.X, this.lineSpacing);
                dest.X = Transform.Position.X - (dest.X - dest.X / 2);
                dest.Y = Transform.Position.Y - (dest.Y - dest.Y / 2);

                this.fontRenderer.DrawText(spriteBatch, (int)dest.X, (int)dest.Y, this.text.ToString(), Transform.Scale.X, this.OverlayColor, this.lineSpacing, this.alignMode);

                spriteBatch.End();
            }
        }
    }
}