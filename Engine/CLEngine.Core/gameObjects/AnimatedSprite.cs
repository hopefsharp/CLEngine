﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using CLEngine.Core.design;
using CLEngine.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CLEngine.Core
{
#if WIN
    [Serializable]
#endif
    [DataContract]
    public enum PlayMode
    {
        Normal, Reverse, YoYo
    }

#if WIN
    [Serializable]
#endif
    [DataContract]
    public class AnimatedSprite : Sprite
    {
        private bool reverse = false;

        [DataMember]
        private string imageName = string.Empty;

        [DataMember]
        private int totalFramesPerRow = 0;

        [DataMember]
        private int totalRows = 0;
        [DataMember]
        private int currentColumn = 0;
        [DataMember]
        private int currentRow = 0;
        [DataMember]
        private SpriteEffects spriteEffect = SpriteEffects.None;
        [DataMember]
        private bool isPlaying = false;
        [DataMember]
        private int totalMillisecondsPassed = 0;
        [DataMember]
        private int delay = 0;
        [DataMember]
        private PlayMode playMode = PlayMode.Normal;
        [DataMember]
        private bool playAllRows = false;
        [DataMember]
        private bool loop = true;
        [DataMember]
        private bool resetOnStart = true;
        [DataMember]
        private BlendModes blendMode = BlendModes.NonPremultiplied;
        [DataMember]
        private Color color = Color.White;

        public event EventHandler AnimationCompleted;
        public event EventHandler RowCompleted;
        public event EventHandler IsAtLastRowFrame;

#if WIN
        [NonSerialized]
#endif
        private BlendState blendState;

#if WIN
        [Editor(typeof(ContentBrowserEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("精灵属性")]
        [DisplayName("图片路径"), Description("图像的相对路径")]
#endif
        public string ImageName
        {
            get { return imageName; }
            set
            {
                imageName = value;
                LoadTexture();
                SetFrameSize();
            }
        }

#if WIN
        [Category("精灵属性")]
        [DisplayName("播放模式"), Description("确定动画精灵播放模式")]
#endif
        public PlayMode PlayMode
        {
            get { return playMode; }
            set
            {
                playMode = value;

                UpdatePlayMode();
            }
        }

#if WIN
        [Browsable(false)]
#endif
        public int FrameWidth
        {
            get;
            set;
        }

#if WIN
        [Category("精灵属性")]
        [DisplayName("总行数"), Description("每个图像中可用的总行数")]
#endif
        public int TotalRows
        {
            get { return totalRows; }
            set { totalRows = value; SetFrameSize(); }
        }

#if WIN
        [Category("精灵属性")]
        [DisplayName("延迟"), Description("每个动画步骤的延迟，以毫秒为单位")]
#endif
        public int Delay
        {
            get { return delay; }
            set { delay = value; }
        }

#if WIN
        [Category("精灵属性")]
        [EditorAttribute(typeof(XNAColorEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DisplayName("颜色"), Description("填充颜色")]
#endif
        public Color Color
        {
            get { return color; }
            set { color = value; }
        }

#if WIN
        [Category("精灵属性")]
        [DisplayName("每行帧数"), Description("每个纹理中可用的总帧数")]
#endif
        public int TotalFramesPerRow
        {
            get { return totalFramesPerRow; }
            set { totalFramesPerRow = value; SetFrameSize(); }
        }

#if WIN
        [Category("精灵属性")]
        [DisplayName("播放所有行"), Description("确定当前行完成时动画是否应继续到下一行")]
#endif
        public bool PlayAllRows
        {
            get { return playAllRows; }
            set { playAllRows = value; }
        }

#if WIN
        [Category("精灵属性")]
        [DisplayName("当前列"), Description("当前行中当前动画的活动列")]
#endif
        public int CurrentColumn
        {
            get { return currentColumn; }
            set { currentColumn = value; }
        }

#if WIN
        [Category("精灵属性")]
        [DisplayName("当前行"), Description("当前动画的活动行")]
#endif
        public int CurrentRow
        {
            get { return currentRow; }
            set { currentRow = value; }
        }

#if WIN
        [Browsable(false)]
#endif
        public int FrameHeight
        {
            get;
            set;
        }

#if WIN
        [Browsable(false)]
#endif
        public Texture2D Texture
        {
            get { return texture; }
            set { texture = value; }
        }

#if WIN
        [Category("精灵属性")]
        [DisplayName("循环"), Description("确定在播放当前行的所有列之后是否应重复动画")]
#endif
        public bool Loop
        {
            get { return loop; }
            set { loop = value; }
        }

#if WIN
        [Category("精灵属性")]
        [DisplayName("正在播放"), Description("确定当前纹理是否正在设置动画")]
#endif
        public bool IsPlaying
        {
            get { return isPlaying; }
            set { isPlaying = value; }
        }

#if WIN
        [Category("精灵属性")]
        [DisplayName("精灵效果"), Description("精灵效果")]
#endif
        public SpriteEffects SpriteEffect
        {
            get { return spriteEffect; }
            set { spriteEffect = value; }
        }

#if WIN
        [Category("精灵属性")]
        [DisplayName("混合模式"), Description("混合模式")]
#endif
        public BlendModes BlendMode
        {
            get { return blendMode; }
            set { blendMode = value; this.LoadState(); }
        }
        

#if WIN
        [NonSerialized]
#endif
        private Texture2D texture;

        private void LoadTexture()
        {
            texture = TextureLoader.FromContent(imageName);
        }

        private void SetFrameSize()
        {
            if (texture == null)
                return;

            if (totalFramesPerRow != 0)
                FrameWidth = texture.Width / totalFramesPerRow;

            if (totalRows != 0)
                FrameHeight = texture.Height / totalRows;

        }

        private void UpdatePlayMode()
        {
            if (playMode == PlayMode.Normal)
            {
                reverse = false;
            }
            else if (playMode == PlayMode.Reverse)
            {
                reverse = true;
            }
        }

        public override void Initialize()
        {
            base.Initialize();

            if (resetOnStart)
            {
                currentRow = 0;
                currentColumn = 0;
            }

            if (texture == null && imageName != null && imageName.Trim() != "")
            {
                LoadTexture();
                SetFrameSize();
            }

            UpdatePlayMode();
            LoadState();
        }

        private void LoadState()
        {
            switch (this.blendMode)
            {
                case BlendModes.NonPremultiplied:
                    this.blendState = BlendState.NonPremultiplied;
                    break;
                case BlendModes.AlphaBlend:
                    this.blendState = BlendState.AlphaBlend;
                    break;
                case BlendModes.Additive:
                    this.blendState = BlendState.Additive;
                    break;
            }
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            // Is the animation playing?
            if (isPlaying)
            {
                totalMillisecondsPassed += gameTime.ElapsedGameTime.Milliseconds;

                // Time to increment a step?
                if (totalMillisecondsPassed >= delay)
                {
                    totalMillisecondsPassed = 0;

                    CurrentColumn += (reverse ? -1 : 1);

                    if (TotalFramesPerRow > 0) // if has at least one frame
                    {
                        if ((!reverse && CurrentColumn == TotalFramesPerRow - 1) || (reverse && CurrentColumn == 0)) // if last frame
                        {
                            if (IsAtLastRowFrame != null)
                            {
                                IsAtLastRowFrame(this, null); // trigger last frame
                            }
                        }

                    }


                    if (currentColumn >= totalFramesPerRow || currentColumn < 0)
                    {
                        if (playMode == PlayMode.YoYo && !playAllRows)
                        {
                            reverse = !reverse;
                            currentColumn = (reverse ? totalFramesPerRow - 2 : 1);
                        }
                        else
                        {
                            currentColumn = (reverse ? totalFramesPerRow - 1 : 0);
                        }

                        if (playAllRows)
                        {
                            currentRow += (reverse ? -1 : 1);

                            EventHandler handler = RowCompleted;
                            if (handler != null)
                            {
                                handler(this, null); // notify completed
                            }

                            if (currentRow >= totalRows || currentRow < 0)
                            {
                                if (playMode == PlayMode.YoYo)
                                {
                                    reverse = !reverse;
                                    currentRow = (reverse ? totalRows - 1 : 0);
                                    currentColumn = (reverse ? totalFramesPerRow - 2 : 1);
                                }
                                else
                                {
                                    currentRow = (reverse ? totalRows - 1 : 0);
                                }

                                if (!loop)
                                    isPlaying = false;

                                handler = AnimationCompleted;
                                if (handler != null)
                                {
                                    handler(this, null); // notify completed
                                }
                            }
                        }
                        else
                        {
                            if (!loop)
                                isPlaying = false;

                            //if (PlayMode == Library.PlayMode.YoYo)
                            //    reverse = !reverse;

                            EventHandler handler = RowCompleted;
                            if (handler != null)
                            {
                                handler(this, null); // notify completed
                            }

                            handler = AnimationCompleted;
                            if (handler != null)
                            {
                                handler(this, null); // notify completed
                            }
                        }
                    }
                    //else if (currentColumn < 0)
                    //{
                    //    currentColumn = totalFramesPerRow - 1;

                    //    if (!loop)
                    //        isPlaying = false;
                    //}

                    CurrentColumn = (int)MathHelper.Clamp(CurrentColumn, 0, totalFramesPerRow);
                }
            }

            //if (autoCollisionModel && texture != null && totalFramesPerRow > 0 && totalRows > 0)
            //{
            //    this.CollisionModel.Width = texture.Width / totalFramesPerRow;
            //    this.CollisionModel.Height = texture.Height / totalRows;
            //}
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);

            if (texture == null && imageName != string.Empty)
            {
                LoadTexture();
            }

            if (texture != null && totalFramesPerRow > 0 && totalRows > 0 && Visible) //  && CollisionModel.CollisionBoundry.Intersects(SceneManager.ActiveCamera.BoundingBox)
            {
                spriteBatch.Begin(SpriteSortMode.Deferred, this.blendState, null, null, null, null, SceneManager.ActiveCamera.TransformMatrix);

                spriteBatch.Draw(texture, Transform.Position, new Rectangle(currentColumn * FrameWidth, currentRow * FrameHeight, FrameWidth, FrameHeight), Color, Transform.Rotation, new Vector2(FrameWidth / 2, FrameHeight / 2), Transform.Scale, spriteEffect, 1);

                spriteBatch.End();
            }
        }

        public override RotatedRectangle MeasureDimension()
        {
            if (texture != null && Body == null && totalRows != 0 && totalFramesPerRow != 0)
            {
                Rectangle r = new Rectangle((int)(Transform.position.X - (FrameWidth / 2) * Transform.scale.X),
                    (int)(Transform.position.Y - (FrameHeight / 2) * Transform.scale.Y), (int)(FrameWidth * Transform.scale.X),
                    (int)(FrameHeight * Transform.scale.Y));
                return new RotatedRectangle(r, Transform.Rotation);
            }
            else
            {
                return base.MeasureDimension();
            }
        }
    }
}