﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using CLEngine.Core.design;
using CLEngine.Core.framework;
using CLEngine.Editor.core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RoyT.AStar;
using Position = RoyT.AStar.Position;

namespace CLEngine.Core
{
    /// <summary>
    /// 适用于大部分RPG地图/传奇类地图
    /// </summary>
#if WIN
    [Serializable]
#endif
    [DataContract]
    public class MapObject : GameObject
    {

        [DataMember]
        private string filePath = string.Empty;

        [DataMember] private int _tileWidth;
        [DataMember] private int _tileHeight;
        [DataMember] private bool _showCollision = true;
        [DataMember] private bool _showOpacity = true;
        [DataMember] private bool _autoBoundCollider;

        [NonSerialized] private Grid _grid;

        /// <summary>
        /// 地图文件的相对路径
        /// </summary>
#if WIN
        [Editor(typeof(ContentBrowserEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("地图属性")]
        [DisplayName("文件路径"), Description("地图文件的相对路径")]
#endif
        public string MapFilePath
        {
            get { return filePath; }
            set
            {
                filePath = value;
                LoadData();
            }
        }

        /// <summary>
        /// 地图图块宽度
        /// </summary>
#if WIN
        [Category("地图属性")]
        [DisplayName("图块宽度"), Description("地图图块宽度")]
#endif
        public int TileWidth => _tileWidth;

        /// <summary>
        /// 地图图块高度
        /// </summary>
#if WIN
        [Category("地图属性")]
        [DisplayName("图块高度"), Description("地图图块高度")]
#endif
        public int TileHeight => _tileHeight;

        [NonSerialized]
        public Texture2D Texture;

        /// <summary>
        /// 碰撞格子区域
        /// </summary>
        [Browsable(false)]
        [DataMember]
        public int[][] CollisionGrid;

        /// <summary>
        /// 透明区块
        /// </summary>
        [Browsable(false)]
        [DataMember]
        public int[][] OpacityGrid;

        /// <summary>
        /// 是否显示障碍
        /// </summary>
#if WIN
        [Category("地图属性")]
        [DisplayName("显示障碍"), Description("是否显示障碍")]
#endif
        public bool ShowCollision
        {
            get { return _showCollision; }
            set { _showCollision = value; }
        }

        /// <summary>
        /// 是否显示透明区域
        /// </summary>
#if WIN
        [Category("地图属性")]
        [DisplayName("显示透明"), Description("是否显示透明区域")]
#endif
        public bool ShowOpacity
        {
            get { return _showOpacity; }
            set { _showOpacity = value; }
        }

        /// <summary>
        /// 是否给地图边缘自动添加边界碰撞
        /// </summary>
#if WIN
        [Category("地图属性")]
        [DisplayName("是否添加边界碰撞"), Description("是否给地图边缘自动添加边界碰撞")]
#endif
        public bool AutoBoundCollider
        {
            get { return _autoBoundCollider; }
            set
            {
                _autoBoundCollider = value;
                AutoAddBoundCollider();
            }
        }


        /// <summary>
        /// 
        /// </summary>
#if WIN
        [Browsable(false)]
#endif
        public Grid Grid
        {
            get { return _grid;}
            set { _grid = value; }
        }

        /// <summary>
        /// 事件对象集合
        /// </summary>
        public List<EventObject> EventObjects = new List<EventObject>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tileWidth"></param>
        /// <param name="tileHeight"></param>
        public MapObject(int tileWidth, int tileHeight)
        {
            _tileWidth = tileWidth;
            _tileHeight = tileHeight;
        }

        /// <summary>
        /// 根据世界坐标获取地图块位置
        /// </summary>
        /// <param name="worldPos"></param>
        /// <param name="clampToBounds"></param>
        /// <returns></returns>
        public Vector2 GetTilePos(Vector2 worldPos, bool clampToBounds = true)
        {
            var tileX = FastFloorToInt(worldPos.X / TileWidth);
            var tileY = FastFloorToInt(worldPos.Y / TileHeight);

            if (!clampToBounds)
                return new Vector2(tileX, tileY);

            return new Vector2(MathHelper.Clamp(tileX, 0, Texture.Width - 1),
                MathHelper.Clamp(tileY, 0, Texture.Height - 1));
        }

        private int FastFloorToInt(float x)
        {
            return (int) (x + 32768f) - 32768;
        }

        /// <summary>
        /// 根据地图块获取世界坐标
        /// </summary>
        /// <param name="tilePos"></param>
        /// <returns></returns>
        public Vector2 GetWorldPos(Vector2 tilePos)
        {
            return new Vector2(tilePos.X * TileWidth, tilePos.Y * TileHeight);
        }

        private void AutoAddBoundCollider()
        {
            var rowLength = CollisionGrid.Length;
            var colmnLength = CollisionGrid[0].Length;


            for (int i = 0; i < rowLength ; i++)
            {
                // 上
                CollisionGrid[i][0] = _autoBoundCollider ? 1 : 0;
                // 下
                CollisionGrid[i][colmnLength - 1] = _autoBoundCollider ? 1 : 0;
            }

            for (int i = 0; i < colmnLength; i++)
            {
                // 左
                CollisionGrid[0][i] = _autoBoundCollider ? 1 : 0;
                // 右
                CollisionGrid[rowLength - 1][i] = _autoBoundCollider ? 1 : 0;
            }
        }

        private void LoadData()
        {
            string mapImage = Path.Combine(SceneManager.GameProject.ProjectPath, MapFilePath);

            if (string.IsNullOrEmpty(mapImage))
                return;

            Texture = TextureLoader.FromFile(mapImage);

            var gridWidth = (int)Math.Ceiling((decimal)Texture.Width / TileWidth);
            var gridHeight = (int)Math.Ceiling((decimal)Texture.Height / TileHeight);

            CollisionGrid = new int[gridWidth][];
            OpacityGrid = new int[gridWidth][];

            for (int i = 0; i < gridWidth; i++)
            {
                if (CollisionGrid[i] == null)
                    CollisionGrid[i] = new int[gridHeight];

                if (OpacityGrid[i] == null)
                    OpacityGrid[i] = new int[gridHeight];

                for (int j = 0; j < gridHeight; j++)
                {
                    CollisionGrid[i][j] = 0;
                    OpacityGrid[i][j] = 0;
                }
            }

            if (SceneManager.IsEditor)
                return;

            _grid = new Grid(gridWidth, gridHeight, 1f);
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Initialize()
        {
            string mapImage = Path.Combine(SceneManager.GameProject.ProjectPath, MapFilePath);

            if (string.IsNullOrEmpty(mapImage))
                return;

            Texture = TextureLoader.FromFile(mapImage);

            if (Texture == null || SceneManager.IsEditor)
                return;

            var gridWidth = (int)Math.Ceiling((decimal)Texture.Width / TileWidth);
            var gridHeight = (int)Math.Ceiling((decimal)Texture.Height / TileHeight);

            _grid = new Grid(gridWidth, gridHeight, 1f);

            for (int i = 0; i < gridWidth - 1; i++)
            {
                for (int j = 0; j < gridHeight - 1; j++)
                {
                    if (CollisionGrid[i][j] == 1)
                        _grid.BlockCell(new Position(i, j));
                }
            }

            base.Initialize();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="spriteBatch"></param>
        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (Texture == null)
                return;

            base.Draw(gameTime, spriteBatch);


            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, null, null, null, null,
                SceneManager.ActiveCamera.TransformMatrix);
            spriteBatch.Draw(Texture, Transform.Position, null, Color.White, Transform.Rotation,
                Vector2.Zero, 
                Transform.Scale, SpriteEffects.None, 1);

            DrawCollision(spriteBatch);
            DrawOpacity(spriteBatch);

            spriteBatch.End();
        }

        private void DrawCollision(SpriteBatch spriteBatch)
        {
            if (!ShowCollision)
                return;

            if (CollisionGrid.Length > 0)
                for (int i = 0; i < Math.Ceiling((decimal)Texture.Width / TileWidth); i++)
                {
                    for (int j = 0; j < Math.Ceiling((decimal)Texture.Height / TileHeight); j++)
                    {
                        if (i >= CollisionGrid.Length)
                            continue;

                        if (j >= CollisionGrid[i].Length)
                            continue;

                        if (CollisionGrid[i][j] == 1)
                        {
                            Primitives.DrawBox(spriteBatch,
                                new Rectangle(i * TileWidth,
                                    j * TileHeight, TileWidth,
                                    TileHeight), Color.Red, 1);
                            Primitives.DrawBoxFilled(spriteBatch, new Rectangle(i * TileWidth,
                                j * TileHeight, TileWidth,
                                TileHeight), Color.FromNonPremultiplied(255, 0, 0, 128));
                        }
                    }
                }
        }

        private void DrawOpacity(SpriteBatch spriteBatch)
        {
            if (!ShowOpacity)
                return;

            if (OpacityGrid.Length > 0)
                for (int i = 0; i < Texture.Width / TileWidth; i++)
                {
                    for (int j = 0; j < Texture.Height / TileHeight; j++)
                    {
                        if (OpacityGrid[i][j] == 1)
                        {
                            Primitives.DrawBox(spriteBatch,
                                new Rectangle(i * TileWidth,
                                    j * TileHeight, TileWidth,
                                    TileHeight), Color.FromNonPremultiplied(0, 255, 255, 255), 1);
                            Primitives.DrawBoxFilled(spriteBatch, new Rectangle(i * TileWidth,
                                j * TileHeight, TileWidth,
                                TileHeight), Color.FromNonPremultiplied(0, 255, 255, 128));
                        }
                    }
                }
        }
    }
}