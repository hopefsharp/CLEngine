﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using CLEngine.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CLEngine.Core
{
#if WIN
    [Serializable]
#endif
    [DataContract]
    public class ParticleEmitter : GameObject
    {
        #region fields

        private int particleBurstCount = 0;

        [DataMember]
        private bool burst = false;

        [DataMember]
        private int maxParticles;
        [DataMember]
        private float nextSpawn;
        [DataMember]
        private float secElapsed;

        [DataMember]
        private float secPerSpawnMin;
        [DataMember]
        private float secPerSpawnMax;

        [DataMember]
        private float lifespanMin;
        [DataMember]
        private float lifespanMax;

        [DataMember]
        private float initialScaleMin;
        [DataMember]
        private float initialScaleMax;

        [DataMember]
        private float finalScaleMin;
        [DataMember]
        private float finalScaleMax;

        [DataMember]
        private float initialSpeedMin;
        [DataMember]
        private float initialSpeedMax;

        [DataMember]
        private float finalSpeedMin;
        [DataMember]
        private float finalSpeedMax;

        [DataMember]
        private float rotationStrength;

        [DataMember]
        private Vector2 spawnDirection;
        [DataMember]
        private Vector2 spawnAngleNoise;
        [DataMember]
        private Vector2 spawnAngleNoiseUser;
        [DataMember]
        private Color initialColor1;
        [DataMember]
        private Color initialColor2;

        [DataMember]
        private Color finalColor1;
        [DataMember]
        private Color finalColor2;

        //private bool restrictToBoundries;
        [DataMember]
        private bool enabled = true;

        [DataMember]
        private string texturePath;

        [DataMember]
        private BlendModes blendMode = BlendModes.NonPremultiplied;

#if WIN
        [NonSerialized]
#endif
        private LinkedList<Particle> particles;

#if WIN
        [NonSerialized]
#endif
        internal Texture2D texture;

#if WIN
        [NonSerialized]
#endif
        internal BlendState blendState;

#if WIN
        [NonSerialized]
#endif
        private Random random;

        #endregion

        #region properties

        /// <summary>
        /// 
        /// </summary>
#if WIN
        [Category("发射器基本属性")]
        [DisplayName("终止"), Description("确定粒子发射器是否应该终止或重复播放")]
#endif
        public bool Burst
        {
            get { return burst; }
            set { burst = value; particleBurstCount = 0; }
        }

        /// <summary>
        /// 
        /// </summary>
        //[PropertyOrder(10)]
#if WIN
        [Category("发射器基本属性")]
        [DisplayName("最大粒子"), Description("此发射器中存活的最大粒子数量")]
#endif
        public int MaxParticles
        {
            get { return maxParticles; }
            set { maxParticles = value; }
        }

        /// <summary>
        /// 
        /// </summary>
#if WIN
        [Category("发射器基本属性")]
        [DisplayName("每秒产生（最小）"), Description("产生另一个粒子之前保持的最短时间")]
#endif
        public float SecondsPerSpawnMin
        {
            get { return secPerSpawnMin; }
            set { secPerSpawnMin = value; }
        }

        /// <summary>
        /// 
        /// </summary>
#if WIN
        [Category("发射器基本属性")]
        [DisplayName("每秒产生（最大）"), Description("产生另一个粒子之前保留的最长时间")]
#endif
        public float SecondsPerSpawnMax
        {
            get { return secPerSpawnMax; }
            set
            {
                if (value == 0) value = 0.00001f;
                secPerSpawnMax = value;
            }
        }

        /// <summary>
        /// 粒子保持活动的最短时间
        /// </summary>
#if WIN
        [Category("发射器基本属性")]
        [DisplayName("生命周期(最小)"), Description("粒子保持活动的最短时间")]
#endif
        public float LifespanMin
        {
            get { return lifespanMin; }
            set { lifespanMin = value; }
        }

        /// <summary>
        /// 粒子保持活动的最长时间
        /// </summary>
#if WIN
        [Category("发射器基本属性")]
        [DisplayName("生命周期(最大)"), Description("粒子保持活动的最长时间")]
#endif
        public float LifespanMax
        {
            get { return lifespanMax; }
            set { lifespanMax = value; }
        }

        /// <summary>
        /// 粒子的初始尺度（最小范围）
        /// </summary>
#if WIN
        [Category("发射器变换属性")]
        [DisplayName("初始缩放(最小)"), Description("粒子的初始比例（最小范围）")]
#endif
        public float InitialScaleMin
        {
            get { return initialScaleMin; }
            set { initialScaleMin = value; }
        }

        /// <summary>
        /// 粒子的初始尺度（最小范围）
        /// </summary>
#if WIN
        [Category("发射器变换属性")]
        [DisplayName("初始缩放(最大)"), Description("粒子的初始比例（最大范围）")]
#endif
        public float InitialScaleMax
        {
            get { return initialScaleMax; }
            set { initialScaleMax = value; }
        }

        /// <summary>
        /// 粒子的初始尺度（最小范围）
        /// </summary>
#if WIN
        [Category("发射器变换属性")]
        [DisplayName("最终缩放(最小)"), Description("粒子的最终比例（最小范围）")]
#endif
        public float FinalScaleMin
        {
            get { return finalScaleMin; }
            set { finalScaleMin = value; }
        }

        /// <summary>
        /// 粒子的初始尺度（最小范围）
        /// </summary>
#if WIN
        [Category("发射器变换属性")]
        [DisplayName("最终缩放(最大)"), Description("粒子的最终比例（最大范围）")]
#endif
        public float FinalScaleMax
        {
            get { return finalScaleMax; }
            set { finalScaleMax = value; }
        }

#if WIN
        [Category("发射器变换属性")]
        [DisplayName("初始速度(最小)"), Description("粒子的初始速度（最小范围）")]
#endif
        public float InitialSpeedMin
        {
            get { return initialSpeedMin; }
            set { initialSpeedMin = value; }
        }

        /// <summary>
        /// 
        /// </summary>
#if WIN
        [Category("发射器变换属性")]
        [DisplayName("初始速度(最大)"), Description("粒子的初始速度（最大范围）")]
#endif
        public float InitialSpeedMax
        {
            get { return initialSpeedMax; }
            set { initialSpeedMax = value; }
        }

        /// <summary>
        /// 
        /// </summary>
#if WIN
        [Category("发射器变换属性")]
        [DisplayName("最终速度(最小)"), Description("粒子的最终速度（最小范围）")]
#endif
        public float FinalSpeedMin
        {
            get { return finalSpeedMin; }
            set { finalSpeedMin = value; }
        }

        /// <summary>
        /// 
        /// </summary>
#if WIN
        [Category("发射器变换属性")]
        [DisplayName("最终速度(最大)"), Description("粒子的最终速度（最大范围）")]
#endif
        public float FinalSpeedMax
        {
            get { return finalSpeedMax; }
            set { finalSpeedMax = value; }
        }

        /// <summary>
        /// 
        /// </summary>
#if WIN
        [Category("发射器行为属性")]
        [DisplayName("产生方向"), Description("粒子的方向")]
#endif
        public Vector2 SpawnDirection
        {
            get { return spawnDirection; }
            set { spawnDirection = value; }
        }

        /// <summary>
        /// 
        /// </summary>
#if WIN
        [Category("发射器行为属性")]
        [DisplayName("产生角度噪音"), Description("粒子的产生角噪声")]
#endif
        public Vector2 SpawnAngleNoise
        {
            get { return spawnAngleNoiseUser; }
            set
            {
                spawnAngleNoiseUser = value;
                spawnAngleNoise = new Vector2((float)value.X * (float)MathHelper.Pi, (float)value.Y * -(float)MathHelper.Pi);
            }
        }

        /// <summary>
        /// 
        /// </summary>    
#if WIN
        [Category("发射器颜色属性")]
        [DisplayName("初始颜色1"), Description("粒子的一种可能的初始颜色")]
#endif
        public Color InitialColor1
        {
            get { return initialColor1; }
            set { initialColor1 = value; }
        }

        /// <summary>
        /// 
        /// </summary>
#if WIN
        [Category("发射器颜色属性")]
        [DisplayName("初始颜色2"), Description("粒子的一种可能的初始颜色")]
#endif
        public Color InitialColor2
        {
            get { return initialColor2; }
            set { initialColor2 = value; }
        }

        /// <summary>
        /// 
        /// </summary>
#if WIN
        [Category("发射器颜色属性")]
        [DisplayName("最终颜色1"), Description("粒子的一种可能的最终颜色")]
#endif
        public Color FinalColor1
        {
            get { return finalColor1; }
            set { finalColor1 = value; }
        }

        /// <summary>
        /// 
        /// </summary>
#if WIN
        [Category("发射器颜色属性")]
        [DisplayName("最终颜色2"), Description("粒子的一种可能的最终颜色")]
#endif
        public Color FinalColor2
        {
            get { return finalColor2; }
            set { finalColor2 = value; }
        }

        /// <summary>
        /// 
        /// </summary>
#if WIN
        [Category("发射器基本属性")]
        [DisplayName("启用"), Description("确定是否启用了粒子")]
#endif
        public bool Enabled
        {
            get { return enabled; }
            set { enabled = value; }
        }

        /// <summary>
        /// 
        /// </summary>
#if WIN
        [Category("发射器基本属性")]
        [DisplayName("图片名称"), Description("纹理的相对路径")]
#endif
        public string ImageName
        {
            get { return texturePath; }
            set { texturePath = value; this.LoadTexture(); }
        }

        /// <summary>
        /// 
        /// </summary>
#if WIN
        [Category("发射器颜色属性")]
        [DisplayName("混合模式"), Description("混合模式")]
#endif
        public BlendModes BlendMode
        {
            get { return blendMode; }
            set { blendMode = value; this.LoadState(); }
        }

        /// <summary>
        /// 
        /// </summary>
#if WIN
        [Category("发射器行为属性")]
        [DisplayName("旋转强度"), Description("颗粒的旋转强度")]
#endif
        public float RotationStrength
        {
            get { return rotationStrength; }
            set { rotationStrength = value; }
        }

        #endregion

        #region constructors

        /// <summary>
        /// 
        /// </summary>
        public ParticleEmitter()
        {
            SecondsPerSpawnMin = 0.01f;
            SecondsPerSpawnMax = 0.015f;
            SpawnDirection = new Vector2(0, -1);
            SpawnAngleNoise = new Vector2(1, 1);
            LifespanMin = 1;
            LifespanMax = 2;
            InitialScaleMin = 1;
            InitialScaleMax = 2;
            FinalScaleMin = 2;
            FinalScaleMax = 4;
            InitialColor1 = Color.White;
            InitialColor2 = Color.White;
            FinalColor1 = Color.Black;
            FinalColor2 = Color.Black;
            InitialSpeedMin = 80;
            InitialSpeedMax = 120;
            FinalSpeedMin = 0;
            FinalSpeedMax = 20;
            MaxParticles = 100;

            random = new Random();
        }

        #endregion

        #region methods

        /// <summary>
        /// 
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();

            random = new Random();

            if (texture == null && texturePath != null && texturePath.Trim() != "")
            {
                LoadTexture();
            }

            LoadState();

            nextSpawn = MathExtension.LinearInterpolate(SecondsPerSpawnMin, SecondsPerSpawnMax, random.NextDouble());
            secElapsed = 0.0f;
            particles = new LinkedList<Particle>();
        }

        private void LoadTexture()
        {
            texture = TextureLoader.FromContent(texturePath);
        }

        private void LoadState()
        {
            switch (this.blendMode)
            {
                case BlendModes.NonPremultiplied:
                    this.blendState = BlendState.NonPremultiplied;
                    break;
                case BlendModes.AlphaBlend:
                    this.blendState = BlendState.AlphaBlend;
                    break;
                case BlendModes.Additive:
                    this.blendState = BlendState.Additive;
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameTime">The gametime</param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (texture != null)
            {
                secElapsed += (float)gameTime.ElapsedGameTime.TotalSeconds;
                while (secElapsed > nextSpawn)
                {
                    if (particles.Count < MaxParticles && enabled)
                    {
                        // Spawn a particle
                        Vector2 StartDirection = Vector2.Transform(SpawnDirection, Matrix.CreateRotationZ(MathExtension.LinearInterpolate(spawnAngleNoise.X, spawnAngleNoise.Y, random.NextDouble())));
                        StartDirection.Normalize();
                        Vector2 EndDirection = StartDirection * MathExtension.LinearInterpolate(FinalSpeedMin, FinalSpeedMax, random.NextDouble());
                        StartDirection *= MathExtension.LinearInterpolate(InitialSpeedMin, InitialSpeedMax, random.NextDouble());
                        particles.AddLast(new Particle(
                            Transform.Position,
                            StartDirection,
                            EndDirection,
                            MathExtension.LinearInterpolate(LifespanMin, LifespanMax, random.NextDouble()),
                            MathExtension.LinearInterpolate(InitialScaleMin, InitialScaleMax, random.NextDouble()),
                            MathExtension.LinearInterpolate(FinalScaleMin, FinalScaleMax, random.NextDouble()),
                            MathExtension.LinearInterpolate(InitialColor1, InitialColor2, random.NextDouble()),
                            MathExtension.LinearInterpolate(FinalColor1, FinalColor2, random.NextDouble()),
                            this)
                        );

                        particles.Last.Value.Update(secElapsed);
                    }

                    secElapsed -= nextSpawn;
                    nextSpawn = MathExtension.LinearInterpolate(SecondsPerSpawnMin, SecondsPerSpawnMax, random.NextDouble());

                    if (burst)
                        particleBurstCount++;

                    if (burst && particleBurstCount >= maxParticles)
                    {
                        this.enabled = false;
                        this.particleBurstCount = 0;
                    }
                }
            }

            LinkedListNode<Particle> node = particles.First;
            while (node != null)
            {
                bool isAlive = node.Value.Update((float)gameTime.ElapsedGameTime.TotalSeconds);
                node = node.Next;
                if (!isAlive)
                {
                    if (node == null)
                    {
                        particles.RemoveLast();
                    }
                    else
                    {
                        particles.Remove(node.Previous);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameTime">The gametime</param>
        /// <param name="spriteBatch">The spriteBatch</param>
        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);

            if (texture != null && Visible)
            {
                spriteBatch.Begin(SpriteSortMode.Deferred, this.blendState, null, null, null, null, SceneManager.ActiveCamera.TransformMatrix);

                LinkedListNode<Particle> node = particles.First;
                while (node != null)
                {
                    node.Value.Draw(spriteBatch);
                    node = node.Next;
                }

                spriteBatch.End();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Clear()
        {
            particles.Clear();
        }

        #endregion
    }
}