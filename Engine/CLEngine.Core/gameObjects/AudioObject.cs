﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using CLEngine.Core.design;
using CLEngine.Core;
using NAudio.Wave;

namespace CLEngine.Core
{
#if WIN
    [Serializable]
#endif
    [DataContract]
    public class AudioObject : GameObject
    {
#if WIN
        [NonSerialized]
        private IWavePlayer waveOutDevice;

        [NonSerialized]
        private LoopStream mainOutputStream;

        [NonSerialized]
        private string lastLoadedPath = string.Empty;

        [DataMember]
        private bool playOnStart = false;
#endif

        [DataMember]
        private bool loop = false;

        [DataMember]
        private float volume = 1.0f;

        [DataMember]
        private string filePath = string.Empty;

#if WIN
        [Editor(typeof(ContentBrowserEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("音频属性")]
        [DisplayName("文件路径"), Description("音频文件的相对路径")]
#endif
        public string FilePath
        {
            get { return filePath; }
            set { filePath = value; }
        }

#if WIN
        [Category("音频属性")]
        [DisplayName("自动播放"), Description("确定加载场景时音频是否会自动播放")]
#endif
        public bool PlayOnStart
        {
            get { return playOnStart; }
            set { playOnStart = value; }
        }

#if WIN
        [Category("音频属性")]
        [DisplayName("正在播放"), Description("确定音频是否正在播放")]
        [Browsable(false)]
#endif
        public bool IsPlaying
        {
            get { return (waveOutDevice.PlaybackState == PlaybackState.Playing ? true : false); }
        }

#if WIN
        [Category("音频属性")]
        [DisplayName("循环"), Description("确定音频是否会循环播放")]
#endif
        public bool Loop
        {
            get { return loop; }
            set { loop = value; }
        }

#if WIN
        [Category("音频属性")]
        [DisplayName("音量"), Description("音频音量")]
#endif
        public float Volume
        {
            get { return volume; }
            set
            {
                volume = value;

                if (volume > 1) volume = 1;
                else if (volume < 0) volume = 0;

                if (IsPlaying)
                {
                    mainOutputStream.Volume = volume;
                }
            }
        }

#if WIN
        [Category("音频属性")]
        [DisplayName("位置"), Description("当前曲目的音频位置")]
        [Browsable(false)]
#endif
        public long Position
        {
            get
            {
                if (mainOutputStream != null)
                    return mainOutputStream.Position;
                else
                    return -1;
            }
            set
            {
                if (mainOutputStream != null)
                    mainOutputStream.Position = value;
            }
        }

#if WIN
        [Category("音频属性")]
        [DisplayName("禁用"), Description("确定对象是否已禁用")]
#endif
        public override bool Disabled
        {
            get { return disabled; }
            set
            {
                disabled = value;
                if (disabled) this.Stop();
            }
        }

        public void Stop()
        {
            if (waveOutDevice != null && mainOutputStream != null)
            {
                // DO NOT USE waveOut Stop method!!
                if (waveOutDevice.PlaybackState == PlaybackState.Playing)
                    waveOutDevice.Pause();

                mainOutputStream.Position = -1;
            }
        }

        public override void Initialize()
        {
            base.Initialize();
#if WIN
            waveOutDevice = new DirectSoundOut(50);
#endif
            if (playOnStart)
            {
                Play();
            }
        }

        public void Play()
        {
            if (this.Disabled) return;

            string _filePath = SceneManager.GameProject.ProjectPath + "//" + filePath;

            if (SceneManager.IsEditor) return;

#if WIN
            if (_filePath != lastLoadedPath || waveOutDevice == null)
            {

                if (!File.Exists(filePath)) return;

                WaveChannel32 stream = Audio.CreateInputStream(_filePath, volume, false);

                if (stream != null)
                {
                    mainOutputStream = new LoopStream(stream);
                    mainOutputStream.EnableLooping = loop;

                    try
                    {
                        waveOutDevice.Init(mainOutputStream);
                    }
                    catch (Exception initException)
                    {
                        Console.WriteLine(String.Format("{0}", initException.Message), "初始化输出时出错");
                        return;
                    }

                    waveOutDevice.Play();

                    lastLoadedPath = _filePath;
                }
            }
            else
            {
                waveOutDevice.Play(); 
            }
#endif
        }
    }
}