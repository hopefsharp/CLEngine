﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using CLEngine.Core.design;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NLua;

// ReSharper disable once CheckNamespace
namespace CLEngine.Core
{
	/// <summary>
	/// 
	/// </summary>
#if WIN
	[Serializable]
#endif
    [DataContract]
    public class LuaObject : ObjectComponent
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string _filePath;

        /// <summary>
        /// 
        /// </summary>
        [NonSerialized]
        public object[] LuaResult;

        /// <summary>
        /// 
        /// </summary>
#if WIN
		[Editor(typeof(ContentBrowserEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("脚本属性")]
        [DisplayName("文件路径"), Description("脚本文件的相对路径")]
#endif
        public string LuaFilePath
        {
            get { return _filePath; }
            set
            {
                _filePath = value;
            }
        }


        /// <summary>
        /// 
        /// </summary>
#if WIN
		[NonSerialized]
#endif
        [Browsable(false)]
        public ObjectComponent Object;


        /// <summary>
        /// 
        /// </summary>
        public LuaObject()
        {
            Object = this;
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Initialize()
        {
            Object = this;

            string _filePath = SceneManager.GameProject.ProjectPath + "//" + this._filePath;
            
            LuaResult = ScriptEngine.LuaEngine.LoadFile(_filePath).Call(this);

            base.Initialize();

            DoFile("Initialize", Object);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            DoFile("Update", gameTime);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="spriteBatch"></param>
        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);

            DoFile("Draw", gameTime, spriteBatch);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        public override void OnCollisionEnter(GameObject other)
        {
            base.OnCollisionEnter(other);

            DoFile("OnCollisionEnter", other);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buttonPressed"></param>
        public override void OnMouseDown(MouseEventButton buttonPressed)
        {
            base.OnMouseDown(buttonPressed);

            DoFile("OnMouseDown", buttonPressed);
        }

        /// <summary>
        /// 
        /// </summary>
        public override void OnMouseMove()
        {
            base.OnMouseMove();

            DoFile("OnMouseMove");
        }

        /// <summary>
        /// 
        /// </summary>
        public override void OnCollisionFree()
        {
            base.OnCollisionFree();

            DoFile("OnCollisionFree");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buttonPressed"></param>
        public override void OnMouseClick(MouseEventButton buttonPressed)
        {
            base.OnMouseClick(buttonPressed);

            DoFile("OnMouseClick", buttonPressed);
        }

        /// <summary>
        /// 
        /// </summary>
        public override void OnMouseEnter()
        {
            base.OnMouseEnter();

            DoFile("OnMouseEnter");
        }

        /// <summary>
        /// 
        /// </summary>
        public override void OnMouseOut()
        {
            base.OnMouseOut();

            DoFile("OnMouseOut");
        }

        /// <summary>
        /// 
        /// </summary>
        public override void OnMouseUp()
        {
            base.OnMouseUp();

            DoFile("OnMouseUp");
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Removed()
        {
            base.Removed();

            DoFile("Removed");
        }

        private void DoFile(string function, params object[] args)
        {
            if (LuaResult == null)
                return;

            foreach (LuaTable o in LuaResult)
            {
                o[function + "Args"] = args;
                (o[function] as LuaFunction)?.Call(args);
                return;
            }
        }
    }
}