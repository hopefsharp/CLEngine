﻿using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework.Graphics;

namespace CLEngine.Core
{
    /// <summary>
    /// 此类负责管理纹理加载。
    /// 将纹理加载到内存中以便快速使用，如果加载相同的纹理两次，它将在内存中使用相同的空间以保留资源。
    /// 如果要在运行时清除缓存，请执行TextureLoader.Clear() 方法。
    /// </summary>
    public static class TextureLoader
    {
        private static readonly Dictionary<string, Texture2D> _textures = new Dictionary<string, Texture2D>();

        /// <summary>
        /// 从游戏目录中的Content文件夹下加载资源
        /// 这是一个相对路径
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static Texture2D FromContent(string filename)
        {
            if (SceneManager.GameProject == null) return null;

#if WIN
            return FromFile(SceneManager.GameProject.ProjectPath + "//" + filename);
#endif
        }

        /// <summary>
        /// 从路径加载资源
        /// 这是一个绝对路径
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static Texture2D FromFile(string filename)
        {
            if (SceneManager.GraphicsDevice == null) return null;

            // 图像是否已经加载到内存中了
            if (!_textures.ContainsKey(filename) || SceneManager.IsEditor)
            {
#if WIN
                FileInfo aTexturePath = new FileInfo(filename);

                // 该文件是否存在
                if (!aTexturePath.Exists) return null;

                FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
                Texture2D texture = Texture2D.FromStream(SceneManager.GraphicsDevice, fs);
                fs.Close();

                // 在字典中存储加载的纹理
                _textures[filename] = texture;
#elif WINRT
                if(!MetroHelper.AppDataFileExists(filename)) return null;

                using (Stream stream = Windows.ApplicationModel.Package.Current.InstalledLocation.OpenStreamForReadAsync(filename).Result)
                {
                    Texture2D texture = Texture2D.FromStream(SceneManager.GraphicsDevice, stream);

                    _textures[filename] = texture;
                }
#endif
            }

            return _textures[filename];
        }

        /// <summary>
        /// 清理所有图片缓存
        /// </summary>
        public static void Clear()
        {
            _textures.Clear();
        }
    }
}