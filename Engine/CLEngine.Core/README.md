## 文件结构

- astar
    - A星寻路
- attributes
    - 引擎特性
- components
    - 组件
- design
    - 设计类
- DragonBones
    - 龙骨动画
- farseer
    - 物理引擎
- framework
    - 游戏框架
- game-objects
    - 对象
- gui
    - fairygui
- network
    - 网络服务
- steam
    - steam服务