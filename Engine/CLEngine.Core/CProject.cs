﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace CLEngine.Core
{
#if WIN
    [Serializable]
#endif
    [DataContract]
    public class CProject
    {
        [DataMember]
        private String projectPath;

        [DataMember]
        private Settings settings = new Settings();

        [DataMember]
        private String projectName;

        [DataMember]
        private CProjectEditorSettings editorSettings = new CProjectEditorSettings();

        [DataMember]
        private bool debug = true;

        [DataMember]
        private String projectFilePath;

        [DataMember]
        private String sceneStartPath;

        [DataMember]
        private CProjectSettings projectSettings = new CProjectSettings();

#if WIN
        [Category("工程属性"), Browsable(false)]
#endif
        public String SceneStartPath
        {
            get { return sceneStartPath; }
            set { sceneStartPath = value; }
        }

#if WIN
        [Category("工程属性")]
        [DisplayName("工程设置"), Description("工程设置")]
#endif
        public CProjectSettings ProjectSettings
        {
            get { return projectSettings; }
            set { projectSettings = value; }
        }

#if WIN
        [Browsable(false)]
#endif
        public String ProjectFilePath
        {
            get { return projectFilePath; }
            set { projectFilePath = value; }
        }


        public CProject(String projectName, String projectPath)
        {
            this.projectName = projectName;
            this.projectPath = projectPath + "\\" + projectName;
            this.projectFilePath = this.projectPath + "\\" + projectName + ".clengine";
#if WIN
            this.CreateProjectDirectory();
#endif

            this.Save();
        }

        public void Save()
        {
            this.Settings.SaveToFile();

            CHelper.SerializeObject(this.projectFilePath, this);
        }

        public static CProject Load(string filepath)
        {
#if WIN
            CProject project = (CProject)CHelper.DeserializeObject(filepath);
#elif WINRT
            string npath = filepath.Replace(Windows.ApplicationModel.Package.Current.InstalledLocation.Path + "\\", string.Empty);
            CProject project = (CProject)CHelper.DeserializeObject(typeof(CProject), npath);
#endif    
            // 更新项目路径：
            project.ProjectPath = System.IO.Path.GetDirectoryName(filepath);
            project.ProjectFilePath = filepath;

            // Create a virtual settings file:
            project.settings.ReloadPath(project.ProjectPath);

            return project;
        }

#if WIN
        private void CreateProjectDirectory()
        {
            Directory.CreateDirectory(this.projectPath);

            //TODO: 用于创建其他目录的空间
            Directory.CreateDirectory(this.projectPath + "//Libs");
            Directory.CreateDirectory(this.projectPath + "//Scenes");
            Directory.CreateDirectory(this.projectPath + "//Content");
            Directory.CreateDirectory(this.projectPath + "//Scripts");
        }
#endif

#if WIN
        [Browsable(false)]
#endif
        public String ProjectPath
        {
            get { return projectPath; }
            set { projectPath = value; }
        }

#if WIN
        [Category("项目属性")]
        [DisplayName("设置"), Description("运行设置")]
#endif
        public Settings Settings
        {
            get { return settings; }
        }

#if WIN
        [Category("项目属性")]
        [DisplayName("项目名称"), Description("项目名称")]
#endif
        public String ProjectName
        {
            get { return projectName; }
            set { projectName = value; }
        }

#if WIN
        [Category("项目属性")]
        [DisplayName("编辑器设置"), Description("编辑器设置")]
#endif
        public CProjectEditorSettings EditorSettings
        {
            get { return editorSettings; }
            set { editorSettings = value; }
        }

#if WIN
        [Category("项目属性")]
        [DisplayName("调试"), Description("确定应用程序是否应编译并以调试模式运行.")]
#endif
        public bool Debug
        {
            get { return debug; }
            set { debug = value; }
        }
		/// <summary>
		/// 获取工程路径
		/// </summary>
		/// <returns></returns>
		public string GetPath(){
			return projectPath;
		}
    }
}