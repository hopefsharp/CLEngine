﻿using System;
using System.Collections.Generic;

namespace CLEngine.Core
{
    /// <summary>
    /// 一个简单的事件通知器
    /// </summary>
    public static class Emitter
    {
        private static readonly Dictionary<string, List<Action>> _messageTable;

        /// <summary>
        /// 
        /// </summary>
        static Emitter()
        {
            _messageTable = new Dictionary<string, List<Action>>();
        }

        /// <summary>
        /// 添加事件监听
        /// </summary>
        /// <param name="eventType">事件类型</param>
        /// <param name="handler">执行函数</param>
        public static void AddObserver(string eventType, Action handler)
        {
            if (!_messageTable.TryGetValue(eventType, out var list))
            {
                list = new List<Action>();
                _messageTable.Add(eventType, list);
            }

            if (list.Contains(handler))
            {
                Console.WriteLine("[错误] 您试图两次添加相同的事件通知器:" + eventType);
                return;
            }
            
            list.Add(handler);
        }

        /// <summary>
        /// 是否拥有该事件
        /// </summary>
        /// <param name="eventType">事件类型</param>
        /// <returns></returns>
        public static bool HasObserver(string eventType)
        {
            if (_messageTable.TryGetValue(eventType, out var list))
            {
                if (list.Count > 0)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// 移除事件监听
        /// </summary>
        /// <param name="eventType">事件类型</param>
        /// <param name="handler">事件函数</param>
        public static void RemoveObserver(string eventType, Action handler)
        {
            _messageTable[eventType].Remove(handler);
        }

        /// <summary>
        /// 执行函数
        /// </summary>
        /// <param name="eventType">事件类型</param>
        public static void Emit(string eventType)
        {
            if (_messageTable.TryGetValue(eventType, out var list))
            {
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    list[i]();
                }
            }
        }
    }
}