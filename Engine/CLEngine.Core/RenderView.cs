﻿using System;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework.Graphics;

namespace CLEngine.Core
{
#if WIN
    [Serializable]
#endif
    [DataContract]
    public class RenderView
    {
        [DataMember]
        private Camera camera = new Camera();

        [DataMember]
        private Viewport viewport = new Viewport();

        public Viewport Viewport
        {
            get { return viewport; }
            set { viewport = value; }
        }

        public Camera Camera
        {
            get { return camera; }
            set { camera = value; }
        }
    }
}