﻿using System.Runtime.CompilerServices;

namespace CLEngine.Core
{
    /// <summary>
    /// 时间类
    /// </summary>
    public static class Time
    {
        /// <summary>
        /// 游戏运行的总时间
        /// </summary>
        public static float time;

        /// <summary>
        /// 从前一帧到当前的增量时间，由timeScale缩放
        /// </summary>
        public static float deltaTime;

        /// <summary>
        /// deltaTime的非缩放版本。 不受timeScale的影响
        /// </summary>
        public static float unscaledDeltaTime;

        /// <summary>
        /// 辅助deltaTime，用于在需要同时缩放两个不同的增量时使用
        /// </summary>
        public static float altDeltaTime;

        /// <summary>
        /// 加载场景后的总时间
        /// </summary>
        public static float timeSinceSceneLoad;

        /// <summary>
        /// deltaTime的时间尺度
        /// </summary>
        public static float timeScale = 1f;

        /// <summary>
        /// altDeltaTime的时间尺度
        /// </summary>
        public static float altTimeScale = 1f;

        /// <summary>
        /// 已通过的总帧数
        /// </summary>
        public static uint frameCount;


        internal static void Update(float dt)
        {
            time += dt;
            deltaTime = dt * timeScale;
            altDeltaTime = dt * altTimeScale;
            unscaledDeltaTime = dt;
            timeSinceSceneLoad += dt;
            frameCount++;
        }


        internal static void SceneChanged()
        {
            timeSinceSceneLoad = 0f;
        }


        /// <summary>
        /// 允许检查间隔。 只应与deltaTime以上的区间值一起使用，
        /// 否则它将始终返回true
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool checkEvery(float interval)
        {
            // 我们减去deltaTime，因为timeSinceSceneLoad已经包含此更新的时间点deltaTime
            return (int)(timeSinceSceneLoad / interval) > (int)((timeSinceSceneLoad - deltaTime) / interval);
        }
    }
}