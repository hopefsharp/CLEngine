﻿using System;
using CLEngine.Core;
using Microsoft.Xna.Framework;

namespace CLEngine.Core
{
    public static class ExtensionMethods
    {
        public static Vector2 Rotate(this Vector2 vector, float value)
        {
            var sin = (float)Math.Sin(value);
            var cos = (float)Math.Cos(value);
            return new Vector2(vector.X * cos - vector.Y * sin, vector.X * sin + vector.Y * cos);
        }

        public static bool Intersects(this Rectangle r, RotatedRectangle rectangle)
        {
            RotatedRectangle rotated = new RotatedRectangle(r, 0);
            return rotated.Intersects(rectangle);
        }
    }
}