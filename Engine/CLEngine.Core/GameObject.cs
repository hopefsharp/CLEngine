﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using CLEngine.Core.attributes;
using CLEngine.Core.components;
using FarseerPhysics;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CLEngine.Core
{
    /// <summary>
    /// 游戏对象
    /// 该类可用于表示游戏中的对象。 您可以将组件附加到它
    /// </summary>
#if WIN
    [Serializable, TypeConverter(typeof(ExpandableObjectConverter))]
#endif
    [DataContract(IsReference = true)]
    [SuppressMessage("ReSharper", "ForCanBeConvertedToForeach")]
    public class GameObject : SystemObject, IDisposable
#if WIN
    ,ICloneable
#endif
    {
#if WIN
        [NonSerialized]
#endif
        internal PhysicalBody physicalBody;

        [DataMember]
        private List<string> componentReferences = new List<string>();

        [DataMember]
        private GameObjectCollection children;

        [DataMember]
        private string name = string.Empty;

        [DataMember]
        private Transform transform = new Transform();

        [DataMember]
        private bool rotationIndependent;

        [DataMember]
        private bool visible = true;

        /// <summary>
        /// 是否禁用
        /// </summary>
        [DataMember] protected bool disabled;

        [DataMember]
        private bool selectable = true;

        [DataMember]
        private string tag = string.Empty;

#if WIN
        [NonSerialized]
#endif
        internal bool mouseDown;

#if WIN
        [NonSerialized]
#endif
        internal bool mouseOver;

#if WIN
        [NonSerialized]
#endif
        private object objectTag;

        /// <summary>
        /// 对象的属性
        /// </summary>
#if WIN
        [Category("对象属性")]
        [DisplayName("Transform"), Description("对象的属性")]
#endif
        public Transform Transform
        {
            get { return transform ?? (transform = new Transform()); }
            set
            {
                transform = value;
            }
        }

        /// <summary>
        /// 对象的名称
        /// </summary>
#if WIN
        [Category("对象属性"), ReadOnly(true)]
        [DisplayName("名称"), Description("对象的名称"), Browsable(false)]
#endif
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// 确定此对象是否可见
        /// </summary>
#if WIN
        [Category("对象属性")]
        [DisplayName("可见"), Description("确定此对象是否可见")]
#endif
        public bool Visible
        {
            get { return visible; }
            set { visible = value; }
        }

        /// <summary>
        /// 确定对象是否已禁用
        /// </summary>
#if WIN
        [Category("对象属性")]
        [DisplayName("禁用"), Description("确定对象是否已禁用")]
#endif
        public virtual bool Disabled
        {
            get { return disabled; }
            set
            {
                disabled = value;
                if (body != null) body.Enabled = !value;
            }
        }

        /// <summary>
        /// 对象的标签
        /// </summary>
#if WIN
        [Category("对象属性")]
        [DisplayName("标签"), Description("对象的标签")]
#endif
        public string Tag
        {
            get { return tag; }
            set { tag = value; }
        }

        /// <summary>
        /// 对象的标签
        /// </summary>
#if WIN
        [Browsable(false)]
#endif
        public object ObjectTag
        {
            get { return objectTag; }
            set { objectTag = value; }
        }

        /// <summary>
        /// 确定是否选择了此对象
        /// </summary>
#if WIN
        [Category("对象属性")]
        [DisplayName("选中"), Description("确定是否选择了此对象")]
#endif
        public bool Selectable
        {
            get { return selectable; }
            set { selectable = value; }
        }

        /// <summary>
        /// 是否独立旋转
        /// </summary>
#if WIN
        [Browsable(false)]
#endif
        public bool RotationIndependent
        {
            get { return rotationIndependent; }
            set { rotationIndependent = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [SuppressMessage("ReSharper", "VirtualMemberCallInConstructor")]
        public GameObject()
        {
            transform.GameObject = this;

            Initialize();

            // 初始化碰撞模型
            //collisionModel.Initialize(this.transform);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public GameObject(string name)
            : this()
        {
            Name = name;
        }

#if WIN
        [NonSerialized]
#endif
        private List<ObjectComponent> components = new List<ObjectComponent>();

        [DataMember]
        private Dictionary<string, Dictionary<PropertyLabel, object>> componentValues = new Dictionary<string, Dictionary<PropertyLabel, object>>();
        

#if WIN
        [NonSerialized]
#endif
        private Body body;

        /// <summary>
        /// 对象的刚体组件 每个物体仅有一个刚体组件
        /// </summary>
#if WIN
        [Browsable(false)]
#endif
        public Body Body
        {
            get { return body; }
            set
            {
                // 每个body只有一个object
                if (body != null)
                {
                    SceneManager.ActiveScene.World.RemoveBody(body);
                }

                body = value;
                if (body != null)
                {
                    body.Position = ConvertUnits.ToSimUnits(Transform.Position);
                    body.Rotation = Transform.Rotation;
                    body.gameObject = this;

                    if (!SceneManager.IsEditor)
                    {
                        body.OnCollision += body_OnCollision;
                        body.OnSeparation += body_OnSeparation;
                    }
                }
            }
        }

        /// <summary>
        /// 子对象
        /// </summary>
#if WIN
        [Browsable(false)]
#endif
        public GameObjectCollection Children
        {
            get { return children; }
            set { children = value; }
        }

        /// <summary>
        /// 设置父对象
        /// </summary>
        /// <param name="t">父对象的transform</param>
        public void SetParent(Transform t)
        {
            transform.parent = t;
        }

        /// <summary>
        /// 获取所有游戏对象
        /// </summary>
        /// <returns></returns>
        public static List<GameObject> GetAllGameObjects()
        {
            if (SceneManager.ActiveScene == null) return null;

            List<GameObject> gameObjectsBuffer = new List<GameObject>();

            for (var index = 0; index < SceneManager.ActiveScene.GameObjects.Count; index++)
            {
                GameObject gameObject = SceneManager.ActiveScene.GameObjects[index];
                gameObjectsBuffer.Add(gameObject);
                LoadNext(gameObject, gameObjectsBuffer);
            }

            return gameObjectsBuffer;
        }

        /// <summary>
        /// 获取组件
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <returns>组件</returns>
        public T GetComponent<T>() where T : ObjectComponent
        {
            for (int i = 0; i < components.Count; i++)
            {
                if (components[i] is T o)
                    return o;
            }

            return default;
        }

        /// <summary>
        /// 移除对象
        /// </summary>
        /// <param name="gameObject">要移除的对象</param>
        /// <returns>移除的对象</returns>
        public static GameObject Remove(GameObject gameObject)
        {
            gameObject.Remove();

            return gameObject;
        }

        /// <summary>
        /// 移除自身
        /// </summary>
        public void Remove()
        {
            if (transform.Parent == null)
                SceneManager.ActiveScene.GameObjects.Remove(this);
            else
                transform.Parent.GameObject.Children.Remove(this);
        }

        private static void LoadNext(GameObject _gameObject, List<GameObject> gameObjectsBuffer)
        {
            for (int i = 0; i < _gameObject.children?.Count; i++)
            {
                gameObjectsBuffer.Add(_gameObject.children[i]);
                LoadNext(_gameObject.children[i], gameObjectsBuffer);
            }
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="gameTime"></param>
        public virtual void Update(GameTime gameTime)
        {
            try
            {
                if (body != null)
                {
                    var bodyPos = ConvertUnits.ToDisplayUnits(body.Position);
                    if (bodyPos != transform.position)
                        transform.Position = bodyPos;
                }

                // 更新此对象的子项
                for (int i = 0; i < children.Count; i++)
                    if (!children[i].Disabled)
                        children[i].Update(gameTime);

                if (components == null)
                    components = new List<ObjectComponent>();

                // 更新此对象组件
                for (var index = 0; index < components.Count; index++)
                {
                    var component = components[index];
                    if (component.Disabled) continue;

                    if (SceneManager.IsEditor && component is ExtendedObjectComponent)
                    {
                        component.Update(gameTime);
                    }
                    else if (!SceneManager.IsEditor)
                    {
                        component.Update(gameTime);
                    }
                }

                //if (!SceneManager.IsEditor && body != null && !transform.physicsPositionReached)
                //{
                //    MoveBodyToPosition(transform.desiredPosition);
                //}

                if (mouseOver)
                {
                    var detected = SceneManager.ActiveScene.World.TestPoint(ConvertUnits.ToSimUnits(GameInput.MousePosition));
                    if (detected == null || detected.Body.GameObject != this && !SceneManager.IsEditor)
                    {
                        mouseOver = false;
                        OnMouseOut();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
            }
        }

        /// <summary>
        /// 绘制
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="spriteBatch"></param>
        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            try
            {
                // 绘制此对象组件
                for (var index = 0; index < components.Count; index++)
                {
                    var component = components[index];
                    if (component.Disabled) continue;

                    if (SceneManager.IsEditor && component is ExtendedObjectComponent)
                    {
                        component.Draw(gameTime, spriteBatch);
                    }
                    else if (!SceneManager.IsEditor)
                    {
                        component.Draw(gameTime, spriteBatch);
                    }
                }

                if (visible)
                {
                    // Draw this object children
                    for (int i = 0; i < children.Count; i++)
                        if (!children[i].Disabled)
                            children[i].Draw(gameTime, spriteBatch);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
            }
        }

        /// <summary>
        /// 获取自身的所有的组件
        /// </summary>
        /// <returns></returns>
        public List<ObjectComponent> GetComponents()
        {
            if (components == null)
                components = new List<ObjectComponent>();

            return components;
        }

        /// <summary>
        /// 移除所有组件
        /// </summary>
        public void RemoveAllComponents()
        {
            for (int i = components.Count - 1; i >= 0; i--)
            {
                components[i].Removed();
                RemoveComponent(components[i]);
            }

            for (var index = 0; index < children.Count; index++)
            {
                GameObject child = children[index];
                child.RemoveAllComponents();
            }
        }

        /// <summary>
        /// 移除组件
        /// </summary>
        /// <param name="component">组件</param>
        public void RemoveComponent(ObjectComponent component)
        {
            RemoveComponent(component, true);
        }

        /// <summary>
        /// 移除组件
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        public void RemoveComponent<T>()
        {
            for (int i = 0; i < components.Count; i++)
            {
                if (components[i] is T)
                {
                    components.Remove(components[i]);
                    return;
                }
            }

            Console.WriteLine("[警告] 未找到可以删除的组件:" + typeof(T).Name);
        }

        /// <summary>
        /// 保存对象为状态
        /// </summary>
        /// <param name="filename">文件名</param>
        public void Save(string filename)
        {
            CHelper.SerializeObject(filename, this);
        }

        internal void Delete()
        {
            body?.Dispose();

            if (transform.Parent == null)
                SceneManager.ActiveScene.GameObjects.Delete(this);
            else
                transform.Parent.GameObject.Children.Delete(this);
        }

        /// <summary>
        /// 当鼠标与游戏对象发生碰撞时抛出的事件
        /// </summary>
        public void OnMouseEnter()
        {
            for (var i = 0; i < components.Count; i++)
            {
                ObjectComponent component = components[i];
                if ((SceneManager.IsEditor && component is ExtendedObjectComponent) || !SceneManager.IsEditor)
                    if (!component.Disabled)
                        component.OnMouseEnter();
            }
        }

        /// <summary>
        /// 鼠标未在对象上时抛出的事件
        /// </summary>
        public void OnMouseOut()
        {
            // 向该对象移动鼠标的组件发送通知
            for (var i = 0; i < components.Count; i++)
            {
                ObjectComponent component = components[i];
                if ((SceneManager.IsEditor && component is ExtendedObjectComponent) || !SceneManager.IsEditor)
                    if (!component.Disabled)
                        component.OnMouseOut();
            }
        }

        /// <summary>
        /// 鼠标未在对象上时抛出的事件
        /// </summary>
        public void OnMouseUp()
        {
            // 向该对象移动鼠标的组件发送通知
            for (var i = 0; i < components.Count; i++)
            {
                ObjectComponent component = components[i];
                if ((SceneManager.IsEditor && component is ExtendedObjectComponent) || !SceneManager.IsEditor)
                    if (!component.Disabled)
                        component.OnMouseUp();
            }
        }

        /// <summary>
        /// 当鼠标单击碰撞时抛出事件
        /// </summary>
        /// <param name="buttonPressed"></param>
        public void OnMouseClick(MouseEventButton buttonPressed)
        {
            // 向单击此对象的组件发送通知
            for (var i = 0; i < components.Count; i++)
            {
                ObjectComponent component = components[i];
                if ((SceneManager.IsEditor && component is ExtendedObjectComponent) || !SceneManager.IsEditor)
                    if (!component.Disabled)
                        component.OnMouseClick(buttonPressed);
            }
        }

        /// <summary>
        /// 当鼠标按下碰撞时抛出事件
        /// </summary>
        /// <param name="buttonPressed"></param>
        public void OnMouseDown(MouseEventButton buttonPressed)
        {
            // 向该对象移动鼠标的组件发送通知
            for (var i = 0; i < components.Count; i++)
            {
                ObjectComponent component = components[i];
                if ((SceneManager.IsEditor && component is ExtendedObjectComponent) || !SceneManager.IsEditor)
                    if (!component.Disabled)
                        component.OnMouseDown(buttonPressed);
            }
        }

        /// <summary>
        /// 当鼠标移动碰撞时抛出事件
        /// </summary>
        public void OnMouseMove()
        {
            // 向该对象移动鼠标的组件发送通知
            for (var i = 0; i < components.Count; i++)
            {
                ObjectComponent component = components[i];
                if ((SceneManager.IsEditor && component is ExtendedObjectComponent) || !SceneManager.IsEditor)
                    if (!component.Disabled)
                        component.OnMouseMove();
            }
        }

        /// <summary>
        /// 初始化此对象
        /// </summary>
        public virtual void Initialize()
        {
            if (Body != null)
                Body = null;

            transform.GameObject = this;

            components = new List<ObjectComponent>();
            for (int i = componentReferences.Count - 1; i >= 0; i--)
            {
                string componentReference = componentReferences[i];

                Type _type = SceneManager.ScriptsAssembly.GetType(componentReference);

                if (_type == null)
                {
#if WIN
                    _type = Assembly.GetExecutingAssembly().GetType(componentReference); // system components
#elif WINRT
                    _type = typeof(GameObject).GetTypeInfo().Assembly.GetType(name);
#endif
                    //scriptsAssembly = false;
                }

                // still null? delete reference:
                //if (_type == null)
                //{
                //    componentReferences.RemoveAt(i);
                //}

                // (the user removed the component?)
                if (_type != null)
                {
                    try
                    {
                        var oc = (ObjectComponent)Activator.CreateInstance(_type);

                        //if (scriptsAssembly)
                        //    oc = (ObjectComponent)SceneManager.ScriptsAssembly // .CreateInstance(name);
                        //else
                        //    oc = (ObjectComponent)typeof(GameObject).GetTypeInfo().Assembly.CreateInstance(name);

                        oc.Transform = Transform;

                        LoadComponentValues(oc);

                        components.Insert(0, oc);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(
                            "尝试激活时出错 " + componentReference + " 在 " + Name + " : " +
                            ex.Message + "\n" + ex.StackTrace);
                    }
                }
            }

            // 初始化组件
            for (var index = 0; index < components.Count; index++)
            {
                var cmp = components[index];
                if (!SceneManager.IsEditor || (SceneManager.IsEditor && cmp is ExtendedObjectComponent))
                    cmp.Initialize();
            }

            if (children == null)
                children = new GameObjectCollection(this);

            // 初始化游戏子对象
            for (var index = 0; index < children.Count; index++)
            {
                GameObject gameObject = children[index];
                gameObject.Initialize();
            }

            Color.FromNonPremultiplied(255, 64, 0, 120);

            // 初始化碰撞模型
            //collisionModel.Initialize(this.transform);

            CheckAllAttributes();
        }

        /// <summary>
        /// 测量尺寸
        /// </summary>
        /// <returns></returns>
        public virtual RotatedRectangle MeasureDimension()
        {
            return new RotatedRectangle(new Rectangle((int)transform.position.X, (int)transform.position.Y, 1, 1));
        }

        /// <summary>
        /// 保存组件值
        /// </summary>
        public void SaveComponentValues()
        {
            if (components == null)
                components = new List<ObjectComponent>();

            for (var index = 0; index < components.Count; index++)
            {
                ObjectComponent component = components[index];
                try
                {
#if WIN
                    List<PropertyInfo> props = new List<PropertyInfo>(component.GetType().GetProperties());
#elif WINRT
                    List<PropertyInfo> props = new List<PropertyInfo>(component.GetType().GetRuntimeProperties());
#endif
                    for (var i = 0; i < props.Count; i++)
                    {
                        PropertyInfo propInfo = props[i];
                        PropertyLabel label = new PropertyLabel(propInfo.PropertyType.FullName, propInfo.Name);

                        //if(propInfo.GetValue(component, null).GetType().IsSerializable)
                        componentValues[component.GetType().FullName ?? throw new InvalidOperationException()][label] =
                            propInfo.GetValue(component, null);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("SaveComponentValues(): " + ex.Message);
                }
            }

            if (children == null)
                children = new GameObjectCollection(this);

            for (var index = 0; index < children.Count; index++)
            {
                GameObject gameObject = children[index];
                gameObject.SaveComponentValues();
            }
        }

        /// <summary>
        /// 添加组件
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <returns>组件</returns>
        public ObjectComponent AddComponent<T>() where T : ObjectComponent, new()
        {
            var type = new T();
            var result = AddComponent(type);
            if (result)
                return type;

            return null;
        }

        /// <summary>
        /// 添加组件
        /// </summary>
        /// <param name="component">组件</param>
        /// <returns>是否成功</returns>
        public bool AddComponent(ObjectComponent component)
        {
#if WIN
            if (!CheckAttributes(component))
                return false;

            // 检查现有组件属性：
            for (int i = 0; i < components.Count; i++)
            {
                var cmp = components[i];
                var info = cmp.GetType();
                object[] attributes = info.GetCustomAttributes(true);

                for (int j = 0; j < attributes.Length; j++)
                {
                    if (attributes[i] is Unique)
                    {
                        if (((Unique) attributes[i]).Options == Unique.UniqueOptions.Explicit)
                        {
                            if (component.GetType() == cmp.GetType())
                                return false;
                        }
                        else
                        {
                            if (component.GetType().IsInstanceOfType(cmp) || cmp.GetType().IsInstanceOfType(component))
                                return false;

                            var baseA = component.GetType().BaseType;
                            var baseB = cmp.GetType().BaseType;

                            while (true)
                            {
                                if (baseA == typeof(ExtendedObjectComponent) ||
                                    baseB == typeof(ExtendedObjectComponent) ||
                                    baseA == typeof(ObjectComponent) || baseB == typeof(ObjectComponent))
                                {
                                    break;
                                }
                                else if (baseA == baseB)
                                {
                                    return false;
                                }
                                else if (baseB != null &&
                                         (baseA != null &&
                                          (baseA.IsAssignableFrom(baseB) || baseB.IsAssignableFrom(baseA))))
                                {
                                    return false;
                                }

                                baseA = baseA?.BaseType;
                                baseB = baseB?.BaseType;
                            }
                        }
                    }
                }
            }
#endif
            // 该组件已分配？
            var fullName = component.GetType().FullName;
            if (fullName != null && !componentValues.ContainsKey(fullName))
            {
                var key = component.GetType().FullName;
                if (key != null)
                    componentValues[key] =
                        new Dictionary<PropertyLabel, object>(new PropertyLabel.EqualityComparer());
            }
            else
            {
                // 组件已添加，此处无关，返回。
                return false;
            }

            component.Transform = transform;
            component.Name = component.GetType().Name;

            components.Add(component);
            componentReferences.Add(component.GetType().FullName);

            // 浏览组件中的所有属性并分配它们
#if WIN
            List<PropertyInfo> props = new List<PropertyInfo>(component.GetType().GetProperties());
#elif WINRT
            List<PropertyInfo> props = new List<PropertyInfo>(component.GetType().GetRuntimeProperties());
#endif
            for (int i = 0; i < props.Count; i++)
            {
                var propInfo = props[i];
                PropertyLabel label = new PropertyLabel(propInfo.PropertyType.FullName, propInfo.Name);
                componentValues[component.GetType().FullName ?? throw new InvalidOperationException()][label] =
                    propInfo.GetValue(component, null);
            }

            if (!SceneManager.IsEditor)
            {
                component.Initialize();
            }
            else if (SceneManager.IsEditor && component is ExtendedObjectComponent)
            {
                component.Initialize();
            }

            return true;
        }

        /// <summary>
        /// 复制
        /// </summary>
        /// <returns>对象</returns>
        public GameObject Copy()
        {
            return (GameObject)Clone();
        }

        private void LoadComponentValues(ObjectComponent component)
        {
            // The component is assigned?
            var fullName = component.GetType().FullName;
            if (fullName != null && !componentValues.ContainsKey(fullName)) return;

#if WIN
            List<PropertyInfo> props = new List<PropertyInfo>(component.GetType().GetProperties());
#elif WINRT
            List<PropertyInfo> props = new List<PropertyInfo>(component.GetType().GetRuntimeProperties());
            foreach (PropertyInfo info in props)
            {
                Debug.Info("cc: " + component.GetType().Name + " c:" + component.Transform.gameObject + " b: " + pinfo.Name);
                Debug.Info("VALUE: " + pinfo.GetValue(component, null));
            }
#endif
            for (var index = 0; index < props.Count; index++)
            {
                PropertyInfo propInfo = props[index];
                try
                {
#if WINRT
                    bool found = false;
                    foreach (var item in componentValues[component.GetType().FullName])
                    {
                        if (item.Key.Name == propInfo.Name && item.Key.TypeName == propInfo.PropertyType.FullName)
                        {
                            propInfo.SetValue(component, componentValues[component.GetType().FullName][item.Key], null);
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        PropertyLabel label = new PropertyLabel(propInfo.PropertyType.FullName, propInfo.Name);
                        this.componentValues[component.GetType().FullName][label] = propInfo.GetValue(component, null);
                    }
#elif WIN
                    // 虚拟标签
                    PropertyLabel label = new PropertyLabel(propInfo.PropertyType.FullName, propInfo.Name);

                    // 有一个存储组件值的地方
                    if (!componentValues[component.GetType().FullName ?? throw new InvalidOperationException()].ContainsKey(label))
                    {
                        componentValues[component.GetType().FullName ?? throw new InvalidOperationException()][label] = propInfo.GetValue(component, null);
                    }
                    else
                    {
                        propInfo.SetValue(component, componentValues[component.GetType().FullName ?? throw new InvalidOperationException()][label], null);
                    }
#endif
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("属性未加载: " + ex);
                }
            }
        }

        private void CheckAllAttributes()
        {
            for (int i = components.Count - 1; i >= 0; i--)
            {
                CheckAttributes(components[i]);
            }
        }

        private bool CheckAttributes(ObjectComponent component)
        {
#if WIN
            MemberInfo info = component.GetType();
            object[] attributes = info.GetCustomAttributes(true);

            for (int i = 0; i < attributes.Length; i++)
            {
                if (attributes[i] is RequireComponent)
                {
                    bool found = false;
                    int foundCount = 0;
                    string[] typeNames = ((RequireComponent) attributes[i]).ComponentsTypeNames.Split('|');

                    for (var index = 0; index < typeNames.Length; index++)
                    {
                        string typeName = typeNames[index];
                        string typeNameTrim = typeName.Trim();

                        for (var j = 0; j < components.Count; j++)
                        {
                            var cmp = components[j];
                            if (cmp.GetType().Name.Equals(typeNameTrim))
                            {
                                foundCount++;
                                break;
                            }
                        }
                    }

                    if ((foundCount == typeNames.Length && ((RequireComponent) attributes[i]).requireAll) ||
                        (foundCount > 0 && !((RequireComponent) attributes[i]).requireAll))
                        found = true;

                    var fullName = component.GetType().FullName;
                    if (fullName != null && (!found && componentValues.ContainsKey(fullName)))
                        RemoveComponent(component, false);

                    if (!found)
                        return false;
                }
            }
#endif
            return true;
        }

        private void RemoveComponent(ObjectComponent component, bool checkAttributes)
        {
            component.Removed();

            components.Remove(component);
            componentReferences.Remove(component.GetType().FullName);
            var fullName = component.GetType().FullName;
            if (fullName != null) componentValues.Remove(fullName);

            if (checkAttributes)
                CheckAllAttributes();
        }

        /// <summary>
        /// 按名称删除活动场景中的游戏对象
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        public static GameObject Remove(string src)
        {
            if (SceneManager.ActiveScene == null) return null;

            for (var index = 0; index < SceneManager.ActiveScene.GameObjects.Count; index++)
            {
                GameObject gameObject = SceneManager.ActiveScene.GameObjects[index];
                if (gameObject.name == src)
                {
                    SceneManager.ActiveScene.GameObjects.Remove(gameObject);
                    return gameObject;
                }

                GameObject result = RemoveNext(gameObject, src);

                if (result != null)
                {
                    return result;
                }
            }

            // 未找到
            return null;
        }

        private static GameObject RemoveNext(GameObject gameObject, string src)
        {
            for (var index = 0; index < gameObject.Children.Count; index++)
            {
                GameObject _gameObject = gameObject.Children[index];
                if (_gameObject.name == src)
                {
                    gameObject.Children.Remove(gameObject);
                    return gameObject;
                }

                GameObject result = RemoveNext(_gameObject, src);

                // The game object was found?
                if (result != null)
                {
                    _gameObject.Children.Remove(result);
                    return result;
                }
            }

            // Not found
            return null;
        }

        /// <summary>
        /// 根据标签寻找对象
        /// </summary>
        /// <param name="src">标签名</param>
        /// <returns>对象</returns>
        public static GameObject FindByTag(string src)
        {
            return FindSingle(src, SearchOptions.Tag);
        }

        private static GameObject FindSingle(string src, SearchOptions searchOption)
        {
            if (SceneManager.ActiveScene == null) return null;

            for (var index = 0; index < SceneManager.ActiveScene.GameObjects.Count; index++)
            {
                GameObject gameObject = SceneManager.ActiveScene.GameObjects[index];
                GameObject result = FindNext(gameObject, src, searchOption);

                // The game object was found?
                if (result != null)
                    return result;
            }

            // Not found
            return null;
        }

        private static GameObject FindNext(GameObject gameObject, string src, SearchOptions searchOption)
        {
            if (FindComparer(gameObject, src, searchOption) != null) return gameObject;

            for (var index = 0; index < gameObject.Children.Count; index++)
            {
                GameObject _gameObject = gameObject.Children[index];
                GameObject result = FindNext(_gameObject, src, searchOption);

                // The game object was found?
                if (result != null)
                    return result;
            }

            // Not found
            return null;
        }

        private static GameObject FindComparer(GameObject gameObject, string src, SearchOptions searchOption)
        {
            switch (searchOption)
            {
                case SearchOptions.Hash:
                    if (gameObject.GetHashCode() == Convert.ToInt32(src))
                        return gameObject;
                    break;
                case SearchOptions.Name:
                    if (gameObject.name == src)
                        return gameObject;
                    break;
                case SearchOptions.Tag:
                    if (gameObject.Tag == src)
                        return gameObject;
                    break;
            }

            return null;
        }

        /// <summary>
        /// 转字符串
        /// </summary>
        /// <returns>对象名称</returns>
        public override string ToString()
        {
            return name;
        }

        /// <summary>
        /// 销毁
        /// </summary>
        public void Dispose()
        {
            for (var index = 0; index < Children.Count; index++)
            {
                GameObject obj = Children[index];
                obj.Dispose();
            }

            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 复制
        /// </summary>
        /// <returns>对象</returns>
        public object Clone()
        {
            var formatter = new BinaryFormatter();
            var stream = new MemoryStream();
            using (stream)
            {
                formatter.Serialize(stream, this);
                stream.Seek(0, SeekOrigin.Begin);
                var obj = (GameObject) formatter.Deserialize(stream);
                obj.Initialize();
				SceneManager.ActiveScene.GameObjects.Add(obj);
                return obj;
            }
        }

        /// <summary>
        /// 在没有与其他对象发生碰撞的框架中抛出的事件
        /// </summary>
        public void OnCollisionFree()
        {
            // 向组件发送通知
            for (var index = 0; index < components.Count; index++)
            {
                ObjectComponent component = components[index];
                if ((SceneManager.IsEditor && component is ExtendedObjectComponent) || !SceneManager.IsEditor)
                    if (!component.Disabled)
                        component.OnCollisionFree();
            }
        }

        void body_OnSeparation(Fixture fixtureA, Fixture fixtureB)
        {
            OnCollisionFree();
        }

        /// <summary>
        /// 当另一个对象与此冲突时抛出的事件
        /// </summary>
        /// <param name="other">另一个对象</param>
        public void OnCollisionEnter(GameObject other)
        {
            // 向此对象与其他对象发生冲突的组件发送通知
            for (var index = 0; index < components.Count; index++)
            {
                ObjectComponent component = components[index];
                if ((SceneManager.IsEditor && component is ExtendedObjectComponent) || !SceneManager.IsEditor)
                    if (!component.Disabled)
                        component.OnCollisionEnter(other);
            }
        }

        bool body_OnCollision(Fixture fixtureA, Fixture fixtureB, FarseerPhysics.Dynamics.Contacts.Contact contact)
        {
            OnCollisionEnter(fixtureB.Body.GameObject);
            return true;
        }
    }
}