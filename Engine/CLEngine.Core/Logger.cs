﻿using System;
using System.Collections.Generic;

namespace CLEngine.Core
{
	/// <summary>
	/// 日志类型
	/// </summary>
	public enum LogType
	{
		/// <summary>
		/// 消息
		/// </summary>
		Info,
		/// <summary>
		/// 警告
		/// </summary>
		Warn,
		/// <summary>
		/// 错误
		/// </summary>
		Error,
	}

	/// <summary>
	/// 日志类
	/// </summary>
	public class Log
	{
		/// <summary>
		/// 消息内容
		/// </summary>
		public string Message { get; set; }
		/// <summary>
		/// 消息类型
		/// </summary>
		public LogType Type { get; set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		/// <param name="type"></param>
		public Log(string message, LogType type = LogType.Info)
		{
			Message = message;
			Type = type;
		}
	}

	/// <summary>
	/// 记录编辑器/核心日志内容
	/// </summary>
    public static class Logger
    {
        #region fields

        /// <summary>
        /// 消息队列
        /// </summary>
        public static readonly Queue<Log> Messages;

		#endregion

		static Logger()
		{
			Messages = new Queue<Log>();
		}

        #region methods

		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
        public static void Info(string message)
        {
			Messages.Enqueue(new Log(message));
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		public static void Warn(string message)
		{
			Messages.Enqueue(new Log(message, LogType.Warn));
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		public static void Error(string message)
		{
			Messages.Enqueue(new Log(message, LogType.Error));
		}

		#endregion
    }
}