﻿namespace CLEngine.Core
{
    /// <summary>
    /// startCoroutine返回的接口，它提供了在中途停止协同程序的能力
    /// </summary>
    public interface ICoroutine
    {
        /// <summary>
        /// 停止Coroutine
        /// </summary>
        void Stop();

        /// <summary>
        /// 设置Coroutine是否应使用deltaTime或unscaledDeltaTime进行计时
        /// </summary>
        /// <returns>The use unscaled delta time.</returns>
        /// <param name="useUnscaledDeltaTime">If set to <c>true</c> use unscaled delta time.</param>
        ICoroutine SetUseUnscaledDeltaTime(bool useUnscaledDeltaTime);
    }

    public static class Coroutine
    {
        /// <summary>
        /// causes a Coroutine to pause for the specified duration. Yield on Coroutine.waitForSeconds in a coroutine to use.
        /// </summary>
        /// <returns>The for seconds.</returns>
        /// <param name="seconds">Seconds.</param>
        public static object waitForSeconds(float seconds)
        {
            return WaitForSeconds.waiter.wait(seconds);
        }
    }

    class WaitForSeconds
    {
        internal static WaitForSeconds waiter = new WaitForSeconds();
        internal float waitTime;

        internal WaitForSeconds wait(float seconds)
        {
            waiter.waitTime = seconds;
            return waiter;
        }
    }
}