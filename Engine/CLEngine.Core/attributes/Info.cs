﻿namespace CLEngine.Core.attributes
{
    public class Info : System.Attribute
    {
        public readonly string Value;

        public Info(string info)
        {
            this.Value = info;
        }
    }
}