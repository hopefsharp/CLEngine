﻿using System.ComponentModel;
using CLEngine.Core.components;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;

namespace CLEngine.Core.attributes
{
    [Unique(Unique.UniqueOptions.AssignableFrom)]
    [Info("Physical Body:\n一个非常简单的物理体，没有形状.")]
    public class PhysicalBody : ExtendedObjectComponent
    {
        #region fields

        //private float mass = 1;
        private bool fixedRotation = false;
        private float friction = 0.5f;
        private float restitution = 0.2f;
        private BodyType bodyType = BodyType.Dynamic;
        private bool isSensor = false;
        private bool isBullet = false;
        private float angularVelocity = 0;
        private float angularDamping = 0;
        private bool ignoreGravity = false;
        private float linearDamping = 0;

        #endregion

        #region properties

        /// <summary>
        /// 物体的线性阻尼
        /// </summary>
#if WIN
        [Category("刚体属性"), DisplayName("线性阻尼")]
#endif
        public float LinearDamping
        {
            get { return linearDamping; }
            set
            {
                linearDamping = value;

                if (Transform.GameObject.Body != null)
                    Transform.gameObject.Body.LinearDamping = value;
            }
        }

        /// <summary>
        /// 确定身体是否是子弹
        /// </summary>
#if WIN
        [Category("刚体属性"), DisplayName("是否子弹")]
#endif
        public bool IsBullet
        {
            get { return isBullet; }
            set
            {
                isBullet = value;

                if (Transform.GameObject.Body != null)
                    Transform.gameObject.Body.IsBullet = value;
            }
        }

        /// <summary>
        /// 确定身体是否应忽略重力
        /// </summary>
#if WIN
        [Category("刚体属性"), DisplayName("忽略重力")]
#endif
        public bool IgnoreGravity
        {
            get { return ignoreGravity; }
            set
            {
                ignoreGravity = value;

                if (Transform.GameObject.Body != null)
                    if (value)
                        Transform.GameObject.Body.GravityScale = 0;
                    else
                        Transform.GameObject.Body.GravityScale = 1;
            }
        }

        /// <summary>
        /// 物体的角速度
        /// </summary>
#if WIN
        [Category("刚体属性"), DisplayName("角速度")]
#endif
        public float AngularVelocity
        {
            get { return angularVelocity; }
            set
            {
                angularVelocity = value;

                if (Transform.GameObject.Body != null)
                    Transform.GameObject.Body.AngularVelocity = value;
            }
        }

        /// <summary>
        /// 物体的角度阻尼
        /// </summary>
#if WIN
        [Category("刚体属性"), DisplayName("角度阻尼")]
#endif
        public float AngularDamping
        {
            get { return angularDamping; }
            set
            {
                angularDamping = value;

                if (Transform.GameObject.Body != null)
                    Transform.GameObject.Body.AngularDamping = value;
            }
        }

        /// <summary>
        /// 确定刚体是否应该像传感器一样
        /// </summary>
#if WIN
        [Category("刚体属性"), DisplayName("是否传感器")]
#endif
        public bool IsSensor
        {
            get { return isSensor; }
            set
            {
                isSensor = value;

                if (Transform.GameObject.Body != null && !SceneManager.IsEditor)
                    Transform.GameObject.Body.IsSensor = value;
            }
        }

        ///// <summary>
        ///// The mass of the body
        ///// </summary>
        //[Category("Physical Body Properties"), DisplayName("Mass")]
        //public float Mass
        //{
        //    get
        //    {
        //        return mass;
        //    }
        //    set
        //    {
        //        mass = value;

        //        if (Transform.GameObject.Body != null)
        //            Transform.GameObject.Body.Mass = value;
        //    }
        //}

        /// <summary>
        /// 确定主体是否具有固定旋转
        /// </summary>
#if WIN
        [Category("刚体属性"), DisplayName("固定旋转")]
#endif
        public bool FixedRotation
        {
            get { return fixedRotation; }
            set
            {
                fixedRotation = value;

                if (Transform.GameObject.Body != null)
                    Transform.GameObject.Body.FixedRotation = value;
            }
        }

        /// <summary>
        /// 身体的摩擦力
        /// </summary>
#if WIN
        [Category("刚体属性"), DisplayName("摩擦力")]
#endif
        public float Friction
        {
            get { return friction; }
            set
            {
                friction = value;

                if (Transform.GameObject.Body != null)
                    Transform.GameObject.Body.Friction = value;
            }
        }

        /// <summary>
        /// 身体弹起
        /// </summary>
#if WIN
        [Category("刚体属性"), DisplayName("弹起")]
#endif
        public float Restitution
        {
            get { return restitution; }
            set
            {
                restitution = value;

                if (Transform.GameObject.Body != null)
                    Transform.GameObject.Body.Restitution = value;
            }
        }

        /// <summary>
        /// 身体类型
        /// </summary>
#if WIN
        [Category("刚体属性"), DisplayName("刚体类型")]
#endif
        public BodyType BodyType
        {
            get { return bodyType; }
            set
            {
                bodyType = value;

                if (Transform.GameObject.Body != null)
                    Transform.GameObject.Body.BodyType = value;
            }
        }

        #endregion

        #region methods

        /// <summary>
        /// 初始化
        /// </summary>
        public override void Initialize()
        {
            Transform.GameObject.Body = BodyFactory.CreateBody(SceneManager.ActiveScene.World);

            Transform.Position = Transform.position;
            Transform.Rotation = Transform.rotation;

            
            UpdateBodyProperties();
        }

        /// <summary>
        /// 移除组件后，将刚体从世界中移除
        /// </summary>
        public override void Removed()
        {
            Transform.GameObject.Body = null;
        }

        /// <summary>
        /// 更新刚体属性
        /// </summary>
        protected virtual void UpdateBodyProperties()
        {
            if (Transform.GameObject.Body == null) return;

            Transform.GameObject.Body.BodyType = bodyType;
            Transform.GameObject.Body.Restitution = restitution;
            Transform.GameObject.Body.Friction = friction;
            Transform.GameObject.Body.FixedRotation = fixedRotation;
            if (SceneManager.IsEditor)
                Transform.GameObject.Body.IsSensor = true;
            else
                Transform.GameObject.Body.IsSensor = isSensor;
            Transform.GameObject.Body.AngularDamping = angularDamping;
            Transform.GameObject.Body.AngularVelocity = angularVelocity;
            Transform.gameObject.Body.IsBullet = isBullet;
            Transform.gameObject.Body.LinearDamping = linearDamping;

            if (ignoreGravity)
                Transform.GameObject.Body.GravityScale = 0;

            //Transform.GameObject.Body.Mass = mass;
        }

        internal virtual void ResetBody()
        {

        }

        #endregion
    }
}