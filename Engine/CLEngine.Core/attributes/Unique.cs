﻿using System;

namespace CLEngine.Core.attributes
{
    /// <summary>
    /// 用于确定组件是否应该每个对象只有1个实例的属性（显式或可分配）
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public class Unique : System.Attribute
    {
        /// <summary>
        /// Explicit'是组件类型本身。
        ///'AssinableFrom'也可以从父分配（例如，是组件的子类）。
        /// </summary>
        public enum UniqueOptions { Explicit, AssignableFrom }

        public UniqueOptions Options
        {
            get
            {
                return options;
            }
            set
            {

                options = value;
            }
        }

        public Unique()
        {
            this.Options = UniqueOptions.Explicit;
        }

        public Unique(UniqueOptions options)
        {
            this.Options = options;
        }

        private UniqueOptions options;
    }
}