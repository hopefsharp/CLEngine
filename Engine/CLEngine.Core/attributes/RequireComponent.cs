﻿using System;

namespace CLEngine.Core.attributes
{
    /// <summary>
    /// 向对象添加一个或多个组件要求的属性。
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class RequireComponent : Attribute
    {
        public readonly string ComponentsTypeNames;

        public readonly bool requireAll;

        /// <summary>
        /// 基于类型名称的要求
        /// </summary>
        /// <param name="componentTypeNames">类型名称，如果要应用多个要求，请使用“|” 拆分他们.</param>
        /// <param name="requireAll">如果为true则需要每个组件</param>
        public RequireComponent(string componentTypeNames, bool requireAll = true)
        {
            this.ComponentsTypeNames = componentTypeNames;
            this.requireAll = requireAll;
        }
    }
}