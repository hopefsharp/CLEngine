﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace CLEngine.Core
{
    /// <summary>
    /// 基本的CoroutineManager。 协同程序可以执行以下操作：
    /// -  yield return null
    /// -  yield return Coroutine.waitForSeconds(3)
    /// -  yield return Coroutine.waitForSeconds(5.5f)
    /// -  yield return startCoroutine(another())
    /// </summary>
    public class CoroutineManager
    {
        /// <summary>
        /// CoroutineManager用来隐藏Coroutine所需数据的内部类
        /// </summary>
        class CoroutineImpl : ICoroutine, IPoolable
        {
            public IEnumerator enumerator;
            /// <summary>
            /// 无论何时产生延迟，都会将其添加到跟踪延迟的waitTimer中
            /// </summary>
            public float waitTimer;
            public bool isDone;
            public CoroutineImpl waitForCoroutine;
            public bool useUnscaledDeltaTime = false;


            public void Stop()
            {
                isDone = true;
            }


            public ICoroutine SetUseUnscaledDeltaTime(bool useUnscaledDeltaTime)
            {
                this.useUnscaledDeltaTime = useUnscaledDeltaTime;
                return this;
            }


            internal void prepareForReuse()
            {
                isDone = false;
            }


            void IPoolable.reset()
            {
                isDone = true;
                waitTimer = 0;
                waitForCoroutine = null;
                enumerator = null;
                useUnscaledDeltaTime = false;
            }
        }

        static bool _isInUpdate;
        static List<CoroutineImpl> _unblockedCoroutines = new List<CoroutineImpl>();
        static List<CoroutineImpl> _shouldRunNextFrame = new List<CoroutineImpl>();

        internal static ICoroutine StartCoroutine(IEnumerator enumerator)
        {
            // find or create a CoroutineImpl
            var coroutine = Pool<CoroutineImpl>.obtain();
            coroutine.prepareForReuse();

            // setup the coroutine and add it
            coroutine.enumerator = enumerator;
            var shouldContinueCoroutine = tickCoroutine(coroutine);

            // guard against empty coroutines
            if (!shouldContinueCoroutine)
                return null;

            if (_isInUpdate)
                _shouldRunNextFrame.Add(coroutine);
            else
                _unblockedCoroutines.Add(coroutine);

            return coroutine;
        }

        internal static void Update()
        {
            _isInUpdate = true;
            for (var i = 0; i < _unblockedCoroutines.Count; i++)
            {
                var coroutine = _unblockedCoroutines[i];

                // check for stopped coroutines
                if (coroutine.isDone)
                {
                    Pool<CoroutineImpl>.free(coroutine);
                    continue;
                }

                // are we waiting for any other coroutines to finish?
                if (coroutine.waitForCoroutine != null)
                {
                    if (coroutine.waitForCoroutine.isDone)
                    {
                        coroutine.waitForCoroutine = null;
                    }
                    else
                    {
                        _shouldRunNextFrame.Add(coroutine);
                        continue;
                    }
                }

                // deal with timers if we have them
                if (coroutine.waitTimer > 0)
                {
                    // still has time left. decrement and run again next frame being sure to decrement with the appropriate deltaTime.
                    coroutine.waitTimer -= coroutine.useUnscaledDeltaTime ? Time.unscaledDeltaTime : Time.deltaTime;
                    _shouldRunNextFrame.Add(coroutine);
                    continue;
                }

                if (tickCoroutine(coroutine))
                    _shouldRunNextFrame.Add(coroutine);
            }

            _unblockedCoroutines.Clear();
            _unblockedCoroutines.AddRange(_shouldRunNextFrame);
            _shouldRunNextFrame.Clear();

            _isInUpdate = false;
        }

        static bool tickCoroutine(CoroutineImpl coroutine)
        {
            // This coroutine has finished
            if (!coroutine.enumerator.MoveNext() || coroutine.isDone)
            {
                Pool<CoroutineImpl>.free(coroutine);
                return false;
            }

            if (coroutine.enumerator.Current == null)
            {
                return true;
            }

            if (coroutine.enumerator.Current is WaitForSeconds)
            {
                coroutine.waitTimer = (coroutine.enumerator.Current as WaitForSeconds).waitTime;
                return true;
            }

#if DEBUG
            // 产生int / float的弃用警告
            if (coroutine.enumerator.Current is int)
            {
                Console.WriteLine("[错误] 产生Coroutine.waitForSeconds而不是int。 产生int将不适用于发布版本.");
                coroutine.waitTimer = (int)coroutine.enumerator.Current;
                return true;
            }

            if (coroutine.enumerator.Current is float)
            {
                Console.WriteLine("[错误] 产生Coroutine.waitForSeconds而不是float。 产生一个浮点数不适用于发布版本.");
                coroutine.waitTimer = (float)coroutine.enumerator.Current;
                return true;
            }
#endif

            if (coroutine.enumerator.Current is CoroutineImpl)
            {
                coroutine.waitForCoroutine = coroutine.enumerator.Current as CoroutineImpl;
                return true;
            }
            else
            {
                // This coroutine yielded some value we don't understand. run it next frame.
                return true;
            }
        }
    }
}