﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CLEngine.Core
{
#if WIN
    [Serializable]
#endif
    [DataContract]
    public class PropertyLabel
    {
#if WIN
        [Serializable]
#endif
        [DataContract]
        public class EqualityComparer : IEqualityComparer<PropertyLabel>
        {
            public bool Equals(PropertyLabel x, PropertyLabel y)
            {
                return x.Equals(y);
            }

            public int GetHashCode(PropertyLabel obj)
            {
                return base.GetHashCode();
            }
        }

        [DataMember]
        private string typeName;

        [DataMember]
        private string name;

        public PropertyLabel(string typeName, string name)
        {
            this.typeName = typeName;
            this.name = name;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public bool Equals(PropertyLabel other)
        {
            return (this.typeName.Equals(other.typeName) && this.name.Equals(other.name));
        }


        public static bool operator ==(PropertyLabel p1, PropertyLabel p2)
        {
            return p1.Equals(p2);
        }

        public static bool operator !=(PropertyLabel p1, PropertyLabel p2)
        {
            return !p1.Equals(p2);
        }

        public override string ToString()
        {
            return this.name + ", " + this.typeName;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}