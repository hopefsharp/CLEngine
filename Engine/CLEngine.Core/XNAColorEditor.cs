﻿using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using Microsoft.Xna.Framework;

namespace CLEngine.Core
{
    public class XNAColorEditor : UITypeEditor
    {
        private IWindowsFormsEditorService service;

        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            // 这告诉它显示可点击的按钮，触发下面的EditValue。
            return UITypeEditorEditStyle.Modal;
        }
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            if (provider != null)
                service = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));

            if (service != null)
            {
                // 这是您在单击[...]并在验证之后要运行的代码。

                // 我们目前选择的颜色。
                Color c = (Color)value;

                // 创建ColorDialog的新实例
                ColorDialog selectionControl = new ColorDialog();

                // 在对话框中设置所选颜色
                selectionControl.Color = System.Drawing.Color.FromArgb(c.A, c.R, c.G, c.B);

                // 显示对话框.
                selectionControl.ShowDialog();

                // 返回新选择的颜色
                value = Color.FromNonPremultiplied(selectionControl.Color.R,
                    selectionControl.Color.G,
                    selectionControl.Color.B,
                    selectionControl.Color.A);
            }

            return value;
        }
    }
}