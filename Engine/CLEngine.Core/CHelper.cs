﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

namespace CLEngine.Core
{
	/// <summary>
	/// 一个静态类，为游戏开发编辑器或引擎提供有用的方法
	/// </summary>
	public static class CHelper
	{
		private static readonly string _updaterPath = Path.Combine(Application.StartupPath, "qupdater.exe");

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public static int CheckNewFiles()
		{
			return RunUpdater("-checkforupdates", true);
		}

		/// <summary>
		/// 
		/// </summary>
		public static void RunUpdater()
		{
			RunUpdater(string.Empty, false);
		}

		private static int RunUpdater(string arguments, bool waitForExit)
		{
			try
			{
				var info = new FileInfo(_updaterPath);
				if (!info.Exists)
				{
					return 0;
				}

				var info2 = new ProcessStartInfo { FileName = info.FullName };
				if (info.Directory != null) info2.WorkingDirectory = info.Directory.FullName;
				info2.Arguments = arguments;
				var process = new Process { StartInfo = info2 };
				process.Start();
				if (!waitForExit)
				{
					return 0;
				}
				process.WaitForExit();

				return process.ExitCode;
			}
			catch (Exception)
			{
				// ignored
			}

			return 0;
		}

#if WIN
		public static void SerializeObject(string filename, object objectToSerialize)
		{
			try
			{
				MemoryStream stream = new MemoryStream();
				BinaryFormatter formatter = new BinaryFormatter();
				formatter.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
				formatter.Serialize(stream, objectToSerialize);
				byte[] serializedData = stream.ToArray();

				File.WriteAllBytes(filename, serializedData);
			}
			catch (Exception ex)
			{
				Console.Write(string.Format("错误!\n错误信息: {0}\n{1}", ex.Message, ex.StackTrace));
			}
		}
#elif WINRT
        public async static void SerializeObject(string filename, object objectToSerialize)
        {
            await Serializer.Serialize(ApplicationData.Current.LocalFolder, filename, objectToSerialize);
        }
#endif

#if WIN
		public static void SerializeObjectXML(string filename, object objectToSerialize)
		{
			try
			{
				MemoryStream stream = new MemoryStream();
				DataContractSerializer serializer = new DataContractSerializer(objectToSerialize.GetType(), null,
					int.MaxValue, false, true, null);

				var settings = new XmlWriterSettings()
				{
					Indent = true,
					IndentChars = "\t"
				};

				settings.NamespaceHandling = NamespaceHandling.OmitDuplicates;
				//settings.OutputMethod = XmlOutputMethod.Xml;
				settings.ConformanceLevel = ConformanceLevel.Fragment;

				using (XmlDictionaryWriter writer =
					XmlDictionaryWriter.CreateDictionaryWriter(XmlWriter.Create(stream, settings)))
				{
					serializer.WriteObject(writer, objectToSerialize, new MyCustomerResolver());
				}

				byte[] serializedData = stream.ToArray();
				File.WriteAllBytes(filename, serializedData);

				//var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
				//var serializer = new XmlSerializer(objectToSerialize.GetType());
				//var settings = new XmlWriterSettings();
				//settings.Indent = true;
				//settings.OmitXmlDeclaration = true;

				//MemoryStream stream = new MemoryStream();
				//using (var writer = XmlWriter.Create(stream, settings))
				//{
				//    serializer.Serialize(writer, objectToSerialize, emptyNamepsaces);
				//}

				//byte[] serializedData = stream.ToArray();
				//File.WriteAllBytes(filename, serializedData);

				//stream.Dispose();
			}
			catch (Exception ex)
			{
				Console.Write(string.Format("Error!\nError Message: {0}\n{1}", ex.Message, ex.StackTrace));
			}
		}
#endif

#if WIN
		public static object DeserializeObject(string filename)
		{
			try
			{
				byte[] bytes = File.ReadAllBytes(filename);

				MemoryStream stream = new MemoryStream(bytes);
				BinaryFormatter formatter = new BinaryFormatter();
				formatter.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
				formatter.Binder = new VersionConfigToNamespaceAssemblyObjectBinder();

				return (Object)formatter.Deserialize(stream);
			}
			catch (Exception ex)
			{
				Console.Write(string.Format("反序列化时出错!\n错误信息: {0}\n{1}", ex.Message, ex.StackTrace));
				return null;
			}

		}
#elif WINRT
        public static object DeserializeObject(Type type, string filename)
        {
            Task<object> t = new Task<object>(() =>
            {
                //Debug.Info("LE STUPID: " + Windows.ApplicationModel.Package.Current.InstalledLocation.Path +  "\\" + filename);
                return Serializer.Deserialize(type, Windows.ApplicationModel.Package.Current.InstalledLocation, filename).Result;
            });

            t.Start();
            return t.Result;
        }
#endif

		private static readonly Random random = new Random();
		private static readonly object syncLock = new object();

		public static int RandomNumber(int min, int max)
		{
			lock (syncLock)
			{
				return random.Next(min, max);
			}
		}

		/// <summary>
		/// 创建从一个文件或文件夹到另一个文件或文件夹的相对路径
		/// </summary>
		/// <param name="fromPath"></param>
		/// <param name="toPath"></param>
		/// <returns></returns>
		public static String MakeExclusiveRelativePath(String fromPath, String toPath)
		{
			if (String.IsNullOrEmpty(fromPath)) throw new ArgumentNullException("fromPath");
			if (String.IsNullOrEmpty(toPath)) throw new ArgumentNullException("toPath");

#if WIN
			return toPath.Replace(fromPath, string.Empty).TrimStart(System.IO.Path.DirectorySeparatorChar);
#elif WINRT
            return toPath.Replace(fromPath, string.Empty).TrimStart('\\');
#endif
		}

		public static string EncryptMD5(string input)
		{
#if WIN
			MD5 md5Hasher = MD5.Create();
			byte[] data = md5Hasher.ComputeHash(Encoding.UTF8.GetBytes(input));

			// 创建一个新的Stringbuilder来收集字节
			// 并创建一个字符串。
			StringBuilder sBuilder = new StringBuilder();

			// 循环遍历散列数据的每个字节
			// 并将每个格式化为十六进制字符串。
			for (int i = 0; i < data.Length; i++)
			{
				sBuilder.Append(data[i].ToString("x2"));
			}

			// 返回十六进制字符串
			return sBuilder.ToString();
#elif WINRT
            // TODO: 
            throw new NotImplementedException();
#endif
		}

#if WIN
		/// <summary>
		/// 将整个目录复制到目标
		/// </summary>
		/// <param name="sourceDirName">Source directory path</param>
		/// <param name="destDirName">Destination path</param>
		/// <param name="copySubDirs">Copy sub directories</param>
		public static void CopyDirectory(string sourceDirName, string destDirName, bool copySubDirs)
		{
			DirectoryInfo dir = new DirectoryInfo(sourceDirName);
			DirectoryInfo[] dirs = dir.GetDirectories();

			// 如果源目录不存在，则抛出异常
			if (!dir.Exists)
			{
				throw new DirectoryNotFoundException(
					"源目录不存在或找不到: "
					+ sourceDirName);
			}

			// 如果目标目录不存在，请创建它
			if (!Directory.Exists(destDirName))
			{
				Directory.CreateDirectory(destDirName);
			}

			// 获取要复制的目录的文件内容
			FileInfo[] files = dir.GetFiles();

			foreach (FileInfo file in files)
			{
				// 创建文件新副本的路径
				string temppath = System.IO.Path.Combine(destDirName, file.Name);

				// 复制文件
				file.CopyTo(temppath, true);
			}

			// 如果copySubDirs为true，则复制子目录
			if (copySubDirs)
			{
				foreach (DirectoryInfo subdir in dirs)
				{
					// Create the subdirectory.
					string temppath = System.IO.Path.Combine(destDirName, subdir.Name);

					// Copy the subdirectories.
					CopyDirectory(subdir.FullName, temppath, copySubDirs);
				}
			}
		}
#endif

		public static string SplitCamelCase(string str)
		{
			string camelCase = Regex.Replace(Regex.Replace(str, @"(\P{Ll})(\P{Ll}\p{Ll})", "$1 $2"),
				@"(\p{Ll})(\P{Ll})", "$1 $2");

			// 删除双空格并返回
			return Regex.Replace(camelCase, @"\s+", " ");

		}

		/// <summary>
		/// 改变Settings.ini文件为release模式
		/// </summary>
		public static void ChangeSettingsToRelease(string path)
		{
			var destination = Path.Combine(path, "settings.ini");
			var iniFile = new IniFile(destination);
			if (!File.Exists(destination))
				return;

			iniFile.IniWriteValue("Profile", "Visible", "False");
			iniFile.IniWriteValue("Console", "Visible", "False");
			iniFile.IniWriteValue("Console", "WriteToConsole", "False");
		}
	}

#if WIN
	internal sealed class VersionConfigToNamespaceAssemblyObjectBinder : SerializationBinder
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="assemblyName"></param>
		/// <param name="typeName"></param>
		/// <returns></returns>
		public override Type BindToType(string assemblyName, string typeName)
		{
			Type typeToDeserialize = null;

			try
			{
				string ToAssemblyName = assemblyName.Split(',')[0];

				Assembly[] Assemblies = AppDomain.CurrentDomain.GetAssemblies();
				foreach (Assembly ass in Assemblies)
				{
					if (ass.FullName.Split(',')[0] == ToAssemblyName)
					{
						typeToDeserialize = ass.GetType(typeName);

						break;
					}
				}
			}

			catch (System.Exception exception)
			{
				throw exception;
			}

			return typeToDeserialize;
		}
	}
#endif

	public class MyCustomerResolver : DataContractResolver
	{
		public override bool TryResolveType(Type dataContractType, Type declaredType,
			DataContractResolver knownTypeResolver, out XmlDictionaryString typeName,
			out XmlDictionaryString typeNamespace)
		{
			//return knownTypeResolver.TryResolveType(dataContractType, declaredType, null, out typeName, out typeNamespace);
			//XmlDictionary dictionary = new XmlDictionary();
			//typeName = dictionary.Add(dataContractType.FullName);
			//typeNamespace = dictionary.Add(dataContractType.Namespace);
			////knownTypeResolver.TryResolveType(dataContractType, declaredType, null, out typeName, out typeNamespace);
			//return true;

			//if (dataContractType == typeof(Customer))
			//{
			//    XmlDictionary dictionary = new XmlDictionary();
			//    typeName = dictionary.Add("SomeCustomer");
			//    typeNamespace = dictionary.Add("http://tempuri.com");
			//    return true;
			//}
			//else
			//{
			//    return knownTypeResolver.TryResolveType(dataContractType, declaredType, null, out typeName, out typeNamespace);
			//}

			string name = dataContractType.Name;
			string namesp = dataContractType.Namespace;
			typeName = new XmlDictionaryString(XmlDictionary.Empty, name, 0);
			typeNamespace = new XmlDictionaryString(XmlDictionary.Empty, namesp, 0);

			return true;
		}

		public override Type ResolveName(string typeName, string typeNamespace, Type declaredType,
			DataContractResolver knownTypeResolver)
		{
			//Console.Info(typeNamespace + "." + typeName);
			//Type tx = declaredType;
			//Console.Info(tx);
			//Type t = Assembly.GetExecutingAssembly().GetType(typeName);
			//return t;
			//Debug.Info("TESTE::: " + typeName + "::" + declaredType.Name);
			//return declaredType;

			//Debug.Info("TESTE: " + typeNamespace + "." + typeName + " (" + declaredType + ")");
			Type t = SceneManager.ScriptsAssembly.GetType(typeNamespace + "." + typeName);

#if WINRT
            if (t == null)
            {
                //Debug.Info("M: " + typeof(object).GetTypeInfo().Assembly.GetType(typeNamespace + "." + typeName));
                t = declaredType.GetType().GetTypeInfo().Assembly.GetType(typeNamespace + "." + typeName);
            }
#endif
			//if (typeName == "Single")
			//    return typeof(Single);
			//else if (typeName == "Boolean")
			//    return typeof(Boolean);
			//else if (typeName.ToLower() == "string")
			//    return typeof(string);
			//else if (typeName.ToLower() == "float")
			//    return typeof(float);
			//else if (typeName.ToLower() == "int32")
			//    return typeof(Int32);

			return t;
		}
	}
}