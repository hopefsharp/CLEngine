﻿namespace CLEngine.Core
{
    public interface ITimer
    {
        object Context { get; }


        /// <summary>
        /// 调用停止以停止再次运行此计时器。 这对非重复计时器没有影响。
        /// </summary>
        void Stop();

        /// <summary>
        /// 将计时器的经过时间重置为0
        /// </summary>
        void Reset();

        /// <summary>
        /// 为方便起见，返回转换为T的上下文
        /// </summary>
        /// <returns>The context.</returns>
        /// <typeparam name="T">第一类参数.</typeparam>
        T GetContext<T>();
    }
}