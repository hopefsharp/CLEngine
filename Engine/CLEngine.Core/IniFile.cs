﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace CLEngine.Core
{
    /// <summary>
    /// 创建一个新的INI文件来存储或加载数据
    /// </summary>
    public class IniFile
    {
        public string path;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section,
            string key, string val, string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section,
            string key, string def, StringBuilder retVal,
            int size, string filePath);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="INIPath"></param>
        public IniFile(string INIPath)
        {
            path = INIPath;
            checkForCreate();
        }

        /// <summary>
        /// 检查文件是否存在
        /// 如果不存在默认则创建一个文件
        /// </summary>
        private void checkForCreate()
        {
            if (!File.Exists(path))
                using (File.Create(path))
                {
                    // TODO： 创建文件后我们需要为里面添加什么？
                }
        }

        /// <summary>
        /// 将数据写入INI文件
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// Section name
        /// <PARAM name="Key"></PARAM>
        /// Key Name
        /// <PARAM name="Value"></PARAM>
        /// Value Name
        public void IniWriteValue(string Section, string Key, string Value)
        {
            WritePrivateProfileString(Section, Key, Value, this.path);
        }

        /// <summary>
        /// 从Ini文件中读取数据值
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// <PARAM name="Key"></PARAM>
        /// <PARAM name="Path"></PARAM>
        /// <returns></returns>
        public string IniReadValue(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(1024);
            int i = GetPrivateProfileString(Section, Key, "", temp,
                1024, this.path);
            return temp.ToString();
        }

        /// <summary>
        /// 读取所有节点
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public List<string> ReadSections(string fileName)
        {
            var result = new List<string>();
            var buffer = new StringBuilder();
            var length = GetPrivateProfileString(null, null, null, buffer, buffer.Length, path);

            for (var i = 0; i < length; i++)
                if (buffer[i] == 0)
                    result.Add(buffer.ToString());

            return result;
        }
    }
}