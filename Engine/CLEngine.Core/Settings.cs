﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace CLEngine.Core
{
#if WIN
    [Serializable, TypeConverter(typeof(ExpandableObjectConverter))]
#endif
    [DataContract]
    public class Settings
    {
        /// <summary>
        /// 凸多边形上的最大顶点数
        /// </summary>
        public static int MaxPolygonVertices = 8;

#if WIN
        [NonSerialized]
#endif
        private IniFile settingsFile;

#if WIN
        [NonSerialized]
#endif
        private string rootPath;


#if WIN
        [NonSerialized]
#endif
        private int virtualScreenWidth = 0, virtualScreenHeight = 0;

#if WIN
        [Browsable(false)]
#endif
        public int VirtualScreenWidth { get { return virtualScreenWidth; } set { virtualScreenWidth = value; } }

#if WIN
        [Browsable(false)] 
#endif
        public int VirtualScreenHeight { get { return virtualScreenHeight; } set { virtualScreenHeight = value; } }

#if WIN
        [DisplayName("屏幕宽度"), Description("场景宽度")]
#endif
        public int ScreenWidth { get; set; }

#if WIN
        [DisplayName("屏幕高度"), Description("屏幕高度")]
#endif
        public int ScreenHeight { get; set; }

        internal void SaveToFile()
        {
            if (settingsFile != null)
            {
                settingsFile.IniWriteValue("Window", "Width", " " + ScreenWidth.ToString());
                settingsFile.IniWriteValue("Window", "Height", " " + ScreenHeight.ToString());
            }
        }

        internal void ReloadPath(string rootPath)
        {
            this.rootPath = rootPath;
            settingsFile = new IniFile(rootPath + "\\settings.ini");

            this.ReloadSettings();
        }

        internal void ReloadSettings()
        {
            if (settingsFile != null)
            {
                ScreenWidth = Convert.ToInt32(settingsFile.IniReadValue("Window", "Width").Trim());
                ScreenHeight = Convert.ToInt32(settingsFile.IniReadValue("Window", "Height").Trim());
            }
        }
    }
}