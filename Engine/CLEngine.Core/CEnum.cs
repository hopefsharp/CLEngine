﻿namespace CLEngine.Core
{
    /// <summary>
    /// 切换枚举
    /// </summary>
    public enum Switch { Off, On }

    /// <summary>
    /// 搜索选项枚举
    /// </summary>
    public enum SearchOptions { Hash, Name, Tag }

    /// <summary>
    ///鼠标事件按钮枚举
    /// </summary>
    public enum MouseEventButton { LeftButton, RightButton, MiddleButton }

    /// <summary>
    /// 文本对齐模式
    /// </summary>
    public enum TextAlignModes { Left, Center }

    /// <summary>
    /// 锚点
    /// </summary>
    public enum Origins { TopLeft, Center, Top, TopRight, Right, BottomRight, Bottom, BottomLeft, Left }

    /// <summary>
    /// 混合模式
    /// </summary>
    public enum BlendModes { NonPremultiplied, AlphaBlend, Additive }

    /// <summary>
    /// 采样器模式
    /// </summary>
    public enum SamplerModes { AnisotropicClamp, AnisotropicWrap , LinearClamp , LinearWrap , PointClamp , PointWrap }
}